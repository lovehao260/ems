<?php

use App\Models\Setting;
use App\Models\User;
use App\Supports\Facades\PageTitleFacade;
use App\Supports\PageTitle;
use App\Supports\SettingStore;
use Carbon\Carbon;
use Illuminate\Support\Facades\Cache;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Event;
use Illuminate\Support\Facades\File;
use Illuminate\Support\Facades\Storage;
use PHPUnit\Util\Filter;

if (!function_exists('page_title')) {

    /**
     * @return PageTitle
     */
    function page_title()
    {
        return PageTitleFacade::getFacadeRoot();
    }
}
if (!function_exists('human_file_size')) {
    /**
     * @param int $bytes
     * @param int $precision
     * @return string
     */
    function human_file_size($bytes, $precision = 2): string
    {
        $units = ['B', 'KB', 'MB', 'GB', 'TB'];

        $bytes = max($bytes, 0);
        $pow = floor(($bytes ? log($bytes) : 0) / log(1024));
        $pow = min($pow, count($units) - 1);

        $bytes /= pow(1024, $pow);

        return number_format($bytes, $precision, ',', '.') . ' ' . $units[$pow];
    }
}
if (!function_exists('format_time')) {
    /**
     * @param Carbon $timestamp
     * @param string $format
     * @return string
     */
    function format_time(Carbon $timestamp, $format = 'j M Y H:i')
    {
        $first = Carbon::create(0000, 0, 0, 00, 00, 00);
        if ($timestamp->lte($first)) {
            return '';
        }

        return $timestamp->format($format);
    }
}
if (!function_exists('date_from_database')) {
    /**
     * @param string $time
     * @param string $format
     * @return string
     */
    function date_from_database($time, $format = 'Y-m-d')
    {
        if (empty($time)) {
            return $time;
        }

        return format_time(Carbon::parse($time), $format);
    }
}
if (!function_exists('getFormattedDateObject')) {
    function getFormattedDateObject($date, $type = 'datetime', $array = true)
    {

        if ($date == '') {
            return null;
        }

        $tmp_date = new Carbon($date);

        if ($type == 'datetime') {
            $dt['datetime'] = $tmp_date->translatedFormat('Y-m-d H:i:s');
            $dt['formatted'] = $tmp_date->translatedFormat('D M d, Y' . ' ' . 'D M d, Y');
        } else {
            $dt['date'] = $tmp_date->translatedFormat('Y-m-d');
            $dt['formatted'] = $tmp_date->translatedFormat('D M d, Y');
        }

        if ($array == 'true') {
            return $dt;
        }
        return $dt['formatted'];

    }
}
if (!function_exists('formatCurrencyOutput')) {
    function formatCurrencyOutput($cost)
    {
        if (is_numeric($cost)) {
            return number_format($cost, 2, '.', ',');

        }
        // It's already been parsed.
        return $cost;
    }
}
if (!function_exists('link_table')) {
    /**
     * @param string $route
     * @param string|null $text
     * @return string
     */
    function link_table(string $route, string $text = null): string
    {
        if (empty($route)) {
            return $route;
        }
        if (empty($route))
            return true;

        return view('partials.link', ['text' => $text, 'route' => $route])->render();
    }
}
/**
 * @param string $path
 * @return string
 */
if (!function_exists('url_storage')) {

    function url_storage($path): string
    {

        return Storage::url($path);

    }
}

if (!function_exists('table_actions')) {
    /**
     * @param string $edit
     * @param string $delete
     * @param $item
     * @param null $clone
     * @param bool $disable
     * @return string
     */
    function table_actions(string $edit, string $delete, $item, $clone = null, $disable = false): string
    {
        return view('partials.actions', compact('edit', 'delete', 'item', 'clone', 'disable'))->render();
    }
}
if (!function_exists('table_checkout')) {
    /**
     * @param $table
     * @param $id
     * @param  $avail
     * @param null $seat_id
     * @param null $status
     * @return string
     */
    function table_checkout($table, $id, $avail, $seat_id = null, $status = null): string
    {
        return view('partials.table.checkout', compact('table', 'avail', 'id', 'seat_id', 'status'))->render();
    }
}
if (!function_exists('table_accessory_checkout')) {
    /**
     * @param $table
     * @param $route
     * @param $id
     * @param  $avail
     * @param null $status
     * @return string
     */
    function table_accessory_checkout($table, $route, $id, $avail = 0, $status = null): string
    {
        return view('partials.table.checkout_accessory', compact('table', 'route', 'avail', 'id', 'status'))->render();
    }
}

if (!function_exists('table_asset_checkout')) {
    /**
     * @param $id
     * @param bool $checkout
     * @param int $status
     * @return string
     */
    function table_asset_checkout($id, bool $checkout, int $status): string
    {
        return view('partials.table.checkout_asset', compact('id', 'checkout', 'status'))->render();
    }
}
if (!function_exists('table_import_actions')) {
    /**
     * @param string $process
     * @param string $delete
     * @param $item
     * @return string
     * @throws Throwable
     */
    function table_import_actions(string $process, string $delete, $item): string
    {
        return view('imports.actions', compact('process', 'delete', 'item'))->render();
    }
}

if (!function_exists('table_actions_trash')) {
    /**
     * @param string $restore
     * @param string $deleted
     * @param $item
     * @return string
     * @throws Throwable
     */
    function table_actions_trash(string $deleted, string $restore, $item): string
    {
        return view('partials.actions', compact('deleted', 'restore', 'item'))->render();
    }
}
if (!function_exists('table_checkbox')) {
    /**
     * @param int $id
     * @return string
     * @throws Throwable
     */
    function table_checkbox($id): string
    {
        return view('partials.checkbox', compact('id'))->render();
    }
}
if (!function_exists('apply_filters')) {
    /**
     * @return mixed
     */
    function apply_filters()
    {
        $args = func_get_args();
        return Filter::fire(array_shift($args), $args);
    }
}
if (!function_exists('get_data_select')) {
    /**
     * @param string $table
     * @return \Illuminate\Support\Collection
     * @throws Throwable
     */
    function get_data_select(string $table): \Illuminate\Support\Collection
    {
        return DB::table($table)->get();
    }
}
if (!function_exists('isJapanese')) {
    /**
     * Check String is Japanese
     * @param $lang
     * @return Boolean
     */
    function isJapanese($lang)
    {
        return preg_match('/[\x{4E00}-\x{9FBF}\x{3040}-\x{309F}\x{30A0}-\x{30FF}]/u', $lang) === 1 ? true : false;
    }
}
if (!function_exists('first_character_name')) {
    function first_character_name($name): ?string
    {
        $word = explode(" ", $name);
        $first_character_name = "";
        if (isJapanese($name)) {
            $first_character_name = mb_substr($word[0], 0, 2);
        } else {
            if (count($word) == 1) {
                $first_character_name = substr($word[0], 0, 2);
            } else if (count($word) > 1) {
                $length = count($word);
                $first_character_name = substr($word[$length - 2], 0, 1) . '' . substr($word[$length - 1], 0, 1);
            }
            $first_character_name = strtoupper($first_character_name);
        }
        return $first_character_name;
    }
}
if (!function_exists('get_login_background')) {
    function clearCache(): bool
    {
        Event::dispatch('cache:clearing');

        try {
            Cache::flush();
            if (!File::exists($storagePath = storage_path('framework/cache'))) {
                return true;
            }

            foreach (File::files($storagePath) as $file) {
                if (preg_match('/facade-.*\.php$/', $file)) {
                    File::delete($file);
                }
            }
        } catch (Exception $exception) {
            info($exception->getMessage());
        }

        Event::dispatch('cache:cleared');

        return true;
    }
}
if (!function_exists('total_checkout')) {
    function total_checkout()
    {
        return User::with('assets', 'assets.model', 'accessories', 'licenses', 'userloc')->withTrashed()->find(auth()->id());

    }
}
if (!function_exists('custom_field_tag')) {
    /**
     * @param string $route
     * @param $data
     * @return string
     */
    function custom_field_tag(string $route, $data)
    {
        if (empty($data))
            return true;

        return view('partials.table.tag', ['data' => $data, 'route' => $route])->render();
    }
}
if (!function_exists('default_value_field')) {
    /**
     * @param $id
     * @param $mode_id
     * @return string
     */
    function default_value_field($id,$mode_id = null): string
    {
        $field_value_default = DB::table('models_custom_fields')
            ->where('asset_model_id', $mode_id)->where('custom_field_id', $id)->first();
        if ($field_value_default)
            return $field_value_default->default_value;
        else
            return '';

    }
}
if (!function_exists('setting')) {
    /**
     * Get the setting instance.
     *
     * @param null $key
     * @param null $default
     * @return array|App\Supports\SettingStore|string|null
     */

    function setting($key = null, $default = null)
    {
        if (!empty($key)) {
            try {
                $settings= Setting::all()->toArray();
                foreach ($settings as $setting){
                    if ($setting['key'] ===$key){
                        return   $setting['value'];
                    }
                }
                return $default;
            } catch (Exception $exception) {
                info($exception->getMessage());
                return $default;
            }
        }
        return SettingStore::class;
    }
}
