<?php

namespace App\Rules;

use Illuminate\Contracts\Validation\Rule;

class CustomFormatRule implements Rule
{
    /**
     * Create a new rule instance.
     *
     * @return void
     */
    public function __construct()
    {
        //
    }

    /**
     * Determine if the validation rule passes.
     *
     * @param string $attribute
     * @param mixed $value
     * @return bool
     */
    public function passes($attribute, $value)
    {
        // Make sure it's not just an ANY format
        if ($value != '') {

            //  Check that the string starts with regex:
            if (strpos($value, 'regex:') === false) {
                return false;
            }

            $test_string = 'My hovercraft is full of eels';

            // We have to stip out the regex: part here to check with preg_match
            $test_pattern = str_replace('regex:', '', $value);

            try {
                preg_match($test_pattern, $test_string);
                return true;
            } catch (\Exception $e) {
                return false;
            }

        }
        return true;
    }

    /**
     * Get the validation error message.
     *
     * @return string
     */
    public function message()
    {
        return 'This field allows you to use a regex expression for validation. It should start with "regex:"
        - for example, to validate that a custom field value contains a valid IMEI (15 numeric digits), you would use regex:/^[0-9]{15}$/.';
    }
}
