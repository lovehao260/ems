<?php

namespace App\TableList;

use App\Models\Department;
use Illuminate\Contracts\Routing\UrlGenerator;
use Illuminate\Http\JsonResponse;
use Illuminate\Support\Facades\View;
use Throwable;
use Yajra\DataTables\DataTables;

use Form;

class DepartmentTable extends TableAbstract
{
    protected $guarantees;
    /**
     * @var bool
     */
    protected $hasActions = false;
    protected $trash = false;
    protected $hasCheckbox = false;

    public function __construct(DataTables $table, Department $model, UrlGenerator $urlGenerator)
    {
        $this->setOption('id', 'table-departments');
        $this->setType('advanced');
        $this->model = $model;
        parent::__construct($table, $model, $urlGenerator);
    }

    /**
     * @param string|null $trash
     * @param array $data
     * @param array $mergeData
     * @return JsonResponse|View
     * @throws Throwable
     * @since 1.0
     */
    public function renderTable(string $trash = null, $data = [], $mergeData = [])
    {
        $data['breadcrumb']='departments';
        return $this->render($this->view, $data, $mergeData);
    }

    public function ajax()
    {

        return $this->table
            ->eloquent($this->query())
            ->editColumn('image', function ($item) {
                if ($this->request()->input('action')) {
                    return $item->image;
                }
                if ($item->image){
                    return "<img  src='".url_storage($item->image)."' style='height: 40px'/>";
                }
                return "";
            })
            ->editColumn('created_at', function ($item) {
                return date_from_database($item->created_at);
            })
            ->addColumn('manager', function ($item) {
                return $item->manager->first_name .''.$item->manager->last_name;
            })
            ->addColumn('users', function ($item) {
                return count($item->users);
            })
            ->addColumn('operations', function ($item) {
                return $this->getOperations('departments.edit', 'departments.destroy', $item);
            })
            ->escapeColumns([])
            ->make(true);
    }

    public function query()
    {
        //Relationships
        $model = $this->model->with('users');
        return $this->applyScopes($model);

    }


    public function columns()
    {
        return [

            'name' => [
                'name' => 'departments.name',
                'title' => 'Name',
                'class' => 'text-left',
                'targets' => 1,
            ],
            'image' => [
                'name' => 'departments.image',
                'title' => 'Image',
                'class' => 'text-left',
            ],
            'manager' => [
                'name' => 'departments.manager',
                'title' => 'Manager',
                'class' => 'text-left',
            ],
            'users' => [
                'title' => '<i class="fa fa-user"></i>' . Form::label('Users', 'Users', ['class' => 'label-check-all'])->toHtml(),
                'width' => '134px',
                'class' => 'text-center count-user',
                'orderable' => false,
                'searchable' => false,
                'visible' => true

            ],
            'created_at' => [
                'name' => 'users.created_at',
                'title' => 'Created at',
                'width' => '100px',
            ],
        ];
    }

    /**
     * {@inheritDoc}
     */
    public function buttons()
    {
        return $this->addCreateButton(['create' => route('departments.create')]);
    }

    /**
     * {@inheritDoc}
     */
    public function htmlDrawCallbackFunction(): ?string
    {
        return parent::htmlDrawCallbackFunction();
    }

    /**
     * {@inheritDoc}
     */
    public function getDefaultButtons(): array
    {
        return [
            'colvis',
            'export',
            'reload',
        ];
    }

}
