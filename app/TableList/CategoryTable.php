<?php

namespace App\TableList;

use App\Models\Category;
use Illuminate\Contracts\Routing\UrlGenerator;
use Illuminate\Http\JsonResponse;
use Illuminate\Support\Facades\View;
use Throwable;
use Yajra\DataTables\DataTables;
use Form;

class CategoryTable extends TableAbstract
{
    protected $guarantees;
    /**
     * @var bool
     */
    protected $hasActions = false;
    protected $trash = false;
    protected $hasCheckbox = false;

    public function __construct(DataTables $table, Category $model, UrlGenerator $urlGenerator)
    {
        $this->setOption('id', 'table-categories');
        $this->setType('advanced');
        $this->model = $model;
        parent::__construct($table, $model, $urlGenerator);
    }

    /**
     * @param string|null $trash
     * @param array $data
     * @param array $mergeData
     * @return JsonResponse|View
     * @throws Throwable
     * @since 1.0
     */
    public function renderTable(string $trash = null, $data = [], $mergeData = [])
    {
        $data['breadcrumb']='categories';
        return $this->render($this->view, $data, $mergeData);
    }
    /**
     * {@inheritDoc}
     */
    public function ajax()
    {
        return $this->table
            ->eloquent($this->query())
            ->editColumn('name', function ($item) {
                return link_table(route('categories.show',$item->id),$item->name);
            })
            ->editColumn('image', function ($item) {
                if ($this->request()->input('action')) {
                    return $item->image;
                }
                if ($item->image) {
                    return "<img  src='".url_storage($item->image)."' style='height: 40px'/>";
                }
                return "";
            })
            ->addColumn('qty', function ($item) {
                return $item->itemCount();
            })
            ->editColumn('created_at', function ($item) {
                return date_from_database($item->created_at);
            })
            ->editColumn('checkin_email', function ($item) {
                return $item->checkin_email ==1 ? '<i class="fa fa-check text-success"></i>':'<i class="fa fa-times text-danger"></i>';
            })

            ->addColumn('operations', function ($item) {
                return $this->getOperations('categories.edit', 'categories.destroy', $item,null,$item->isDeletable());
            })
            ->escapeColumns([])
            ->make(true);
    }
    /**
     * {@inheritDoc}
     */
    public function query()
    {
        //Relationships
        $model = $this->model->query();
        return $this->applyScopes($model);

    }

    /**
     * {@inheritDoc}
     */
    public function columns()
    {
        return [

            'name' => [
                'name' => 'categories.name',
                'title' => trans('layout.name'),
                'class' => 'text-left',
                'targets' => 1,
            ],
            'image' => [
                'name' => 'categories.image',
                'title' => trans('layout.image'),
                'class' => 'text-left',
            ],
            'category_type' => [
                'name' => 'categories.category_type',
                'title' => trans('layout.type'),
                'class' => 'text-left text-capitalize',
            ],
            'qty' => [
                'title' =>trans('layout.qty'),
                'class' => 'text-left',
            ],
            'checkin_email' => [
                'name' => 'categories.checkin_email',
                'title' => '<i class="far fa-envelope"></i>' . Form::label('Users', trans('layout.category.check_mail'), ['class' => 'label-check-all'])->toHtml(),
                'class' => 'text-left',
            ],

            'created_at' => [
                'name' => 'categories.created_at',
                'title' => trans('table.created_at'),
                'width' => '100px',
            ],
        ];
    }

    /**
     * {@inheritDoc}
     */
    public function buttons()
    {
        return $this->addCreateButton(['create' => route('categories.create')]);
    }

    /**
     * {@inheritDoc}
     */
    public function htmlDrawCallbackFunction(): ?string
    {
        return parent::htmlDrawCallbackFunction();
    }

    /**
     * {@inheritDoc}
     */
    public function getDefaultButtons(): array
    {
        return [
            'colvis',
            'export',
            'reload',
        ];
    }

}
