<?php

namespace App\TableList;

use App\Models\Component;
use Illuminate\Contracts\Routing\UrlGenerator;
use Illuminate\Http\JsonResponse;
use Illuminate\Support\Facades\View;
use Throwable;
use Yajra\DataTables\DataTables;
use Form;

class ComponentTable extends TableAbstract
{
    protected $guarantees;
    /**
     * @var bool
     */
    protected $hasActions = false;
    protected $trash = false;
    protected $hasCheckbox = false;
    public function __construct(DataTables $table, Component $model, UrlGenerator $urlGenerator)
    {
        $this->setOption('id', 'table-components');
        $this->setType('advanced');
        $this->model = $model;
        parent::__construct($table, $model, $urlGenerator);
    }

    /**
     * @param string|null $trash
     * @param array $data
     * @param array $mergeData
     * @return JsonResponse|View
     * @throws Throwable
     * @since 1.0
     */
    public function renderTable(string $query = null,string $trash = null, $data = [], $mergeData = [])
    {
        $this->query=$query ;
        $data['breadcrumb']='components';
        return $this->render($this->view, $data, $mergeData);
    }
    /**
     * {@inheritDoc}
     */
    public function ajax()
    {

        return $this->table
            ->eloquent($this->query())
            ->editColumn('name', function ($item) {
                return link_table(route('components.show',$item->id), $item->name);
            })
            ->editColumn('serial', function ($item) {
               // return link_table(route('components.show',$item->category_id), $item->serial);
                return $item->serial? link_table(route('components.show',$item->id), $item->serial):'';
            })
            ->editColumn('checkbox', function ($item) {
                return $this->getCheckbox($item->id);
            })

            ->editColumn('created_at', function ($item) {
                return date_from_database($item->created_at);
            })
            ->editColumn('category', function ($item) {
                return link_table(route('categories.show',$item->category_id), $item->category->name);
            })
            ->editColumn('location', function ($item) {
                return $item->location['name']??'';
            })
            ->editColumn('company', function ($item) {
                return $item->company['name']??'';
            })
            ->editColumn('purchase_date', function ($item) {
                return $item->purchase_date?getFormattedDateObject($item->purchase_date, 'formatted')['formatted']:'';
            })
            ->editColumn('purchase_cost', function ($item) {
                return $item->purchase_cost?formatCurrencyOutput($item->purchase_cost):'';
            })
            ->addColumn('remaining', function ($item) {
                return $item->numRemaining() ?? 0;
            })
            ->addColumn('checkout', function ($item) {
                $avail=$item->numRemaining() ;
                return  table_accessory_checkout( 'components','components.checkout',$item->id ,$avail );
            })
            ->addColumn('operations', function ($item) {
                if ($this->trash) {
                    return $this->getOperationsTrash('components.deleted', 'components.restore', $item);
                } else {
                    return $this->getOperations('components.edit', 'components.destroy', $item);
                }
            })

            ->escapeColumns([])
            ->make(true);
    }
    /**
     * {@inheritDoc}
     */
    public function query()
    {

        //Relationships
        if ($this->query){
            $model = $this->model->where('category_id',$this->query)->with('category');

        }else{
            $model = $this->model->with('category');
        }

        return $this->applyScopes($model);

    }

    /**
     * {@inheritDoc}
     */
    public function columns()
    {
        return [

            'id' => [
                'name' => 'components.id',
                'title' => 'Id',
                'width' => '10px',
                'visible' => false
            ],
            'name' => [
                'name' => 'components.name',
                'title' => trans('layout.name'),
                'class' => 'text-left',
            ],
            'serial' => [
                'name' => 'components.serial',
                'title' => trans('layout.component.serial'),
                'class' => 'text-left',
            ],
            'category' => [
                'name' => 'components.category_id',
                'title' => trans('layout.category.category'),
                'class' => 'text-left',
            ],
            'company' => [
                'name' => 'components.company_id',
                'title' => trans('layout.company.company'),
                'class' => 'text-left',
                'visible' => false
            ],
            'location' => [
                'name' => 'components.location_id',
                'title' => trans('layout.location.location'),
                'class' => 'text-left',
                'visible' => false
            ],
            'qty' => [
                'name' => 'components.qty',
                'title' => trans('layout.location.location'),
                'class' => 'text-left',
                'targets' => 1,
            ],
            'remaining' => [

                'title' => trans('layout.component.remaining'),
                'class' => 'text-left',
                'targets' => 1,
            ],
            'min_amt' => [
                'name' => 'components.min_amt',
                'title' => trans('layout.min_qty'),
                'class' => 'text-left',
                'targets' => 1,
            ],
            'order_number' => [
                'name' => 'components.order_number',
                'title' => trans('layout.order_number'),
                'class' => 'text-left',
                'targets' => 1,
            ],
            'purchase_date' => [
                'name' => 'components.purchase_date',
                'title' => trans('layout.purchase_date'),
                'class' => 'text-left',
                'targets' => 1,
            ],
            'purchase_cost' => [
                'name' => 'components.purchase_cost',
                'title' =>trans('layout.purchase_cost'),
                'class' => 'text-left',
                'targets' => 1,
            ],
            'created_at' => [
                'name' => 'components.created_at',
                'title' => trans('table.created_at'),
                'width' => '100px',

            ],
            'checkout' => [
                'title' => trans('layout.checkout_check'),
                'width' => '100px',

            ],
        ];
    }

    /**
     * {@inheritDoc}
     */
    public function buttons()
    {
        return $this->addCreateButton(['create' => route('components.create')]);
    }

    /**
     * {@inheritDoc}
     */
    public function htmlDrawCallbackFunction(): ?string
    {
        return parent::htmlDrawCallbackFunction();
    }


    /**
     * {@inheritDoc}
     */
    public function getDefaultButtons(): array
    {
        return [
            'colvis',
            'export',
            'reload',
        ];
    }

}
