<?php

namespace App\TableList;

use App\Models\Asset;
use App\Models\Statuslabel;
use http\Env\Request;
use Illuminate\Contracts\Routing\UrlGenerator;
use Illuminate\Http\JsonResponse;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\View;
use Throwable;
use Yajra\DataTables\DataTables;
use Form;

class AssetTable extends TableAbstract
{
    protected $guarantees;
    const USER='user';
    const LOCATION = 'location';
    const ASSET = 'asset';
    /**
     * @var bool
     */
    protected $hasActions = true;
    protected $trash = false;

    public function __construct(DataTables $table, Asset $model, UrlGenerator $urlGenerator)
    {
        $this->setOption('id', 'table-assets');
        $this->setType('advanced');
        $this->model = $model;
        parent::__construct($table, $model, $urlGenerator);
    }

    /**
     * @param string|null $trash
     * @param array $data
     * @param array $mergeData
     * @return JsonResponse|View
     * @throws Throwable
     * @since 1.0
     */
    public function renderTable(string $trash = null, $data = [], $mergeData = [])
    {
        if ($trash) {
            $this->trash = true;
            $this->setOption('id', 'table-assets-trash');
        }
        $data['breadcrumb'] = 'assets';
        return $this->render($this->view, $data, $mergeData);
    }
    /**
     * {@inheritDoc}
     */
    public function ajax()
    {

        return $this->table
            ->eloquent($this->query())
            ->editColumn('checkbox', function ($item) {
                return $this->getCheckbox($item->id);
            })
            ->editColumn('image', function ($item) {
                if ($this->request()->input('action') ) {
                    return $item->image;
                }

                if ($item->image) {
                    return "<img  src='" . url_storage($item->image) . "' style='height: 40px'/>";
                }
                return "";
            })
            ->editColumn('name', function ($item) {
                return link_table(route('assets.show',encrypt( $item->id )), $item->name);
            })
            ->editColumn('model', function ($item) {
                return $item->model?$item->model['name']:"";
            })
            ->addColumn('category', function ($item) {
                return $item->model?$item->model['category']['name']:"";
            })
            ->editColumn('status', function ($item) {
                return $item->assetstatus['name']?? '';
            })
            ->editColumn('location', function ($item) {
                return $item->location['name'] ?? '';
            })
            ->addColumn('checkout_to', function ($item) {
              return $item-> assetCheckoutTo();
            })
            ->addColumn('checkout', function ($item) {
                return link_table(route('assets.checkout', $item->id), 'Checkout');
            })
            ->editColumn('created_at', function ($item) {
                return date_from_database($item->created_at);
            })
            ->editColumn('checkout', function ($item) {
                $checkout = false;
                $item->assigned_to ?? $checkout = true;
                return table_asset_checkout($item->id, $checkout, $item->assetstatus['deployable']);
            })
            ->addColumn('operations', function ($item) {
                if ($this->trash) {
                    return $this->getOperationsTrash('assets.deleted', 'assets.restore', $item);
                } else {
                    return $this->getOperations('assets.edit', 'assets.destroy', $item, 'assets.clone');
                }
            })
            ->editColumn('company', function ($item) {
                return $item->company ? $item->company['name'] : '';
            })
            ->addColumn('hao',function ($item){

                return 'Hao';
            })->rawColumns(['hao', 'types'])
            ->escapeColumns([])
            ->make(true);

    }
    /**
     * {@inheritDoc}
     */
    public function query()
    {
        //Relationships
        if (!$this->trash) {
            $model = $this->model->with('company')->where('deleted_at', null);

        } else {
            $model = $this->model->with('company')->onlyTrashed();
        }
        return $this->applyScopes($model);

    }

    protected $td=[];
    /**
     * {@inheritDoc}
     */
    public function columns()
    {


         $columns=[
            'checkbox' => [
                'width' => '10px',
                'class' => 'text-left no-sort table-check',
                'title' => Form::input('checkbox', null, null, [
                        'class' => 'table-check-all',
                        'data-set' => '.dataTable .checkboxes'

                    ]) . Form::label('checkbox', trans('table.checkbox'), ['class' => 'label-check-all'])->toHtml(),
                'orderable' => false,
                'searchable' => false,
                'exportable' => false,
                'printable' => false,
            ],
            'id' => [
                'name' => 'assets.id',
                'title' => 'Id',
                'width' => '10px',
                'visible' => false
            ],
            'company' => [
                'name' => 'assets.company_id',
                'title' => trans('layout.company.company'),
                'class' => 'text-left',
                'visible' => false
            ],
            'name' => [
                'name' => 'assets.name',
                'title' => trans('layout.name'),
                'class' => 'text-left',
                'targets' => 1,
            ],

            'image' => [
                'name' => 'assets.image',
                'title' => trans('layout.asset.device_image'),
                'class' => 'text-left',
                'targets' => 1,
            ],
            'asset_tag' => [
                'name' => 'assets.asset_tag',
                'title' => trans('layout.asset.tag'),
                'class' => 'text-left',
            ],
            'serial' => [
                'name' => 'assets.serial',
                'title' => trans('layout.component.serial'),
                'width' => '100px',
            ],
            'model' => [
                'name' => 'assets.model_id',
                'title' => trans('layout.model.model'),
            ],
            'category' => [
                'name' => 'assets.category_id',
                'title' => trans('layout.category.category'),
                "searchable" => false
            ],
            'location' => [
                'name' => 'assets.location_id',
                'title' => trans('layout.location.location'),
            ],
            'status' => [
                'name' => 'assets.status_id',
                'title' => trans('layout.status'),
                'width' => '100px',
                'class' => 'text-pre'
            ],
            'checkout_to' => [
                'title' => trans('layout.asset.checkout_to'),
                'width' => '100px',
            ],
            'checkin_counter' => [
                'name' => 'assets.checkin_counter',
                'title' => trans('layout.checkin'),
                "searchable" => false
            ],
            'checkout_counter' => [
                'name' => 'assets.checkout_counter',
                'title' => trans('layout.checkout'),
                "searchable" => false
            ],
            'purchase_cost' => [
                'name' => 'assets.purchase_cost',
                'title' =>trans('layout.purchase_cost'),
            ],
            'checkout' => [
                'title' => trans('layout.checkout'),
                'width' => '100px',
                'orderable' => false,
                'searchable' => false,
                'exportable' => false,
                'printable' => false,
            ],
        ];
        return array_merge($this->td,$columns);
    }

    /**
     * {@inheritDoc}
     */
    public function buttons()
    {
        if ($this->trash) {
            return $this->addCreateButton(['current' => route('assets.index'),
                'empty_trash' => route('assets.empty_trash')]);
        } else {
            return $this->addCreateButton(['create' => route('assets.create'),
                'trash' => route('assets.trash', 'trash')]);
        }
    }

    /**
     * {@inheritDoc}
     */
    public function htmlDrawCallbackFunction(): ?string
    {
        return parent::htmlDrawCallbackFunction();
    }

    /**
     * {@inheritDoc}
     */
    public function bulkActions(): array
    {
        if ($this->trash )
            return [];
        else
            return $this->addDeleteAction(route('assets.deletes'), 'delete-asset', parent::bulkActions());
    }
    //    /**
    //     * {@inheritDoc}
    //     * You can export a column customised header if manually set.
    //     */
    //    protected $exportColumns = [
    //        ['data' => 'name', 'title' => 'Name'],
    //        ['data' => 'email', 'title' => 'Registered Email'],
    //    ];

    /**
     * {@inheritDoc}
     */
    public function getBulkChanges(): array
    {
        if (!auth()->user()->can('edit-asset')) {
            return [];
        }else{
            return [
                'name' => [
                    'title' => trans('layout.name'),
                    'type' => 'text',
                    'validate' => 'required|max:120',
                ],
                'serial' => [
                    'title' => trans('layout.component.serial'),
                    'type' => 'text',
                    'validate' => 'required|max:120',
                ],
                'model_id' => [
                    'title' => trans('layout.model.model'),
                    'choices'  => $this->getDataSelectOption('models'),
                    'type' => 'select-search',
                    'validate' => 'required',
                ],
                'status_id' => [
                    'title' => trans('layout.status'),
                    'choices'  => $this->getDataSelectOption('status_labels'),
                    'type' => 'select-search',
                    'validate' => 'required',
                ],
                'location_id' => [
                    'title' => trans('layout.location.location'),
                    'choices'  => $this->getDataSelectOption('locations'),
                    'type' => 'select-search',
                    'validate' => 'required',
                ],
                'created_at' => [
                    'title' => trans('table.created_at'),
                    'type' => 'date',
                ],
            ];
        }

    }



    /**
     * {@inheritDoc}
     */
    public function getDefaultButtons(): array
    {
        return [
            'colvis',
            'export',
            'reload',
        ];
    }

}
