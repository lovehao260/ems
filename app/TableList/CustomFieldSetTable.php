<?php

namespace App\TableList;

use App\Models\Company;
use App\Models\FieldSet;
use Illuminate\Contracts\Routing\UrlGenerator;
use Yajra\DataTables\DataTables;
use Form;

class CustomFieldSetTable extends TableAbstract
{
    protected $guarantees;
    /**
     * @var bool
     */
    protected $hasActions = false;
    protected $trash = false;
    protected $hasCheckbox = false;
    protected $view = 'custom_fields.fieldset-table';
    public function __construct(DataTables $table, FieldSet $model, UrlGenerator $urlGenerator)
    {
        $this->setOption('id', 'table-fieldsets');
        $this->setType('advanced');
        $this->model = $model;
        parent::__construct($table, $model, $urlGenerator);
    }
    /**
     * {@inheritDoc}
     */
    public function renderTable(string $query = null,string $trash = null, $data = [], $mergeData = [])
    {
        $this->query=$query ;
        $data['breadcrumb']='accessories';
        return $this->render($this->view, $data, $mergeData);
    }
    /**
     * {@inheritDoc}
     */
    public function ajax()
    {

        return $this->table
            ->eloquent($this->query())
            ->editColumn('name', function ($item) {
                return link_table(route('fields.fieldset.show',$item->id), $item->name);
            })

            ->addColumn('models', function ($item) {
                return custom_field_tag('', $item->models);
            })
            ->editColumn('created_at', function ($item) {
                return date_from_database($item->created_at);
            })

            ->addColumn('operations', function ($item) {
                return $this->getOperations('', 'fields.fieldset.destroy', $item,null);
//                return $this->getOperations('', 'companies.destroy', $item,null,$item->isDeletable());
            })
            ->escapeColumns([])
            ->make(true);
    }
    /**
     * {@inheritDoc}
     */
    public function query()
    {
        //Relationships
        $model = $this->model->with('fields');
        return $this->applyScopes($model);

    }

    /**
     * {@inheritDoc}
     */
    public function columns()
    {
        return [

            'name' => [
                'name' => 'custom_fieldsets.name',
                'title' =>  trans('layout.name') ,
                'class' => 'text-left',
                'targets' => 1,
            ],

            'models' => [
                'title' =>  trans('layout.asset_models'),
                'class' => 'text-left',
                'targets' => 1,
            ],

            'created_at' => [
                'name' => 'custom_fieldsets.created_at',
                'title' =>  trans('table.created_at'),
                'width' => '100px',
            ],
        ];
    }

    /**
     * {@inheritDoc}
     */
    public function buttons()
    {
        return $this->addCreateButton([]);
    }

    /**
     * {@inheritDoc}
     */
    public function htmlDrawCallbackFunction(): ?string
    {
        return parent::htmlDrawCallbackFunction();
    }

    /**
     * {@inheritDoc}
     */
    public function getDefaultButtons(): array
    {
        return [
            'colvis',
            'export',
            'reload',
        ];
    }

}
