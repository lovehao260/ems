<?php

namespace App\TableList;

use App\Models\Location;
use Illuminate\Contracts\Routing\UrlGenerator;
use Illuminate\Http\JsonResponse;
use Illuminate\Support\Facades\View;
use Throwable;
use Yajra\DataTables\DataTables;

use Form;

class LocationTable extends TableAbstract
{
    protected $guarantees;
    /**
     * @var bool
     */
    protected $hasActions = false;
    protected $trash = false;
    protected $hasCheckbox = false;

    public function __construct(DataTables $table, Location $model, UrlGenerator $urlGenerator)
    {
        $this->setOption('id', 'table-locations');
        $this->setType('advanced');
        $this->model = $model;
        parent::__construct($table, $model, $urlGenerator);
    }

    /**
     * @param string|null $trash
     * @param array $data
     * @param array $mergeData
     * @return JsonResponse|View
     * @throws Throwable
     * @since 1.0
     */
    public function renderTable(string $trash = null, $data = [], $mergeData = [])
    {
        $data['breadcrumb']='locations';
        return $this->render($this->view, $data, $mergeData);
    }
    /**
     * {@inheritDoc}
     */
    public function ajax()
    {
        return $this->table
            ->eloquent($this->query())
            ->editColumn('image', function ($item) {
                if ($this->request()->input('action')) {
                    return $item->image;
                }
                if ($item->image){
                    return "<img  src='".url_storage($item->image)."' style='height: 40px'/>";
                }
                return "";
            })
            ->addColumn('users', function ($item) {
                return count($item->users);
            })
            ->addColumn('assets', function ($item) {
                return count($item->assets);
            })
            ->addColumn('assigned_assets', function ($item) {
                return count($item->assignedAssets);
            })
            ->editColumn('created_at', function ($item) {
                return date_from_database($item->created_at);
            })
            ->editColumn('parent', function ($item) {
                return $item->parent ? $item->parent['name'] : '';
            })

            ->addColumn('operations', function ($item) {
                return $this->getOperations('locations.edit', 'locations.destroy', $item,null,$item->isDeletable());
            })
            ->escapeColumns([])
            ->make(true);
    }
    /**
     * {@inheritDoc}
     */
    public function query()
    {
        //Relationships
        $model = $this->model->with('manager');
        return $this->applyScopes($model);
    }

    /**
     * {@inheritDoc}
     */
    public function columns()
    {
        return [

            'name' => [
                'name' => 'locations.name',
                'title' => trans('layout.name'),
                'class' => 'text-left',
                'targets' => 1,
            ],
            'image' => [
                'name' => 'locations.image',
                'title' => trans('layout.image'),
                'class' => 'text-left',
            ],
            'parent' => [
                'name' => 'locations.parent',
                'title' => trans('layout.location.parent'),
                'class' => 'text-left',
            ],

            'users' => [
                'title' => '<i class="fa fa-user"></i>' . Form::label('Users', trans('layout.users'), ['class' => 'label-check-all'])->toHtml(),
                'class' => 'text-center count-user',
                'orderable' => false,
                'searchable' => false,
                'visible' => true
            ],
            'assets' => [
                'title' => '<i class="fa fa-save"></i>' . Form::label('Assets',trans('layout.assets'), ['class' => 'label-check-all'])->toHtml(),
                'class' => 'text-center count-user',
                'orderable' => false,
                'searchable' => false,
                'visible' => true
            ],
            'assigned_assets' => [
                'title' => '<i class="fa fa-tasks"></i>' . Form::label('Assigned Assets', trans('layout.asset.assigned'), ['class' => 'label-check-all'])->toHtml(),
                'class' => 'text-center count-user',
                'orderable' => false,
                'searchable' => false,
                'visible' => true
            ],
            'currency' => [
                'name' => 'locations.currency',
                'title' => trans('layout.currency'),
                'class' => 'text-left',
            ],
            'address' => [
                'name' => 'locations.address',
                'title' => trans('layout.address'),
                'class' => 'text-left',
            ],
            'city' => [
                'name' => 'locations.city',
                'title' => trans('layout.location.city'),
                'class' => 'text-left',
            ],
            'state' => [
                'name' => 'locations.state',
                'title' => trans('layout.location.state'),
                'class' => 'text-left',
            ],
            'created_at' => [
                'name' => 'users.created_at',
                'title' => trans('table.created_at'),
                'width' => '100px',
            ],
            'zip' => [
                'name' => 'users.zip',
                'title' => trans('layout.location.post_code'),
                'width' => '100px',
            ],
        ];
    }

    /**
     * {@inheritDoc}
     */
    public function buttons()
    {
        return $this->addCreateButton(['create' => route('locations.create')]);
    }

    /**
     * {@inheritDoc}
     */
    public function htmlDrawCallbackFunction(): ?string
    {
        return parent::htmlDrawCallbackFunction();
    }

    /**
     * {@inheritDoc}
     */
    public function getDefaultButtons(): array
    {
        return [
            'colvis',
            'export',
            'reload',
        ];
    }

}
