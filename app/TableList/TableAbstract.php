<?php

namespace App\TableList;

use App\Models\User;
use Carbon\Carbon;
use Illuminate\Contracts\Routing\UrlGenerator;
use Illuminate\Http\JsonResponse;
use Illuminate\Support\Facades\DB;
use Request;
use Illuminate\Support\Arr;
use Illuminate\Support\Facades\Auth;
use Illuminate\View\View;
use Throwable;
use Yajra\DataTables\DataTables;
use Yajra\DataTables\QueryDataTable;
use Yajra\DataTables\Services\DataTable;
use Form;
use Html;

abstract class TableAbstract extends DataTable
{
    const TABLE_TYPE_ADVANCED = 'advanced';

    const TABLE_TYPE_SIMPLE = 'simple';
    /**
     * @var bool
     */
    protected $bStateSave = true;

    /**
     * @var DataTables
     */
    protected $table;

    /**
     * @var string
     */
    protected $type = self::TABLE_TYPE_ADVANCED;

    /**
     * @var string
     */
    protected $ajaxUrl;

    /**
     * @var int
     */
    protected $pageLength = 10;

    /**
     * @var string
     */
    protected $view = 'partials.table';


    /**
     * @var array
     */
    protected $options = [];

    /**
     * @var bool
     */
    protected $hasCheckbox = true;

    /**
     * @var bool
     */
    protected $hasOperations = true;

    /**
     * @var bool
     */
    protected $hasActions = false;

    /**
     * @var string
     */
    protected $bulkChangeUrl = '';


    /**
     * @var bool
     */
    protected $useDefaultSorting = true;

    /**
     * @var int
     */
    protected $defaultSortColumn = 1;
    /**
     * @var array
     */
    protected $model;


    /**
     * TableAbstract constructor.
     * @param DataTables $table
     * @param $model
     * @param UrlGenerator $urlGenerator
     */
    public function __construct(Datatables $table, $model, UrlGenerator $urlGenerator)
    {
        $this->table = $table;
        $this->ajaxUrl = $urlGenerator->current();
        $this->model = $model;

        if ($this->type == self::TABLE_TYPE_SIMPLE) {
            $this->pageLength = 1;
        }

        if (!$this->getOption('id')) {
            $this->setOption('id', 'table_' . md5(get_class($this)));
        }

        if (!$this->getOption('class')) {
            $this->setOption('class', 'table table-striped table-hover vertical-middle');
        }

        $this->bulkChangeUrl = route('tables.bulk-change.save');
    }

    /**
     * @param string $key
     * @return string
     */
    public function getOption(string $key): ?string
    {
        return Arr::get($this->options, $key);
    }

    /**
     * @param string $key
     * @param mixed $value
     * @return TableAbstract
     */
    public function setOption(string $key, $value): self
    {
        $this->options[$key] = $value;

        return $this;

    }


    /**
     * @return string
     */
    public function getType(): string
    {
        return $this->type;
    }

    /**
     * @param string $type
     * @return $this
     */
    public function setType(string $type): self
    {

        $this->type = $type;

        return $this;
    }

    /**
     * @return string
     */
    public function getView(): string
    {
        return $this->view;
    }

    /**
     * @param string $view
     * @return $this
     */
    public function setView(string $view): self
    {
        $this->view = $view;

        return $this;
    }

    /**
     * @return array
     */
    public function getOptions(): array
    {
        return $this->options;
    }

    /**
     * @param array $options
     * @return $this
     */
    public function setOptions(array $options): self
    {
        $this->options = array_merge($this->options, $options);

        return $this;
    }


    /**
     * Optional method if you want to use html builder.
     *
     * @return \Yajra\DataTables\Html\Builder
     *
     * @throws Throwable
     * @since 2.1
     */
    public function html()
    {
//        if ($this->request->has('filter_table_id')) {
//            $this->bStateSave = false;
//        }

        return $this->builder()
            ->columns($this->getColumns())
            ->ajax(['url' => $this->getAjaxUrl()])
            ->parameters([
                'dom' => $this->getDom(),
                'buttons' => $this->getBuilderParameters(),
                'initComplete' => $this->htmlInitComplete(),
                'drawCallback' => $this->htmlDrawCallback(),
                'paging' => true,
                'searching' => true,
                'info' => true,
                'searchDelay' => 350,
                'bStateSave' => $this->bStateSave,
                'lengthMenu' => Arr::sortRecursive([
                    array_values(array_unique(array_merge([10, 30, 50], [$this->pageLength, -1]))),
                    array_values(array_unique(array_merge([10, 30, 50],
                        [$this->pageLength, trans('table.all')]))),
                ]),

                'processing' => true,
                'serverSide' => true,
                'bServerSide' => true,
                'bDeferRender' => true,
                'bProcessing' => true,
//                "scrollX"=> true,
                'language' => [
                    'aria' => [
                        'sortAscending' => 'orderby asc',
                        'sortDescending' => 'orderby desc',
                        'paginate' => [
                            'next' => trans('table.next'),
                            'previous' => trans('table.previous'),
                        ],
                    ],
                    "info"=> app()->getLocale()=='ja'?' _START_ 件中 _END_ から _END_ まで表示' : trans('table.paginate.showing')." _START_ ".
                        trans('table.paginate.to')." _END_ ".trans('table.paginate.of')."  _TOTAL_ ".
                        trans('table.paginate.entries'),
                    'emptyTable' => trans('table.no_data'),
                    'infoEmpty' => trans('table.no_record'),
                    'lengthMenu' => Html::tag('span', '_MENU_', ['class' => 'dt-length-style'])->toHtml(),
                    'searchPlaceholder' => trans('table.search'),
                    'zeroRecords' => trans('table.no_record'),
                    'paginate' => [
                        'next' => trans('table.next'),
                        'previous' => trans('table.previous'),
                    ],
                ],
                'aaSorting' => $this->useDefaultSorting ? [
                    [
                        ($this->hasCheckbox ? $this->defaultSortColumn : 0),
                        'desc',
                    ],
                ] : [],

            ]);
    }

    /**
     * @return array
     * @since 2.1
     */
    public function getColumns(): array
    {
        $columns = $this->columns();


        foreach ($columns as $key => &$column) {
            $column['class'] = Arr::get($column, 'class') . ' column-key-' . $key;
            $columns = array_merge($columns, $this->getColumnsCustom($column));
        }

        $columns = array_merge($columns, $this->getOperationsHeading());


        if ($this->hasCheckbox) {
            $columns = array_merge($this->getCheckboxColumnHeading(), $columns);
        }

        return $columns;
    }

    /**
     * @return array
     * @since 2.1
     */
    abstract public function columns();

    /**
     * @return array
     */
    public function getOperationsHeading()
    {
        return [
            'operations' => [
                'title' => trans('table.operations'),
                'width' => '134px',
                'class' => 'text-center',
                'orderable' => false,
                'searchable' => false,
                'exportable' => false,
                'printable' => false,
            ],
        ];
    }
    public function getColumnsCustom($column)
    {
        return [

        ];
    }


    /**
     * @param string|null $edit
     * @param string|null $delete
     * @param mixed $item
     * @param string|null $clone
     * @param bool $disable
     * @return string
     * @throws Throwable
     */
    protected function getOperations(?string $edit, ?string $delete, $item, ?string $clone = null, $disable = false): string
    {
        return table_actions($edit, $delete, $item, $clone, $disable);
    }

    /**
     * @param string|null $deleted
     * @param string|null $restore
     * @param mixed $item
     * @return string
     * @throws Throwable
     */
    protected function getOperationsTrash(?string $deleted, ?string $restore, $item): string
    {
        return table_actions_trash($deleted, $restore, $item);
    }

    /**
     * @return array
     */
    public function getCheckboxColumnHeading()
    {
        return [
            'checkbox' => [
                'width' => '10px',
                'class' => 'text-left no-sort',
                'title' => Form::input('checkbox', null, null, [
                    'class' => 'table-check-all',
                    'data-set' => '.dataTable .checkboxes',
                ])->toHtml(),
                'orderable' => false,
                'searchable' => false,
                'exportable' => false,
                'printable' => false,
            ],
        ];
    }

    /**
     * @param int $id
     * @return string
     * @throws Throwable
     */
    protected function getCheckbox(int $id): string
    {
        return table_checkbox($id);
    }

    /**
     * @return string
     */
    public function getAjaxUrl(): string
    {
        return $this->ajaxUrl;
    }

    /**
     * @param string $ajaxUrl
     * @return $this
     */
    public function setAjaxUrl(string $ajaxUrl): self
    {
        $this->ajaxUrl = $ajaxUrl;
        return $this;
    }

    /**
     * @return null|string
     */
    protected function getDom(): ?string
    {
        $dom = null;

        switch ($this->type) {
            case self::TABLE_TYPE_ADVANCED:
                $dom = "fBrt<'datatables__info_wrap'pli<'clearfix'>>";
                break;
            case self::TABLE_TYPE_SIMPLE:
                $dom = "t<'datatables__info_wrap'<'clearfix'>>";
                break;
        }

        return $dom;
    }


    /**
     * @param string $view
     * @param array $data
     * @param array $mergeData
     * @return mixed
     * @throws Throwable
     */
    public function render($view, $data = [], $mergeData = [])
    {

        $data['id'] = Arr::get($data, 'id', $this->getOption('id'));
        $data['class'] = Arr::get($data, 'class', $this->getOption('class'));

        $this->setAjaxUrl($this->ajaxUrl . '?' . http_build_query(request()->input()));
        $this->setOptions($data);
        $data['actions'] = $this->hasActions ? $this->bulkActions() : [];
        $data['table'] = $this;
        return parent::render($view, $data, $mergeData);
    }


    /**
     * @return array
     * @throws Throwable
     * @since 1.0
     */
    public function getBuilderParameters(): array
    {
        $params = [
            'stateSave' => true,
        ];

        if ($this->type == self::TABLE_TYPE_SIMPLE) {
            return $params;
        }

        $buttons = array_merge($this->getButtons(), $this->getActionsButton());

        $buttons = array_merge($buttons, $this->getDefaultButtons());
        if (!$buttons) {
            return $params;
        }

        return $params + compact('buttons');
    }

    /**
     * @return array
     * @throws Throwable
     * @since 1.0
     */
    public function getButtons(): array
    {
        $buttons = [];

        if (!$this->buttons()) {
            return $buttons;
        }

        foreach ($this->buttons() as $key => $button) {
            if (Arr::get($button, 'extend') == 'collection') {
                $buttons[] = $button;
            } else {
                $className = Arr::exists($button, 'type') ? 'delete-all-trash' : 'action-item';
                $buttons[] = [
                    'className' => $className,
                    'text' => Html::tag('span', $button['text'], [
                        'data-action' => $key,
                        'data-href' => Arr::get($button, 'link'),
                    ])->toHtml(),
                ];
            }
        }

        return $buttons;
    }

    /**
     * @return array
     * @throws \Throwable
     * @since 1.0
     */
    public function buttons()
    {
        return ['reload'];
    }

    /**
     * @return array
     * @throws Throwable
     */
    public function getActionsButton(): array
    {
        if (!$this->getActions()) {
            return [];
        }

        return [
            [
                'extend' => 'collection',
                'text' => '<span> ' . trans('table.actions') . '  <span class="caret"></span></span>',
                'buttons' => $this->getActions(),
            ],
        ];
    }

    /**
     * @return array
     * @throws Throwable
     * @since 2.1
     */
    public function getActions(): array
    {
        if ($this->type == self::TABLE_TYPE_SIMPLE || !$this->actions()) {
            return [];
        }

        $actions = [];

        foreach ($this->actions() as $key => $action) {
            $actions[] = [
                'className' => 'action-item',
                'text' => '<span data-action="' . $key . '" data-href="' . $action['link'] . '"> ' . $action['text'] . '</span>',
            ];
        }
        return $actions;
    }

    /**
     * @return array
     * @since 2.1
     */
    public function actions()
    {
        return [];
    }

    /**
     * @return array
     * @throws Throwable
     */
    public function getDefaultButtons(): array
    {
        return [
            'reload',
        ];
    }

    /**
     * @return array
     * @throws Throwable
     */
    public function bulkActions(): array
    {
        $actions = [];
        if ($this->getBulkChanges()) {
            $actions['bulk-change'] = view('partials.bulk-changes', [
                'bulk_changes' => $this->getBulkChanges(),
                'class' => get_class($this),
                'url' => $this->bulkChangeUrl,
            ])->render();
        }

        return $actions;
    }

    /**
     * @return array
     */
    public function getBulkChanges(): array
    {
        return [];
    }

    /**
     * @return string
     */
    public function htmlInitComplete(): ?string
    {
        return 'function () {' . $this->htmlInitCompleteFunction() . '}';
    }

    /**
     * @return string
     */
    public function htmlInitCompleteFunction(): ?string
    {
        return '
                $(".dataTables_wrapper").css({"width": "100%"});

                if (jQuery().select2) {
                    $(document).find(".select-multiple").select2({
                        width: "100%",
                        allowClear: true,
                        placeholder: $(this).data("placeholder")
                    });
                    $(document).find(".select-search-full").select2({
                        width: "100%"
                    });
                    $(document).find(".select-full").select2({
                        width: "100%",
                        minimumResultsForSearch: -1
                    });
                }
            ';
    }


    /**
     * @return string
     */
    public function htmlDrawCallback(): ?string
    {
        if ($this->type == self::TABLE_TYPE_SIMPLE) {
            return null;
        }

        return 'function () {' . $this->htmlDrawCallbackFunction() . '}';
    }

    /**
     * @return string
     */
    public function htmlDrawCallbackFunction(): ?string
    {
        return '
            var pagination = $(this).closest(".dataTables_wrapper").find(".dataTables_paginate");
            pagination.toggle(this.api().page.info().pages > 1);

            var data_count = this.api().data().count();

            var length_select = $(this).closest(".dataTables_wrapper").find(".dataTables_length");
            var length_info = $(this).closest(".dataTables_wrapper").find(".dataTables_info");
            length_select.toggle(data_count >= 10);
            length_info.toggle(data_count > 0);

            if (jQuery().select2) {
                $(document).find(".select-multiple").select2({
                    width: "100%",
                    allowClear: true,
                    placeholder: $(this).data("placeholder")
                });
                $(document).find(".select-search-full").select2({
                    width: "100%"
                });
                $(document).find(".select-full").select2({
                    width: "100%",
                    minimumResultsForSearch: -1
                });
            }
          $(".table-check-all").prop( "checked", false);
            $("[data-toggle=tooltip]").tooltip({
                placement: "top"
            });
        ';
    }

    /**
     * @param array $buttons
     * @param  $url
     * @param null|string $permission
     * @return array
     * @throws Throwable
     */
    protected function addCreateButton($url, $permission = null, array $buttons = []): array
    {
        $queryString = http_build_query(Request::query());

        if ($queryString) {
            $url = '?' . $queryString;
        }
        if (is_array($url)) {
            if (Arr::exists($url, 'trash'))
                $buttons['trash'] = [
                    'link' => $url['trash'],
                    'text' => view('partials.table.trash')->render(),
                ];
            if (Arr::exists($url, 'create'))
                $buttons['create'] = [
                    'link' => $url['create'],
                    'text' => view('partials.table.create')->render(),
                ];
            if (Arr::exists($url, 'current'))
                $buttons['list'] = [
                    'link' => $url['current'],
                    'text' => view('partials.table.current')->render(),
                ];
            if (Arr::exists($url, 'empty_trash'))
                $buttons['empty_trash'] = [
                    'link' => $url['empty_trash'],
                    'type' => 'empty_trash',
                    'text' => view('partials.table.delete_all')->render(),
                ];

        }

        return $buttons;
    }


    /**
     * @param string $url
     * @param null|string $permission
     * @param array $actions
     * @return array
     */
    protected function addDeleteAction(string $url, $permission = null, $actions = []): array
    {
        if (auth()->user()->can($permission))
        $actions['delete-many'] = view('partials.delete', [
            'href' => $url,
            'data_class' => get_called_class(),
        ]);

        return $actions;
    }

    /**
     * @param string|null $title
     * @param string|null $value
     * @param string| null $type
     * @param null $data
     * @return array
     * @throws Throwable
     */
    public function getValueInput(?string $title, ?string $value, ?string $type, $data = null): array
    {
        $inputName = 'value';

        if (empty($title)) {
            $inputName = 'filter_values[]';
        }
        $attributes = [
            'class' => 'form-control input-value filter-column-value',
            'placeholder' => trans('table.value'),
            'autocomplete' => 'off',
        ];

        switch ($type) {
            case 'select':
                $attributes['class'] = $attributes['class'] . ' select';
                $attributes['placeholder'] = trans('table.select_option');
                $html = Form::select($inputName, $data, $value, $attributes)->toHtml();
                break;
            case 'select-search':
                $attributes['class'] = $attributes['class'] . ' select-search-full';
                $attributes['placeholder'] = trans('table.select_option');
                $html = Form::select($inputName, $data, $value, $attributes)->toHtml();
                break;
            case 'number':
                $html = Form::number($inputName, $value, $attributes)->toHtml();
                break;
            case 'date':

                $attributes['class'] = $attributes['class'] . ' datepicker';
                $attributes['data-date-format'] = config('datatables.date_format.js.date');
                $content = Form::text($inputName, $value, $attributes)->toHtml();
                $html = view('partials.table.date-field', compact('content'))->render();
                break;
            default:
                $html = Form::text($inputName, $value, $attributes)->toHtml();
                break;
        }

        return compact('html', 'data');
    }

    /**
     * @param array $ids
     * @param string $inputKey
     * @param string $inputValue
     * @return boolean
     */
    public function saveBulkChanges(array $ids, string $inputKey, ?string $inputValue): bool
    {

        foreach ($ids as $id) {
            $item = $this->model->findOrFail($id);

            if ($item) {
                $this->saveBulkChangeItem($item, $inputKey, $inputValue);
//                event(new UpdatedContentEvent(User, request(), $item));
            }
        }

        return true;
    }

    /**
     * @param User $item
     * @param string $inputKey
     * @param string $inputValue
     * @return false|User
     */
    public function saveBulkChangeItem($item, string $inputKey, ?string $inputValue)
    {
        if (strpos($inputKey, '.') !== -1) {
            $key = Arr::last(explode('.', $inputKey));
        }
        $item->{$key} = $this->prepareBulkChangeValue($inputKey, $inputValue);
        $item->save();
        return true;
    }

    /**
     * @param string $key
     * @param string $value
     * @return string
     */
    public function prepareBulkChangeValue(string $key, ?string $value): string
    {
        if (strpos($key, '.') !== -1) {
            $key = Arr::last(explode('.', $key));
        }

        switch ($key) {
            case 'created_at':
            case 'updated_at':
                $value = Carbon::createFromFormat(config('datatables.date_format.date'), $value)
                    ->toDateTimeString();
                break;
        }

        return $value;
    }

    /**
     * @param QueryDataTable $data
     * @param array $escapeColumn
     * @param bool $mDataSupport
     * @return mixed
     * @throws \Exception
     */
    public function toJson($data, $escapeColumn = [], $mDataSupport = true)
    {
        return $data
            ->escapeColumns($escapeColumn)
            ->make($mDataSupport);
    }

    /**
     * @param $table
     * @return mixed
     */
    public function getDataSelectOption($table): array
    {
        $data=DB::table($table)->select('id','name')->get();
        $values = [];
        foreach ($data as $item){
            $values[$item->id] = $item->name;
        }
        return $values;
    }
}
