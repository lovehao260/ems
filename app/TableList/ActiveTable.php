<?php

namespace App\TableList;

use Illuminate\Contracts\Routing\UrlGenerator;
use Illuminate\Http\JsonResponse;
use Illuminate\Support\Facades\View;
use Spatie\Activitylog\Models\Activity;
use Throwable;
use Yajra\DataTables\DataTables;

use Form;

class ActiveTable extends TableAbstract
{
    protected $guarantees;
    /**
     * @var bool
     */
    protected $hasActions = true;
    protected $trash = false;
    public function __construct(DataTables $table, Activity $model, UrlGenerator $urlGenerator)
    {
        $this->setOption('id', 'table-activities');
        $this->setType('advanced');
        $this->model = $model;
        parent::__construct($table, $model, $urlGenerator);
    }

    /**
     * @param string|null $trash
     * @param array $data
     * @param array $mergeData
     * @return JsonResponse|View
     * @throws Throwable
     * @since 1.0
     */
    public function renderTable(string $trash = null, $data = [], $mergeData = [])
    {
        $data['breadcrumb']='activities';
        return $this->render($this->view, $data, $mergeData);
    }
    /**
     * {@inheritDoc}
     */
    public function ajax()
    {
        return $this->table
            ->eloquent($this->query())
            ->addColumn('checkbox', function ($item) {
                return $this->getCheckbox($item->id);
            })
            ->editColumn('created_at', function ($item) {
                return date_from_database($item->created_at);
            })
            ->addColumn('user_name', function ($item) {
                return $item->first_name.' '.$item->last_name;
            })
            ->editColumn('properties', function ($item) {
                return  json_decode($item->properties)->ip;
            })
            ->addColumn('browser', function ($item) {
                return  json_decode($item->properties)->browser;
            })
            ->addColumn('log_name', function ($item) {
                return $item->log_name;
            })
            ->addColumn('operations', function ($item) {
                return $this->getOperations('', 'activities.destroy', $item);
            })
            ->rawColumns(['created_at','log_name','operations'])
            ->escapeColumns([])
            ->make(true);
    }
    /**
     * {@inheritDoc}
     */
    public function query()
    {
        //Relationships
        $query = $this->model
            ->select('users.first_name','users.last_name','activity_log.*')
            ->join('users', 'activity_log.causer_id', '=', 'users.id')->orderBy('id', 'DESC');
        return $query;

    }
    /**
     * {@inheritDoc}
     */
    public function bulkActions(): array
    {
        return $this->addDeleteAction(route('activities.deletes'), 'activities.destroy', parent::bulkActions());

    }
    /**
     * {@inheritDoc}
     */
    public function columns()
    {
        return [
            'checkbox' => [
                'width' => '10px',
                'class' => 'text-left no-sort table-check',
                'title' => Form::input('checkbox', null, null, [
                        'class' => 'table-check-all',
                        'data-set' => '.dataTable .checkboxes'

                    ]) . Form::label('checkbox',trans('table.checkbox'), ['class' => 'label-check-all'])->toHtml(),
                'orderable' => false,
                'searchable' => false,
                'exportable' => false,
                'printable' => false,
            ],
            'user_name' => [
                'title' => trans('layout.log.action_user'),
                'class' => 'text-left',
                'targets' => 1,
            ],

            'log_name' => [
                'title' => trans('layout.log.action'),
                'class' => 'text-left',
                'targets' => 1,
            ],
            'properties' => [
                'title' => trans('layout.log.ip_address'),
                'class' => 'text-left',
                'targets' => 1,
            ],
            'browser' => [
                'title' => trans('layout.log.browser'),
                'class' => 'text-left',
                'targets' => 1,
            ],
            'created_at' => [
                'name' => 'activity_log.created_at',
                'title' => trans('table.created_at'),
                'width' => '100px',
            ],
            'operations' => [
                'title' => trans('table.operations'),
                'width' => '54px',
                'class' => 'text-center',
                'orderable' => false,
                'searchable' => false,
                'exportable' => false,
                'printable' => false,
            ],
        ];
    }

    /**
     * {@inheritDoc}
     */
    public function buttons()
    {
        return $this->addCreateButton([
            'empty_trash' => route('activities.clear')]);
    }

    /**
     * {@inheritDoc}
     */
    public function htmlDrawCallbackFunction(): ?string
    {
        return parent::htmlDrawCallbackFunction();
    }

    /**
     * {@inheritDoc}
     */
    public function getDefaultButtons(): array
    {
        return [
            'colvis',
            'export',
            'reload',
        ];
    }

}
