<?php

namespace App\TableList;

use App\Models\Accessory;
use Illuminate\Contracts\Routing\UrlGenerator;
use Illuminate\Http\JsonResponse;
use Illuminate\Support\Facades\View;
use Throwable;
use Yajra\DataTables\DataTables;
use Form;

class AccessoryTable extends TableAbstract
{
    protected $guarantees;
    /**
     * @var bool
     */
    protected $hasActions = false;
    protected $trash = false;
    protected $hasCheckbox = false;

    public function __construct(DataTables $table, Accessory $model, UrlGenerator $urlGenerator)
    {
        $this->setOption('id', 'table-accessories');
        $this->setType('advanced');
        $this->model = $model;
        parent::__construct($table, $model, $urlGenerator);
    }

    /**
     * @param string|null $trash
     * @param array $data
     * @param array $mergeData
     * @return JsonResponse|View
     * @throws Throwable
     * @since 1.0
     */

    public function renderTable(string $query = null,string $trash = null, $data = [], $mergeData = [])
    {
        $this->query=$query ;
        $data['breadcrumb']='accessories';
        return $this->render($this->view, $data, $mergeData);
    }
    /**
     * {@inheritDoc}
     */
    public function ajax()
    {

        return $this->table
            ->eloquent($this->query())
            ->editColumn('name', function ($item) {
                return link_table(route('accessories.show',$item->id), $item->name);
            })
            ->editColumn('image', function ($item) {
                if ($this->request()->input('action')) {
                    return $item->image;
                }
                if ($item->image) {
                    return "<img  src='".url_storage($item->image)."' style='height: 40px'/>";
                }
                return "";
            })
            ->editColumn('created_at', function ($item) {
                return date_from_database($item->created_at);
            })
            ->editColumn('category', function ($item) {
                return link_table(route('categories.show',$item->category_id), $item->category['name']??null);
            })
            ->editColumn('location', function ($item) {
                return $item->location['name']??'';
            })
            ->editColumn('company', function ($item) {
                return $item->company['name']??'';
            })
            ->addColumn('avail', function ($item) {
                return $item->numRemaining();
            })
            ->editColumn('purchase_date', function ($item) {
                return $item->purchase_date?getFormattedDateObject($item->purchase_date, 'formatted')['formatted']:'';
            })
            ->editColumn('purchase_cost', function ($item) {
                return $item->purchase_cost?formatCurrencyOutput($item->purchase_cost):'';
            })
            ->addColumn('checkout', function ($item) {
                $avail=$item->numRemaining() ;
                return  table_accessory_checkout( 'accessories','accessories.checkout',$item->id ,$avail );
            })
            ->addColumn('operations', function ($item) {
                if ($this->trash) {
                    return $this->getOperationsTrash('accessories.deleted', 'accessories.restore', $item);
                } else {
                    return $this->getOperations('accessories.edit', 'accessories.destroy', $item);
                }
            })

            ->escapeColumns([])
            ->make(true);
    }
    /**
     * {@inheritDoc}
     */
    public function query()
    {
        //Relationships
        if ($this->query){
            $model = $this->model->where('category_id',$this->query)->with('category');

        }else{
            $model = $this->model->with('category');
        }

        return $this->applyScopes($model);

    }

    /**
     * {@inheritDoc}
     */
    public function columns()
    {
        return [

            'id' => [
                'name' => 'accessories.id',
                'title' => 'Id',
                'width' => '10px',
                'visible' => false
            ],
            'image' => [
                'name' => 'accessories.image',
                'title' => trans('layout.accessory.device'),
                'class' => 'text-left',
            ],
            'name' => [
                'name' => 'accessories.name',
                'title' => trans('layout.name'),
                'class' => 'text-left',
            ],

            'category' => [
                'name' => 'accessories.category_id',
                'title' =>  trans('layout.category.category'),
                'class' => 'text-left',
            ],
            'model_number' => [
                'name' => 'accessories.model_number',
                'title' => trans('layout.model.no'),
                'class' => 'text-left',
            ],
            'company' => [
                'name' => 'accessories.company_id',
                'title' => trans('layout.company.company'),
                'class' => 'text-left',
            ],
            'location' => [
                'name' => 'accessories.location_id',
                'title' => trans('layout.location.location'),
                'class' => 'text-left',
            ],
            'qty' => [
                'name' => 'accessories.qty',
                'title' =>  trans('layout.total'),
                'class' => 'text-left',
                'targets' => 1,
            ],

            'min_amt' => [
                'name' => 'accessories.min_amt',
                'title' =>  trans('layout.min_qty'),
                'class' => 'text-left',
                'targets' => 1,
            ],
            'avail' => [
                'title' =>  trans('layout.accessory.avail'),
                'class' => 'text-left',
                'targets' => 1,
            ],

            'order_number' => [
                'name' => 'accessories.order_number',
                'title' =>  trans('layout.order_number'),
                'class' => 'text-left',
                'targets' => 1,
            ],
            'purchase_date' => [
                'name' => 'accessories.purchase_date',
                'title' =>  trans('layout.purchase_date'),
                'class' => 'text-left',
                'targets' => 1,
            ],
            'purchase_cost' => [
                'name' => 'accessories.purchase_cost',
                'title' =>  trans('layout.purchase_cost'),
                'class' => 'text-left',
                'targets' => 1,
            ],
            'created_at' => [
                'name' => 'accessories.created_at',
                'title' =>  trans('table.created_at'),
                'width' => '100px',

            ],
            'checkout' => [
                'title' =>  trans('layout.checkout_check'),
                'width' => '100px',
                'orderable' => false,
                'searchable' => false,
                'exportable' => false,
                'printable' => false,
            ],
        ];
    }

    /**
     * {@inheritDoc}
     */
    public function buttons()
    {
        return $this->addCreateButton(['create' => route('accessories.create')]);
    }

    /**
     * {@inheritDoc}
     */
    public function htmlDrawCallbackFunction(): ?string
    {
        return parent::htmlDrawCallbackFunction();
    }

    /**
     * {@inheritDoc}
     */
    public function getDefaultButtons(): array
    {
        return [
            'colvis',
            'export',
            'reload',
        ];
    }

}
