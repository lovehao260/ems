<?php

namespace App\TableList;

use App\Models\License;
use Illuminate\Contracts\Routing\UrlGenerator;
use Illuminate\Http\JsonResponse;
use Illuminate\Support\Facades\View;
use Throwable;
use Yajra\DataTables\DataTables;
use Form;

class LicenceTable extends TableAbstract
{
    protected $guarantees;
    /**
     * @var bool
     */
    protected $hasActions = false;
    protected $trash = false;
    protected $hasCheckbox = false;

    public function __construct(DataTables $table, License $model, UrlGenerator $urlGenerator)
    {
        $this->setOption('id', 'table-licenses');
        $this->setType('advanced');
        $this->model = $model;
        parent::__construct($table, $model, $urlGenerator);
    }

    /**
     * @param string|null $trash
     * @param array $data
     * @param array $mergeData
     * @return JsonResponse|View
     * @throws Throwable
     * @since 1.0
     */

    public function renderTable(string $query = null,string $trash = null, $data = [], $mergeData = [])
    {
        $this->query=$query ;
        $data['breadcrumb']='licenses';
        return $this->render($this->view, $data, $mergeData);
    }
    /**
     * {@inheritDoc}
     */
    public function ajax()
    {

        return $this->table
            ->eloquent($this->query())
            ->editColumn('name', function ($item) {
                return link_table(route('licenses.show',$item->id), $item->name);
            })
            ->editColumn('serial', function ($item) {
                return link_table(route('licenses.show',$item->id), $item->serial);
            })

            ->editColumn('category', function ($item) {
                return link_table(route('categories.show',$item->category_id), $item->category->name);
            })
            ->addColumn('avail', function ($item) {
                return $item->availCount[0]['count']?? 0;
            })
            ->addColumn('checkout', function ($item) {
                $avail=$item->availCount[0]['count']?? 0;
                return  table_checkout( 'table-licenses',$item->id ,$avail );
            })
            ->editColumn('purchase_date', function ($item) {
                return $item->purchase_date?getFormattedDateObject($item->purchase_date, 'formatted')['formatted']:'';
            })
            ->editColumn('purchase_cost', function ($item) {
                return $item->purchase_cost?formatCurrencyOutput($item->purchase_cost):'';
            })
            ->editColumn('termination_date', function ($item) {
                return $item->termination_date?getFormattedDateObject($item->termination_date, 'formatted')['formatted']:'';

            })

            ->addColumn('operations', function ($item) {
                return $this->getOperations('licenses.edit', 'licenses.destroy', $item);
            })

            ->escapeColumns([])
            ->make(true);
    }
    /**
     * {@inheritDoc}
     */
    public function query()
    {
        //Relationships

        if ($this->query){
            $model = $this->model->where('category_id',$this->query)->with('category');
        }else{
            $model = $this->model->with('company','category','availCount');
        }
        return $this->applyScopes($model);
    }
    /**
     * {@inheritDoc}
     */
    public function columns()
    {
        return [

            'id' => [
                'name' => 'licenses.id',
                'title' => 'Id',
                'width' => '10px',
                'visible' => false
            ],
            'name' => [
                'name' => 'licenses.name',
                'title' => trans('layout.name'),
                'class' => 'text-left',
                'targets' => 1,
            ],
            'serial' => [
                'name' => 'licenses.serial',
                'title' =>trans('layout.license.key'),
                'class' => 'text-left',
            ],
            'expiration_date' => [
                'name' => 'licenses.expiration_date',
                'title' =>trans('layout.license.expiration_date'),
                'class' => 'text-left',
                'visible' => false
            ],
            'license_email' => [
                'name' => 'licenses.license_email',
                'title' => trans('layout.license.to_mail'),
                'class' => 'text-left',
                'targets' => 1,
            ],
            'license_name' => [
                'name' => 'licenses.license_name',
                'title' => trans('layout.license.to_name'),
                'class' => 'text-left',
            ],
            'category' => [
                'name' => 'licenses.category_id',
                'title' => trans('layout.category.category'),
                'class' => 'text-left',
            ],
            'seats' => [
                'name' => 'licenses.seats',
                'title' => trans('layout.total'),
            ],
            'avail' => [
                'title' => trans('layout.accessory.avail'),
            ],
            'purchase_date' => [
                'name' => 'licenses.purchase_date',
                'title' => trans('layout.purchase_date'),
                'visible' => false
            ],
            'purchase_cost' => [
                'name' => 'licenses.purchase_cost',
                'title' => trans('layout.purchase_cost'),
                'width' => '100px',
                'visible' => false
            ],
            'purchase_order' => [
                'name' => 'licenses.purchase_order',
                'title' => trans('layout.purchase_order'),
                'width' => '100px',
                'visible' => false
            ],
            'termination_date' => [
                'name' => 'licenses.termination_date' ,
                'title' => trans('layout.license.termination_date'),
                'visible' => false
            ],
            'order_number' => [
                'name' => 'licenses.order_number',
                'title' => trans('layout.order_number'),
                'visible' => false
            ],
            'notes' => [
                'name' => 'licenses.notes',
                'title' => trans('layout.note'),
                'visible' => false
            ],
            'checkout' => [
                'title' => trans('layout.checkout'),
                'width' => '100px',
                'orderable' => false,
                'searchable' => false,
                'exportable' => false,
                'printable' => false,
            ],
        ];
    }

    /**
     * {@inheritDoc}
     */
    public function buttons()
    {
        return $this->addCreateButton(['create' => route('licenses.create')]);
    }

    /**
     * {@inheritDoc}
     */
    public function htmlDrawCallbackFunction(): ?string
    {
        return parent::htmlDrawCallbackFunction();
    }

    /**
     * {@inheritDoc}
     */
    public function getDefaultButtons(): array
    {
        return [
            'colvis',
            'export',
            'reload',
        ];
    }

}
