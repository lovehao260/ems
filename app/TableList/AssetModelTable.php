<?php

namespace App\TableList;

use App\Models\AssetModel;
use Illuminate\Contracts\Routing\UrlGenerator;
use Illuminate\Http\JsonResponse;
use Illuminate\Support\Facades\View;
use Throwable;
use Yajra\DataTables\DataTables;
use Form;

class AssetModelTable extends TableAbstract
{
    protected $guarantees;
    /**
     * @var bool
     */
    protected $hasActions = true;
    protected $trash = false;

    public function __construct(DataTables $table, AssetModel $model, UrlGenerator $urlGenerator)
    {
        $this->setOption('id', 'table-models');
        $this->setType('advanced');
        $this->model = $model;
        parent::__construct($table, $model, $urlGenerator);
    }

    /**
     * @param string|null $trash
     * @param array $data
     * @param array $mergeData
     * @return JsonResponse|View
     * @throws Throwable
     * @since 1.0
     */
    public function renderTable(string $trash = null, $data = [], $mergeData = [])
    {
        if ($trash) {
            $this->trash = true;
            $this->setOption('id', 'table-models-trash');
        }
        $data['breadcrumb']='models';
        return $this->render($this->view, $data, $mergeData);
    }
    /**
     * {@inheritDoc}
     */
    public function ajax()
    {

        return $this->table
            ->eloquent($this->query())
            ->editColumn('checkbox', function ($item) {
                return $this->getCheckbox($item->id);
            })
            ->editColumn('image', function ($item) {

                if ($this->request()->input('action')) {
                    return $item->image;
                }

                if ($item->image){
                    return "<img  src='".url_storage($item->image)."' style='height: 40px'/>";
                }
                return "";
            })
            ->editColumn('created_at', function ($item) {
                return date_from_database($item->created_at);
            })
            ->editColumn('category', function ($item) {
                return link_table(route('categories.show',$item->category_id), $item->category['name']);
            })
            ->addColumn('assets', function ($item) {
                return  count($item->assets);
            })
            ->addColumn('operations', function ($item) {
                if ($this->trash) {
                    return $this->getOperationsTrash('models.deleted', 'models.restore', $item);
                } else {
                    $disable=false;
                    if (count($item->assets))
                        $disable = true;

                    return $this->getOperations('models.edit', 'models.destroy', $item, 'models.clone',$disable);
                }
            })

            ->escapeColumns([])
            ->make(true);
    }
    /**
     * {@inheritDoc}
     */
    public function query()
    {
        //Relationships
        if (!$this->trash) {
            $model = $this->model->with('company')->where('deleted_at',null);

        } else {
            $model = $this->model->with('company')->onlyTrashed();
        }
        return $this->applyScopes($model);

    }

    /**
     * {@inheritDoc}
     */
    public function columns()
    {
        return [
            'checkbox' => [
                'width' => '10px',
                'class' => 'text-left no-sort table-check',
                'title' => Form::input('checkbox',null,null, [
                        'class' => 'table-check-all',
                        'data-set' => '.dataTable .checkboxes'

                    ]). Form::label('checkbox',trans('table.checkbox'),['class'=>'label-check-all'])->toHtml(),
                'orderable' => false,
                'searchable' => false,
                'exportable' => false,
                'printable' => false,
            ],
            'id' => [
                'name' => 'models.id',
                'title' => 'Id',
                'width' => '10px',
                'visible' => false
            ],
            'company' => [
                'name' => 'models.company_id',
                'title' =>trans('layout.company.company'),
                'class' => 'text-left',
                'visible' => false
            ],
            'name' => [
                'name' => 'models.name',
                'title' =>trans('layout.name'),
                'class' => 'text-left',
                'targets' => 1,
            ],
            'image' => [
                'name' => 'models.image',
                'title' => trans('layout.image'),
                'class' => 'text-left',
            ],

            'model_number' => [
                'name' => 'models.model_number',
                'title' => trans('layout.model.no'),
                'class' => 'text-left',
            ],
            'assets' => [
                'title' => trans('layout.assets'),
                'class' => 'text-left',
            ],
            'eol' => [
                'name' => 'models.eol',
                'title' => 'EOL',
                'class' => 'text-left',
            ],
            'category' => [
                'name' => 'models.category_id',
                'title' => trans('layout.category.category'),
                'class' => 'text-left',
            ],
            'note' => [
                'name' => 'models.note',
                'title' => trans('layout.note'),
                'class' => 'text-left',
                'visible' => false
            ],
            'created_at' => [
                'name' => 'models.created_at',
                'title' => trans('table.created_at'),
                'width' => '100px',
            ],
        ];
    }

    /**
     * {@inheritDoc}
     */
    public function buttons()
    {
        if ($this->trash) {
            return $this->addCreateButton(['current' => route('models.index'),
                'empty_trash' => route('models.empty_trash')]);
        } else {
            return $this->addCreateButton(['create' => route('models.create'),
                'trash' => route('models.trash', 'trash')]);
        }
    }

    /**
     * {@inheritDoc}
     */
    public function htmlDrawCallbackFunction(): ?string
    {
        return parent::htmlDrawCallbackFunction();
    }

    /**
     * {@inheritDoc}
     */
    public function bulkActions(): array
    {
        if ($this->trash)
            return [];
        else
            return $this->addDeleteAction(route('models.deletes'), 'delete-model', parent::bulkActions());
    }
    //    /**
    //     * {@inheritDoc}
    //     * You can export a column customised header if manually set.
    //     */
    //    protected $exportColumns = [
    //        ['data' => 'name', 'title' => 'Name'],
    //        ['data' => 'email', 'title' => 'Registered Email'],
    //    ];

    /**
     * {@inheritDoc}
     */
    public function getBulkChanges(): array
    {
        if (!auth()->user()->can('edit-model')) {
            return [];
        }else {
            return [
                'name' => [
                    'title' => trans('layout.name'),
                    'type' => 'text',
                    'validate' => 'required|max:120',
                ],
                'model_number' => [
                    'title' => trans('layout.model.no'),
                    'type' => 'text',
                    'validate' => 'required|max:120',
                ],
                'created_at' => [
                    'title' => trans('table.created_at'),
                    'type' => 'date',
                ],
            ];
        }
    }


    /**
     * {@inheritDoc}
     */
    public function getDefaultButtons(): array
    {
        return [
            'colvis',
            'export',
            'reload',
        ];
    }

}
