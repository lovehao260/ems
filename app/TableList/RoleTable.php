<?php

namespace App\TableList;

use App\Models\Role;
use Illuminate\Contracts\Routing\UrlGenerator;
use Illuminate\Http\JsonResponse;
use Illuminate\Support\Facades\View;
use Throwable;
use Yajra\DataTables\DataTables;

use Form;

class RoleTable extends TableAbstract
{
    protected $guarantees;
    /**
     * @var bool
     */
    protected $hasActions = true;
    protected $trash = false;
    public function __construct(DataTables $table, Role $model, UrlGenerator $urlGenerator)
    {
        $this->setOption('id', 'table-roles');
        $this->setType('advanced');
        $this->model = $model;
        parent::__construct($table, $model, $urlGenerator);
    }

    /**
     * @param string|null $trash
     * @param array $data
     * @param array $mergeData
     * @return JsonResponse|View
     * @throws Throwable
     * @since 1.0
     */
    public function renderTable(string $trash = null, $data = [], $mergeData = [])
    {
        $data['breadcrumb']='roles';
        return $this->render($this->view, $data, $mergeData);
    }
    /**
     * {@inheritDoc}
     */
    public function ajax()
    {
        return $this->table
            ->eloquent($this->query())
            ->addColumn('checkbox', function ($item) {
                return $this->getCheckbox($item->id);
            })
            ->editColumn('created_at', function ($item) {
                return date_from_database($item->created_at);
            })
            ->editColumn('created_by', function ($item) {
                return $item->created_by->getFullNameAttribute()??'Seeder';
            })

            ->addColumn('operations', function ($item) {
                return $this->getOperations('roles.edit', 'roles.destroy', $item,null);
            })
            ->rawColumns(['created_at','created_by','operations'])
            ->escapeColumns([])
            ->make(true);
    }
    /**
     * {@inheritDoc}
     */
    public function query()
    {
        //Relationships
        $query = $this->model->with('created_by');
        return $query;
    }
    /**
     * {@inheritDoc}
     */
    public function bulkActions(): array
    {
        return $this->addDeleteAction(route('roles.deletes'), 'delete-role', parent::bulkActions());
    }
    /**
     * {@inheritDoc}
     */
    public function columns()
    {
        return [
            'checkbox' => [
                'width' => '10px',
                'class' => 'text-left no-sort table-check',
                'title' => Form::input('checkbox', null, null, [
                        'class' => 'table-check-all',
                        'data-set' => '.dataTable .checkboxes'

                    ]) . Form::label('checkbox', trans('table.checkbox'), ['class' => 'label-check-all'])->toHtml(),
                'orderable' => false,
                'searchable' => false,
                'exportable' => false,
                'printable' => false,
            ],
            'name' => [
                'name' => 'roles.name',
                'title' => trans('layout.name'),
                'class' => 'text-left',
                'targets' => 1,
            ],

            'description' => [
                'title' => trans('layout.description'),
                'class' => 'text-left',
                'targets' => 1,
            ],
            'created_at'  => [
                'title' =>trans('table.created_at'),
                'width' => '100px',
            ],
            'created_by'  => [
                'title' => trans('table.created_by'),
                'width' => '100px',
            ],

        ];
    }

    /**
     * {@inheritDoc}
     */
    public function buttons()
    {
        return $this->addCreateButton(['create' => route('roles.create')]);

    }

    /**
     * {@inheritDoc}
     */
    public function htmlDrawCallbackFunction(): ?string
    {
        return parent::htmlDrawCallbackFunction();
    }
    /**
     * {@inheritDoc}
     */
    public function getBulkChanges(): array
    {
        if (!auth()->user()->can('edit-role')) {
            return [];
        }else {
            return [
                'users.name' => [
                    'title' => trans('layout.name'),
                    'type' => 'text',
                    'validate' => 'required|max:120',
                ],
            ];
        }
    }

    /**
     * {@inheritDoc}
     */
    public function getDefaultButtons(): array
    {
        return [
            'colvis',
            'export',
            'reload',
        ];
    }

}
