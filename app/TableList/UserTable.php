<?php

namespace App\TableList;

use App\Models\User;
use Illuminate\Contracts\Routing\UrlGenerator;
use Illuminate\Http\JsonResponse;
use Illuminate\Support\Facades\View;
use Throwable;
use Yajra\DataTables\DataTables;
use Form;

class UserTable extends TableAbstract
{
    protected $guarantees;
    /**
     * @var bool
     */
    protected $hasActions = true;
    protected $trash = false;

    public function __construct(DataTables $table, User $model, UrlGenerator $urlGenerator)
    {
        $this->setOption('id', 'table-users');
        $this->setType('advanced');
        $this->model = $model;
        parent::__construct($table, $model, $urlGenerator);
    }

    /**
     * @param string|null $trash
     * @param array $data
     * @param array $mergeData
     * @return JsonResponse|View
     * @throws Throwable
     * @since 1.0
     */
    public function renderTable(string $trash = null, $data = [], $mergeData = [])
    {
        if ($trash) {
            $this->trash = true;
            $this->setOption('id', 'table-users-trash');
        }
        $data['breadcrumb'] = 'users';
        return $this->render($this->view, $data, $mergeData);
    }
    /**
     * {@inheritDoc}
     */
    public function ajax()
    {
        return $this->table
            ->eloquent($this->query())
            ->editColumn('checkbox', function ($item) {
                return $this->getCheckbox($item->id);
            })
            ->editColumn('name', function ($item) {
                return link_table(route('users.show', $item->id), $item->first_name . ' ' . $item->last_name);
            })
            ->editColumn('location', function ($item) {
                return $item->userloc['name'] ?? '';
            })
            ->addColumn('accessories', function ($item) {
                return count($item->accessories);
            })
            ->addColumn('licenses', function ($item) {
                return count($item->licenses);
            })
            ->editColumn('created_at', function ($item) {
                return date_from_database($item->created_at);
            })
            ->addColumn('operations', function ($item) {
                if ($this->trash) {
                    return $this->getOperationsTrash('users.deleted', 'users.restore', $item);
                } else {
                    $disable = false;
                    if (count($item->accessories) || count($item->licenses))
                        $disable = true;

                    return $this->getOperations('users.edit', 'users.destroy', $item, 'users.clone', $disable);
                }
            })
            ->editColumn('company', function ($item) {
                return $item->company->name;
            })
            ->editColumn('manager', function ($item) {

                return $item->manager['username'] ?? '';

            })
            ->escapeColumns([])
            ->make(true);
    }
    /**
     * {@inheritDoc}
     */
    public function query()
    {
        //Relationships
        if (!$this->trash) {
            $model = $this->model->with('company', 'userloc')->where('deleted_at', null);

        } else {
            $model = $this->model->with('company', 'userloc')->onlyTrashed();
        }
        return $this->applyScopes($model);
    }
    /**
     * {@inheritDoc}
     */
    public function columns()
    {
        return [
            'checkbox' => [
                'width' => '10px',
                'class' => 'text-left no-sort table-check',
                'title' => Form::input('checkbox', null, null, [
                        'class' => 'table-check-all',
                        'data-set' => '.dataTable .checkboxes'

                    ]) . Form::label('checkbox', trans('table.checkbox'), ['class' => 'label-check-all'])->toHtml(),
                'orderable' => false,
                'searchable' => false,
                'exportable' => false,
                'printable' => false,
            ],
            'id' => [
                'name' => 'users.id',
                'title' => 'Id',
                'width' => '10px',
                'visible' => false
            ],
            'company' => [
                'name' => 'users.company_id',
                'title' => trans('layout.company.company'),
                'class' => 'text-left',
                'visible' => false
            ],
            'name' => [
                'name' => 'users.first_name',
                'title' => trans('layout.name'),
                'class' => 'text-left',
                'targets' => 1,
            ],

            'phone' => [
                'name' => 'users.phone',
                'title' => trans('layout.user.phone'),
                'class' => 'text-left',
                'visible' => false
            ],
            'username' => [
                'name' => 'users.username',
                'title' => trans('layout.user.username'),
                'class' => 'text-left',
                'targets' => 1,
            ],
            'email' => [
                'name' => 'users.email',
                'title' => trans('layout.email'),
                'class' => 'text-left',

            ],
            'title' => [
                'name' => 'users.title',
                'title' => trans('layout.user.title'),
                'class' => 'text-left',
                'visible' => false
            ],
            'location' => [
                'name' => 'users.location_id',
                'title' => trans('layout.location.location'),
                'class' => 'text-left',
                'orderable' => false,
            ],
            'manager' => [
                'name' => 'users.manager_id',
                'title' =>trans('layout.manager'),
                'class' => 'text-left',
            ],
            'licenses' => [
                'title' => '<i class="fa fa-barcode"></i>' . Form::label('Licenses', trans('layout.licenses'), ['class' => 'label-check-all'])->toHtml(),
                'class' => 'text-center count-user',
                'orderable' => false,
                'searchable' => false,
                'visible' => true
            ],
            'accessories' => [
                'title' => '<i class="fa fa-keyboard"></i>' . Form::label('Accessories', trans('layout.accessories'), ['class' => 'label-check-all'])->toHtml(),
                'class' => 'text-center count-user',
                'orderable' => false,
                'searchable' => false,
                'visible' => true
            ],
            'address' => [
                'name' => 'users.address',
                'title' => trans('layout.address'),
                'class' => 'text-left',
                'visible' => false
            ],
            'notes' => [
                'name' => 'users.notes',
                'title' => trans('layout.note'),
                'class' => 'text-left',
                'visible' => false
            ],
            'created_at' => [
                'name' => 'users.created_at',
                'title' =>trans('table.created_at'),
                'width' => '100px',

            ],
        ];
    }

    /**
     * {@inheritDoc}
     */
    public function buttons()
    {
        if ($this->trash) {
            return $this->addCreateButton(['current' => route('users.index'),
                'empty_trash' => route('users.empty_trash')]);
        } else {

            return $this->addCreateButton(['create' => route('users.create'),
                'trash' => route('users.trash', 'trash')]);
        }
    }

    /**
     * {@inheritDoc}
     */
    public function htmlDrawCallbackFunction(): ?string
    {
        return parent::htmlDrawCallbackFunction();
    }

    /**
     * {@inheritDoc}
     */
    public function bulkActions(): array
    {
        if ($this->trash)
            return [];
        else
            return $this->addDeleteAction(route('users.deletes'), 'delete-user', parent::bulkActions());

    }

    /**
     * {@inheritDoc}
     * You can export a column customised header if manually set.
     */
    protected $exportColumns = [
        ['data' => 'first_name', 'title' => 'First Name'],
        ['data' => 'last_name', 'title' => 'Last Name'],
        ['data' => 'username', 'title' => 'Username'],
        ['data' => 'email', 'title' => 'Email'],
        ['data' => 'phone', 'title' => 'Phone'],
        ['data' => 'location', 'title' => 'Location'],
        ['data' => 'company', 'title' => 'Company'],
        ['data' => 'manager', 'title' => 'Manager'],
        ['data' => 'licenses', 'title' => 'Licenses'],
        ['data' => 'accessories', 'title' => 'Accessories'],
        ['data' => 'created_at', 'title' => 'Created at'],
        ['data' => 'created_at', 'title' => 'Created at'],

    ];

    /**
     * {@inheritDoc}
     */
    public function getBulkChanges(): array
    {
        if (!auth()->user()->can('edit-user')) {
            return [];
        }else {
            return [
                'name' => [
                    'title' => trans('layout.name'),
                    'type' => 'text',
                    'validate' => 'required|max:120',
                ],
                'company_id' => [
                    'title' => trans('layout.company.company'),
                    'choices' => $this->getDataSelectOption('companies'),
                    'type' => 'select-search',
                ],
                'location_id' => [
                    'title' => trans('layout.location.location'),
                    'choices' => $this->getDataSelectOption('locations'),
                    'type' => 'select-search',
                ],
                'created_at' => [
                    'title' => trans('table.created_at'),
                    'type' => 'date',
                ],
            ];
        }
    }


    /**
     * {@inheritDoc}
     */
    public function getDefaultButtons(): array
    {
        return [
            'colvis',
            'export',
            'reload',
        ];
    }

}
