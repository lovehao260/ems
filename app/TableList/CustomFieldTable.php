<?php

namespace App\TableList;

use App\Models\Accessory;
use App\Models\CustomField;
use Illuminate\Contracts\Routing\UrlGenerator;
use Illuminate\Http\JsonResponse;
use Illuminate\Support\Facades\View;
use Throwable;
use Yajra\DataTables\DataTables;
use Form;

class CustomFieldTable extends TableAbstract
{
    protected $guarantees;
    /**
     * @var bool
     */
    protected $hasActions = false;
    protected $trash = false;
    protected $hasCheckbox = false;
    protected $view = 'custom_fields.field-table';

    public function __construct(DataTables $table, CustomField $model, UrlGenerator $urlGenerator)
    {
        $this->setOption('id', 'table-custom-fields');

        $this->setType('advanced');
        $this->model = $model;
        parent::__construct($table, $model, $urlGenerator);
    }

    /**
     * @param string|null $trash
     * @param array $data
     * @param array $mergeData
     * @return JsonResponse|View
     * @throws Throwable
     * @since 1.0
     */

    public function renderTable(string $query = null, string $trash = null, $data = [], $mergeData = [])
    {
        $this->query = $query;
        return $this->render($this->view, $data, $mergeData);
    }
    /**
     * {@inheritDoc}
     */
    public function ajax()
    {
        return $this->table
            ->eloquent($this->query())
            ->addColumn('fieldset', function ($item) {
                return custom_field_tag('fields.fieldset.show', $item->fieldset);
            })
            ->addColumn('format', function ($item) {
                return empty($item->format)?'Any':$item->format;

            })
            ->addColumn('operations', function ($item) {
                return $this->getOperations('fields.field.edit', 'fields.field.destroy', $item);
            })
            ->escapeColumns([])
            ->make(true);
    }
    /**
     * {@inheritDoc}
     */
    public function query()
    {
        //Relationships
        $model = $this->model->with('fieldset');

        return $this->applyScopes($model);

    }

    /**
     * {@inheritDoc}
     */
    public function columns()
    {
        return [

            'id' => [
                'name' => 'custom_fields.id',
                'title' => 'Id',
                'width' => '10px',
                'visible' => false
            ],

            'name' => [
                'name' => 'custom_fields.name',
                'title' =>  trans('layout.name'),
                'class' => 'text-left',
            ],

            'help_text' => [
                'name' => 'custom_fields.help_text',
                'title' => trans('layout.custom_field.help'),
                'class' => 'text-left',
            ],
            'format' => [
                'name' => 'custom_fields.format',
                'title' => trans('layout.format'),
                'class' => 'text-left',
            ],
            'element' => [
                'name' => 'custom_fields.element',
                'title' =>  trans('layout.custom_field.element'),
                'class' => 'text-left',
            ],
            'fieldset' => [
                'title' =>  trans('layout.custom_field.fieldset'),
                'class' => 'text-left',
            ],
        ];
    }

    /**
     * {@inheritDoc}
     */
    public function buttons()
    {
        return $this->addCreateButton(['create' => route('fields.field.create')]);
    }


    /**
     * {@inheritDoc}
     */
    public function getDefaultButtons(): array
    {
        return [
            'colvis',
            'export',
            'reload',
        ];
    }

}
