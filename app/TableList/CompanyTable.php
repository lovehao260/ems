<?php

namespace App\TableList;

use App\Models\Company;
use Illuminate\Contracts\Routing\UrlGenerator;
use Illuminate\Http\JsonResponse;
use Illuminate\Support\Facades\View;
use Throwable;
use Yajra\DataTables\DataTables;

use Form;

class CompanyTable extends TableAbstract
{
    protected $guarantees;
    /**
     * @var bool
     */
    protected $hasActions = false;
    protected $trash = false;
    protected $hasCheckbox = false;

    public function __construct(DataTables $table, Company $model, UrlGenerator $urlGenerator)
    {
        $this->setOption('id', 'table-companies');
        $this->setType('advanced');
        $this->model = $model;
        parent::__construct($table, $model, $urlGenerator);
    }

    /**
     * @param string|null $trash
     * @param array $data
     * @param array $mergeData
     * @return JsonResponse|View
     * @throws Throwable
     * @since 1.0
     */
    public function renderTable(string $trash = null, $data = [], $mergeData = [])
    {
        $data['breadcrumb']='companies';
        return $this->render($this->view, $data, $mergeData);
    }
    /**
     * {@inheritDoc}
     */
    public function ajax()
    {

        return $this->table
            ->eloquent($this->query())
            ->editColumn('image', function ($item) {
                if ($this->request()->input('action')) {
                    return $item->image;
                }
                if ($item->image){
                    return "<img  src='".url_storage($item->image)."' style='height: 40px'/>";
                }
                return "";
            })
            ->editColumn('created_at', function ($item) {
                return date_from_database($item->created_at);
            })
            ->addColumn('users', function ($item) {
                return count($item->users);
            })
            ->addColumn('assets', function ($item) {
                return count($item->assets);
            })
            ->addColumn('licenses', function ($item) {
                return count($item->licenses);
            })
            ->addColumn('accessories', function ($item) {
                return count($item->accessories);
            })
            ->addColumn('components', function ($item) {
                return count($item->components);
            })
            ->addColumn('operations', function ($item) {
                return $this->getOperations('companies.edit', 'companies.destroy', $item,null,$item->isDeletable());
            })
            ->escapeColumns([])
            ->make(true);
    }
    /**
     * {@inheritDoc}
     */
    public function query()
    {
        //Relationships
        $model = $this->model->with('users');
        return $this->applyScopes($model);

    }
    /**
     * {@inheritDoc}
     */

    public function columns()
    {
        return [

            'name' => [
                'name' => 'companies.name',
                'title' =>trans('layout.name'),
                'class' => 'text-left',
                'targets' => 1,
            ],
            'image' => [
                'name' => 'companies.image',
                'title' =>trans('layout.image'),
                'class' => 'text-left',
            ],
            'users' => [
                'title' => '<i class="fa fa-user"></i>' . Form::label('Users', trans('layout.users'), ['class' => 'label-check-all'])->toHtml(),
                'class' => 'text-center count-user',
                'orderable' => false,
                'searchable' => false,
                'visible' => true
            ],
            'assets' => [
                'title' => '<i class="fa fa-save"></i>' . Form::label('Assets', trans('layout.assets'), ['class' => 'label-check-all'])->toHtml(),
                'class' => 'text-center count-user',
                'orderable' => false,
                'searchable' => false,
                'visible' => true

            ],
            'licenses' => [
                'title' => '<i class="fa fa-barcode"></i>' . Form::label('Licenses', trans('layout.licenses'), ['class' => 'label-check-all'])->toHtml(),
                'class' => 'text-center count-user',
                'orderable' => false,
                'searchable' => false,
                'visible' => true

            ],
            'accessories' => [
                'title' => '<i class="fa fa-keyboard"></i>' . Form::label('Accessories',  trans('layout.accessories'), ['class' => 'label-check-all'])->toHtml(),
                'class' => 'text-center count-user',
                'orderable' => false,
                'searchable' => false,
                'visible' => true

            ],
            'components' => [
                'title' => '<i class="fa fa-hdd"></i>' . Form::label('Components',  trans('layout.components'), ['class' => 'label-check-all'])->toHtml(),
                'class' => 'text-center count-user',
                'orderable' => false,
                'searchable' => false,
                'visible' => true

            ],
            'created_at' => [
                'name' => 'users.created_at',
                'title' => trans('table.created_at'),
                'width' => '100px',
            ],
        ];
    }

    /**
     * {@inheritDoc}
     */
    public function buttons()
    {
        return $this->addCreateButton(['create' => route('companies.create')]);
    }

    /**
     * {@inheritDoc}
     */
    public function htmlDrawCallbackFunction(): ?string
    {
        return parent::htmlDrawCallbackFunction();
    }

    /**
     * {@inheritDoc}
     */
    public function getDefaultButtons(): array
    {
        return [
            'colvis',
            'export',
            'reload',
        ];
    }

}
