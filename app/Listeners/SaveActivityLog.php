<?php

namespace App\Listeners;
use Illuminate\Support\Facades\Auth;
use Spatie\Activitylog\Models\Activity;

class SaveActivityLog
{
    /**
     * Create the event listener.
     *
     * @return void
     */
    public function __construct()
    {
        //
    }

    /**
     * Handle the event.
     * @param $data
     * @return void
     */
    public function handle(  $data)
    {

        Activity::saving(function (Activity $activity) {
            $activity->properties = $activity->properties->put('ip', request()->ip());
            $activity->properties = $activity->properties->put('browser', request()->header('User-Agent'));
        });
        activity()
            ->causedBy(Auth::user()) //save user
             ->performedOn($data->target)
            -> inLog( $data->message??'')
            ->log(json_encode($data->data)); //description


    }
}
