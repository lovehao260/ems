<?php

namespace App\Providers;


use App\Supports\SettingsManager;
use App\Supports\SettingStore;
use Illuminate\Foundation\Application;
use Illuminate\Support\ServiceProvider;
use Illuminate\Contracts\Cache\Factory;
use App\Models\Setting;
use DateTimeZone;
class SettingServiceProvider extends ServiceProvider
{
    /**
     * Register services.
     *
     * @return void
     */
    public function register()
    {
        //
    }

    /**
     * Bootstrap services.
     *
     * @return void
     */
    public function boot(Factory $cache, Setting $settings)
    {
        $this->app->singleton(SettingsManager::class, function (Application $app) {
            return new SettingsManager($app);
        });
        $this->app->bind(SettingStore::class, function (Application $app) {
            return $app->make(SettingsManager::class)->driver();
        });
        $this->app->booted(function () {
            $config = $this->app->make('config');
            $setting = $this->app->make(SettingStore::class);
            $timezone = $setting->get('time_zone', $config->get('app.timezone'));
            $locale = $setting->get('locale', $config->get('core.base.general.locale', $config->get('app.locale')));
            $config->set([
                'app.locale'   => $locale,
                'app.timezone' => $timezone,
            ]);
            $this->app->setLocale($locale);
            if (in_array($timezone, DateTimeZone::listIdentifiers())) {
                date_default_timezone_set($timezone);
            }
        });

    }
}
