<?php

namespace App\Http\Resources;
use Illuminate\Http\Request;
use Illuminate\Http\Resources\Json\JsonResource;

class FolderResource extends JsonResource
{
    /**
     * Transform the resource into an array.
     * @param Request $request
     * @return array
     */
    public function toArray($request)
    {
        return [
            'id'         => $this->id,
            'name'       => $this->name,
            'parent_id'       => $this->parent_id,
            'created_at' =>date_from_database($this->created_at, 'Y-m-d H:i:s'),
            'updated_at' =>date_from_database($this->updated_at, 'Y-m-d H:i:s'),
        ];
    }
}
