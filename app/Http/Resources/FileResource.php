<?php

namespace App\Http\Resources;

use App\Models\MediaFolder;
use Illuminate\Http\Request;
use Illuminate\Http\Resources\Json\JsonResource;
use Illuminate\Support\Facades\File;
use Illuminate\Support\Facades\Storage;

/**
 * @mixin MediaFolder
 */
class FileResource extends JsonResource
{
    /**
     * Transform the resource into an array.
     *
     * @param Request $request
     * @return array
     */
    public function toArray($request)
    {
        return [
            'id'         => $this->id,
            'name'       => $this->name,
            'basename'   => File::basename($this->url),
            'url'        => $this->url,
            'full_url'   => url_storage($this->url),
            'type'       => $this->type,
            'icon'       => $this->icon,
            'size'       => $this->human_size,
            'mime_type'  => $this->mime_type,
            'created_at' => date_from_database($this->created_at, 'Y-m-d H:i:s'),
            'updated_at' => date_from_database($this->updated_at, 'Y-m-d H:i:s'),
            'options'    => $this->options,
            'folder_id'  => $this->folder_id,
        ];
    }
}
