<?php

namespace App\Http\Controllers;

use App\Events\CreatedContentEvent;
use App\Http\Responses\BaseHttpResponse;
use App\Models\Permission;
use App\Models\Role;
use App\TableList\RoleTable;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Http\JsonResponse;
use Illuminate\Http\RedirectResponse;
use Illuminate\Http\Request;
use Illuminate\Support\Arr;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Str;


class RoleController extends Controller
{
    protected $response;

    public function __construct(BaseHttpResponse $response)
    {
        $this->response = $response;
        $this->middleware('auth');
    }

    /**
     * Display a listing of the resource.
     * @return BaseHttpResponse|JsonResponse|\Illuminate\Support\Facades\View
     * @throws \Throwable
     */
    public function index(Request $request, RoleTable $datatable, BaseHttpResponse $response)
    {
        if ($request->user()->can('view-role')) {
            page_title()->setTitle(trans('layout.role.roles'));
            return $datatable->renderTable();
        } else {
            return $response
                ->setError()
                ->setNextUrl(redirect()->back()->getTargetUrl())
                ->setMessage(trans('table.no_permission'));
        }
    }

    /**
     * @return \Illuminate\Contracts\View\View|JsonResponse|BaseHttpResponse
     */
    public function create(Request $request)
    {
        if (!$request->user()->can('create-role')) {
            return $this->response
                ->setError()
                ->setNextUrl(redirect()->back()->getTargetUrl())
                ->setMessage(trans('table.no_permission'));
        }
        page_title()->setTitle(trans('layout.role.create'));
        $permissions = Permission::get()->groupBy('group');

        return view('roles.add', compact('permissions'));
    }

    /**
     * Store a newly created resource in storage.
     * @return BaseHttpResponse
     * @throws \Illuminate\Validation\ValidationException
     */
    public function store(Request $request, BaseHttpResponse $response)
    {

        $this->validate($request, [
            'name' => 'required|unique:roles,name,',
            'description' => 'required',
        ]);
        try {
            DB::beginTransaction();
            if ($request->input('is_default')) {
                Role::where('default', 1)->update(['default' => 0]);
            }
            // Save role
            $role = new Role();
            $role->name = $request->input('name');
            $role->slug = Str::slug($request->input('name'));
            $role->description = $request->input('description');
            $role->default = $request->input('is_default') ?? 0;
            $role->user_id = auth()->id();
            $role->save();

            $role->permissions()->sync($request->permissions);

            event(new CreatedContentEvent($request->all(), $role, config('general.log.creat')));
            DB::commit();
        } catch (Throwable $e) {
            DB::rollback();
            report($e);
            return false;
        }
        return $response
            ->setNextUrl(route('roles.index'))
            ->setMessage(trans('table.create_success_message'));
    }

    /**
     * @param int $id
     * @param Request $request
     * @param BaseHttpResponse $response
     * @return BaseHttpResponse
     */
    public function destroy($id, Request $request, BaseHttpResponse $response): BaseHttpResponse
    {
        if (!$request->user()->can('delete-role')) {
            return $this->response
                ->setError()
                ->setNextUrl(redirect()->back()->getTargetUrl())
                ->setMessage(trans('table.no_permission'));
        }
        $role = Role::findOrFail($id);
        if ($role->default) {
            return $response->setError()->setMessage(trans('table.role_default'));
        }

        $role->delete();
        clearCache();
        return $response->setMessage(trans('table.delete_success_message'));
    }

    /**
     * @param Request $request
     * @param BaseHttpResponse $response
     * @return BaseHttpResponse
     */
    public function deletes(Request $request, BaseHttpResponse $response)
    {
        if (!$request->user()->can('delete-role')) {
            return $this->response
                ->setError()
                ->setNextUrl(redirect()->back()->getTargetUrl())
                ->setMessage(trans('table.no_permission'));
        }
        $ids = $request->input('ids');
        if (empty($ids)) {
            return $response
                ->setError()
                ->setMessage(trans('table.no_select'));
        }
        foreach ($ids as $id) {
            $role = Role::find($id);
            if (!$role->default) {
                $role->delete();
            }
        }
        clearCache();
        return $response->setMessage(trans('table.delete_success_message'));
    }

    /**
     * Show the form for editing the specified resource.
     * @param int $id
     * @return string | BaseHttpResponse
     */
    public function edit($id, Request $request, BaseHttpResponse $response)
    {
        if (!$request->user()->can('edit-role')) {
            return $this->response
                ->setError()
                ->setNextUrl(redirect()->back()->getTargetUrl())
                ->setMessage(trans('table.no_permission'));
        }
        page_title()->setTitle(trans('layout.role.edit'));
        $role = Role::with('permissions')->get()->find($id);
        $per_arr = Arr::pluck($role->permissions->toArray(), 'id');
        if (!$role) {
            return $response
                ->setPreviousUrl(route('roles.index'))
                ->setError()
                ->setMessage(trans('table.not_found'));
        }
        try {
            $permissions = Permission::get()->groupBy('group');
            return view('roles.edit', compact('role', 'permissions', 'per_arr'))->render();
        } catch (\Exception $exception) {
            return response()->json(['status' => '500', 'message' => $exception->getMessage()]);
        }

    }

    /**
     * Update the specified resource in storage.
     *
     * @param Request $request
     * @param   $id
     * @param BaseHttpResponse $response
     * @return BaseHttpResponse
     * @throws \Illuminate\Validation\ValidationException
     */
    public function update(Request $request, $id, BaseHttpResponse $response)
    {
        $this->validate($request, [
            'name' => 'required|unique:roles,name,' . $id,
            'description' => 'required',
        ]);
        $role = Role::get()->find($id);
        // Begin transaction
        DB::beginTransaction();
        try {
            if ($request->input('is_default')) {
                Role::where('default', 1)->update(['default' => 0]);
            }
            // Save role
            $role->update([
                'name' => $request->input('name'),
                'slug' => Str::slug($request->input('name')),
                'description' => $request->input('description'),
                'default' => $request->input('is_default') ?? 0,
                'user_id' => auth()->id()
            ]);
            event(new CreatedContentEvent($request->all(), $role, config('general.log.update')));
            $role->permissions()->sync($request->permissions);
            DB::commit();
            return $response
                ->setNextUrl(route('roles.index'))
                ->setMessage(trans('table.update_success_message'));
        } catch (Throwable $e) {
            DB::rollback();
            report($e);
            return false;
        }
    }
}
