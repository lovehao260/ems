<?php

namespace App\Http\Controllers;

use App\Events\CreatedContentEvent;
use App\Http\Responses\BaseHttpResponse;
use App\Models\Location;
use App\TableList\LocationTable;
use Illuminate\Http\Request;

class LocationController extends Controller
{

    protected $rules = [
        'name' => 'required|min:2|max:255|',
        'city' => 'min:2|max:255|nullable',
        'state' => 'min:2|max:10|nullable',
        'country' => 'min:2|max:255|nullable',
        'address' => 'max:80|nullable',
        'address2' => 'max:80|nullable',
        'zip' => 'min:3|max:10|nullable',
        'manager_id' => 'exists:users,id|nullable',
        'parent_id' => 'non_circular:locations,id'
    ];
    protected $response;
    public function __construct(BaseHttpResponse $response)
    {
        $this->response = $response;
        $this->middleware('auth');
    }

    /**
     * Display a listing of the resource.
     * @param LocationTable $dataTable
     * @param Request $request
     * @return \Illuminate\Support\Facades\View|BaseHttpResponse
     * @throws \Throwable
     */
    public function index(LocationTable $dataTable,Request $request)
    {
        if (!$request->user()->can('view-location')) {
            return $this->response
                ->setError()
                ->setNextUrl(redirect()->back()->getTargetUrl())
                ->setMessage(trans('table.no_permission'));
        }
        page_title()->setTitle(trans('layout.locations'));
        return $dataTable->renderTable();
    }

    /**
     * Show the form for creating a new resource.
     * @return \Illuminate\Contracts\View\View|BaseHttpResponse
     */
    public function create(Request $request)
    {
        if (!$request->user()->can('view-location')) {
            return $this->response
                ->setError()
                ->setNextUrl(redirect()->back()->getTargetUrl())
                ->setMessage(trans('table.no_permission'));
        }
        page_title()->setTitle(trans('layout.location.create'));
        $countries = config('general.country');
        return view('locations.add', compact('countries'));
    }

    /**
     * Store a newly created resource in storage.
     * @param \Illuminate\Http\Request $request
     * @return BaseHttpResponse
     */
    public function store(Request $request)
    {
        $request->validate($this->rules);
        try {
            $location = new Location();
            $location->create([
                'name' => $request->name,
                'parent_id' => $request->location_id ?? 0,
                'manager_id' => $request->manager_id ?? null,
                'user_id' => auth()->id(),
                'currency' => $request->currency,
                'address' => $request->address,
                'address2' => $request->address2,
                'state' => $request->state,
                'country' => $request->country,
                'zip' => $request->zip,
                'ldap_ou' => $request->ldap_ou,
                'image' => $request->image,
            ]);
            event(new CreatedContentEvent($request->all(), $location, config('general.log.create')));
            return $this->response
                ->setPreviousUrl(route('locations.index'))
                ->setMessage(trans('table.create_success_message'));
        } catch (Exception $exception) {
            return $this->response
                ->setError()
                ->setMessage($exception);
        }
    }

    /**
     * Display the specified resource.
     *
     * @param int $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     * @param int $id
     * @return BaseHttpResponse|\Illuminate\Contracts\View\View
     */
    public function edit($id,Request $request)
    {
        if (!$request->user()->can('edit-location')) {
            return $this->response
                ->setError()
                ->setNextUrl(redirect()->back()->getTargetUrl())
                ->setMessage(trans('table.no_permission'));
        }
        page_title()->setTitle(trans('layout.location.edit'));
        $countries = config('general.country');
        $location = Location::find($id);
        if (!$location) {
            return $this->response
                ->setPreviousUrl(route('locations.index'))
                ->setError()
                ->setMessage(trans('table.not_found'));
        }
        return view('locations.edit', compact('countries', 'location'));
    }

    /**
     * Update the specified resource in storage.
     * @param \Illuminate\Http\Request $request
     * @param int $id
     * @return \Illuminate\Http\RedirectResponse|BaseHttpResponse
     */
    public function update(Request $request, $id)
    {
        // Check if the location exists
        if (is_null($location = Location::find($id))) {
            return $this->response
                ->setError()
                ->setMessage(trans('table.not_found'));
        }
        $request->validate($this->rules);
        // Update the location data
        $location->name = $request->input('name');
        $location->parent_id = $request->input('parent_id', 0);
        $location->currency = $request->input('currency', '$');
        $location->address = $request->input('address');
        $location->address2 = $request->input('address2');
        $location->city = $request->input('city');
        $location->state = $request->input('state');
        $location->country = $request->input('country');
        $location->zip = $request->input('zip');
        $location->ldap_ou = $request->input('ldap_ou');
        $location->manager_id = $request->input('manager_id');
        $location->image = $request->input('image');
        if ($location->save()) {
            event(new CreatedContentEvent($request->all(), $location, config('general.log.update')));
            return $this->response
                ->setPreviousUrl(route('locations.index'))
                ->setMessage(trans('table.update_success_message'));
        }
    }

    /**
     * Remove the specified resource from storage.
     * @param int $id
     * @return BaseHttpResponse
     */
    public function destroy($id,Request $request)
    {
        try {
            if (!$request->user()->can('delete-location')) {
                return $this->response
                    ->setError()
                    ->setNextUrl(redirect()->back()->getTargetUrl())
                    ->setMessage(trans('table.no_permission'));
            }
            $location = Location::find($id);
            $location->delete();
            event(new CreatedContentEvent($id, $location, config('general.log.destroy')));
            return $this->response->setMessage('Location deleted success item');
        } catch (Exception $exception) {
            return $this->response
                ->setError()
                ->setMessage($exception->getMessage());
        }
    }
}
