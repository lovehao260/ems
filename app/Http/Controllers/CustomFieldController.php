<?php

namespace App\Http\Controllers;

use App\Events\CreatedContentEvent;
use App\Http\Responses\BaseHttpResponse;
use App\Models\CustomField;
use App\Models\FieldSet;
use App\Rules\CustomFormatRule;
use App\TableList\CustomFieldSetTable;
use App\TableList\CustomFieldTable;
use Illuminate\Http\JsonResponse;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\View;
use Illuminate\Validation\Rule;
use Throwable;

class CustomFieldController extends Controller
{
    protected $response;

    public function __construct(BaseHttpResponse $response)
    {
        $this->response = $response;
        $this->middleware('auth');
    }

    /**
     * @param Request $request
     * @param CustomFieldSetTable $customFieldSetTable
     * @param CustomFieldTable $customFieldTable
     * @return \Illuminate\Contracts\View\View|JsonResponse|View|BaseHttpResponse
     */
    public function index(Request $request,CustomFieldSetTable $customFieldSetTable, CustomFieldTable $customFieldTable)
    {
        if (!$request->user()->can('view-field')) {
            return $this->response
                ->setError()
                ->setNextUrl(redirect()->back()->getTargetUrl())
                ->setMessage(trans('table.no_permission'));
        }
        page_title()->setTitle(trans('layout.custom_field.manage'));
        $fieldSets = $customFieldSetTable
            ->setAjaxUrl(route('fields.fieldset'));

        $fields = $customFieldTable->setAjaxUrl(route('fields.field'));

        return view('custom_fields.index', compact('fieldSets', 'fields'));
    }

    /**
     * @param CustomFieldSetTable $customFieldSetTable
     * @return JsonResponse|View
     * @throws Throwable
     */
    public function getFieldSets(CustomFieldSetTable $customFieldSetTable)
    {
        return $customFieldSetTable->renderTable();
    }

    /**
     * @param CustomFieldTable $customFieldTable
     * @return JsonResponse|View
     * @throws Throwable
     */
    public function getFields(CustomFieldTable $customFieldTable)
    {
        return $customFieldTable->renderTable();
    }
    /**
     * @return \Illuminate\Contracts\View\View|BaseHttpResponse
     * @throws Throwable
     */
    public function createField(Request $request)
    {
        if (!$request->user()->can('create-field')) {
            return $this->response
                ->setError()
                ->setNextUrl(redirect()->back()->getTargetUrl())
                ->setMessage(trans('table.no_permission'));
        }
        page_title()->setTitle(trans('layout.custom_field.create'));
        $formats = config('general.formats');
        $predefined_formats = config('general.predefined_formats');
        return view('custom_fields.add-field', compact('formats', 'predefined_formats'));
    }

    /**
     * @param Request $request
     * @param BaseHttpResponse $response
     * @return BaseHttpResponse
     * @throws \Illuminate\Validation\ValidationException
     */
    public function storeField(Request $request, BaseHttpResponse $response)
    {
        $this->validate($request, [
            'name' => 'required|unique:custom_fields,name',
            "element" => [
                "required",
                Rule::in(['text', 'listbox', 'textarea', 'checkbox', 'radio'])
            ],
            'format' => [
                Rule::in(array_merge(array_keys(CustomField::PREDEFINED_FORMATS), CustomField::PREDEFINED_FORMATS))
            ],
            'regex_format' => [new CustomFormatRule()]
        ]);

        try {
            $field = new CustomField([
                "name" => $request->get("name"),
                "element" => $request->get("element"),
                "help_text" => $request->get("help_text"),
                "field_values" => $request->get("field_values"),
                "user_id" => Auth::id()
            ]);

            if ($request->filled("regex_format")) {
                $field->format = e($request->get("regex_format"));
            } else {
                $field->format = e($request->get("format"));
            }
            $field->save();
            event(new CreatedContentEvent($request->all(), $field, config('general.log.create')));
            return $response
                ->setPreviousUrl(route('fields.index'))
                ->setMessage(trans('table.create_success_message'));
        } catch (\Exception $exception) {
            return $response
                ->setError()
                ->setMessage($exception->getMessage());
        }

    }

    /**
     * @param int $id
     * @param Request $request
     * @param BaseHttpResponse $response
     * @return BaseHttpResponse|\Illuminate\Contracts\View\View
     */
    public function editField($id,Request $request,BaseHttpResponse $response)
    {
        if (!$request->user()->can('edit-field')) {
            return $this->response
                ->setError()
                ->setNextUrl(redirect()->back()->getTargetUrl())
                ->setMessage(trans('table.no_permission'));
        }
        $field = CustomField::find($id);
        $regex = '';
        if (strpos($field->format, 'regex:') !== false) {
            $regex = 'regex';
        }

        if (!$field) {
            return $response
                ->setError()
                ->setMessage(trans('table.not_found'));
        } else {
            page_title()->setTitle(trans('layout.custom_field.edit'));
            $formats = config('general.formats');
            $predefined_formats = config('general.predefined_formats');
            return view('custom_fields.edit-field', compact('formats', 'predefined_formats', 'field', 'regex'));
        }

    }

    /**
     * @param int $id
     * @param Request $request
     * @param BaseHttpResponse $response
     * @return BaseHttpResponse|\Illuminate\Contracts\View\View
     * @throws \Illuminate\Validation\ValidationException
     */
    public function updateField($id, Request $request, BaseHttpResponse $response)
    {
        $this->validate($request, [
            'name' => 'required|unique:custom_fields,name,' . $id,
            "element" => [
                "required",
                Rule::in(['text', 'listbox', 'textarea', 'checkbox', 'radio'])
            ],
            'format' => [
                Rule::in(array_merge(array_keys(CustomField::PREDEFINED_FORMATS), CustomField::PREDEFINED_FORMATS))
            ],
            'regex_format' => [new CustomFormatRule()]
        ]);

        try {
            $field = CustomField::find($id);
            $field->update([
                "name" => $request->get("name"),
                "element" => $request->get("element"),
                "help_text" => $request->get("help_text"),
                "field_values" => $request->get("field_values"),
                "user_id" => Auth::id()
            ]);
            if ($request->filled("regex_format")) {
                $field->format = e($request->get("regex_format"));
            } else {
                $field->format = e($request->get("format"));
            }
            $field->save();
            event(new CreatedContentEvent($request->all(), $field, config('general.log.update')));
            return $response
                ->setPreviousUrl(route('fields.index'))
                ->setMessage(trans('table.update_success_message'));
        } catch (\Exception $exception) {
            return $response
                ->setError()
                ->setMessage($exception->getMessage());
        }
    }

    /**
     * @param int $id
     * @param Request $request
     * @param BaseHttpResponse $response
     * @return BaseHttpResponse|\Illuminate\Contracts\View\View
     */
    public function destroyField($id,Request $request,BaseHttpResponse $response)
    {
        try {
            if (!$request->user()->can('delete-field')) {
                return $this->response
                    ->setError()
                    ->setNextUrl(redirect()->back()->getTargetUrl())
                    ->setMessage(trans('table.no_permission'));
            }
            $field = CustomField::find($id);
            $field->fieldset()->detach();
            $field->delete();
            event(new CreatedContentEvent($id, $field, config('general.log.destroy')));
            return $response
                ->setMessage(trans('table.delete_success_message'));
        } catch (Exception $exception) {
            return $response
                ->setError()
                ->setMessage($exception->getMessage());
        }
    }

    /**
     * @param Request $request
     * @param BaseHttpResponse $response
     * @return BaseHttpResponse|\Illuminate\Contracts\View\View
     * @throws \Illuminate\Validation\ValidationException
     */
    public function storeFieldSet(Request $request, BaseHttpResponse $response)
    {
        $this->validate($request, [
            'name' => 'required|unique:custom_fieldsets,name',
        ]);

        try {
            $fieldset = new FieldSet([
                "name" => $request->get("name"),
                "user_id" => \auth()->id(),
            ]);
            $fieldset->save();
            event(new CreatedContentEvent($request->all(), $fieldset, config('general.log.create')));
            return $response
                ->setMessage(trans('table.creat_success_message'));
        } catch (\Exception $exception) {
            return $response
                ->setError()
                ->setMessage('Errors exception');
        }

    }

    /**
     * @param $id
     * @param Request $request
     * @return \Illuminate\Contracts\View\View
     * @throws \Exception
     */
    public function showFieldSet($id, Request $request)
    {
        page_title()->setTitle(trans('layout.custom_fields'));
        $fieldSet = FieldSet::with('fields')
            ->where('id', $id)->orderBy('id', 'ASC')->first();
        if ($fieldSet) {
            if ($request->ajax()) {
                return datatables()->of($fieldSet->fields)
                    ->addColumn('handle', function ($item) {
                        return '<span class="handle">
                                <i class="fa fa-ellipsis-v"></i>
                                <i class="fa fa-ellipsis-v"></i>
                                </span>';
                    })
                    ->editColumn('order', function ($item) {
                        return $item->pivot->order;
                    })
                    ->editColumn('name', function ($item) {
                        return $item->name;
                    })
                    ->editColumn('required', function ($item) {
                        return $item->pivot->required == '1' ? 'Yes' : 'No';
                    })
                    ->editColumn('created_at', function ($item) {
                        return date_from_database($item->created_at);
                    })
                    ->addColumn('operations', function ($item) {
                        return '<a href="#" class="btn btn-icon btn-sm btn-danger deleteDialog" data-toggle="tooltip"
                               data-section="'.route('fields.fieldset.disassociate',['id'=>$item->pivot->custom_fieldset_id, 'field_id'=>$item->id]).'" role="button"
                                 title="delete">
                                <i class="fa fa-trash"></i>
                            </a>';

                    })
                    ->escapeColumns([])
                    ->make(true);
            }
            return view('custom_fields.detail', compact('fieldSet'));
        }

    }

    /**
     * @param $id
     * @param Request $request
     * @param BaseHttpResponse $response
     * @return BaseHttpResponse
     */
    public function destroyFieldSet($id,Request $request,BaseHttpResponse $response)
    {
        if (!$request->user()->can('delete-field')) {
            return $this->response
                ->setError()
                ->setNextUrl(redirect()->back()->getTargetUrl())
                ->setMessage(trans('table.no_permission'));
        }
        $fieldSet = FieldSet::find($id);
        $fieldSet->fields()->detach();
        $fieldSet->delete();
        event(new CreatedContentEvent($id, $fieldSet, config('general.log.destroy')));
        return $response
            ->setMessage(trans('table.delete_success_message'));
    }

    /**
     * @param $id
     * @param Request $request
     * @param BaseHttpResponse $response
     * @return BaseHttpResponse
     * @throws \Illuminate\Validation\ValidationException
     */
    public function storeCustomFieldSet($id, Request $request, BaseHttpResponse $response)
    {
        $this->validate($request, [
            'field_id' => 'required',
        ]);
        $set = Fieldset::find($id);
        if ($request->filled('field_id')) {
            foreach ($set->fields as $field) {
                if ($field->id == $request->input('field_id')) {
                    return $response
                        ->setError()
                        ->setMessage('Field already added');
                }
            }
            $set->fields()->attach($request->input('field_id'),
                ["required" => ($request->input('required') == "on" ? 1 : 0),
                    "order" => $request->input('order') ?? 1]);
            return $response
                ->setMessage(trans('table.field_to_fieldset'));
        }
        return $response
            ->setError()
            ->setMessage(trans('table.not_found'));
    }

    /**
     * @param Request $request
     * @param $id
     * @return BaseHttpResponse
     */
    public function postReorder(Request $request, $id)
    {
        $fieldset = Fieldset::find($id);
        $fields = array();
        $order_array = array();

        $items = $request->input('item');

        foreach ($items as $order => $field_id) {
            $order_array[$field_id] = $order;
        }

        foreach ($fieldset->fields as $field) {
            $fields[$field->id] = ['required' => $field->pivot->required, 'order' => $order_array[$field->id]];
        }

        return $fieldset->fields()->sync($fields);

    }

    /**
     * @param $id
     * @param $field_id
     * @param BaseHttpResponse $response
     * @return BaseHttpResponse
     */
    public function disassociate( $id, $field_id, BaseHttpResponse $response)
    {
        $field = CustomField::find($field_id);

        foreach ($field->fieldset as $fieldset) {
            if ($fieldset->id == $id) {
                $fieldset->fields()->detach($field->id);
                return $response->setMessage(trans('table.disassociate'));
            }
        }
        return $response->setMessage($field_id);
    }
}
