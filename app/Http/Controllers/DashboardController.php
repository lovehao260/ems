<?php

namespace App\Http\Controllers;

use App\Models\Accessory;
use App\Models\Asset;
use App\Models\License;
use App\Models\User;
use Illuminate\Support\Facades\DB;

class DashboardController extends Controller
{
    public function __construct()
    {
        $this->middleware('auth');
    }
    /**
     * @return \Illuminate\Contracts\View\View
     */
    public function index()
    {
        page_title()->setTitle(trans('layout.home'));
        $counts['asset'] = Asset::count();
        $counts['asset_checkout'] = Asset::checkoutcount();
        $counts['accessory'] = Accessory::count();
        $counts['accessory_total'] = Accessory::quantity();
        $counts['accessory_checkout'] = Accessory::checkoutcount();
        $counts['user'] = User::count();
        $counts['license'] = License::count();
        $counts['seats'] = License::assetcount();
        $counts['license_checkout'] = License::assetcountCheckout();

        $counts['grand_total'] =  $counts['asset'] +  $counts['accessory'] +  $counts['license'] +  $counts['user'];

        return view('dashboard.index',compact('counts'));
    }
}
