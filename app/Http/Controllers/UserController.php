<?php

namespace App\Http\Controllers;

use App\Events\CreatedContentEvent;
use App\Http\Responses\BaseHttpResponse;
use App\Models\AccessoryUser;
use App\Models\Asset;
use App\Models\Company;
use App\Models\LicenseSeat;
use App\Models\User;
use App\TableList\AssetTable;
use App\TableList\UserTable;
use Illuminate\Http\Request;
use Exception;
use Illuminate\Support\Arr;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\DB;

class UserController extends Controller
{
    protected $response;


    public function __construct(BaseHttpResponse $response)
    {
        $this->response = $response;
        $this->middleware('auth');
    }

    /**
     * @throws \Throwable
     */
    public function index(Request $request, UserTable $dataTable, ?string $trash = null)
    {
        if (!$request->user()->can('view-user')) {
            return $this->response
                ->setError()
                ->setNextUrl(redirect()->back()->getTargetUrl())
                ->setMessage(trans('table.no_permission'));
        }
        page_title()->setTitle(trans('layout.users'));
        return $dataTable->renderTable($trash);
    }

    /**
     * @return \Illuminate\Contracts\View\View|BaseHttpResponse
     */
    public function create(Request $request)
    {
        if (!$request->user()->can('create-user')) {
            return $this->response
                ->setError()
                ->setNextUrl(redirect()->back()->getTargetUrl())
                ->setMessage(trans('table.no_permission'));
        }
        page_title()->setTitle(trans('layout.user.create'));
        $companies = Company::all();
        $managers = User::all();
        return view('users.add', ['managers' => $managers, 'companies' => $companies]);
    }

    /**
     * @param Request $request
     * @return BaseHttpResponse
     * @throws \Illuminate\Validation\ValidationException
     */
    public function store(Request $request)
    {
        $this->validate($request, [
            'first_name' => 'required',
            'username' => 'required|unique:users,username',
            'email' => 'required|email|unique:users,email',
            'password' => 'required|min:6|confirmed',
            'password_confirmation' => 'required|min:6',
            'role_id' => 'required'
        ]);

        try {
            $user = User::create($request->input());
            $user->roles()->attach($request->role_id ?? $user->is_default());
            event(new CreatedContentEvent($request->all(), $user, config('general.log.create')));
            return $this->response
                ->setNextUrl(route('users.index'))
                ->setMessage(trans('table.create_success_message'));
        } catch (Exception $exception) {
            return $this->response
                ->setError()
                ->setMessage('Errors exception');
        }
    }

    /**
     * @return BaseHttpResponse|\Illuminate\Contracts\View\View
     */
    public function edit($id, Request $request)
    {
        if (!$request->user()->can('edit-user')) {
            return $this->response
                ->setError()
                ->setNextUrl(redirect()->back()->getTargetUrl())
                ->setMessage(trans('table.no_permission'));
        }
        page_title()->setTitle(trans('layout.user.edit'));
        $companies = Company::all();
        $managers = User::withTrashed()->where('id', '<>', $id)->get();
        $user = User::withTrashed()->with('roles')->find($id);
        if (!$user) {
            return $this->response
                ->setPreviousUrl(route('users.index'))
                ->setError()
                ->setMessage(trans('table.not_found'));
        }
        return view('users.edit', ['managers' => $managers, 'companies' => $companies, 'user' => $user,
            'type' => config('general.types_table.edit')]);

    }

    /**
     * @param $id
     * @param Request $request
     * @param BaseHttpResponse $response
     * @return BaseHttpResponse
     * @throws \Illuminate\Validation\ValidationException
     */
    public function update($id, Request $request, BaseHttpResponse $response)
    {
        $this->validate($request, [
            'first_name' => 'required',
            'username' => 'required|unique:users,username,' . $id,
            'email' => 'required|email|unique:users,email,' . $id,
            'role_id' => 'required',
        ]);
        try {
            $user = User::withTrashed()->find($id);
            DB::beginTransaction();
            $user->update($request->input());
            event(new CreatedContentEvent($request->all(), $user, config('general.log.update')));
            $user->roles()->sync($request->role_id ?? $user->is_default());
            // Update the location of any assets checked out to this user
            Asset::where('assigned_type', User::class)
                ->where('assigned_to', $user->id)
                ->update(['location_id' => $request->input('location_id', null)]);

            DB::commit();
            return $response
                ->setPreviousUrl($request->profile?route('profile.index'):route('users.index'))
                ->setMessage(trans('table.update_success_message'))->withInput();
        } catch (Exception $exception) {
            return $this->response
                ->setError()
                ->setMessage('Errors exception')->withInput();;
        }
    }

    /**
     * @param int $id
     * @return \Illuminate\Contracts\View\View
     */
    public function clone($id)
    {
        page_title()->setTitle(trans('layout.user.create'));
        $companies = Company::all();
        $managers = User::where('id', '<>', $id)->get();
        $user = User::withTrashed()->find($id);

        return view('users.add', ['managers' => $managers, 'companies' => $companies, 'user' => $user,
            'type' => config('general.types_table.clone')]);

    }

    /**
     * @param $id
     * @param Request $request
     * @param BaseHttpResponse $response
     * @return BaseHttpResponse
     * @throws \Illuminate\Validation\ValidationException
     */
    public function destroy($id, Request $request, BaseHttpResponse $response)
    {
        if (!$request->user()->can('delete-user')) {
            return $this->response
                ->setError()
                ->setNextUrl(redirect()->back()->getTargetUrl())
                ->setMessage(trans('table.no_permission'));
        }
        // Check user login now
        if ($request->user()->getKey() == $id) {
            return $response
                ->setError()
                ->setMessage(trans('table.delete_user_logged_in'));
        }
        try {
            $user = User::withTrashed()->find($id);
            $user->delete();
            event(new CreatedContentEvent($request->all(), $user, config('general.log.destroy')));
            return $response->setMessage(trans('table.delete_success_message'));
        } catch (Exception $exception) {
            return $response
                ->setError()
                ->setMessage('User could not be deleted');
        }
    }

    /**
     * @param Request $request
     * @param BaseHttpResponse $response
     * @return BaseHttpResponse
     */
    public function deletes(Request $request, BaseHttpResponse $response)
    {
        if (!$request->user()->can('delete-user')) {
            return $this->response
                ->setError()
                ->setNextUrl(redirect()->back()->getTargetUrl())
                ->setMessage(trans('table.no_permission'));
        }
        $ids = $request->input('ids');
        if (empty($ids)) {
            return $response
                ->setError()
                ->setMessage(trans('notices.no_select'));
        }

        foreach ($ids as $id) {
            if ($request->user()->getKey() == $id) {
                return $response
                    ->setError()
                    ->setMessage(trans('table.delete_user_logged_in'));
            }
            try {
                $user = User::find($id);
                if (count($user->accessories) == 0 && count($user->licenses) == 0) {
                    User::where('id', $id)->delete();
                    event(new CreatedContentEvent($request->all(), $user, config('general.log.destroy')));
                }
                return $response->setMessage(trans('table.delete_success_message'));
            } catch (Exception $exception) {
                return $response
                    ->setError()
                    ->setMessage($exception->getMessage());
            }
        }
        return null;
    }

    /**
     * @param $id
     * @param BaseHttpResponse $response
     * @return BaseHttpResponse
     */
    public function deleted($id, Request $request, BaseHttpResponse $response)
    {
        try {
            if (!$request->user()->can('delete-user')) {
                return $this->response
                    ->setError()
                    ->setNextUrl(redirect()->back()->getTargetUrl())
                    ->setMessage(trans('table.no_permission'));
            }
            User::withTrashed()->where('id', $id)->forceDelete();
            return $response->setMessage(trans('table.delete_success_message'));
        } catch (Exception $exception) {
            return $response
                ->setError()
                ->setMessage($exception->getMessage());
        }
    }

    /**
     * @param $id
     * @param BaseHttpResponse $response
     * @return BaseHttpResponse
     */
    public function restore($id, BaseHttpResponse $response)
    {
        try {
            $user = User::withTrashed()->find($id);
            $user->restore();
            event(new CreatedContentEvent($id, $user, config('general.log.restore')));
            return $response
                ->setPreviousUrl(route('users.index'))
                ->setMessage(trans('table.user_restore'));
        } catch (Exception $exception) {
            return $response
                ->setError()
                ->setMessage($exception->getMessage());
        }
    }

    /**
     * @param BaseHttpResponse $response
     * @param Request $request
     * @return BaseHttpResponse
     */
    public function emptyAllTrash(BaseHttpResponse $response, Request $request)
    {
        try {
            if (!$request->user()->can('delete-user')) {
                return $this->response
                    ->setError()
                    ->setNextUrl(redirect()->back()->getTargetUrl())
                    ->setMessage(trans('table.no_permission'));
            }
            User::onlyTrashed()->forceDelete();
            return $response->setMessage(trans('table.success_empty__msg'));

        } catch (Exception $exception) {
            return $response
                ->setError()
                ->setMessage($exception->getMessage());
        }
    }

    /**
     * @param $id
     * @param Request $request
     * @return BaseHttpResponse
     * @throws Exception
     */
    public function show($id, Request $request)
    {
        page_title()->setTitle(trans('layout.user.detail'));
        $user = User::with('assets', 'assets.model', 'accessories', 'licenses', 'userloc')->withTrashed()->find($id);
        if (!$user) {
            return $this->response
                ->setPreviousUrl(route('users.index'))
                ->setError()
                ->setMessage(trans('table.not_found'));
        }
        if ($request->ajax()) {

            return datatables()->of(LicenseSeat::where('assigned_to', $id)->with('license')->get())
                ->addColumn('name', function ($item) {
                    return link_table(route('licenses.show', $item->license['id']), $item->license['name']);
                })
                ->addColumn('serial', function ($item) {
                    return $item->license['serial'] ? link_table(route('licenses.show', $item->license['id']), $item->license['serial']) : '';
                })
                ->addColumn('purchase_cost', function ($item) {
                    return $item->license['purchase_cost'];
                })
                ->addColumn('purchase_order', function ($item) {
                    return $item->license['purchase_order'];
                })
                ->addColumn('order_number', function ($item) {
                    return $item->license['order_number'];
                })
                ->addColumn('checkout', function ($item) use ($id) {
                    return table_checkout('table-seats', $id, $avail = 1, $item['id'], 'checkin');
                })
                ->addIndexColumn()
                ->rawColumns(['checkout', 'name', 'serial', 'purchase_cost'])
                ->make(true);
        }
        return view('users.show', compact('user'));
    }

    /**
     * @param $id
     * @param Request $request
     * @return BaseHttpResponse
     * @throws Exception
     */
    public function accessory($id, Request $request)
    {
        if ($request->ajax()) {

            return datatables()->of(AccessoryUser::where('assigned_to', $id)->with('accessory')->get())
                ->addColumn('name', function ($item) {
                    return link_table(route('accessories.show', $item->accessory['id']), $item->accessory['name']);
                })
                ->addColumn('purchase_cost', function ($item) {
                    return $item->accessory['purchase_cost'];
                })
                ->addColumn('checkout', function ($item) {

                    return table_accessory_checkout('accessories', 'accessories.checkin', $item->id, 0, 'checkin');
                })
                ->addIndexColumn()
                ->rawColumns(['checkout', 'name', 'purchase_cost'])
                ->make(true);
        }
    }

    /**
     * @param $id
     * @param Request $request
     * @return BaseHttpResponse
     * @throws Exception
     */
    public function asset($id, Request $request)
    {
        $user = User::with('assets')->withTrashed()->find($id);
        if ($request->ajax()) {
            return datatables()->of($user->assets)
                ->addColumn('name', function ($item) {
                    return link_table(route('assets.show', $item->id), $item->name);
                })
                ->addColumn('purchase_cost', function ($item) {
                    return $item->purchase_cost;
                })
                ->editColumn('checkout', function ($item) {
                    $checkout = false;
                    $item->assigned_to ?? $checkout = true;
                    return table_asset_checkout($item->id, $checkout, $item->assetstatus['deployable']);
                })
                ->addIndexColumn()
                ->rawColumns(['checkout', 'name', 'purchase_cost'])
                ->make(true);
        }
    }
}
