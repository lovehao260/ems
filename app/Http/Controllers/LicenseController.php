<?php

namespace App\Http\Controllers;

use App\Events\CreatedContentEvent;
use App\Http\Responses\BaseHttpResponse;
use App\Models\Company;
use App\Models\License;
use App\Models\LicenseSeat;
use App\Models\User;
use App\Notifications\CalcNotification;
use App\TableList\LicenceTable;
use Illuminate\Http\Request;
use Exception;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Validator;
use Illuminate\Support\Str;


class LicenseController extends Controller
{
    protected $response;
    protected $rules=[
        'name' => 'required',
        'category_id' => 'required',
        'seats' => 'required|numeric|min:0|not_in:0',
        'license_email'   => 'email|nullable|max:120',
        'license_name'   => 'string|nullable|max:100',
        'purchase_date'  => 'date|nullable',
        'purchase_cost'  => 'numeric|nullable',
        'expiration_date'  => 'date|nullable',
        'termination_date'  => 'date|nullable',
    ];
    public function __construct(BaseHttpResponse $response)
    {
        $this->response = $response;
        $this->middleware('auth');
    }

    /**
     * @throws \Throwable
     */
    public function index(Request $request,LicenceTable $dataTable, ?string $trash = null)
    {
        if (!$request->user()->can('view-license')) {
            return $this->response
                ->setError()
                ->setNextUrl(redirect()->back()->getTargetUrl())
                ->setMessage(trans('table.no_permission'));
        }
        page_title()->setTitle(trans('layout.licenses'));
        return $dataTable->renderTable();
    }
    /**
     * @return \Illuminate\Contracts\View\View|BaseHttpResponse
     * @throws \Throwable
     */
    public function create(Request $request)
    {
        if (!$request->user()->can('create-license')) {
            return $this->response
                ->setError()
                ->setNextUrl(redirect()->back()->getTargetUrl())
                ->setMessage(trans('table.no_permission'));
        }
        page_title()->setTitle(trans('layout.license.create'));
        $companies = Company::all();
        $managers = User::all();
        return view('licenses.add', ['managers' => $managers, 'companies' => $companies]);
    }
    /**
     * @param Request $request
     * @return BaseHttpResponse
     * @throws \Throwable
     */
    public function store(Request $request)
    {
        $this->validate($request, $this->rules);
        unset($request['_token']);
        $licence = new License();
        $licence->create($request->input());
        event(new CreatedContentEvent($request->all(), $licence, config('general.log.creat')));
        try {
            return $this->response
                ->setPreviousUrl(route('licenses.index'))
                ->setMessage(trans('table.create_success_message'));
        } catch (Exception $exception) {
            return $this->response
                ->setError()
                ->setMessage($exception);
        }
    }
    /**
     * @return BaseHttpResponse|\Illuminate\Contracts\View\View
     * @throws \Throwable
     */
    public function edit($id,Request $request)
    {
        if (!$request->user()->can('edit-license')) {
            return $this->response
                ->setError()
                ->setNextUrl(redirect()->back()->getTargetUrl())
                ->setMessage(trans('table.no_permission'));
        }
        page_title()->setTitle(trans('layout.license.edit'));
        $license = License::withTrashed()->find($id);
        if (!$license){
            return $this->response
                ->setPreviousUrl(route('licenses.index'))
                ->setError()
                ->setMessage(trans('table.not_found'));
        }
        return view('licenses.edit', ['license' => $license]);
    }

    /**
     * @param $id
     * @param Request $request
     * @param BaseHttpResponse $response
     * @return BaseHttpResponse
     * @throws \Illuminate\Validation\ValidationException
     */
    public function update($id, Request $request, BaseHttpResponse $response)
    {
        $this->validate($request,$this->rules);

        try {
            $licence = License::find($id);
            $licence->update([
                'name' => $request->name,
                'license_name' => $request->license_name,
                'license_email' => $request->license_email,
                'serial' => $request->serial,
                'purchase_date' => $request->purchase_date,
                'purchase_cost' => $request->purchase_cost,
                'purchase_order' => $request->purchase_order,
                'order_number' => $request->order_number,
                'seats' => $request->seats,
                'user_id' => auth()->id(),
                'company_id' => $request->company_id,
                'category_id' => $request->category_id,
                'termination_date' => $request->termination_date,
//            'depreciate' => $request->depreciate,
                'maintained' => $request->maintained,
                'reassignable' => $request->reassignable ?? 0,
                'expiration_date' => $request->expiration_date,
                'notes' => $request->notes,
            ]);
            event(new CreatedContentEvent($request->all(), $licence, config('general.log.update')));
            return $response
                ->setPreviousUrl(route('licenses.index'))
                ->setMessage(trans('table.update_success_message'))->withInput();;
        } catch (Exception $exception) {
            return $this->response
                ->setError()
                ->setMessage($exception);
        }
    }

    /**
     * @param $id
     * @param Request $request
     * @param BaseHttpResponse $response
     * @return BaseHttpResponse
     */
    public function destroy($id, Request $request, BaseHttpResponse $response)
    {
        try {
            if (!$request->user()->can('destroy-license')) {
                return $this->response
                    ->setError()
                    ->setNextUrl(redirect()->back()->getTargetUrl())
                    ->setMessage(trans('table.no_permission'));
            }
            $licence = License::withTrashed()->find($id);
            $licence->delete();
            LicenseSeat::withTrashed()->where('license_id',$id)->delete();
            event(new CreatedContentEvent($request->all(), $licence, config('general.log.destroy')));
            return $response->setMessage('table.delete_success_message');
        } catch (Exception $exception) {
            return $response
                ->setError()
                ->setMessage($exception);
        }
    }

    /**
     * @param $licenseId
     * @param string|null $seatId
     * @param Request $request
     * @param BaseHttpResponse $response
     * @return BaseHttpResponse
     */
    public function checkout($licenseId, ?string $seatId = null, Request $request, BaseHttpResponse $response)
    {
        if (!License::find($licenseId)) {
            return $response
                ->setError()
                ->setMessage(trans('table.not_found'));
        }
        if ($seatId) {
            $licenseSeat = LicenseSeat::where('id', $seatId)->where('assigned_to', null)->first();
        } else {
            $licenseSeat = LicenseSeat::where('license_id', $licenseId)->where('assigned_to', null)->first();
        }
        if (!$licenseSeat) {
            return $response
                ->setError()
                ->setMessage(trans('table.seat_available'));
        }
        if (!$request->assigned_to && !$request->asset_id) {
            return $response
                ->setError()
                ->setMessage(trans('table.select_user_checkout'));
        }

        try {
            $licenseSeat->update(
                [
                    'assigned_to' => $request->assigned_to ?? null,
                    'asset_id' => $request->asset_id ?? null,
                    'user_id' => Auth::id(),
                    'notes' => $request->note]);

            event(new CreatedContentEvent($request->all(), $licenseSeat, config('general.log.checkout')));
            if ($request->assigned_to) {
                $message = trans('table.license_by') . Auth::user()->first_name;
                $notification = [
                    'license_id' => $licenseId,
                    'assigned_to' => $request->assigned_to,
                    'message' => $message,
                    'creat_by' => auth()->id(),
                ];
                User::find($request->assigned_to)->notify(new CalcNotification($notification));
            }
            return $response->setMessage(trans('table.checkout_success_message'));
        } catch (Exception $exception) {
            return $response
                ->setError()
                ->setMessage('Exception errors');
        }
    }
    /**
     * @return BaseHttpResponse|\Illuminate\Contracts\View\View
     * @throws \Throwable
     */
    public function show($id,Request $request)
    {

        page_title()->setTitle(trans('layout.license.show'));
        $license = License::with('category', 'company')->find($id);
        $detail_value=setting(License::LICENSE)?? json_encode(array_values(config('general.license_detail')));
        $table_detail= json_decode($detail_value);
        $table_keys=array_keys(config('general.license_detail'));
        if (request()->ajax()) {
            return datatables()->of(LicenseSeat::where('license_id', $id)->with('user')->get())
                ->addColumn('seat', function ($item) {
                    return 'seat';
                })
                ->editColumn('user', function ($item) {
                    return $item->user ? $item->user['first_name'] . ' ' . $item->user['last_name'] : '';
                })
                ->editColumn('asset', function ($item) {
                    return $item->asset ? $item->asset['name']: '';
                })
                ->addColumn('checkout', function ($item) use ($id) {
                    return table_checkout('table-seats', $id, $avail = 1, $item['id'], $item['user_id'] ? 'checkin' : "");
                })
                ->addColumn('license', function ($item) use ($license) {
                    return $license->name;
                })
                ->addColumn('serial', function ($item) use ($license) {
                    return $license->serial;
                })
                ->addIndexColumn()
                ->rawColumns(['seat', 'user', 'asset', 'checkout', 'serial', 'name'])
                ->make(true);
        }
        return view('licenses.show', compact('license','table_detail','table_keys'));
    }

    /**
     * @param $licenseId
     * @param $seat_id
     * @param Request $request
     * @param BaseHttpResponse $response
     * @return BaseHttpResponse
     */
    public function checkin($licenseId, $seat_id, Request $request, BaseHttpResponse $response)
    {
        // Check if the asset exists
        if (is_null($licenseSeat = LicenseSeat::find($seat_id))) {
            // Redirect to the asset management page with error
            return $this->response
                ->setError()
                ->setNextUrl(route('licenses.index'))
                ->setMessage(trans('table.not_found'));
        }

        // Declare the rules for the form validation
        $rules = [
            'note' => 'string|nullable',
        ];

        // Create a new validator instance from our validation rules
        $validator = Validator::make($request->all(), $rules);
        // If validation fails, we'll exit the operation now.
        if ($validator->fails()) {
            // Ooops.. something went wrong
            return $response
                ->setError()
                ->setMessage(trans('table.not_found_seat'));
        }
        $message = trans('table.license_by') . Auth::user()->first_name;
        if ( $licenseSeat->assigned_to){
            $notification = [
                'seat_id' => $seat_id,
                'assigned_to' => $licenseSeat->assigned_to,
                'message' => $message,
                'creat_by' => auth()->id(),
            ];
        }
        // Update the asset data
        $licenseSeat->assigned_to = null;
        $licenseSeat->asset_id = null;
        $licenseSeat->user_id = null;
        // Was the asset updated?
        if ($licenseSeat->save()) {
            event(new CreatedContentEvent($request->all(), $licenseSeat, config('general.log.checkin')));
            if (isset($notification['assigned_to'])) {
                User::find($notification['assigned_to'])->notify(new CalcNotification($notification));
            }
            return $response->setMessage(trans('table.checkin_success_message'));
        }
    }

    /**
     * @param int $id
     * @return BaseHttpResponse
     * @throws \Exception
     */
    public function qrCodeDownload  ($id){
        $license=License::find($id);

        if (!$license){
            return $this->response
                ->setError()
                ->setNextUrl(route('licenses.index'))
                ->setMessage(trans('table.not_found'));
        }
        $image   = \QrCode::size(250)->generate(route('licenses.show',$license->id));
        $svgTemplate = new \SimpleXMLElement($image);
        $svgTemplate->registerXPathNamespace('svg', 'http://www.w3.org/2000/svg');
        $svgTemplate->rect->addAttribute('fill-opacity', 0);
        $image = $svgTemplate->asXML();
        $imageName = 'qr-code-license-'.Str::slug($license->name.'-'.$license->serial);

        \Storage::disk('public')->put($imageName, $image);

        return response()->download('storage/'.$imageName, $imageName.'.svg');
    }
}
