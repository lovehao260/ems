<?php

namespace App\Http\Controllers;

use App\Events\CreatedContentEvent;
use App\Http\Resources\FileResource;
use App\Http\Resources\FolderResource;
use App\Jobs\UploadImage;
use App\Models\MediaFile;
use App\Models\MediaFolder;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\File;
use Illuminate\Support\Facades\Storage;
use Illuminate\Support\Str;

use ZipArchive;


class MediaController extends Controller
{
    public function __construct()
    {
        $this->middleware('auth');
    }
    /**
     * @return \Illuminate\Contracts\View\View
     */
    public function index()
    {
        page_title()->setTitle(trans('layout.medias'));
        return view('medias.index');
    }
    /**
     * @param int $id
     * @param Request $request
     * @return array
     */
    public function getData($id, Request $request)
    {
        if($request->loadType==='all'){
            $query_folders = MediaFolder::where('parent_id', $id);
            $query_files = MediaFile::where('folder_id', $id);
        }

        if($request->loadType==='trash'){
            $query_folders = MediaFolder::onlyTrashed();
            $query_files = MediaFile::onlyTrashed();
        }

        if ($request->search) {
            $query_folders = $query_folders->where('name', 'LIKE', '%' . $request->search . '%');
            $query_files = $query_files->where('name', 'LIKE', '%' . $request->search . '%');
        }
        $folders = FolderResource::collection($query_folders->get());
        $files = FileResource::collection($query_files->get());
        return [
            'folders' => $folders,
            'files' => $files,
        ];
    }

    /**
     * @param int $id
     * @param Request $request
     * @return string
     * @throws \Illuminate\Validation\ValidationException
     */
    public function uploadFile($id, Request $request)
    {

        $this->validate($request, [
            'file' => 'required|mimes:jpg,jpeg,png,gif,txt,docx,zip,mp3,bmp,csv,xls,xlsx,ppt,pptx,pdf,mp4,doc,wav|max:10000'
        ]);
        if ($request->hasFile('file')) {
            // upload file
            $base_url = storage_path('app/public/');

            $file = $request->file('file');
            $file_name_full = $request->file('file')->getClientOriginalName();
            $filename = pathinfo($file_name_full, PATHINFO_FILENAME);
            $extension = pathinfo($file_name_full, PATHINFO_EXTENSION);
            if ($id == 0) {
                $file_path = $filename . '-' . time() . '.' . $extension;
                $folder='';
            } else {
                $folder = MediaFolder::find($id);
                $folder=$folder->slug.'/';
                $file_path = $filename . '-' . time() . '.' . $extension;
            }

            $request->file('file')->storeAs( $folder,$file_path);
            $file_new=new MediaFile();
            $file_new->create([
                'name' => $filename,
                'folder_id' => $id,
                'mime_type' => $extension,
                'size' => $file->getSize(),
                'url' => $folder.$file_path,
                'user_id' => 1,
            ]);
            event(new CreatedContentEvent($request->all(), $file_new, config('general.log.creat')));
        }
        return $file_path;
    }

    /**
     * @param Request $request
     * @return \Illuminate\Http\JsonResponse
     * @throws \Illuminate\Validation\ValidationException
     */
    public function storeFolder(Request $request)
    {
        $this->validate($request, [
            'name' => 'required|unique:media_folders'
        ]);
        $folder =new MediaFolder;
        $folder->create([
            'name' => $request->name,
            'slug' => Str::slug($request->name),
            'parent_id' => $request->folder_id,
            'user_id' => 1,
        ]);
        event(new CreatedContentEvent($request->all(), $folder, config('general.log.creat')));
        if ( $request->folder_id !=0){
            $folder_parent=MediaFolder::find($request->folder_id);
            $folder_path=$folder_parent->slug.'/'. $folder->slug;
        }else{
            $folder_path= $folder->slug;
        }
        if(! Storage::exists( $folder_path)){
            Storage::makeDirectory($folder_path);
        }
        return response()->json([
            "messages" =>trans('media.store')
        ], 200);
    }
    /**
     * @param Request $request
     * @return \Illuminate\Http\JsonResponse
     * @throws \Illuminate\Validation\ValidationException
     */
    public function renameMedia(Request $request)
    {
        $this->validate($request, [
            'item_data' => 'required',
        ]);
        foreach ($request->item_data as $item){
            if (isset($item['parent_id'])) {
                $folder = MediaFolder::withTrashed()->find($item['id']);
                $folder->name = $item['name'];
                $folder->save();
                event(new CreatedContentEvent($request->all(), $folder, config('general.log.update')));
            } else {
                $file = MediaFile::withTrashed()->find($item['id']);
                $file->name = $item['name'];
                $file->save();
                event(new CreatedContentEvent($request->all(), $file, config('general.log.update')));
            }
        }

        return response()->json([
            "messages" => $request->item_data
        ], 200);
    }
    /**
     * @param Request $request
     * @return \Illuminate\Http\JsonResponse
     * @throws \Illuminate\Validation\ValidationException
     */
    public function moveTrashMedia(Request $request)
    {
        foreach ($request->item_data as $item){
            if (isset($item['parent_id'])) {
                $folder = MediaFolder::find($item['id']);
                $folder->delete();
            } else {
                $file = MediaFile::find($item['id']);
                $file->delete();
            }
        }

        return response()->json([
            "messages" => trans('media.move')
        ], 200);
    }
    /**
     * @param int $id
     * @return \Symfony\Component\HttpFoundation\BinaryFileResponse|\Illuminate\Http\JsonResponse
     */
    public function downloadMedia( $id)
    {
        $folder = MediaFolder::find($id);
        $zip = new ZipArchive;
        $fileName = $folder->slug.'.zip';
        if ($zip->open(public_path($fileName), ZipArchive::CREATE) === TRUE) {
            $files = File::files(storage_path('app/public/') . $folder->slug);
            if (!$files){
                return response()->json([
                    "messages" => trans('media.exists')
                ], 404);
            }
            foreach ($files as $key => $value) {
                $relativeNameInZipFile = basename($value);
                $zip->addFile($value, $relativeNameInZipFile);
            }
            $zip->close();
        }
        return response()->download(public_path($fileName));

    }

    /**
     * @param Request $request
     * @return \Illuminate\Http\JsonResponse
     */
    public function deleteMedia(Request $request)
    {
        foreach ($request->item_data as $item){
            if (isset($item['parent_id'])) {
                $folder= MediaFolder::withTrashed()->where('id',  $item['id'])->first();
                if( Storage::exists( $folder->slug)){
                    Storage::deleteDirectory($folder->slug);
                }
                MediaFolder::onlyTrashed()->find($item['id'])->forceDelete();
                MediaFolder::withTrashed()->where('parent_id',  $item['id'])->forceDelete();
                MediaFile::withTrashed()->where('folder_id', $item['id'])->forceDelete();
            } else {
                $file= MediaFile::withTrashed()->where('id',$item['id'])->first();
                if( Storage::exists( $file->url)){
                    Storage::disk($file->type_s3)->delete($file->url);
                }
                MediaFile::withTrashed()->where('id', $item['id'])->forceDelete();
            }
        }

        return response()->json([
            "messages" => $request->media_type
        ], 200);
    }
    /**
     * @param Request $request
     * @return \Illuminate\Http\JsonResponse
     */
    public function restoreMedia(Request $request){
        foreach ($request->item_data as $item){
            if (isset($item['parent_id'])) {
                MediaFolder::withTrashed()->find($item['id'])->restore();
            } else {
                MediaFile::withTrashed()->find($item['id'])->restore();
            }
        }

        return response()->json([
            "messages" => trans('media.move')
        ], 200);
    }

}
