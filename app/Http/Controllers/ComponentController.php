<?php

namespace App\Http\Controllers;

use App\Events\CreatedContentEvent;
use App\Http\Responses\BaseHttpResponse;
use App\Models\Asset;
use App\Models\Category;
use App\Models\Component;
use App\Models\ComponentAsset;
use App\TableList\ComponentTable;
use http\Exception\BadConversionException;
use Illuminate\Http\Request;
use Exception;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Str;

class ComponentController extends Controller
{
    protected $response;
    protected $rules = [
        'name' => 'required',
        'qty'            => 'required|integer|min:1',
        'category_id'    => 'required|integer|exists:categories,id',
        'min_amt'        => 'integer|min:0|nullable',
        'purchase_date'  => 'date|nullable',
        'purchase_cost'  => 'numeric|nullable',
    ];
    public function __construct(BaseHttpResponse $response)
    {
        $this->response = $response;
        $this->middleware('auth');
    }

    /**
     * @throws \Throwable
     */
    public function index(Request $request,ComponentTable $dataTable, ?string $trash = null)
    {
        if (!$request->user()->can('view-component')) {
            return $this->response
                ->setError()
                ->setNextUrl(redirect()->back()->getTargetUrl())
                ->setMessage(trans('table.no_permission'));
        }
        page_title()->setTitle(trans('layout.components'));
        return $dataTable->renderTable($trash);
    }
    /**
     * @return \Illuminate\Contracts\View\View|BaseHttpResponse
     */
    public function create(Request $request)
    {
        if (!$request->user()->can('create-component')) {
            return $this->response
                ->setError()
                ->setNextUrl(redirect()->back()->getTargetUrl())
                ->setMessage(trans('table.no_permission'));
        }
        page_title()->setTitle(trans('layout.component.create'));
        $categories = Category::where('category_type', 'components')->get();
        return view('components.add', ['categories' => $categories]);
    }

    /**
     * @param Request $request
     * @return BaseHttpResponse
     * @throws \Illuminate\Validation\ValidationException
     */
    public function store(Request $request)
    {
        $this->validate($request, $this->rules);
        try {
            $component =new Component();
            $component->create([
                'name' => $request->name,
                'image' => $request->image,
                'user_id' => auth()->id(),
                'company_id' => $request->company_id,
                'category_id' => $request->category_id,
                'qty' => $request->qty ?? 1,
                'serial' => $request->serial,
                'min_amt' => $request->min_amt ?? 0,
                'order_number' => $request->order_number ?? 0,
                'purchase_date' => $request->purchase_date,
                'purchase_cost' => $request->purchase_cost ?? 0
            ]);
            event(new CreatedContentEvent($request->all(), $component, config('general.log.create')));
            return $this->response
                ->setPreviousUrl(route('components.index'))
                ->setMessage(trans('table.create_success_message'));
        } catch (Exception $exception) {
            return $this->response
                ->setError()
                ->setMessage($exception->getMessage());
        }
    }
    /**
     * @param int $id
     * @return \Illuminate\Contracts\View\View|BaseHttpResponse
     */
    public function edit($id,Request $request)
    {
        if (!$request->user()->can('edit-component')) {
            return $this->response
                ->setError()
                ->setNextUrl(redirect()->back()->getTargetUrl())
                ->setMessage(trans('table.no_permission'));
        }
        page_title()->setTitle(trans('layout.component.edit'));
        $categories = Category::where('category_type', 'components')->get();
        $component = Component::find($id);
        if (!$component){
            return $this->response
                ->setPreviousUrl(route('components.index'))
                ->setError()
                ->setMessage(trans('table.not_found'));
        }
        return view('components.edit', ['component' => $component, 'categories' => $categories]);
    }

    /**
     * @param int $id
     * @param Request $request
     * @param BaseHttpResponse $response
     * @return BaseHttpResponse
     * @throws \Illuminate\Validation\ValidationException
     */
    public function update($id, Request $request, BaseHttpResponse $response)
    {

        $this->validate($request, [
            'name' => 'required',
            'qty'            => 'required|integer|min:1',
            'category_id'    => 'required|integer|exists:categories,id',
            'min_amt'        => 'integer|min:0|nullable',
            'purchase_date'  => 'date|nullable',
            'purchase_cost'  => 'numeric|nullable',
        ]);

        try {
            $component= Component::find($id);
                $component->update([
                'name' => $request->name,
                'image' => $request->image,
                'user_id' => auth()->id(),
                'company_id' => $request->company_id,
                'category_id' => $request->category_id,
                'location_id' => $request->location_id,
                'qty' => $request->qty ?? 1,
                'serial' => $request->serial,
                'min_amt' => $request->min_amt ?? 0,
                'order_number' => $request->order_number ?? 0,
                'purchase_date' => $request->purchase_date,
                'purchase_cost' => $request->purchase_cost ?? 0
            ]);
            event(new CreatedContentEvent($request->all(), $component, config('general.log.update')));
            return $response
                ->setPreviousUrl(route('components.index'))
                ->setMessage(trans('table.update_success_message'))->withInput();
        } catch (Exception $exception) {
            return $this->response
                ->setError()
                ->setMessage($exception->getMessage());
        }
    }
    /**
     * @param int $id
     * @param Request $request
     * @param BaseHttpResponse $response
     * @return BaseHttpResponse
     */

    public function destroy($id, Request $request, BaseHttpResponse $response)
    {
        try {
            if (!$request->user()->can('delete-component')) {
                return $this->response
                    ->setError()
                    ->setNextUrl(redirect()->back()->getTargetUrl())
                    ->setMessage(trans('table.no_permission'));
            }
            $component = Component::find($id);
            $component->delete();
            ComponentAsset::where('component_id',$id)->delete();
            event(new CreatedContentEvent($request->all(), $component, config('general.log.destroy')));
            return $response->setMessage(trans('table.update_success_message'));
        } catch (Exception $exception) {
            return $response
                ->setError()
                ->setMessage($exception->getMessage());
        }
    }

    /**
     * @param int $id
     * @param Request $request
     * @param BaseHttpResponse $response
     * @return BaseHttpResponse
     * @throws \Illuminate\Validation\ValidationException
     */
    public function checkout($id, Request $request, BaseHttpResponse $response)
    {
        // Check if the component exists
        if (is_null($component = Component::find($id))) {
            // Redirect to the component management page with error
            return $response
                ->setError()
                ->setMessage(trans('table.not_found'));
        }
        $max_to_checkout = $component->numRemaining();
        $this->validate($request, [
            "asset_id" => "required",
            "qty" => "required|numeric|between:1,$max_to_checkout"
        ]);

        try {
            $admin_user = Auth::user();
            $asset_id = e($request->input('asset_id'));

            // Check if the user exists
            if (is_null($asset = Asset::find($asset_id))) {
                // Redirect to the component management page with error
                return $response
                    ->setError()
                    ->setMessage(trans('table.not_found'));
            }

            // Update the component data
            $component->asset_id = $asset_id;

            $component->assets()->attach($component->id, [
                'component_id' => $component->id,
                'user_id' => $admin_user->id,
                'created_at' => date('Y-m-d H:i:s'),
                'assigned_qty' => $request->input('qty'),
                'asset_id' => $asset_id
            ]);
            event(new CreatedContentEvent($request->all(), $component, config('general.log.checkout')));
            return $response->setMessage(trans('table.checkout_success_message'));
        } catch (Exception $exception) {
            return $response
                ->setError()
                ->setMessage($response->getMessage());
        }
    }
    /**
     * @param int $id
     * @return BaseHttpResponse|\Illuminate\Contracts\View\View
     * @throws \Illuminate\Validation\ValidationException|Exception
     */
    public function show ($id){
        $component=Component::find($id);
        page_title()->setTitle(trans('layout.component.create'));
        $detail_value=setting(Component::COMPONENT)?? json_encode(array_values(config('general.component_detail')));
        $table_detail= json_decode($detail_value);
        $table_keys=array_keys(config('general.component_detail'));
        if (request()->ajax()) {
            return datatables()->of(ComponentAsset::where('component_id',$id)->with('assets:id,name,asset_tag')->get())

                ->editColumn('asset', function ($item) {
                    return link_table(route('assets.show',$item->asset_id), $item->assets['name'].' ('. $item->assets['asset_tag'].')' );
                })
                ->addColumn('checkout', function ($item) {
                    return  table_accessory_checkout( 'components','components.checkin',$item->id ,0,'checkin');
                })
                ->editColumn('created_at', function ($item) {
                    return date_from_database($item->created_at);
                })

                ->addIndexColumn()
                ->rawColumns([ 'asset', 'location','checkout','name'])
                ->make(true);
        }

        return view('components.show',compact('component','table_detail','table_keys'));
    }
    /**
     * @param int $id
     * @param Request $request
     * @param BaseHttpResponse $response
     * @return BaseHttpResponse
     * @throws \Illuminate\Validation\ValidationException
     */
    public function checkin ($id, Request $request,BaseHttpResponse $response){
        if ($component_assets = DB::table('components_assets')->find($request->checkin_id)) {
            if (is_null($component = Component::find($component_assets->component_id))) {
                return $response
                    ->setError()
                    ->setMessage(trans('layout.not_found'));
            }
            $max_to_checkin = $component_assets->assigned_qty;

            $this->validate($request, [
                "qty" => "required|numeric|between:1,$max_to_checkin"
            ]);

            // Validation passed, so let's figure out what we have to do here.
            $qty_remaining_in_checkout = ($component_assets->assigned_qty - (int)$request->input('qty'));

            // We have to modify the record to reflect the new qty that's
            // actually checked out.
            $component_assets->assigned_qty = $qty_remaining_in_checkout;
            DB::table('components_assets')->where('id',
                $id)->update(['assigned_qty' => $qty_remaining_in_checkout]);

            // If the checked-in qty is exactly the same as the assigned_qty,
            // we can simply delete the associated components_assets record
            if ($qty_remaining_in_checkout == 0) {
                DB::table('components_assets')->where('id', '=', $request->checkin_id)->delete();
            }
            event(new CreatedContentEvent($request->all(), $component, config('general.log.checkin')));
            return $response
                ->setPreviousUrl(route('components.index'))
                ->setMessage(trans('table.checkin_success_message'))->withInput();
        }
        return $response
            ->setError()
            ->setPreviousUrl(route('components.index'))
            ->setMessage(trans('table.not_found'))->withInput();
    }

    /**
     * @param int $id
     * @return BaseHttpResponse
     * @throws \Exception
     */
    public function qrCodeDownload  ($id){

        $component=Component::find($id);

        if (!$component){
            return $this->response
                ->setError()
                ->setNextUrl(route('components.index'))
                ->setMessage(trans('table.not_found'));
        }
        $image   = \QrCode::size(250)->generate(route('components.show',$component->id));
        $svgTemplate = new \SimpleXMLElement($image);
        $svgTemplate->registerXPathNamespace('svg', 'http://www.w3.org/2000/svg');
        $svgTemplate->rect->addAttribute('fill-opacity', 0);
        $image = $svgTemplate->asXML();
        $imageName = 'qr-code-component-'.Str::slug($component->name.'-'.$component->serial);

        \Storage::disk('public')->put($imageName, $image);

        return response()->download('storage/'.$imageName, $imageName.'.svg');
    }
}
