<?php

namespace App\Http\Controllers;

use App\Events\CreatedContentEvent;
use App\Http\Responses\BaseHttpResponse;
use App\Models\Asset;
use App\Models\AssetModel;
use App\Models\ComponentAsset;
use App\Models\FieldSet;
use App\Models\Location;
use App\Models\User;
use App\Notifications\CalcNotification;
use App\Supports\SettingStore;
use App\TableList\AssetTable;
use Carbon\Carbon;
use Illuminate\Support\Facades\View;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Str;

class AssetController extends Controller
{

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     * @throws \Throwable
     */
    protected $response;
    /**
     * @var SettingStore
     */
    protected $settingStore;
    protected $rules = [
        'name' => 'max:255|nullable',
        'model_id' => 'required|integer|exists:models,id',
        'status_id' => 'required|integer|exists:status_labels,id',
        'company_id' => 'integer|nullable',
        'warranty_months' => 'numeric|nullable|digits_between:0,240',
        'physical' => 'numeric|max:1|nullable',
        'checkout_date' => 'date|max:10|min:10|nullable',
        'checkin_date' => 'date|max:10|min:10|nullable',
        'supplier_id' => 'exists:suppliers,id|numeric|nullable',
        'location_id' => 'exists:locations,id|nullable',
        'status' => 'integer',
        'purchase_cost' => 'numeric|nullable',
        'next_audit_date' => 'date|nullable',
        'last_audit_date' => 'date|nullable',
    ];

    /**
     * SettingController constructor.
     * @param BaseHttpResponse $response
     * @param SettingStore $settingStore
     */
    public function __construct(BaseHttpResponse $response, SettingStore $settingStore)
    {
        $this->middleware('auth');
        $this->response = $response;
        $this->settingStore = $settingStore;
    }

    /**
     * @param Request $request
     * @param AssetTable $dataTable
     * @param string|null $trash
     * @return BaseHttpResponse|\Illuminate\Http\JsonResponse|View
     * @throws \Throwable
     */
    public function index(Request $request, AssetTable $dataTable, ?string $trash = null)
    {
        if ($request->user()->can('view-asset')) {
            page_title()->setTitle(trans('layout.asset.asset'));
            return $dataTable->renderTable($trash);
        } else {
            return $this->response
                ->setError()
                ->setNextUrl(redirect()->back()->getTargetUrl())
                ->setMessage(trans('table.no_permission'));
        }
    }

    /**
     * @param Request $request
     * @return BaseHttpResponse|\Illuminate\Http\JsonResponse|View
     */
    public function create(Request $request)
    {
        if ($request->user()->can('create-asset')) {
            page_title()->setTitle(trans('layout.asset.create'));
            return view('assets.add');
        } else {
            return $this->response
                ->setError()
                ->setNextUrl(redirect()->back()->getTargetUrl())
                ->setMessage(trans('table.no_permission'));
        }
    }

    /**
     * Store a newly created resource in storage.
     * @param \Illuminate\Http\Request $request
     * @return BaseHttpResponse
     * @throws \Illuminate\Validation\ValidationException
     */
    public function store(Request $request)
    {

        $validate = [
            'asset_tag' => 'required|min:1|max:255,unique:assets,asset_tag',
            'serial' => 'required|unique:assets,serial',
        ];
        $rules = array_merge($this->rules, $validate);

        // Check validate custom field
        if ($request->model_id != '') {
            $model = AssetModel::find($request->model_id);
            if (($model) && ($model->fieldset)) {
                $rules += $model->fieldset->validation_rules();
            }
        }

        $this->validate($request, $rules);

        $asset = new Asset();
        $asset->name = $request->input('name');
        $asset->asset_tag = $request->input('asset_tag');
        $asset->serial = $request->input('serial');
        $asset->company_id = $request->input('company_id');
        $asset->model_id = $request->input('model_id');
        $asset->order_number = $request->input('order_number');
        $asset->notes = $request->input('notes');
        $asset->user_id = auth()->id();
        $asset->archived = '0';
        $asset->physical = '1';
        $asset->status_id = request('status_id', 0);
        $asset->warranty_months = request('warranty_months', null);
        $asset->purchase_cost = $request->input('purchase_cost');
        $asset->purchase_date = request('purchase_date', null);
        $asset->assigned_to = request('assigned_to', null);
        $asset->requestable = request('requestable', 0);
        $asset->location_id = $request->input('location_id');
        $asset->image = request('image', null);
        if (!empty($settings->audit_interval)) {
            $asset->next_audit_date = Carbon::now()->addMonths($settings->audit_interval)->toDateString();
        }

        if ($asset->assigned_to == '') {
            $asset->location_id = $request->input('rtd_location_id', null);
        }

        $asset->save();
        event(new CreatedContentEvent($request->all(), $asset, config('general.log.create')));
        return $this->response->setPreviousUrl(route('assets.index'))
            ->setMessage(trans('table.create_success_message'));
    }

    /**
     * Display the specified resource.
     * @param  $id
     * @param Request $request
     * @return \Illuminate\Contracts\View\View|BaseHttpResponse
     */
    public function show($id, Request $request)
    {
        $id = decrypt($id);
        if ($request->user()->can('create-asset')) {
            page_title()->setTitle(trans('layout.asset.create'));
            $asset = Asset::with('model')->find($id);
            $detail_value = setting(Asset::ASSET) ?? json_encode(array_values(config('general.asset_detail')));
            $table_detail = json_decode($detail_value);

            $table_keys = array_keys(config('general.asset_detail'));
            $model = AssetModel::find($asset->model_id);
            if ($model)
                $custom_fields_list = FieldSet::where('id', $model->fieldset_id)->with('fields')->first();
            else
                $custom_fields_list = [];

            return view('assets.show', compact('asset', 'table_detail', 'table_keys','custom_fields_list'));
        } else {
            return $this->response
                ->setError()
                ->setNextUrl(redirect()->back()->getTargetUrl())
                ->setMessage(trans('table.no_permission'));
        }
    }

    /**
     * Show the form for editing the specified resource.
     * @param int $id
     * @return \Illuminate\Contracts\View\View|BaseHttpResponse
     */
    public function edit($id, Request $request)
    {
        if (!$request->user()->can('edit-asset')) {
            return $this->response
                ->setError()
                ->setNextUrl(redirect()->back()->getTargetUrl())
                ->setMessage(trans('table.no_permission'));
        }
        page_title()->setTitle(trans('layout.asset.edit'));
        $asset = Asset::find($id);
        $model = AssetModel::find($asset->model_id);
        if ($model)
            $custom_fields_list = FieldSet::where('id', $model->fieldset_id)->with('fields')->first();
        else
            $custom_fields_list = [];

        return view('assets.edit', ['asset' => $asset,
            'type' => config('general.types_table.edit'),
            'custom_fields_list' => $custom_fields_list]);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param \Illuminate\Http\Request $request
     * @param int $id
     * @return BaseHttpResponse
     * @throws \Illuminate\Validation\ValidationException
     */
    public function update(Request $request, $id)
    {

        // Check if the asset exists
        if (!$asset = Asset::find($id)) {
            // Redirect to the asset management page with error
            return $this->response
                ->setNextUrl(route('assets.index'))
                ->setMessage(trans('table.asset_checked'));
        }
        $validate = [
            'asset_tag' => 'required|min:1|max:255,unique:assets,asset_tag,' . $id,
            'serial' => 'required|min:1|max:255|unique:assets,serial,' . $id,
        ];
        $rules = array_merge($this->rules, $validate);
        // Check validate custom field
        if ($request->model_id != '') {
            $model = AssetModel::with('fieldset')->find($request->model_id);
            if (($model) && ($model->fieldset)) {
                $rules += $model->fieldset->validation_rules();
            }
        }

        $this->validate($request, $rules);

        $asset->status_id = $request->input('status_id', null);
        $asset->warranty_months = $request->input('warranty_months', null);
        $asset->purchase_cost = $request->input('purchase_cost', null);
        $asset->purchase_date = $request->input('purchase_date', null);
        $asset->expected_checkin = $request->input('expected_checkin', null);

        // If the box isn't checked, it's not in the request at all.
        $asset->requestable = $request->filled('requestable');
        $asset->rtd_location_id = $request->input('rtd_location_id', null);

        if ($asset->assigned_to == '') {
            $asset->location_id = $request->input('rtd_location_id', null);
        }


        // Update the asset data
        $asset_tag = $request->input('asset_tag');
        $serial = $request->input('serial');
        $asset->name = $request->input('name');
        $asset->status_id = $request->input('status_id');
        $asset->serial = $serial;
        $asset->company_id = $request->input('company_id');
        $asset->location_id = $request->input('location_id');
        $asset->model_id = $request->input('model_id');
        $asset->order_number = $request->input('order_number');
        $asset->asset_tag = $asset_tag;
        $asset->notes = $request->input('notes');
        $asset->image = $request->input('image');
        $asset->physical = '1';
        $model = AssetModel::find($request->input('model_id'));
        if (($model) && ($model->fieldset)) {
            foreach ($model->fieldset->fields as $field) {
                $asset->{$field->db_column} = $request->input($field->db_column) ?? '';
            }
        }
        $asset->save();
        event(new CreatedContentEvent($request->all(), $asset, config('general.log.update')));
        return $this->response->setNextUrl(route('assets.index'))
            ->setMessage(trans('table.update_success_message'));
    }

    /**
     * Remove the specified resource from storage.
     * @param int $id
     * @param Request $request
     * @return BaseHttpResponse
     */
    public function destroy($id, Request $request)
    {
        try {
            if (!$request->user()->can('delete-asset')) {
                return $this->response
                    ->setError()
                    ->setNextUrl(redirect()->back()->getTargetUrl())
                    ->setMessage(trans('table.no_permission'));
            }
            $asset = Asset::withTrashed()->find($id);
            $asset->delete();
            event(new CreatedContentEvent($id, $asset, config('general.log.destroy')));
            return $this->response
                ->setMessage(trans('table.delete_success_message'));
        } catch (Exception $exception) {
            return $this->response->setError()
                ->setMessage($exception->getMessage());
        }
    }

    /**
     * @param int $id
     * @param Request $request
     * @return BaseHttpResponse
     */
    public function deleted($id, Request $request)
    {
        try {
            if (!$request->user()->can('delete-asset')) {
                return $this->response
                    ->setError()
                    ->setNextUrl(redirect()->back()->getTargetUrl())
                    ->setMessage(trans('table.no_permission'));
            }
            $asset = Asset::onlyTrashed()->where('id', $id);
            $asset->forceDelete();
            ComponentAsset::where('asset_id', $id)->delete();
            return $this->response->setMessage(trans('table.delete_success_message'));
        } catch (Exception $exception) {
            return $this->response->setError()->setMessage($exception->getMessage());
        }
    }

    /**
     * @return BaseHttpResponse
     */
    public function emptyAllTrash(Request $request)
    {
        try {
            if (!$request->user()->can('delete-asset')) {
                return $this->response
                    ->setError()
                    ->setNextUrl(redirect()->back()->getTargetUrl())
                    ->setMessage(trans('table.no_permission'));
            }
            $assets = Asset::onlyTrashed()->get();
            foreach ($assets as $asset) {
                ComponentAsset::where('asset_id', $asset->id)->delete();
                $asset->forceDelete();
            }
            return $this->response
                ->setMessage(trans('table.success_empty__msg'));
        } catch (Exception $exception) {
            return $this->response->setError()
                ->setMessage($exception->getMessage());
        }
    }

    /**
     * @param int $id
     * @return \Illuminate\Contracts\View\View|BaseHttpResponse
     * @throws \Exception
     */
    public function clone($id, Request $request)
    {
        if (!$request->user()->can('create-asset')) {
            return $this->response
                ->setError()
                ->setNextUrl(redirect()->back()->getTargetUrl())
                ->setMessage(trans('table.no_permission'));
        }
        $user = Asset::withTrashed()->find($id);
        return view('assets.edit', ['asset' => $user,
            'type' => config('general.types_table.clone')]);
    }

    /**
     * @param int $id
     * @return \Illuminate\Contracts\View\View
     * @throws \Exception
     */
    public function checkout($id)
    {
        page_title()->setTitle(trans('layout.asset.checkout'));
        $asset = Asset::find($id);
        return view('assets.checkout', compact('asset'));
    }

    /**
     * @param int $id
     * @param Request $request
     * @return BaseHttpResponse
     * @throws \Exception
     */
    public function checkoutPost($id, Request $request)
    {

        // Check if the asset exists
        if (!$asset = Asset::find($id)) {
            return $this->response
                ->setError()
                ->setNextUrl(route('assets.index'))
                ->setMessage(trans('table.user_invalid'));
        } elseif (!$asset->availableForCheckout()) {
            return $this->response
                ->setError()
                ->setNextUrl(route('assets.index'))
                ->setMessage(trans('table.asset_not_available'));

        }
        // Check type of asset
        if ($request->checkout_to_type == Asset::USER) {
            $request->validate([
                'manager_id' => 'required'
            ]);
        } elseif ($request->checkout_to_type == Asset::ASSET)
            $request->validate([
                'asset_id' => 'required'
            ]);
        else {
            $request->validate([
                'location_id' => 'required'
            ]);
        }
        // Check checkout yourself asset
        if ($request->asset_id == $id) {
            return $this->response
                ->setError()
                ->setNextUrl(route('assets.index'))
                ->setMessage(trans('table.error_check_self'));
        }

        // Perform checkout asset
        $admin = Auth::user();

        $target = $this->determineCheckoutTarget($request->checkout_to_type);

        $checkout_at = date("Y-m-d H:i:s");
        if (($request->filled('checkout_at')) && ($request->get('checkout_at') != date("Y-m-d"))) {

            $checkout_at = $request->get('checkout_at');
        }

        $expected_checkin = '';
        if ($request->filled('expected_checkin')) {
            $expected_checkin = $request->get('expected_checkin');
        }

        if ($request->filled('status_id')) {
            $asset->status_id = $request->get('status_id');
        }

        if ($asset->checkOut($target, $admin, $checkout_at, $expected_checkin, ($request->get('note')), $request->get('name'))) {
            event(new CreatedContentEvent($request->all(), $asset, config('general.log.checkout')));
            if ($request->manager_id) {
                $message = trans('table.asset_by') . Auth::user()->first_name;
                $notification = [
                    'asset_id' => $id,
                    'assigned_to' => $request->manager_id,
                    'message' => $message,
                    'creat_by' => auth()->id(),
                ];
                User::find($request->manager_id)->notify(new CalcNotification($notification));
            }
            return $this->response
                ->setNextUrl(route('assets.index'))
                ->setMessage(trans('table.checkout_success_message'));
        }
    }

    /**
     * Get Data with type asset
     * @param  $asset
     * @return null
     */
    protected function determineCheckoutTarget($asset)
    {
        // This item is checked out to a location
        switch ($asset) {
            case 'location':
                return Location::findOrFail(request('location_id'));
            case 'asset':

                return Asset::findOrFail(request('asset_id'));
            case 'user':
                return User::findOrFail(request('manager_id'));
        }

        return null;
    }

    /**
     * @param int $id
     * @return \Illuminate\Contracts\View\View
     */
    public function checkin($id)
    {
        page_title()->setTitle(trans('layout.asset.checkin'));
        $asset = Asset::find($id);
        return view('assets.checkin', compact('asset'));
    }

    /**
     * @param int $id
     * @param Request $request
     * @return BaseHttpResponse
     */
    public function checkinPost($id, Request $request)
    {
        $request->validate([
            'checkin_at' => 'required|date'
        ]);

        // Check if the asset exists
        if (is_null($asset = Asset::find($id))) {
            // Redirect to the asset management page with error
            return $this->response
                ->setError()
                ->setNextUrl(route('assets.index'))
                ->setMessage(trans('table.not_found'));
        }
        if (is_null($target = $asset->assignedTo)) {
            return $this->response
                ->setError()
                ->setNextUrl(route('assets.index'))
                ->setMessage(trans('table.error_checkin_asset_already'));
        }

        //Send notification
        if ($asset->assignedType() == Asset::USER) {
            $message = trans('table.asset_by') . Auth::user()->first_name;
            $notification = [
                'asset_id' => $id,
                'assigned_to' => $asset->assigned_to,
                'message' => $message,
                'creat_by' => auth()->id(),
            ];
        }
        //Perform Checkin
        $asset->expected_checkin = null;
        $asset->last_checkout = null;
        $asset->assigned_to = null;
        $asset->assignedTo()->disassociate($asset);
        $asset->assigned_type = null;
        $asset->accepted = null;
        $asset->name = $request->get('name');

        if ($request->filled('status_id')) {
            $asset->status_id = e($request->get('status_id'));
        }
        $asset->location_id = $asset->rtd_location_id;
        $checkin_at = date('Y-m-d');
        if ($request->filled('checkin_at')) {
            $checkin_at = $request->input('checkin_at');
        }
        $asset->increment('checkin_counter', 1);
        // Was the asset updated?
        if ($asset->save()) {
            event(new CreatedContentEvent($request->all(), $asset, config('general.log.checkin')));
            if (isset($notification)) {
                User::find($notification['assigned_to'])->notify(new CalcNotification($notification));
            }
            return $this->response
                ->setNextUrl(route('assets.index'))
                ->setMessage(trans('table.checkin_success_message'))->withInput();

        }
        // Redirect to the asset management page with error
        return $this->response
            ->setError()
            ->setNextUrl(route('assets.index'))
            ->setMessage(trans('table.error'));
    }

    /**
     * @param int $id
     * @return \Illuminate\Contracts\View\View
     */
    public function qrCode($id)
    {
        $asset = Asset::find(decrypt($id));
        return view('assets.qr', compact('asset'));
    }

    /**
     * @param int $id
     * @return BaseHttpResponse
     * @throws \Exception
     */
    public function qrCodeDownload($id)
    {
        $asset = Asset::find($id);

        $image = \QrCode::size(250)->generate(route('assets.show', encrypt($asset->id)));
        $svgTemplate = new \SimpleXMLElement($image);
        $svgTemplate->registerXPathNamespace('svg', 'http://www.w3.org/2000/svg');
        $svgTemplate->rect->addAttribute('fill-opacity', 0);
        $image = $svgTemplate->asXML();
        $assignedTo = $asset->assignedTo;

        if ($assignedTo) {
            $imageName = 'qr-code-' . Str::slug($assignedTo->getFullNameAttribute() . '-' . $asset->serial);

        } else {
            $imageName = 'qr-code-' . Str::slug($asset->serial);
        }

        \Storage::disk('public')->put($imageName, $image);

        return response()->download('storage/' . $imageName, $imageName . '.svg');
    }
}
