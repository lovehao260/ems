<?php

namespace App\Http\Controllers;

use App\Http\Responses\BaseHttpResponse;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Hash;

class ProfileController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Contracts\View\View|\Illuminate\Http\Response
     */
    public function index()
    {
        $user = auth()->user();
        $type = 'edit';
        return view('users/profile.index', compact('user', 'type'));
    }

    /**
     * Change the current password
     * @param Request $request
     * @param BaseHttpResponse $response
     * @return BaseHttpResponse
     */
    public function password(Request $request, BaseHttpResponse $response)
    {
        $user = Auth::user();
        $request->validate([
            'password' => 'required|same:password_confirmation|min:6',
            'password_confirmation' => 'required',
        ]);
        $user->password = Hash::make($request->password);
        $user->save();
        return $response
            ->setPreviousUrl(route('profile.index'))
            ->setMessage(trans('layout.profile.change_password_success'))->withInput();

    }


}
