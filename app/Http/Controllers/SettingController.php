<?php

namespace App\Http\Controllers;

use App\Http\Responses\BaseHttpResponse;
use App\Supports\SettingStore;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\App;

class SettingController extends Controller
{
    /**
     * @var SettingStore
     */
    protected $settingStore;

    /**
     * SettingController constructor.
     * @param SettingStore $settingStore
     */
    public function __construct(SettingStore $settingStore)
    {
        $this->settingStore = $settingStore;
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Contracts\View\View|\Illuminate\Http\Response
     */
    public function index()
    {
        page_title()->setTitle(trans('layout.setting.setting'));
        return view('settings.general');
    }

    /**
     * Save settings
     * @return BaseHttpResponse
     */
    public function save(Request $request, BaseHttpResponse $response)
    {
        foreach ($request->except(['_token']) as $settingKey => $settingValue) {
            $this->settingStore->set($settingKey, $settingValue);
        }
        $this->settingStore->save();
        return $response->setMessage(trans('table.update'));
    }
    /**
     * Save settings language
     * @return \Illuminate\Http\RedirectResponse
     */
    public function locale($locale)
    {
        if (!in_array($locale, ['en', 'ja'])) {
            abort(404);
        }
        app()->setLocale($locale);
        session()->put('locale', $locale);
        return redirect()->back();
    }

}
