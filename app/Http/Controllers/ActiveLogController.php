<?php

namespace App\Http\Controllers;

use App\Events\CreatedContentEvent;
use App\Http\Responses\BaseHttpResponse;
use App\TableList\ActiveTable;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\View;
use Spatie\Activitylog\Models\Activity;

class ActiveLogController extends Controller
{
    protected $response;
    public function __construct(BaseHttpResponse $response)
    {
        $this->response = $response;
        $this->middleware('auth');
    }
    /**
     * @param Request $request
     * @param ActiveTable $datatable
     * @return BaseHttpResponse|\Illuminate\Http\JsonResponse|View
     * @throws \Throwable
     */
    public function index( Request $request,ActiveTable $datatable)
    {
        if ($request->user()->can('view-log')) {
            page_title()->setTitle(trans('layout.log.activities'));
            return $datatable->renderTable();
        } else {
            return $this->response
                ->setError()
                ->setNextUrl(redirect()->back()->getTargetUrl())
                ->setMessage(trans('table.no_permission'));
        }
    }
    /**
     * @param Request $request
     * @return BaseHttpResponse
     */
    public function deletes(Request $request)
    {
        if ($request->user()->can('delete-log')) {
            $ids = $request->input('ids');
            if (empty($ids)) {
                return $this->response
                    ->setError()
                    ->setMessage(trans('notices.no_select'));
            }
            foreach ($ids as $id) {
                try {
                    $activity = Activity::find($id);
                    Activity::where('id', $id)->delete();
                    event(new CreatedContentEvent($request->all(), $activity, config('general.log.destroy')));
                } catch (Exception $exception) {
                    return $this->response
                        ->setError()
                        ->setMessage($exception->getMessage());
                }
            }
            return $this->response
                ->setMessage(trans('table.delete_success_message'));
        } else {
            return $this->response
                ->setError()
                ->setNextUrl(redirect()->back()->getTargetUrl())
                ->setMessage(trans('table.no_permission'));
        }
    }

    /**
     * @param int $id
     * @param Request $request
     * @return BaseHttpResponse
     */
    public function destroy($id, Request $request)
    {
        try {
            if ($request->user()->can('delete-log')) {
                $activity = Activity::find($id);
                Activity::where('id', $id)->delete();
                event(new CreatedContentEvent($request->all(), $activity, config('general.log.destroy')));
                return $this->response->setMessage(trans('table.delete_success_message'));
            } else {
                return $this->response
                    ->setError()
                    ->setNextUrl(redirect()->back()->getTargetUrl())
                    ->setMessage(trans('table.no_permission'));
            }
        } catch (Exception $exception) {
            return $this->response
                ->setError()
                ->setMessage(trans('table.error_exception'));
        }
    }

    /**
     * @return BaseHttpResponse|\Illuminate\Http\Response
     */
    public function clearLogs(Request $request)
    {
        if ($request->user()->can('delete-log')) {
            DB::table('activity_log')->delete();
            return response(trans('table.delete_success_message'), 200);
        }
        else
        return $this->response
            ->setError()
            ->setNextUrl(redirect()->back()->getTargetUrl())
            ->setMessage(trans('table.no_permission'));
    }
}
