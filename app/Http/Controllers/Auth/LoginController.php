<?php

namespace App\Http\Controllers\Auth;

use App\Events\CreatedContentEvent;
use App\Http\Controllers\Controller;
use App\Providers\RouteServiceProvider;

use Illuminate\Foundation\Auth\AuthenticatesUsers;
use Illuminate\Http\Request;
use Illuminate\Validation\ValidationException;
use Symfony\Component\HttpFoundation\Response;
use App\Http\Responses\BaseHttpResponse;
class LoginController extends Controller
{
    /*
    |--------------------------------------------------------------------------
    | Login Controller
    |--------------------------------------------------------------------------
    |
    | This controller handles authenticating users for the application and
    | redirecting them to your home screen. The controller uses a trait
    | to conveniently provide its functionality to your applications.
    |
    */

    use AuthenticatesUsers;

    /**
     * Where to redirect users after login.
     *
     * @var string
     */
    protected $redirectTo = RouteServiceProvider::HOME;

    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('guest')->except('logout');
    }
    /**
     * @return string
     */

    /**
     * Handle a login request to the application.
     *
     */
    public function login(Request $request , BaseHttpResponse $response)
    {
        $input = $request->all();

        $this->validate($request, [
            'username' => 'required',
            'password' => 'required',
        ]);

        $fieldType = filter_var($request->username, FILTER_VALIDATE_EMAIL) ? 'email' : 'username';
        if(auth()->attempt(array($fieldType => $input['username'], 'password' => $input['password'])))
        {
            event(new CreatedContentEvent($request->all(), auth()->user(), config('general.log.login')));
            return redirect()->route('dashboard.index');
        }else{
            return redirect()->back()
                ->withInput()
                ->withErrors([
                    'username' => 'These credentials do not match our records.',
                ]);
        }
    }
    public function logout(Request $request)
    {
        event(new CreatedContentEvent($request->all(),auth()->user(), config('general.log.logout')));
        $this->guard()->logout();
        return redirect()->route('login');
    }
}
