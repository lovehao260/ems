<?php

namespace App\Http\Controllers;

use App\Events\CreatedContentEvent;
use App\Http\Responses\BaseHttpResponse;
use App\Models\AssetModel;
use App\Models\FieldSet;
use App\TableList\AssetModelTable;
use Illuminate\Http\Request;
use Exception;


class AssetModelController extends Controller
{
    protected $response;

    public function __construct(BaseHttpResponse $response)
    {
        $this->response = $response;
        $this->middleware('auth');
    }

    /**
     * Get index model page
     * @param Request $request
     * @param AssetModelTable $dataTable
     * @param string|null $trash
     * @return \Illuminate\Http\JsonResponse|BaseHttpResponse
     * @throws \Throwable
     */
    public function index(Request $request,AssetModelTable $dataTable, ?string $trash = null)
    {
        if (!$request->user()->can('view-model')) {
            return $this->response
                ->setError()
                ->setNextUrl(redirect()->back()->getTargetUrl())
                ->setMessage(trans('table.no_permission'));
        }
        page_title()->setTitle(trans('layout.asset_models'));
        return $dataTable->renderTable($trash);
    }

    /**
     * Get add model page
     * @param Request $request
     * @return \Illuminate\Contracts\View\View|BaseHttpResponse
     */
    public function create(Request $request)
    {
        if (!$request->user()->can('create-model')) {
            return $this->response
                ->setError()
                ->setNextUrl(redirect()->back()->getTargetUrl())
                ->setMessage(trans('table.no_permission'));
        }
        page_title()->setTitle(trans('layout.model.create'));
        $custom_fieldset_list = FieldSet::select('id', 'name')->get();
        return view('models.add', compact('custom_fieldset_list'));

    }

    /**
     * Store model and create default value
     * @param Request $request
     * @return BaseHttpResponse
     * @throws \Illuminate\Validation\ValidationException
     */
    public function store(Request $request): BaseHttpResponse
    {
        $this->validate($request, [
            'name' => 'required',
            'category_id' => 'required',
        ]);
        $request->user_id = auth()->id();
        $model=AssetModel::create($request->input());
        $this->removeCustomFieldsDefaultValues($model);
        if ($request->input('default_values')) {
            $this->assignCustomFieldsDefaultValues($model, $request->input('default_values'));
        }
        try {

            event(new CreatedContentEvent($request->all(), $model, config('general.log.create')));
            return $this->response
                ->setPreviousUrl(route('models.index'))
                ->setMessage(trans('table.create_success_message'));
        } catch (Exception $exception) {
            return $this->response
                ->setError()
                ->setMessage($exception->getMessage());
        }
    }

    /**
     * Get Asset model page
     * @param $id
     * @param Request $request
     * @return \Illuminate\Contracts\View\View|BaseHttpResponse
     */
    public function edit($id,Request $request)
    {
        if (!$request->user()->can('edit-model')) {
            return $this->response
                ->setError()
                ->setNextUrl(redirect()->back()->getTargetUrl())
                ->setMessage(trans('table.no_permission'));
        }
        page_title()->setTitle(trans('layout.model.edit'));
        $model = AssetModel::withTrashed()->with('defaultValues')->find($id);
        $custom_fieldset_list = FieldSet::select('id', 'name')->get();

        return view('models.edit', ['model' => $model,
            'type' => config('general.types_table.edit'), 'custom_fieldset_list' => $custom_fieldset_list]);

    }

    /**
     * Update model and update Fields Default Values
     * @param $id
     * @param Request $request
     * @param BaseHttpResponse $response
     * @return BaseHttpResponse
     * @throws \Illuminate\Validation\ValidationException
     */
    public function update($id, Request $request, BaseHttpResponse $response): BaseHttpResponse
    {
        $this->validate($request, [
            'name' => 'required',
            'category_id' => 'required',
        ]);
        $model = AssetModel::withTrashed()->find($id);

        try {
            $this->removeCustomFieldsDefaultValues($model);
            if ($request->input('default_values')) {
                $this->assignCustomFieldsDefaultValues($model, $request->input('default_values'));
            }
            $model->update($request->input());
            event(new CreatedContentEvent($request->all(), $model, config('general.log.update')));
            return $response
                ->setPreviousUrl(route('models.index'))
                ->setMessage(trans('table.update_success_message'));
        } catch (Exception $exception) {
            return $this->response
                ->setError()
                ->setMessage($exception->getMessage());
        }
    }

    /**
     * Removes all default values
     * @return void
     */
    private function removeCustomFieldsDefaultValues(AssetModel $model)
    {
        $model->defaultValues()->detach();
    }

    /**
     * attach default values many to many relationship
     * @return void
     */
    private function assignCustomFieldsDefaultValues(AssetModel $model, array $defaultValues)
    {
        foreach ($defaultValues as $customFieldId => $defaultValue) {
            if ($defaultValue) {
                $model->defaultValues()->attach($customFieldId, ['default_value' => $defaultValue]);
            }
        }
    }

    /**
     * Clone asset model
     * @return \Illuminate\Contracts\View\View|BaseHttpResponse
     */
    public function clone($id,Request $request)
    {
        if (!$request->user()->can('create-model')) {
            return $this->response
                ->setError()
                ->setNextUrl(redirect()->back()->getTargetUrl())
                ->setMessage(trans('table.no_permission'));
        }
        page_title()->setTitle(trans('layout.model.create'));
        $model = AssetModel::withTrashed()->find($id);
        $custom_fieldset_list = FieldSet::select('id', 'name')->get();
        return view('models.add', ['model' => $model,
            'type' => config('general.types_table.clone'),
            'custom_fieldset_list'=>$custom_fieldset_list]);
    }

    /**
     * Destroy asset model, item move to trash
     * @param $id
     * @param Request $request
     * @param BaseHttpResponse $response
     * @return BaseHttpResponse
     */
    public function destroy($id, Request $request, BaseHttpResponse $response)
    {
        try {
            if (!$request->user()->can('delete-model')) {
                return $this->response
                    ->setError()
                    ->setNextUrl(redirect()->back()->getTargetUrl())
                    ->setMessage(trans('table.no_permission'));
            }
            $model = AssetModel::withTrashed()->find($id);
            $model->delete();
            event(new CreatedContentEvent($request->all(), $model, config('general.log.destroy')));
            return $response->setMessage(trans('table.delete_success_message'));
        } catch (Exception $exception) {
            return $response
                ->setError()
                ->setMessage($exception->getMessage());
        }
    }

    /**
     * Destroy many asset model, item move to trash
     * @param Request $request
     * @param BaseHttpResponse $response
     * @return BaseHttpResponse
     */
    public function deletes(Request $request, BaseHttpResponse $response)
    {
        if (!$request->user()->can('delete-model')) {
            return $this->response
                ->setError()
                ->setNextUrl(redirect()->back()->getTargetUrl())
                ->setMessage(trans('table.no_permission'));
        }
        $ids = $request->input('ids');
        // Check array id of model
        if (empty($ids)) {
            return $response
                ->setError()
                ->setMessage(trans('notices.no_select'));
        }

        foreach ($ids as $id) {
            try {
                $model = AssetModel::find($id);
                $model->delete();
                event(new CreatedContentEvent($request->all(), $model, config('general.log.destroy')));
            } catch (Exception $exception) {
                return $response
                    ->setError()
                    ->setMessage($exception->getMessage());
            }
        }
        return $response->setMessage(trans('users.deleted'));
    }

    /**
     * Deleted asset model
     * @param $id
     * @param BaseHttpResponse $response
     * @param Request $request
     * @return BaseHttpResponse
     */
    public function deleted($id, BaseHttpResponse $response,Request $request): BaseHttpResponse
    {
        try {
            if (!$request->user()->can('delete-model')) {
                return $this->response
                    ->setError()
                    ->setNextUrl(redirect()->back()->getTargetUrl())
                    ->setMessage(trans('table.no_permission'));
            }
            $model = AssetModel::withTrashed()->where('id', $id)->forceDelete();
            event(new CreatedContentEvent($id, $model, config('general.log.destroy')));
            return $response->setMessage(trans('table.delete_success_message'));
        } catch (Exception $exception) {
            return $response
                ->setError()
                ->setMessage($exception->getMessage());
        }
    }

    /**
     * Restore model get out trash
     * @param $id
     * @param BaseHttpResponse $response
     * @return BaseHttpResponse
     */
    public function restore($id, BaseHttpResponse $response): BaseHttpResponse
    {
        try {
             AssetModel::withTrashed()->find($id)->restore();
            return $response
                ->setPreviousUrl(route('models.index'))
                ->setMessage(trans('table.restore_success_message'));
        } catch (Exception $exception) {
            return $response
                ->setError()
                ->setMessage($exception->getMessage());
        }
    }

    /**
     * Deleted all model in trash
     * @param BaseHttpResponse $response
     * @param Request $request
     * @return BaseHttpResponse
     */
    public function emptyAllTrash(BaseHttpResponse $response,Request $request): BaseHttpResponse
    {
        try {
            if (!$request->user()->can('delete-model')) {
                return $this->response
                    ->setError()
                    ->setNextUrl(redirect()->back()->getTargetUrl())
                    ->setMessage(trans('table.no_permission'));
            }
            AssetModel::onlyTrashed()->forceDelete();
            return $response->setMessage(trans('table.success_empty__msg'));
        } catch (Exception $exception) {
            return $response
                ->setError()
                ->setMessage($exception->getMessage());
        }
    }
}
