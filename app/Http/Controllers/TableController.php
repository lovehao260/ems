<?php

namespace App\Http\Controllers;

use App\Http\Responses\BaseHttpResponse;
use App\Models\Asset;
use App\Supports\SettingStore;
use App\TableList\TableBuilder;
use Illuminate\Contracts\Container\BindingResolutionException;
use Illuminate\Http\Request;
use Illuminate\Support\Arr;
use Illuminate\Support\Facades\Validator;
use Illuminate\Support\Str;
use Exception;
use Form;
class TableController extends Controller
{
    /**
     * @var TableBuilder
     */
    protected $tableBuilder;
    protected $settingStore;

    /**
     * TableController constructor.
     * @param TableBuilder $tableBuilder
     */
    public function __construct(TableBuilder $tableBuilder, SettingStore $settingStore)
    {
        $this->tableBuilder = $tableBuilder;
        $this->settingStore = $settingStore;
    }

    public function getDataForBulkChanges(Request $request,TableBuilder $tableBuilder){

        $object = $tableBuilder->create($request->input('class'));

        $data = $object->getValueInput(null, null, 'text');

        if (!$request->input('key')) {

            return $data;
        }

        $column = Arr::get($object->getBulkChanges(), $request->input('key'), null);

        if (empty($column)) {

            return $data;
        }

        $labelClass = 'control-label';
        if (!empty($column) && Str::contains(Arr::get($column, 'validate'), 'required')) {
            $labelClass .= ' required';
        }

        $label = '';
        if (!empty($column['title'])) {
            $label = Form::label($column['title'], null, ['class' => $labelClass])->toHtml();
        }

        if (isset($column['callback']) && method_exists($object, $column['callback'])) {
            $data = $object->getValueInput(
                $column['title'],
                null,
                $column['type'],
                call_user_func([$object, $column['callback']])
            );
        } else {
            $data = $object->getValueInput($column['title'], null, $column['type'], Arr::get($column, 'choices', []));
        }

        $data['html'] = $label . $data['html'];

        return $data;
    }
    /**
     * @param Request $request
     * @param BaseHttpResponse $response
     * @return BaseHttpResponse
     * @throws BindingResolutionException
     */
    public function postSaveBulkChange(Request $request, BaseHttpResponse $response)
    {
        $ids = $request->input('ids');
        if (empty($ids)) {
            return $response
                ->setError()
                ->setMessage(trans('table.please_select_record'));
        }

        $inputKey = $request->input('key');
        $inputValue = $request->input('value');

        $object = $this->tableBuilder->create($request->input('class'));

        $columns = $object->getBulkChanges();

        if (!empty($columns[$inputKey]['validate'])) {
            $validator = Validator::make($request->input(), [
                'value' => $columns[$inputKey]['validate'],
            ]);

            if ($validator->fails()) {
                return $response
                    ->setError()
                    ->setMessage($validator->messages()->first());
            }
        }

        try {
            $object->saveBulkChanges($ids, $inputKey, $inputValue);
            return $response->setMessage(trans('table.save_bulk_change_success'));
        } catch (Exception $exception) {
            return $response
                ->setError()
                ->setMessage($exception->getMessage());
        }


    }

    public function showTableDetail(Request $request){
        $this->settingStore->set($request->setting_key, json_encode($request->obj));
        $this->settingStore->save();
        return $request;
    }



}
