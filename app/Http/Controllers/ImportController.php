<?php

namespace App\Http\Controllers;

use App\Http\Responses\BaseHttpResponse;
use App\Imports\AccessoriesImport;
use App\Imports\AssetImport;
use App\Imports\ComponentImport;
use App\Imports\ImportSave;
use App\Imports\LicenseImport;
use App\Imports\UsersImport;
use App\Models\Import;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Storage;
use Maatwebsite\Excel\Facades\Excel;
use Exception;

class ImportController extends Controller
{
    public function __construct()
    {
        $this->middleware('auth');
    }

    /**
     * Display table import
     * @throws \Throwable
     */
    public function index()
    {
        if (request()->ajax()) {
            return datatables()->of(Import::orderBy('created_at','desc')->get())
                ->editColumn('created_at', function ($item) {
                    return $item->created_at->diffForHumans();
                })
                ->editColumn('filesize', function ($item) {
                    return human_file_size($item->filesize);
                })
                ->editColumn('operations', function ($item) {
                    return table_import_actions('imports.user', 'imports.destroy', $item);
                })
                ->addIndexColumn()
                ->rawColumns(['created_at', 'filesize', 'operations'])
                ->make(true);
        }
        page_title()->setTitle(trans('layout.imports'));
        $types_import = config('general.types_import');
        return view('imports.index', ['types_import' => $types_import]);
    }

    /**
     * Upload file import
     * @return BaseHttpResponse
     * @throws \Illuminate\Validation\ValidationException
     */

    public function fileImport(Request $request, BaseHttpResponse $response)
    {
        $request->validate([
            'file' => 'required|mimes:csv,xlsx,xls|max:10048'
        ]);
        try {
            $file_name_full = $request->file('file')->getClientOriginalName();
            $filename = pathinfo($file_name_full, PATHINFO_FILENAME);
            $extension = pathinfo($file_name_full, PATHINFO_EXTENSION);
            $file_path = $filename . '-' . date('Y-m-d') . '.' . $extension;
            $file_size = $request->file('file')->getSize();
            $request->file('file')->storeAs('import', $file_path);
            Excel::import(new ImportSave($file_path, $extension,$file_size), 'import/' . $file_path);
            return $response->setMessage(trans('table.upload_success_message'));
        } catch (Exception $exception) {
            return $response
                ->setError()
                ->setMessage($exception->getMessage());
        }
    }
    /**
     * Load custom field of type import
     * @return string
     */

    public function custom(Request $request, BaseHttpResponse $response)
    {
        $header_field = Import::find($request->import_id);
        switch ($request->type_import) {
            case 'users':
                $import_field = config('general.column_option.users');
                break;
            case 'licenses':
                $import_field = config('general.column_option.licenses');
                break;
            case 'accessories':
                $import_field = config('general.column_option.accessories');
                break;
            case 'components':
                $import_field = config('general.column_option.components');
                break;
            case 'assets':
                $import_field = config('general.column_option.assets');
                break;
            default:
                $import_field = [];
        }
        return view('imports.custom_field',
            ['import_field' => $import_field, 'header_field' => $header_field])->render();

    }
    /**
     * @param int $id
     * @param BaseHttpResponse $response
     * @return BaseHttpResponse
     */
    public function destroy($id, BaseHttpResponse $response)
    {
        try {
            $import = Import::find($id);
            if (Storage::exists($import->file_path)) {
                Storage::delete($import->file_path);
            }
            $import->delete();
            return $response->setMessage(trans('table.delete_success_message'));
        } catch (Exception $exception) {
            return $response
                ->setError()
                ->setMessage();
        }
    }

    /**
     * Progress import by import type
     * @param Request $request
     * @param BaseHttpResponse $response
     * @return BaseHttpResponse
     */

    public function progressImport(Request $request, BaseHttpResponse $response)
    {
        try {
            if (Storage::exists('import/' . $request->file_name)) {
                // Check header row file required
                $check = array_values($request->import_values);
                switch ($request->type_import) {
                    case 'users':
                        $column_require = ['username', 'first_name', 'email'];
                        break;
                    case 'licenses':
                        $column_require = ['name','category','seats'];
                        break;
                    case 'components':
                    case 'accessories':
                        $column_require = ['name','category','qty'];
                        break;
                    case 'assets':
                        $column_require = ['asset_tag','model','status'];
                        break;
                    default:
                        $column_require = [];
                }

                foreach ($column_require as $column) {
                    if (!in_array($column, $check)) {
                        return $response
                            ->setError()
                            ->setMessage(trans('table.column') . $column . trans('table.not_file'));
                    }
                }

                switch ($request->type_import) {
                    case 'users':
                        Excel::import(new UsersImport($request->import_update, $request->import_values), 'import/' . $request->file_name);
                        break;
                    case 'licenses':
                        Excel::import(new LicenseImport($request->import_update, $request->import_values), 'import/' . $request->file_name);
                        break;
                    case 'accessories':
                        Excel::import(new AccessoriesImport($request->import_update, $request->import_values), 'import/' . $request->file_name);
                        break;
                    case 'components':
                        Excel::import(new ComponentImport($request->import_update, $request->import_values), 'import/' . $request->file_name);
                        break;
                    case 'assets':
                        Excel::import(new AssetImport($request->import_update, $request->import_values), 'import/' . $request->file_name);
                        break;
                    default:
                        $column_require = [];
                }
                return $response->setMessage('Import users success');
            } else {
                return $response
                    ->setError()
                    ->setMessage('Can not found the file to import data');
            }

        } catch (\Maatwebsite\Excel\Validators\ValidationException $e) {
            $failures = $e->failures();
            foreach ($failures as $failure) {
                $failure->row(); // row that went wrong
                $failure->attribute(); // either heading key (if using heading row concern) or column index
                $failure->errors(); // Actual error messages from Laravel validator
                $failure->values(); // The values of the row that has failed.
            }
            return $response
                ->setError()
                ->setMessage($failures);
        }

    }



}
