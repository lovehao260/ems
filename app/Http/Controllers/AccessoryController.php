<?php

namespace App\Http\Controllers;

use App\Events\CreatedContentEvent;
use App\Http\Responses\BaseHttpResponse;
use App\Models\Accessory;
use App\Models\AccessoryUser;
use App\Models\User;
use App\Notifications\CalcNotification;
use App\TableList\AccessoryTable;
use Carbon\Carbon;
use Illuminate\Http\Request;
use Exception;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Str;

class AccessoryController extends Controller
{
    protected $response;
    protected $rules = [
        'name' => 'required',
        'category_id' => 'required|integer|exists:categories,id',
        'qty' => 'required|integer|between:1,10000',
        'min_amt' => 'integer|min:0|nullable',
        'purchase_cost' => 'numeric|nullable',
        'purchase_date' => 'date|nullable',
    ];

    public function __construct(BaseHttpResponse $response)
    {
        $this->response = $response;
        $this->middleware('auth');
    }

    /**
     * @throws \Throwable
     */
    public function index( Request $request,AccessoryTable $dataTable, ?string $trash = null)
    {
        if ($request->user()->can('view-accessory')) {
            page_title()->setTitle(trans('layout.accessory.accessory'));
            return $dataTable->renderTable();
        } else {
            return $this->response
                ->setError()
                ->setNextUrl(redirect()->back()->getTargetUrl())
                ->setMessage(trans('table.no_permission'));
        }
    }

    /**
     * Show view created
     * @return BaseHttpResponse|\Illuminate\Contracts\View\View
     */
    public function create(Request$request)
    {
        if ($request->user()->can('create-accessory')) {
            page_title()->setTitle(trans('layout.accessory.create'));
            return view('accessories.add');
        } else {
            return $this->response
                ->setError()
                ->setNextUrl(redirect()->back()->getTargetUrl())
                ->setMessage(trans('table.no_permission'));
        }

    }

    /**
     * @param \Illuminate\Http\Request $request
     * @return \App\Http\Responses\BaseHttpResponse
     * @throws \Illuminate\Validation\ValidationException
     */
    public function store(Request $request): BaseHttpResponse
    {
        $this->validate($request, $this->rules);
        try {
            unset($request['_token']);
            $accessory = new Accessory();
            $accessory->create($request->input());
            event(new CreatedContentEvent($request->all(), $accessory, config('general.log.create')));
            return $this->response
                ->setNextUrl(route('accessories.index'))
                ->setMessage(trans('table.create_success_message'));
        } catch (Exception $exception) {

            return $this->response
                ->setError()
                ->setMessage($exception->getMessage());
        }
    }

    /**
     * Show view update
     * @param $id
     * @param Request $request
     * @return BaseHttpResponse|\Illuminate\Contracts\View\View
     */
    public function edit($id,Request $request)
    {
        if ($request->user()->can('edit-accessory')) {
            page_title()->setTitle(trans('layout.accessory.edit'));
            $accessory = Accessory::find($id);
            return view('accessories.edit', ['accessory' => $accessory]);
        } else {
            return $this->response
                ->setError()
                ->setNextUrl(redirect()->back()->getTargetUrl())
                ->setMessage(trans('table.no_permission'));
        }

    }

    /**
     * @param $id
     * @param \Illuminate\Http\Request $request
     * @param \App\Http\Responses\BaseHttpResponse $response
     * @return \App\Http\Responses\BaseHttpResponse
     * @throws \Illuminate\Validation\ValidationException
     */
    public function update($id, Request $request, BaseHttpResponse $response)
    {
        $this->validate($request, $this->rules);
        $accessory = Accessory::withTrashed()->find($id);
        $accessory->update($request->input());
        event(new CreatedContentEvent($request->all(), $accessory, config('general.log.update')));
        try {
            return $response
                ->setPreviousUrl(route('accessories.index'))
                ->setMessage(trans('table.update_success_message'));
        } catch (Exception $exception) {
            return $this->response
                ->setError()
                ->setMessage($exception->getMessage());
        }

    }

    /**
     * @param int $id
     * @param Request $request
     * @param BaseHttpResponse $response
     * @return BaseHttpResponse
     */
    public function destroy($id, Request $request, BaseHttpResponse $response)
    {
        try {
            if ($request->user()->can('delete-accessory')) {
                $accessory = Accessory::withTrashed()->find($id);
                $accessory->delete();
                event(new CreatedContentEvent($request->all(), $accessory, config('general.log.destroy')));
                return $response->setMessage(trans('table.delete_success_message'));
            } else {
                return $this->response
                    ->setError()
                    ->setNextUrl(redirect()->back()->getTargetUrl())
                    ->setMessage(trans('table.no_permission'));
            }

        } catch (Exception $exception) {
            return $response
                ->setError()
                ->setMessage($exception->getMessage());
        }
    }

    /**
     * @param int $id
     * @param Request $request
     * @param BaseHttpResponse $response
     * @return BaseHttpResponse
     */
    public function checkout($id, Request $request, BaseHttpResponse $response)
    {
        try {
            if (is_null($accessory = Accessory::find($id))) {
                // Redirect to the accessory management page with error
                return $response
                    ->setError()
                    ->setMessage(trans('table.not_found'));
            }
            if (!$user = User::find($request->input('assigned_to'))) {
                return $response
                    ->setError()
                    ->setMessage(trans('table.not_found'));
            }
            $accessory->assigned_to = e($request->input('assigned_to'));

            $accessory->users()->attach($accessory->id, [
                'accessory_id' => $accessory->id,
                'created_at' => Carbon::now(),
                'user_id' => Auth::id(),
                'assigned_to' => $request->get('assigned_to'),
                'note' => $request->input('note')
            ]);

            //  DB::table('accessories_users')->where('assigned_to', $accessory->assigned_to)->where('accessory_id',  $accessory->id)->first();
            event(new CreatedContentEvent($request->all(), $accessory, config('general.log.checkout')));

            if ($request->assigned_to) {
                $message = trans('table.accessory_by') . Auth::user()->first_name;
                $notification = [
                    'accessory_id' => $id,
                    'assigned_to' => $request->assigned_to,
                    'message' => $message,
                    'creat_by' => auth()->id(),
                ];
                User::find($request->assigned_to)->notify(new CalcNotification($notification));
            }
            return $response->setMessage(trans('table.checkout_success_message'));
        } catch (Exception $exception) {
            return $response
                ->setError()
                ->setMessage($exception->getMessage());
        }
    }

    /**
     * @param int $id
     * @return \Illuminate\Contracts\View\View
     * @throws \Exception
     */
    public function show($id)
    {
        page_title()->setTitle(trans('layout.accessory.show'));
        $accessory = Accessory::find($id);
        $detail_value=setting(Accessory::ACCESSORY)?? json_encode(array_values(config('general.accessory_detail')));
        $table_detail= json_decode($detail_value);
        $table_keys=array_keys(config('general.accessory_detail'));
        if (request()->ajax()) {
            return datatables()->of(AccessoryUser::where('accessory_id', $id)->with('user')->get())
                ->editColumn('user', function ($item) {
                    return link_table(route('users.show', $item->user_id), $item->user['first_name'] . ' ' . $item->user->last_name);
                })
                ->addColumn('checkout', function ($item) {

                    return table_accessory_checkout('table-accessories', 'accessories.checkin', $item->id, 0, 'checkin');
                })
                ->editColumn('created_at', function ($item) {
                    return date_from_database($item->created_at);
                })
                ->addIndexColumn()
                ->rawColumns(['user', 'location', 'checkout', 'name'])
                ->make(true);
        }

        return view('accessories.show', compact('accessory','table_detail','table_keys'));
    }

    /**
     * @param int $id
     * @param Request $request
     * @param BaseHttpResponse $response
     * @return \Illuminate\Contracts\View\View
     * @throws \Exception
     */
    public function checkin($id, Request $request, BaseHttpResponse $response)
    {
        if (is_null($accessory_user = DB::table('accessories_users')->find($id))) {
            // Redirect to the accessory management page with error
            return $response
                ->setError()
                ->setMessage(trans('table.not_found'));
        }
        $accessory = Accessory::find($accessory_user->accessory_id);
        $message = trans('table.accessory_by') . Auth::user()->first_name;
        $notification = [
            'accessory_id' => $id,
            'assigned_to' => $accessory_user->assigned_to,
            'message' => $message,
            'creat_by' => auth()->id(),
        ];

        // Was the accessory updated?
        if (DB::table('accessories_users')->where('id', '=', $accessory_user->id)->delete()) {
            event(new CreatedContentEvent($request->all(), $accessory, config('general.log.checkin')));
            if ($notification['assigned_to']) {
                User::find($notification['assigned_to'])->notify(new CalcNotification($notification));
            }
            return $response->setMessage(trans('table.checkin_success_message'));
        }
        // Redirect to the accessory management page with error
        return $response
            ->setError()
            ->setMessage(trans('layout.error'));
    }

    /**
     * @param int $id
     * @return BaseHttpResponse
     * @throws \Exception
     */
    public function qrCodeDownload  ($id){

        $accessory=Accessory::find($id);

        if (!$accessory){
            return $this->response
                ->setError()
                ->setNextUrl(route('accessories.index'))
                ->setMessage(trans('table.not_found'));
        }
        $image   = \QrCode::size(250)->generate(route('accessories.show',$accessory->id));
        $svgTemplate = new \SimpleXMLElement($image);
        $svgTemplate->registerXPathNamespace('svg', 'http://www.w3.org/2000/svg');
        $svgTemplate->rect->addAttribute('fill-opacity', 0);
        $image = $svgTemplate->asXML();
        $imageName = 'qr-code-accessory-'.Str::slug($accessory->name.'-'.$accessory->serial);

        \Storage::disk('public')->put($imageName, $image);

        return response()->download('storage/'.$imageName, $imageName.'.svg');
    }
}
