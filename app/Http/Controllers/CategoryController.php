<?php

namespace App\Http\Controllers;

use App\Events\CreatedContentEvent;
use App\Http\Responses\BaseHttpResponse;
use App\Models\Category;
use App\TableList\AccessoryTable;
use App\TableList\CategoryTable;
use App\TableList\ComponentTable;
use App\TableList\LicenceTable;
use Illuminate\Http\Request;

class CategoryController extends Controller
{

    protected $response;

    public function __construct(BaseHttpResponse $response)
    {
        $this->response = $response;
        $this->middleware('auth');
    }

    /**
     * Display a listing of the resource.
     * @return \Illuminate\Support\Facades\View|BaseHttpResponse
     * @throws \Throwable
     */
    public function index(Request $request, CategoryTable $dataTable)
    {
        if (!$request->user()->can('view-category')) {
            return $this->response
                ->setError()
                ->setNextUrl(redirect()->back()->getTargetUrl())
                ->setMessage(trans('table.no_permission'));
        }
        page_title()->setTitle(trans('layout.categories'));
        return $dataTable->renderTable();
    }

    /**
     * Show the form for creating a new resource.
     * @return \Illuminate\Contracts\View\View|BaseHttpResponse
     */
    public function create(Request $request)
    {
        if (!$request->user()->can('create-category')) {
            return $this->response
                ->setError()
                ->setNextUrl(redirect()->back()->getTargetUrl())
                ->setMessage(trans('table.no_permission'));
        }
        page_title()->setTitle(trans('layout.category.category'));
        return view('categories.add');
    }

    /**
     * Store a newly created resource in storage.
     * @param \Illuminate\Http\Request $request
     * @return BaseHttpResponse
     */
    public function store(Request $request)
    {
        $request->validate([
            'name' => 'required|min:1|max:255|unique:categories,name',
        ]);

        try {
            $category = new Category();
            $category->create([
                'name' => $request->name,
                'image' => $request->image,
                'user_id' => auth()->id(),
                'category_type' => $request->category_type,
                'eula_text' => $request->eula_text,
                'use_default_eula' => $request->use_default_eula ?? 0,
                'require_acceptance' => $request->require_acceptance ?? 0,
                'checkin_email' => $request->checkin_email ?? 0
            ]);
            event(new CreatedContentEvent($request->all(), $category, config('general.log.create')));
            return $this->response
                ->setNextUrl(route('categories.index'))
                ->setMessage(trans('table.create_success_message'));
        } catch (Exception $exception) {
            return $this->response
                ->setError()
                ->setMessage($exception->getMessage());
        }
    }

    /**
     * Display the specified resource.
     * @param int $id
     * @return \Illuminate\Support\Facades\View
     * @throws \Throwable
     */
    public function show($id, ComponentTable $dataTable, AccessoryTable $dataTableAcc, LicenceTable $dataTableLic)
    {
        $category = Category::find($id);
        page_title()->setTitle(trans('layout.categories'));
        switch ($category->category_type) {
            case 'component':
                return $dataTable->renderTable($category->id);

            case 'accessory':
                return $dataTableAcc->renderTable($category->id);

            case 'license':
                return $dataTableLic->renderTable($category->id);

            default:
                //code to be executed
        }
        return null;
    }

    /**
     * Show the form for editing the specified resource.
     * @param int $id
     * @return \Illuminate\Contracts\View\View|BaseHttpResponse
     */
    public function edit($id, Request $request)
    {
        if (!$request->user()->can('edit-category')) {
            return $this->response
                ->setError()
                ->setNextUrl(redirect()->back()->getTargetUrl())
                ->setMessage(trans('table.no_permission'));
        }
        page_title()->setTitle(trans('layout.category.edit'));
        $category = Category::find($id);
        return view('categories.edit', ['category' => $category]);
    }

    /**
     * Update the specified resource in storage.
     * @param \Illuminate\Http\Request $request
     * @param int $id
     * @return BaseHttpResponse
     */
    public function update(Request $request, $id)
    {
        $request->validate([
            'name' => 'required'
        ]);

        try {
            $category = Category::find($id);
            $category->update([
                'name' => $request->name,
                'image' => $request->image,
                'user_id' => auth()->id(),
                'eula_text' => $request->eula_text,
                'use_default_eula' => $request->use_default_eula ?? 0,
                'require_acceptance' => $request->require_acceptance ?? 0,
                'checkin_email' => $request->checkin_email ?? 0
            ]);
            event(new CreatedContentEvent($request->all(), $category, config('general.log.update')));
            return $this->response
                ->setPreviousUrl(route('categories.index'))
                ->setMessage(trans('table.update_success_message'));
        } catch (Exception $exception) {
            return $this->response
                ->setError()
                ->setMessage($exception->getMessage());
        }
    }

    /**
     * Remove the specified resource from storage.
     * @param int $id
     * @return BaseHttpResponse
     */
    public function destroy($id, Request $request)
    {
        try {
            if (!$request->user()->can('delete-category')) {
                return $this->response
                    ->setError()
                    ->setNextUrl(redirect()->back()->getTargetUrl())
                    ->setMessage(trans('table.no_permission'));
            }
            $category = Category::find($id);
            $category->delete();
            event(new CreatedContentEvent($id, $category, config('general.log.destroy')));
            return $this->response->setMessage(trans('table.delete_success_message'));
        } catch (Exception $exception) {
            return $this->response
                ->setError()
                ->setMessage($exception->getMessage());
        }
    }
}
