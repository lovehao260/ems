<?php

namespace App\Http\Controllers\Api;

use App\Http\Controllers\Controller;

use App\Models\Location;
use Illuminate\Http\Request;


class LocationController extends Controller
{

    public function index(Request $request)
    {

        if ($request->ajax()) {

            $term = trim($request->term);
            $posts = Location::select('id', 'name as text')
                ->where('deleted_at', null)
                ->where('name', 'LIKE', '%' . $term . '%')
                ->orderBy('name', 'asc')->simplePaginate(2);

            $morePages = true;
            if (empty($posts->nextPageUrl())) {
                $morePages = false;
            }
            $results = array(
                "results" => $posts->items(),
                "pagination" => array(
                    "more" => $morePages
                )
            );

            return response()->json($results);
        }

    }


}
