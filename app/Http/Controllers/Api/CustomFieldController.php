<?php

namespace App\Http\Controllers\Api;

use App\Http\Controllers\Controller;
use App\Http\Responses\BaseHttpResponse;
use App\Models\CustomField;
use App\Models\FieldSet;
use Illuminate\Http\Request;

class CustomFieldController extends Controller
{
    public function index($id, Request $request)
    {
        if ($request->ajax()) {
            $custom_fields_list = CustomField::select('id', 'name as text')->with('fieldset')->whereDoesntHave('fieldset', function ($query) use ($id) {
                $query->where('custom_field_custom_fieldset.custom_fieldset_id', $id);
            })->simplePaginate(2);

            $morePages = true;
            if (empty($custom_fields_list->nextPageUrl())) {
                $morePages = false;
            }
            $results = array(
                "results" => $custom_fields_list->items(),
                "pagination" => array(
                    "more" => $morePages
                )
            );
            return response()->json($results);
        }
    }

    public function getModelField(  ?string $mode_id = null,Request $request, BaseHttpResponse $response): string
    {

        $custom_fields_list = FieldSet::with('fields')->get()->find($request->id);

        if ($custom_fields_list) {
            return view('partials.fields.model-custom', compact('custom_fields_list','mode_id'))->render();
        } else {
            $response->setError()->setMessage('Not found model');
        }

    }
}
