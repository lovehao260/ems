<?php

namespace App\Http\Controllers\Api;

use App\Http\Controllers\Controller;
use App\Http\Responses\BaseHttpResponse;
use App\Models\Asset;
use App\Models\AssetModel;
use App\Models\FieldSet;
use Illuminate\Http\Request;
use Illuminate\Support\Arr;

class AssetController extends Controller
{
    public function index(Request $request)
    {
        if ($request->ajax()) {
            $term = trim($request->term);
            $posts = Asset::select('id', 'name as text')
                ->where('deleted_at', null)
                ->where('name', 'LIKE', '%' . $term . '%')
                ->orderBy('name', 'asc')->simplePaginate(10);

            $morePages = true;
            if (empty($posts->nextPageUrl())) {
                $morePages = false;
            }
            $results = array(
                "results" => $posts->items(),
                "pagination" => array(
                    "more" => $morePages
                )
            );

            return response()->json($results);
        }

    }

    public function getFiled(?string $asset_id = null, Request $request, BaseHttpResponse $response)
    {
        $model_id = $request->id;
        $model = AssetModel::find($model_id);
        if ($model) {
            $custom_fields_list = FieldSet::where('id', $model->fieldset_id)->with('fields')->first();
            $asset = Asset::find($asset_id);
            if ($asset)
                return view('partials.fields.custom-select', compact('custom_fields_list', 'model_id', 'asset'))->render();
            else
                return view('partials.fields.custom-select', compact('custom_fields_list', 'model_id'))->render();
        } else {
            $response->setError()->setMessage('Not found model');
        }

    }



}
