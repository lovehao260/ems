<?php

namespace App\Http\Controllers\Api;

use App\Http\Controllers\Controller;
use App\Models\AssetModel;
use App\Models\Company;
use Illuminate\Http\Request;
use Illuminate\Pagination\LengthAwarePaginator;

class ModelController extends Controller
{

    public function index(Request $request)
    {

        if ($request->ajax()) {

            $term = trim($request->term);
            $posts = AssetModel::select('id', 'name as text')
                ->where('deleted_at', null)
                ->where('name', 'LIKE', '%' . $term . '%')
                ->orderBy('name', 'asc')->simplePaginate(2);

            $morePages = true;
            if (empty($posts->nextPageUrl())) {
                $morePages = false;
            }
            $results = array(
                "results" => $posts->items(),
                "pagination" => array(
                    "more" => $morePages
                )
            );

            return response()->json($results);
        }

    }


}
