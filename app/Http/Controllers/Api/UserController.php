<?php

namespace App\Http\Controllers\Api;

use App\Http\Controllers\Controller;
use App\Models\User;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;

class UserController extends Controller
{
    public function index(Request $request)
    {
        if ($request->ajax()) {
            $users = User::select('id','first_name','last_name');
            if ($request->filled('term')) {
                $search=$request->term;
                $users = $users->where('first_name', 'LIKE', '%'.$search.'%')
                    ->orWhere('last_name', 'LIKE', '%'.$search.'%');
            }
            $users= $users->where('deleted_at', null)->orderBy('last_name', 'asc')->orderBy('first_name', 'asc')
                ->simplePaginate(50);
            foreach ($users as $user) {
                $name_str = '';
                if ($user->last_name!='') {
                    $name_str .= $user->last_name.' ';
                }
                $name_str .= $user->first_name;
                $user->text = $name_str;

            }
            $morePages = true;
            if (empty($users->nextPageUrl())) {
                $morePages = false;
            }
            $results = array(
                "results" => $users->items(),
                "pagination" => array(
                    "more" => $morePages
                )
            );

            return response()->json($results);
        }

    }
}
