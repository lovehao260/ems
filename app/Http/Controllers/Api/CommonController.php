<?php

namespace App\Http\Controllers\Api;

use App\Http\Controllers\Controller;

use App\Models\Asset;
use App\Models\AssetModel;
use App\Models\FieldSet;
use Illuminate\Support\Arr;
use Illuminate\Http\Request;

class CommonController extends Controller
{
    public function fieldTableDetail($table, Request $request): \Illuminate\Http\JsonResponse
    {
        $data = [];
        $customs = [];
        $field_table = "general." . $table . "_detail";
        $fields = array_values(config($field_table));
        $fields_custom = setting($table);
        foreach (json_decode($fields_custom) as $item) {
            array_push($customs, $item->value);
        }
        foreach ($fields as $field) {
            if (!in_array($field['value'], $customs)) {
                $data = Arr::prepend($data, ['id' => $field['value'], 'label' => $field['label'], 'text' => trans($field['label'])]);
            }
        }
        //Get column custom field of asset detail
        if ($request->detail_id) {
            $asset = Asset::find($request->detail_id);
            $model = AssetModel::find($asset->model_id);
            if ($model)
                $custom_fields_list = FieldSet::where('id', $model->fieldset_id)->with('fields')->first();
            else
                $custom_fields_list = [];

            foreach ($custom_fields_list->fields as $item) {
                if (!in_array($item['db_column'], $customs)) {
                    $data = Arr::prepend($data, ['id' => $item['db_column'], 'label' => $item->name, 'text' => $item->name]);
                }
            }
        }
        $results = array(
            "results" => $data,
        );
        return response()->json($results);
    }
}
