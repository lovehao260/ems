<?php

namespace App\Http\Controllers;

use App\Events\CreatedContentEvent;
use App\Http\Responses\BaseHttpResponse;
use App\Models\Department;
use App\TableList\DepartmentTable;
use Illuminate\Http\Request;

class DepartmentController extends Controller
{
    protected $response;
    public function __construct(BaseHttpResponse $response)
    {
        $this->response = $response;
        $this->middleware('auth');
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\JsonResponse|\Illuminate\Http\Response|\Illuminate\Support\Facades\View
     * @throws \Throwable
     */
    public function index(DepartmentTable $dataTable)
    {
        return $dataTable->renderTable();
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Contracts\View\View
     */
    public function create()
    {
        return view('departments.add');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param \Illuminate\Http\Request $request
     * @return BaseHttpResponse
     */
    public function store(Request $request)
    {

        $request->validate([
            'name' => 'required'
        ]);

        try {
            $department =new Department();
            $department->create([
                'name' => $request->name,
                'company_id' => $request->company_id,
                'manager_id' => $request->manager_id,
                'image' => $request->image
            ]);
             event(new CreatedContentEvent($request->all(), $department, config('general.log.create')));
            return $this->response
                ->setPreviousUrl(route('departments.index'))
                ->setMessage('Add success');
        } catch (Exception $exception) {
            return $this->response
                ->setError()
                ->setMessage('Errors exception');
        }
    }

    /**
     * Display the specified resource.
     *
     * @param int $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param int $id
     * @return \Illuminate\Contracts\View\View
     */
    public function edit($id)
    {
        $department = Department::find($id);
        return view('departments.edit', ['department' => $department]);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param \Illuminate\Http\Request $request
     * @param int $id
     * @return BaseHttpResponse
     */
    public function update(Request $request, $id)
    {
        $request->validate([
            'name' => 'required'
        ]);

        try {
            $department = Department::find($id);
            $department->update([
                'name' => $request->name,
                'company_id' => $request->company_id,
                'manager_id' => $request->manager_id,
                'image' => $request->image
            ]);
            event(new CreatedContentEvent($request->all(), $department, config('general.log.update')));
            return $this->response
                ->setPreviousUrl(route('departments.index'))
                ->setMessage('Add success');
        } catch (Exception $exception) {
            return $this->response
                ->setError()
                ->setMessage('Errors exception');
        }
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param int $id
     * @return BaseHttpResponse
     */
    public function destroy($id)
    {
        try {
            $department = Department::find( $id);
            $department->delete();
            event(new CreatedContentEvent($id, $department, config('general.log.destroy')));
            return $this->response->setMessage('User deleted success item');
        } catch (Exception $exception) {
            return $this->response
                ->setError()
                ->setMessage($exception->getMessage());
        }
    }
}
