<?php

namespace App\Http\Controllers;

use App\Events\CreatedContentEvent;
use App\Http\Responses\BaseHttpResponse;
use App\Models\Company;
use App\TableList\CompanyTable;
use Illuminate\Http\Request;

class CompanyController extends Controller
{

    protected $response;

    public function __construct(BaseHttpResponse $response)
    {
        $this->response = $response;
        $this->middleware('auth');
    }

    /**
     * Display a listing of the resource.
     * @return \Illuminate\Support\Facades\View|BaseHttpResponse
     * @throws \Throwable
     */
    public function index(CompanyTable $dataTable,Request $request)
    {
        if (!$request->user()->can('view-company')) {
            return $this->response
                ->setError()
                ->setNextUrl(redirect()->back()->getTargetUrl())
                ->setMessage(trans('table.no_permission'));
        }
        page_title()->setTitle(trans('layout.companies'));
        return $dataTable->renderTable();
    }

    /**
     * Show the form for creating a new resource.
     * @return \Illuminate\Contracts\View\View|BaseHttpResponse
     */
    public function create(Request $request)
    {
        if (!$request->user()->can('create-company')) {
            return $this->response
                ->setError()
                ->setNextUrl(redirect()->back()->getTargetUrl())
                ->setMessage(trans('table.no_permission'));
        }
        page_title()->setTitle(trans('layout.company.create'));
        return view('companies.add');
    }

    /**
     * Store a newly created resource in storage.
     * @param \Illuminate\Http\Request $request
     * @return BaseHttpResponse
     */
    public function store(Request $request)
    {
        $request->validate([
            'name' => 'required|min:1|max:255|unique:companies,name'
        ]);

        try {
            $company = new Company();
            $company->create([
                'name' => $request->name,
                'image' => $request->image
            ]);
            event(new CreatedContentEvent($request->all(), $company, config('general.log.create')));
            return $this->response
                ->setPreviousUrl(route('companies.index'))
                ->setMessage(trans('table.create_success_message'));
        } catch (Exception $exception) {
            return $this->response
                ->setError()
                ->setMessage('Errors exception');
        }
    }

    /**
     * Display the specified resource.
     * @param int $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     * @param int $id
     * @return \Illuminate\Contracts\View\View|BaseHttpResponse
     */
    public function edit($id,Request $request)
    {
        if (!$request->user()->can('edit-company')) {
            return $this->response
                ->setError()
                ->setNextUrl(redirect()->back()->getTargetUrl())
                ->setMessage(trans('table.no_permission'));
        }
        page_title()->setTitle(trans('layout.company.edit'));
        $company = Company::find($id);
        return view('companies.edit', ['company' => $company]);
    }

    /**
     * Update the specified resource in storage.
     * @param \Illuminate\Http\Request $request
     * @param int $id
     * @return BaseHttpResponse
     */
    public function update(Request $request, $id)
    {
        $request->validate([
            'name' => 'required|min:1|max:255|unique:companies,name' . $id,
        ]);
        try {
            $company = Company::find($id);
            $company->update([
                'name' => $request->name,
                'image' => $request->image
            ]);
            event(new CreatedContentEvent($request->all(), $company, config('general.log.update')));
            return $this->response
                ->setPreviousUrl(route('companies.index'))
                ->setMessage(trans('table.create_success_message'));
        } catch (Exception $exception) {
            return $this->response
                ->setError()
                ->setMessage($exception->getMessage());
        }
    }

    /**
     * Remove the specified resource from storage.
     * @param int $id
     * @return BaseHttpResponse
     */
    public function destroy($id,Request $request)
    {
        try {
            if (!$request->user()->can('delete-company')) {
                return $this->response
                    ->setError()
                    ->setNextUrl(redirect()->back()->getTargetUrl())
                    ->setMessage(trans('table.no_permission'));
            }
            $company = Company::find($id);
            $company->delete();
            event(new CreatedContentEvent($id, $company, config('general.log.destroy')));
            return $this->response->setMessage(trans('table.delete_success_message'));
        } catch (Exception $exception) {
            return $this->response
                ->setError()
                ->setMessage($exception->getMessage());
        }
    }
}
