<?php
namespace App\Http\Traits;

use App\Models\AssetModel;
use App\Models\Company;
use App\Models\Location;
use App\Models\Statuslabel;
trait AssetRelationship {

    /**
     * Relationship company.
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     */
    public function company()
    {
        return $this->belongsTo(Company::class, 'company_id');
    }
    /**
     * Relationship model.
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     */
    public function model()
    {
        return $this->belongsTo(AssetModel::class);
    }
    /**
     * Relationship location
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     */
    public function location()
    {
        return $this->belongsTo(Location::class);
    }
    /**
     * Relationship asset status
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     */
    public function assetstatus()
    {
        return $this->belongsTo(Statuslabel::class, 'status_id');
    }
    /**
     * Relationship asset assignedTo
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     */
    public function assignedTo()
    {
        return $this->morphTo('assigned', 'assigned_type', 'assigned_to')->withTrashed();
    }
}
