<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Component extends Model
{
    use HasFactory;

    protected $table = 'components';
    const COMPONENT='component';
    protected $fillable = ['name', 'image', 'user_id', 'company_id', 'category_id', 'qty', 'min_amt', 'order_number',
        'location_id','purchase_date', 'purchase_cost', 'serial'];

    public function category()
    {
        return $this->belongsTo(Category::class)->withDefault();
    }

    public function company()
    {
        return $this->belongsTo(Company::class)->withDefault();
    }

    public function assets()
    {
        return $this->belongsToMany(Asset::class, 'components_assets')->withPivot('id', 'assigned_qty', 'created_at', 'user_id');
    }

    public function numRemaining()
    {
        $checkedout = $this->numCheckedOut();
        $total = $this->qty;
        $remaining = $total - $checkedout;
        return $remaining;
    }

    /**
     * @return int
     */
    public function numCheckedOut()
    {
        $checkedout = 0;
        foreach ($this->assets as $checkout) {
            $checkedout += $checkout->pivot->assigned_qty;
        }

        return $checkedout;
    }
}
