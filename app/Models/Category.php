<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Category extends Model
{
    use HasFactory;
    protected $table='categories';
    protected $fillable=['name','image','user_id','eula_text','use_default_eula','require_acceptance','checkin_email','category_type'];

    public function isDeletable()
    {
        if ($this->itemCount() == 0) {
            return false;
        }
        return true;
    }
    public function itemCount()
    {
        switch ($this->category_type) {
            case 'asset':
                return $this->assets()->count();
            case 'accessory':
                return $this->accessories()->count();
            case 'component':
                return $this->components()->count();
            case 'consumable':
                return $this->consumables()->count();
            case 'license':
                return $this->licenses()->count();
        }
        return '0';
    }
    public function assets()
    {
        return $this->hasManyThrough(Asset::class, '\App\Models\AssetModel', 'category_id', 'model_id');
    }
    public function models()
    {
        return $this->hasMany(AssetModel::class, 'category_id');
    }
    public function components()
    {
        return $this->hasMany(Component::class);
    }
    public function licenses()
    {
        return $this->hasMany(License::class);
    }
    public function accessories()
    {
        return $this->hasMany(Accessory::class);
    }
}
