<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class Location extends Model
{
    use SoftDeletes;

    protected $table = 'locations';

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'name',
        'parent_id',
        'address',
        'address2',
        'city',
        'state',
        'country',
        'zip',
        'ldap_ou',
        'currency',
        'manager_id',
        'image',
        'user_id'


    ];

    public function parent()
    {
        return $this->belongsTo(Location::class, 'parent_id', 'id')
            ->with('parent');
    }

    public function children()
    {
        return $this->hasMany(Location::class, 'parent_id')
            ->with('children');
    }

    public function users()
    {
        return $this->hasMany(User::class, 'location_id');
    }

    public function assets()
    {
        return $this->hasMany(Asset::class, 'location_id')
            ->whereHas('assetstatus', function ($query) {
                $query->where('status_labels.deployable', '=', 1)
                    ->orWhere('status_labels.pending', '=', 1)
                    ->orWhere('status_labels.archived', '=', 0);
            });
    }

    public function assignedAssets()
    {
        return $this->morphMany(Asset::class, 'assigned', 'assigned_type', 'assigned_to')->withTrashed();
    }

    public function manager()
    {
        return $this->belongsTo(User::class, 'manager_id');
    }

    public function isDeletable()
    {
        if (($this->assignedAssets()->count() === 0)
            && ($this->assets()->count() === 0)
            && ($this->users()->count() === 0)) {
            return false;
        }
        return  true;
    }
}
