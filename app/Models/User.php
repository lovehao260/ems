<?php

namespace App\Models;

use App\Http\Traits\HasPermissionsTrait;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\SoftDeletes;
use Illuminate\Foundation\Auth\User as Authenticatable;
use Illuminate\Notifications\Notifiable;
use Laravel\Sanctum\HasApiTokens;

class User extends Authenticatable
{
    use  Notifiable , SoftDeletes , HasFactory, HasPermissionsTrait,Notifiable;
    protected $appends = ['full_name'];
    protected $table='users';

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'username',
        'first_name',
        'last_name',
        'email',
        'password',
        'company_id',
        'manager_id',
        'location_id',
        'manager_id',
        'password',
        'phone',
        'notes',
        'title',
        'address',
        'image',
        'activated'
    ];

    /**
     * The attributes that should be hidden for arrays.
     *
     * @var array
     */
    protected $hidden = [
        'password',
        'remember_token',
    ];

    /**
     * The attributes that should be cast to native types.
     *
     * @var array
     */
    protected $casts = [
        'email_verified_at' => 'datetime',
    ];
    public function getFullNameAttribute()
    {
        return "{$this->first_name} {$this->last_name}";
    }
    public function files(){
        return $this->hasMany(MediaFile::class);
    }

    public function isDeletable()
    {

        if ($this->itemCount() == 0) {
            return false;
        }
        return true;
    }

    public function company(){
        return $this->belongsTo(Company::class)->withDefault();
    }

    public function department()
    {
        return $this->belongsTo(Department::class, 'department_id');
    }
    public function userloc()
    {
        return $this->belongsTo(Location::class, 'location_id')->withTrashed();
    }

    public function userlog()
    {
        return $this->hasMany(Actionlog::class, 'target_id')->where('target_type', '=', Actionlog::class)->orderBy('created_at', 'DESC')->withTrashed();
    }

    public function licenses()
    {
        return $this->belongsToMany(License::class, 'license_seats', 'assigned_to', 'license_id')->withPivot('id');
    }
    public function assets()
    {
        return $this->morphMany(Asset::class, 'assigned', 'assigned_type', 'assigned_to')->withTrashed();
    }
    public function manager()
    {
        return $this->belongsTo(User::class, 'manager_id')->withTrashed();
    }
    public function accessories()
    {
        return $this->belongsToMany(Accessory::class, 'accessories_users', 'assigned_to', 'accessory_id')
            ->withPivot('id', 'created_at', 'note')->withTrashed();
    }
    public function roles()
    {
        return $this->belongsToMany(Role::class,'users_roles');
    }


}
