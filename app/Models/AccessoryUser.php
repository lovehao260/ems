<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Relations\HasMany;
use Illuminate\Database\Eloquent\Relations\HasOne;
use Illuminate\Database\Eloquent\SoftDeletes;

class AccessoryUser extends Model
{
    use HasFactory;


    /**
     * @var string
     */
    protected $table = 'accessories_users';



    public function user()
    {
        return $this->belongsTo(User::class, 'assigned_to');
    }
    public function accessory()
    {
        return $this->belongsTo(Accessory::class, 'accessory_id');
    }

}
