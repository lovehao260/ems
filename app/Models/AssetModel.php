<?php

namespace App\Models;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;
class AssetModel extends Model
{
    use SoftDeletes;
    protected $dates = ['deleted_at'];
    protected $table = 'models';
    protected $hidden = ['user_id','deleted_at'];
    public function setEolAttribute($value)
    {
        if ($value == '') {
            $value = 0;
        }

        $this->attributes['eol'] = $value;
    }
    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'category_id',
        'depreciation_id',
        'eol',
        'fieldset_id',
        'image',
        'manufacturer_id',
        'model_number',
        'name',
        'note',
        'user_id',
    ];

    /**
     * Establishes the model -> assets relationship
     * @return \Illuminate\Database\Eloquent\Relations\Relation
     */
    public function assets()
    {
        return $this->hasMany(Asset::class, 'model_id');
    }
    /**
     * Establishes the model -> assets relationship
     * @return \Illuminate\Database\Eloquent\Relations\Relation
     */
    public function category()
    {
        return $this->belongsTo(Category::class, 'category_id');

    }

    /**
     * Establishes the model -> assets relationship
     * @return \Illuminate\Database\Eloquent\Relations\Relation
     */
    public function company()
    {
        return $this->belongsTo(Company::class, 'company_id');

    }
    public function fieldset()
    {
        return $this->belongsTo(FieldSet::class, 'fieldset_id');
    }

    public function defaultValues()
    {
        return $this->belongsToMany(CustomField::class, 'models_custom_fields')->withPivot('default_value');
    }
}
