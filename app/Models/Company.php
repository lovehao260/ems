<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\Gate;

class Company extends Model
{
    use HasFactory;

    protected $table = 'companies';
    protected $fillable = ['name', 'image'];

    public function users()
    {
        return $this->hasMany(User::class, 'company_id');
    }

    public function isDeletable()
    {
        if (($this->assets()->count() === 0)
            && ($this->accessories()->count() === 0)
            && ($this->components()->count() === 0)
            && ($this->users()->count() === 0)) {
            return false;
        }
        return true;

    }


    public function assets()
    {
        return $this->hasMany(Asset::class, 'company_id');
    }

    public function licenses()
    {
        return $this->hasMany(License::class, 'company_id');
    }

    public function accessories()
    {
        return $this->hasMany(Accessory::class, 'company_id');
    }

    public function components()
    {
        return $this->hasMany(Component::class, 'company_id');
    }
}
