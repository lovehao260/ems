<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class Accessory extends Model
{
    use SoftDeletes;
    protected $table = 'accessories';
    const ACCESSORY = 'accessory';
    protected $casts = [
        'requestable' => 'boolean'
    ];

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'category_id',
        'company_id',
        'location_id',
        'name',
        'order_number',
        'purchase_cost',
        'purchase_date',
        'model_number',
        'manufacturer_id',
        'supplier_id',
        'image',
        'qty',
        'min_amt',
        'requestable'
    ];
    public function company()
    {
        return $this->belongsTo(Company::class, 'company_id');
    }
    public function location()
    {
        return $this->belongsTo(Location::class, 'location_id');
    }
    public function category()
    {
        return $this->belongsTo(Category::class, 'category_id')->where('category_type', '=', 'accessory');
    }
    public function users()
    {
        return $this->belongsToMany(User::class, 'accessories_users', 'accessory_id', 'assigned_to')->withPivot('id', 'created_at', 'note')->withTrashed();
    }
    public static function quantity()
    {
        return Accessory::select('qty')->sum('qty');
    }
    public static function checkoutcount()
    {
        return AccessoryUser::all()
            ->count();
    }

    public function numRemaining()
    {
        $checkedout = $this->users->count();
        $total = $this->qty;
        $remaining = $total - $checkedout;
        return $remaining;
    }
}
