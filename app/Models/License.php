<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Session;

class License extends Model
{
    use HasFactory , SoftDeletes ;
    const LICENSE = 'license';

    protected $table='licenses';
    protected $fillable=['id','name','serial','license_email','license_name','purchase_date','purchase_cost','purchase_order'
        ,'order_number','seats','category_id','company_id','termination_date','depreciate','maintained','reassignable','expiration_date','notes'];

    public function company(){
        return $this->belongsTo(Company::class)->withDefault();
    }
    public function category(){
        return $this->belongsTo(Category::class)->withDefault();
    }
    public function licenseseats()
    {
        return $this->hasMany( LicenseSeat::class);
    }
    /**
     * Update seat counts when the license is updated
     *
     * @author A. Gianotto <snipe@snipe.net>
     * @since [v3.0]
     */
    public static function boot()
    {
        parent::boot();
        // We need to listen for created for the initial setup so that we have a license ID.
        static::created(function ($license) {
            $newSeatCount = $license->getAttributes()['seats'];
            return static::adjustSeatCount($license, $oldSeatCount = 0, $newSeatCount);
        });
        // However, we listen for updating to be able to prevent the edit if we cannot delete enough seats.
        static::updating(function ($license) {
            $newSeatCount = $license->getAttributes()['seats'];
            $oldSeatCount = isset($license->getOriginal()['seats']) ? $license->getOriginal()['seats'] : 0;
            return static::adjustSeatCount($license, $oldSeatCount, $newSeatCount);
        });
    }

    /**
     * Balance seat counts
     *
     * @author A. Gianotto <snipe@snipe.net>
     * @since [v3.0]
     * @return \Illuminate\Database\Eloquent\Relations\Relation
     */
    public static function adjustSeatCount($license, $oldSeats, $newSeats)
    {
        // If the seats haven't changed, continue on happily.
        if ($oldSeats==$newSeats) {
            return true;
        }
        // On Create, we just make one for each of the seats.
        $change = abs($oldSeats - $newSeats);
        if ($oldSeats > $newSeats) {
            $license->load('licenseseats.user');

            // Need to delete seats... lets see if if we have enough.
            $seatsAvailableForDelete = $license->licenseseats->reject(function ($seat) {
                return (!! $seat->assigned_to) || (!! $seat->asset_id);
            });

            if ($change > $seatsAvailableForDelete->count()) {
                Session::flash('error', trans('admin/licenses/message.assoc_users'));
                return false;
            }
            for ($i=1; $i <= $change; $i++) {
                $seatsAvailableForDelete->pop()->delete();
            }
            // Log Deletion of seats.
//            $logAction = new Actionlog;
//            $logAction->item_type = License::class;
//            $logAction->item_id = $license->id;
//            $logAction->user_id = Auth::id() ?: 1; // We don't have an id while running the importer from CLI.
//            $logAction->note = "deleted ${change} seats";
//            $logAction->target_id =  null;
//            $logAction->logaction('delete seats');
//            return true;
        }
        // Else we're adding seats.
        DB::transaction(function () use ($license, $oldSeats, $newSeats) {
            for ($i = $oldSeats; $i < $newSeats; $i++) {
                $license->licenseSeatsRelation()->save(new LicenseSeat, ['user_id' => Auth::id()]);
            }
        });
        // On initail create, we shouldn't log the addition of seats.
//        if ($license->id) {
//            //Log the addition of license to the log.
//            $logAction = new Actionlog();
//            $logAction->item_type = License::class;
//            $logAction->item_id = $license->id;
//            $logAction->user_id = Auth::id() ?: 1; // Importer.
//            $logAction->note = "added ${change} seats";
//            $logAction->target_id =  null;
//            $logAction->logaction('add seats');
//        }
        return true;
    }


    /**
     * Establishes the license -> seat relationship
     *
     * We do this to eager load the "count" of seats from the controller.
     * Otherwise calling "count()" on each model results in n+1 sadness.
     *
     * @author A. Gianotto <snipe@snipe.net>
     * @since [v2.0]
     * @return \Illuminate\Database\Eloquent\Relations\Relation
     */
    public function licenseSeatsRelation()
    {
        return $this->hasMany(LicenseSeat::class)->whereNull('deleted_at')->selectRaw('license_id, count(*) as count')->groupBy('license_id');
    }

    /**
     * Returns the number of total available seats for this license
     *
     * @author A. Gianotto <snipe@snipe.net>
     * @since [v2.0]
     * @return \Illuminate\Database\Eloquent\Relations\Relation
     */
    public function availCount()
    {
        return $this->licenseSeatsRelation()
            ->whereNull('asset_id')
            ->whereNull('assigned_to')
            ->whereNull('deleted_at');
    }

    public static function assetcount()
    {
        return LicenseSeat::whereNull('deleted_at')
            ->count();
    }
    public static function assetcountCheckout()
    {
        return LicenseSeat::where('user_id','<>',null)->whereNull('deleted_at')
            ->count();
    }
}
