<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class FieldSet extends Model
{
    use HasFactory;
    protected $table='custom_fieldsets';
    protected $fillable = [
        'name',
        'user_id'
    ];
    public function fields()
    {
        return $this->belongsToMany(CustomField::class,'custom_field_custom_fieldset','custom_fieldset_id','custom_field_id')
            ->withPivot(["required","order"])->orderBy("pivot_order");
    }
    public function models()
    {
        return $this->hasMany(AssetModel::class, "fieldset_id");
    }
    public function validation_rules()
    {
        $rules=[];
        foreach ($this->fields as $field) {
            $rule = [];

            $rule[] = ($field->pivot->required=='1') ? "required" : "nullable";
            if ($field->attributes['format']){
                array_push($rule,   $field->attributes['format'] );
            }
            $rules[$field->db_column_name()]=$rule;
        }

        return $rules;
    }
}
