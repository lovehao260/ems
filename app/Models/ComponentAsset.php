<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Relations\HasMany;
use Illuminate\Database\Eloquent\Relations\HasOne;
use Illuminate\Database\Eloquent\SoftDeletes;

class ComponentAsset extends Model
{
    use HasFactory;


    /**
     * @var string
     */
    protected $table = 'components_assets';





    public function assets()
    {
        return $this->belongsTo(Asset::class,'asset_id');
    }
}
