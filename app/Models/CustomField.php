<?php

namespace App\Models;


use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\Schema;
use Illuminate\Support\Str;


class CustomField extends Model
{
    use HasFactory;
    const PREDEFINED_FORMATS = [
        'ANY'           => '',
        'CUSTOM REGEX'  => 'regex',
        'ALPHA'         => 'alpha',
        'NUMERIC'       => 'numeric',
        'EMAIL'         => 'email',
        'DATE'          => 'date',
        'URL'           => 'url',
        'IP'            => 'ip',
        'MAC'           => 'regex:/^[a-fA-F0-9]{2}:[a-fA-F0-9]{2}:[a-fA-F0-9]{2}:[a-fA-F0-9]{2}:[a-fA-F0-9]{2}:[a-fA-F0-9]{2}$/',
        'BOOLEAN'       => 'boolean',
    ];
    protected $table='custom_fields';
    protected $fillable = [
        'name',
        'element',
        'format',
        'field_values',
        'help_text',
        'db_column',
        'user_id'
    ];
    public static $table_name = "assets";
    public static function boot()
    {
        parent::boot();
        self::created(function ($custom_field) {
            // Column already exists on the assets table - nothing to do here.
            // This *shouldn't* happen in the wild.
            if (Schema::hasColumn(CustomField::$table_name, $custom_field->convertUnicodeDbSlug())) {
                return false;
            }
            // Update the column name in the assets table
            Schema::table(CustomField::$table_name, function ($table) use ($custom_field) {
                $table->text($custom_field->convertUnicodeDbSlug())->nullable();
            });

            // Update the db_column property in the custom fields table
            $custom_field->db_column = $custom_field->convertUnicodeDbSlug();
            $custom_field->save();
            return true;
        });
        self::updating(function ($custom_field) {

            // Column already exists on the assets table - nothing to do here.
            if ($custom_field->isDirty("name")) {

                if (Schema::hasColumn(CustomField::$table_name, $custom_field->convertUnicodeDbSlug())) {
                    return true;
                }

                // This is just a dumb thing we have to include because Laraval/Doctrine doesn't
                // play well with enums or a table that EVER had enums. :(
                $platform = Schema::getConnection()->getDoctrineSchemaManager()->getDatabasePlatform();
                $platform->registerDoctrineTypeMapping('enum', 'string');

                // Rename the field if the name has changed
                Schema::table(CustomField::$table_name, function ($table) use ($custom_field) {
                    $table->renameColumn($custom_field->convertUnicodeDbSlug($custom_field->getOriginal("name")), $custom_field->convertUnicodeDbSlug());
                });

                // Save the updated column name to the custom fields table
                $custom_field->db_column = $custom_field->convertUnicodeDbSlug();
                $custom_field->save();

                return true;
            }
            return true;
        });
        // Drop the assets column if we've deleted it from custom fields
        self::deleting(function ($custom_field) {
            return Schema::table(CustomField::$table_name, function ($table) use ($custom_field) {
                $table->dropColumn($custom_field->convertUnicodeDbSlug());
            });
        });
    }

    public function convertUnicodeDbSlug($original = null)
    {
        $name = $original ? $original : $this->name;
        $id = $this->id ? $this->id : 'xx';
        $long_slug = '_ems_' . Str::slug($name, '_');

        return substr($long_slug, 0, 50) . '_' . $id;
    }

    public function fieldset()
    {
        return $this->belongsToMany(FieldSet::class,'custom_field_custom_fieldset','custom_field_id','custom_fieldset_id')
            ->withPivot(["required","order"])->orderBy("pivot_order");
    }

    public function db_column_name()
    {
        return $this->db_column;
    }
    public function defaultValues()
    {
        return $this->belongsToMany(AssetModel::class, 'models_custom_fields')->withPivot('default_value');

    }
}
