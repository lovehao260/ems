<?php

namespace App\Models;

use App\Http\Traits\AssetRelationship;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class Asset extends Model
{
    use SoftDeletes, AssetRelationship;
    const USER='user';
    const LOCATION = 'location';
    const ASSET = 'asset';


    /**
     * The database table used by the model.
     *
     * @var string
     */
    protected $table = 'assets';

    // We set these as protected dates so that they will be easily accessible via Carbon
    protected $dates = [
        'created_at',
        'updated_at',
        'deleted_at',
        'purchase_date',
        'last_checkout',
        'expected_checkin',
        'last_audit_date',
        'next_audit_date'
    ];
    protected $casts = [
        'model_id'       => 'integer',
        'status_id'      => 'integer',
        'company_id'     => 'integer',
        'location_id'    => 'integer',
        'rtd_company_id' => 'integer',
        'supplier_id'    => 'integer',
    ];


    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'asset_tag',
        'assigned_to',
        'assigned_type',
        'company_id',
        'image',
        'location_id',
        'category_id',
        'model_id',
        'name',
        'notes',
        'order_number',
        'purchase_cost',
        'purchase_date',
        'rtd_location_id',
        'serial',
        'status_id',
        'supplier_id',
        'warranty_months',
        'requestable',
        'last_checkout',
        'expected_checkin',
    ];


    /**
     * Determines if an asset is available for checkout.
     * This checks to see if the it's checked out to an invalid (deleted) user
     * OR if the assigned_to and deleted_at fields on the asset are empty AND
     * that the status is deployable
     * @return boolean
     */
    public function availableForCheckout(): bool
    {

        // This asset is not currently assigned to anyone and is not deleted...
        if ((!$this->assigned_to) && (!$this->deleted_at)) {

            // The asset status is not archived and is deployable

            if (($this->assetstatus) && ($this->assetstatus->archived == '0')
                && ($this->assetstatus->deployable == '1'))
            {
                return true;
            }

        }
        return false;
    }
    public function assignedAssets()
    {
        return $this->morphMany(Asset::class, 'assigned', 'assigned_type', 'assigned_to')->withTrashed();
    }
    public function checkOut($target, $admin = null, $checkout_at = null, $expected_checkin = null, $note = null, $name = null, $location = null)
    {
        if (!$target) {
            return false;
        }

        if ($expected_checkin) {
            $this->expected_checkin = $expected_checkin;
        }

        $this->last_checkout = $checkout_at;

        $this->assignedTo()->associate($target);
        $this->notes=$note;

        if ($name != null) {
            $this->name = $name;
        }

        if ($location != null) {
            $this->location_id = $location;
        } else {
            if (isset($target->location)) {
                $this->location_id = $target->location->id;
            }
            if ($target instanceof Location) {
                $this->location_id = $target->id;
            }
        }

        if ($this->save()) {
            if (is_integer($admin)){
                $checkedOutBy = User::findOrFail($admin);
            } elseif (get_class($admin) === User::class) {
                $checkedOutBy = $admin;
            } else {
                $checkedOutBy = Auth::user();
            }
//            event(new CheckoutableCheckedOut($this, $target, $checkedOutBy, $note));

            $this->increment('checkout_counter', 1);
            return true;
        }
        return false;
    }

    /**
     * Gets the lowercased name of the type of target the asset is assigned to
     * @return string
     */
    public function assignedType()
    {
        return strtolower(class_basename($this->assigned_type));
    }
    public static function checkoutcount()
    {
        return Asset::where('assigned_to','<>',null)
            ->count();
    }
    public function checkedOutToUser()
    {
        return $this->assignedType() === self::USER;
    }
    public function model()
    {
        return $this->belongsTo(AssetModel::class, 'model_id')->withTrashed();
    }

    public function assetCheckoutTo(){
        if ($this->checkedOutToUser()) {
            return link_table(route('users.show', $this->assignedTo->id), $this->assignedTo->getFullNameAttribute());
        }else if($this-> assignedType()== self::ASSET){
            return $item->assignedTo['name']??'';
        }else if($this-> assignedType()== self::LOCATION){
            return $this->location['name'] ??'';
        }
        return null;
    }
}
