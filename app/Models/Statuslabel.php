<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Statuslabel extends Model
{
    use HasFactory;
    protected $table='status_labels';
    protected $fillable=['id','name','deployable'];
}
