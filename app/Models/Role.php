<?php

namespace App\Models;

use App\Http\Traits\HasPermissionsTrait;
use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Role extends Model
{
    use HasFactory,HasPermissionsTrait;
    protected $table='roles';
    protected $fillable = ['name','default','description','slug','user_id'];
    public function permissions() {

        return $this->belongsToMany(Permission::class,'roles_permissions');

    }

    public function users() {

        return $this->belongsToMany(User::class,'users_roles');

    }
    public function created_by() {

        return $this->belongsTo(User::class,'user_id');

    }
}
