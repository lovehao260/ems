<?php

namespace App\Supports;

use Illuminate\Database\DatabaseManager;
use Illuminate\Support\Manager;

class SettingsManager extends Manager
{

    /**
     * @return string
     */
    public function getDefaultDriver()
    {
        return env('CMS_SETTING_STORE_DRIVER', 'database');
    }

    /**
     * @return JsonSettingStore
     */
    public function createJsonDriver()
    {
        return new JsonSettingStore(app('files'));
    }

    /**
     * @return DatabaseSettingStore
     */
    public function createDatabaseDriver()
    {

        $connection = app(DatabaseManager::class)->connection();
        $table = 'settings';
        $keyColumn = 'key';
        $valueColumn = 'value';

        return new DatabaseSettingStore($connection, $table, $keyColumn, $valueColumn);
    }
}
