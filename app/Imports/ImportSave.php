<?php

namespace App\Imports;

use App\Models\Import;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Arr;

use Maatwebsite\Excel\Concerns\Importable;
use Illuminate\Support\Collection;
use Maatwebsite\Excel\Concerns\WithHeadingRow;

use Maatwebsite\Excel\Concerns\SkipsFailures;

use Maatwebsite\Excel\Concerns\ToCollection;

class ImportSave implements ToCollection, WithHeadingRow
{

    use Importable;

    protected $name;
    protected $size;
    protected $extension;

    public function __construct($name,$extension, $size)
    {
        $this->name = $name;
        $this->size = $size;
        $this->extension = $extension;
    }

    public function collection(Collection $rows)
    {

        $header_row = Arr::first($rows->toArray(), function ($value, $key) {
            return array_keys($value);
        });

        $first_row = Arr::first($rows->toArray(), function ($value, $key) {
            return array_values($value);
        });

        $import_row = new Import();
        $import_row->name = $this->name;
        $import_row->file_path = 'import/' . $this->name;
        $import_row->filesize = $this->size;
        $import_row->import_type = $this->extension;
        $import_row->header_row = json_encode($header_row);
        $import_row->first_row =json_encode($first_row);
        $import_row->save();
    }

}
