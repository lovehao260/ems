<?php


namespace App\Imports;

use App\Models\Asset;
use App\Models\Category;

use App\Models\Company;
use App\Models\Component;
use App\Models\License;
use App\Models\Location;
use Illuminate\Support\Facades\Validator;
use Maatwebsite\Excel\Concerns\Importable;
use Maatwebsite\Excel\Concerns\ToCollection;
use Illuminate\Support\Collection;
use Maatwebsite\Excel\Concerns\WithHeadingRow;
use Maatwebsite\Excel\Concerns\WithValidation;

class ComponentImport extends ImportAbstract

{
    /**
     * @param Collection $rows
     * @return \Illuminate\Database\Eloquent\Model|null
     * @throws \Illuminate\Validation\ValidationException
     */

    public function collection(Collection $rows)
    {
        foreach ($rows as $key => $row) {
            $data = [];
            foreach ($this->import_value as $index => $value) {
                $data[$value] = $row[$index] ? preg_replace('~[\r\n]+~', '', $row[$index]) : null;
            }

            // Check isset category. If exits then  Find company and assign to user else created company
            if (isset($data['category'])) {
                $data['category_id'] = $this->checkCategory($data['category']);
            }
            if (isset($data['company'])) {
                $data['company_id'] = $this->checkCompany($data['company']);
            }

            if (isset($data['location'])) {
                $data['location_id'] = $this->checkLocation($data['location']);
            }


            if (!isset($data['min_amt'])) {
                $data['min_amt'] = 1;
            }
            if (isset($data['purchase_date'])) {
                $data["purchase_date"] = date("Y-m-d 00:00:01", strtotime($data["purchase_date"]));

            }
            if (isset($data['purchase_cost'])) {
                $data["purchase_cost"] = str_replace(',', '', $data["purchase_cost"]);
            }

            Validator::make($data, [
                'name' => 'required',
                'qty' => 'required|integer|min:1',
                'category_id' => 'required|integer|exists:categories,id',
                'min_amt' => 'integer|min:0|nullable',
                'purchase_date' => 'date|nullable',
                'purchase_cost' => 'numeric|nullable',
            ])->validate();
            // Find license exits by name and serial. Edit user if it exits otherwise create a new user
            $component = Component::where('name', $data['name'])
                ->where('serial', $data['serial'])
                ->first();
            if ($component) {
                if ($this->type_import) {
                    $component->update($data);
                }

            } else {
                Component::create($data);
                // If we have an asset tag, checkout to that asset.
                if (isset($data['asset_tag']) && ($asset = Asset::where('asset_tag', $data['asset_tag'])->first())) {
                    $component->assets()->attach($component->id, [
                        'component_id' => $component->id,
                        'user_id' => auth()->id(),
                        'assigned_qty' => 1, // Only assign the first one to the asset
                        'asset_id' => $asset->id
                    ]);
                }
            }
        }
    }

    public function rules(): array
    {
        return [];

    }
}
