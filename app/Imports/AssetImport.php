<?php


namespace App\Imports;
use App\Models\Asset;
use App\Models\AssetModel;
use App\Models\Statuslabel;
use Illuminate\Support\Facades\Validator;
use Illuminate\Support\Collection;
class AssetImport extends ImportAbstract
{
    /**
     * @param Collection $rows
     * @return \Illuminate\Database\Eloquent\Model|null
     * @throws \Illuminate\Validation\ValidationException
     */

    public function collection(Collection $rows)
    {
        foreach ($rows as $key => $row) {
            $data = [];
            foreach ($this->import_value as $index => $value) {
                $data[$value] = $row[$index]?preg_replace('~[\r\n]+~', '', $row[$index]):null;
            }
            // Check isset category. If exits then  Find company and assign to user else created company
            if (isset($data['category'])) {
                $data['category_id']=$this->checkCategory($data['category']);
            }
            if (isset($data['company'])) {
                $data['company_id']=$this->checkCompany($data['company']);
            }

            if (isset($data['location'])) {
                $data['location_id']=$this->checkLocation($data['location']);

            }

            // Check isset model
            if (isset($data['model'])) {
                $model = AssetModel::where('name', $data['model'])->first();
                if ($model) {
                    $data['model_id'] = $model->id;
                } else {
                    $model_new = AssetModel::create([
                        'name' => $data['model'],
                        'user_id' => auth()->id(),
                        'category_id' => $data['category_id']
                    ]);
                    $data['model_id'] = $model_new['id'];
                }
                unset( $data['category_id']);
            }
            if (isset($data['status'])) {
                $model = Statuslabel::where('name', $data['status'])->first();
                if ($model) {
                    $data['status_id'] = $model->id;
                } else {
                    $data['status_id'] = 1;
                }
            }

            if (isset($data['warranty_months'])) {
                $data["warranty_months"] = intval($data["warranty_months"]);

            }
            if (isset($data['purchase_date'])) {
                $data["purchase_date"] = date("Y-m-d 00:00:01", strtotime($data["purchase_date"]));

            }
            if (isset($data['purchase_cost'])) {
                $data["purchase_cost"] = str_replace(',', '', $data["purchase_cost"]);
            }
            // Find license exits by name and serial. Edit user if it exits otherwise create a new user
            $asset = Asset::withTrashed()->where(['asset_tag' => $data['asset_tag']])->first();


            Validator::make($data, [
                'name' => 'max:255|nullable',
                'model_id' => 'required|integer|exists:models,id',
                'status_id' => 'required|integer|exists:status_labels,id',
                'company_id' => 'integer|nullable',
                'warranty_months' => 'numeric|nullable|digits_between:0,240',
                'physical' => 'numeric|max:1|nullable',
                'checkout_date' => 'date|max:10|min:10|nullable',
                'checkin_date' => 'date|max:10|min:10|nullable',
                'location_id' => 'exists:locations,id|nullable',
                'asset_tag' => 'required|min:1|max:255',
                'serial' => 'nullable',
                'purchase_cost' => 'numeric|nullable',
                'next_audit_date' => 'date|nullable',
                'last_audit_date' => 'date|nullable',
            ])->validate();

            if ($asset) {
                if ($this->type_import) {
                    $asset->update($data);
                }

            } else {
                Asset::create($data);
            }
        }

    }


    public function rules(): array
    {
        return [];

    }
}
