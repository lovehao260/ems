<?php


namespace App\Imports;

use App\Models\Company;
use App\Models\User;
use Illuminate\Support\Facades\Validator;
use Illuminate\Support\Collection;

class UsersImport extends ImportAbstract

{
    /**
     * @param Collection $rows
     * @return \Illuminate\Database\Eloquent\Model|null
     * @throws \Illuminate\Validation\ValidationException
     */

    public function collection(Collection $rows)
    {
        foreach ($rows as $key => $row) {
            $data = [];
            // Check isset company. If exits then  Find company and assign to user else created company
            foreach ($this->import_value as $index => $value) {
                $data[$value] = $row[$index]?preg_replace('~[\r\n]+~', '', $row[$index]):null;
            }

            if (isset($data['company'])) {
                $data['company_id']=$this->checkCompany($data['company']);
            }
            // Check isset manager.
            if (isset($data['manager'])) {
                $manager = User::where('username', $data['manager'])->first();
                if ($manager){
                    $data['manager_id'] =$manager['id'];
                }
            }
            // Find user exits by username or email. Edit user if it exits otherwise create a new user
            $user = User::withTrashed()->where('username', $data['username'])->orWhere('email', $data['email'])->first();

            if ($user) {
                if ($this->type_import){
                    Validator::make($data, [
                        'first_name' => 'required',
                        'username' => 'required|unique:users,username,' . $user->id,
                        'email' => 'required|email|unique:users,email,' . $user->id,
                    ])->validate();

                    $user->update($data);
                }
            } else {
                $data['password'] = bcrypt($data['password'] ?? 'admin123');
                Validator::make($data, [
                    'first_name' => 'required',
                    'email' => 'required|email|unique:users,email',
                    'username' => 'required|unique:users,username',
                ])->validate();
                User::create($data);
            }
        }
    }


    public function rules(): array
    {
        return [];
    }
}
