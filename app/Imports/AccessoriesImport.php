<?php


namespace App\Imports;

use App\Models\Accessory;
use Illuminate\Support\Facades\Validator;
use Illuminate\Support\Collection;

class AccessoriesImport extends ImportAbstract

{

    /**
     * @param Collection $rows
     * @return \Illuminate\Database\Eloquent\Model|null
     * @throws \Illuminate\Validation\ValidationException
     */

    public function collection(Collection $rows)
    {

        foreach ($rows as $key => $row) {
            $data = [];
            foreach ($this->import_value as $index => $value) {
                $data[$value] = $row[$index] ?preg_replace('~[\r\n]+~', '', $row[$index]):null;
            }

            // Check isset category. If exits then  Find company and assign to user else created company
            if (isset($data['category'])) {
                $data['category_id']=$this->checkCategory($data['category']);
            }
            if (isset($data['company'])) {
                $data['company_id']=$this->checkCompany($data['company']);
            }

            if (isset($data['location'])) {
                $data['location_id']=$this->checkLocation($data['location']);

            }

            if (!isset($data['min_amt'])) {
                $data['min_amt'] = 1;
            }
            if (isset($data['purchase_date'])) {
                $data["purchase_date"] = date("Y-m-d 00:00:01", strtotime( $data["purchase_date"]));

            }
            if (isset($data['purchase_cost'])) {
                $data["purchase_cost"] = str_replace(',', '', $data["purchase_cost"]);
            }
            $accessory = Accessory::where('name',$data['name'])->first();

            Validator::make($data, [
                'name' => 'required',
                'category_id' => 'required',
                'qty' => 'required|numeric|min:0|not_in:0'

            ])->validate();

            if ($accessory) {
                if ($this->type_import) {
                    $accessory->update($data);
                }

            } else {
                Accessory::create($data);
            }
        }
    }


    public function rules(): array
    {
        return [];

    }
}
