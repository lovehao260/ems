<?php

namespace App\Imports;

use App\Models\Category;
use App\Models\Company;
use App\Models\Location;
use Maatwebsite\Excel\Concerns\Importable;
use Maatwebsite\Excel\Concerns\ToCollection;
use Maatwebsite\Excel\Concerns\WithHeadingRow;
use Maatwebsite\Excel\Concerns\WithValidation;

abstract class ImportAbstract implements ToCollection, WithHeadingRow, WithValidation
{
    use Importable;

    protected $import_value;
    protected $type_import;

    public function __construct(string $type = null, array $import_value = [])
    {
        $this->type_import = $type;
        $this->import_value = $import_value;
    }

    public function checkCategory($data)
    {
        $category = Category::where('name', $data)->first();
        if ($category) {
            return $category->id;
        } else {
            if (!is_null($data)) {
                $category_new = Category::create([
                    'name' => $data,
                    'user_id' => auth()->id(),
                    'category_type' => 'license'
                ]);
                return $category_new['id'];
            }
            return null;
        }
    }

    public function checkCompany($data)
    {
        $company = Company::where('name', $data)->first();
        if ($company) {
            return $company->id;
        } else {
            if (!is_null($data)) {
                $company_new = Company::create(['name' => $data]);
                return $company_new['id'];
            }
            return null;
        }
    }

    public function checkLocation($data)
    {
        $location = Location::where('name', $data)->first();
        if ($location) {
            return $location->id;
        } else {
            if (!is_null($data)) {
                $location_new = Location::create(['name' => $data]);
                return $location_new['id'];
            }
            return null;
        }

    }
}
