<?php

namespace App\Imports;

use App\Models\License;
use Illuminate\Support\Facades\Validator;
use Illuminate\Support\Collection;

class LicenseImport extends ImportAbstract
{
    /**
     * @param Collection $rows
     * @return \Illuminate\Database\Eloquent\Model|null
     * @throws \Illuminate\Validation\ValidationException
     */

    public function collection(Collection $rows)
    {
        foreach ($rows as $key => $row) {
            $data=[];
            foreach ($this->import_value as $index => $value) {
                $data[$value] = $row[$index]?preg_replace('~[\r\n]+~', '', $row[$index]):null;
            }

            // Check isset category. If exits then  Find company and assign to user else created company
            if (isset($data['category'])) {
                $data['category_id']=$this->checkCategory($data['category']);
            }
            if (isset($data['company'])) {
                $data['company_id']=$this->checkCompany($data['company']);
            }


            if (!isset($data['min_amt'])) {
                $data['min_amt'] = 1;
            }
            if (isset($data['purchase_date'])) {
                $data["purchase_date"] = date("Y-m-d 00:00:01", strtotime( $data["purchase_date"]));
            }
            if (isset($data['expiration_date'])) {
                $data["expiration_date"] = date("Y-m-d 00:00:01", strtotime( $data["expiration_date"]));
            }

            if (isset($data['purchase_cost'])) {
                $data["purchase_cost"] = str_replace(',', '', $data["purchase_cost"]);
            }

            if (isset($data['termination_date'])) {
                $data["termination_date"] = date("Y-m-d 00:00:01", strtotime( $data["termination_date"]));
            }
            // Find license exits by name and serial. Edit user if it exits otherwise create a new user
            $license = License::where('name', $data["name"])->first();

            Validator::make($data, [
                'name' => 'required',
                'category_id' => 'required',
                'seats' => 'required|numeric|min:0|not_in:0',
            ])->validate();

            if ($license) {
                if ($this->type_import) {
                    $license->update($data);
                }
            } else {
                License::create($data);
            }
        }
    }


    public function rules(): array
    {
        return [];

    }
}
