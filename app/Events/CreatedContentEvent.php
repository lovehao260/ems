<?php

namespace App\Events;

use Illuminate\Broadcasting\Channel;
use Illuminate\Broadcasting\InteractsWithSockets;
use Illuminate\Broadcasting\PresenceChannel;
use Illuminate\Broadcasting\PrivateChannel;
use Illuminate\Contracts\Broadcasting\ShouldBroadcast;
use Illuminate\Foundation\Events\Dispatchable;
use Illuminate\Queue\SerializesModels;

class CreatedContentEvent
{
    use SerializesModels;

    public $data;
    public $message;
    public $target;

    /**
     * Create a new event instance.
     *
     * @return void
     */
    public function __construct($data, $target, $message)
    {

        $this->data = $data;
        $this->target = $target ?? '';
        $this->message = $message;

    }

    /**
     * Get the channels the event should broadcast on.
     *
     * @return \Illuminate\Broadcasting\Channel|array
     */
    public function broadcastOn()
    {
        return new PrivateChannel('saving-activity');
    }
}
