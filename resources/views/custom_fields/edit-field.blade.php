@extends('layouts.app')
@section('content')
    <div class=" rv-media-integrate-wrapper">
        {{ Breadcrumbs::render('fields.field.update',$field->id) }}
        <div id="main" class="container">
            <form method="POST" action="{{route( 'fields.field.update',$field->id)}}" accept-charset="UTF-8">
                @csrf
                <div class="row">
                    <div class="col-md-12 ">
                        <div class="main-form content-page">
                            <div class="form-body">
                                <div class="form-group ">
                                    <div class="row">
                                        <div class="col-md-2 text-right">
                                            <label for="name" class="text-title-field control-label required">
                                                {{trans('layout.name')}}</label>
                                        </div>
                                        <div class="col-md-10">
                                            <input class="form-control @error('name') is-invalid @enderror"
                                                   type="text" id="name" value="{{old('name',$field->name)}}"
                                                   name="name">
                                            @error('name')
                                            <span class="text-danger">{{ $message }}</span>
                                            @enderror
                                        </div>
                                    </div>
                                </div>
                                <div class="form-group ">
                                    <div class="row">
                                        <div class="col-md-2 text-right">
                                            <label for="element" class="text-title-field control-label required">
                                                {{trans('layout.custom_field.element')}}</label>
                                        </div>
                                        <div class="col-md-10">
                                            <select class="form-control select-search-full" name="element"
                                                    id="element-field">
                                                @foreach ($formats as $format => $label)
                                                    <option value="{{$format}}" role="option"
                                                            {{old('element',$field->element)===$format?'selected':''}}
                                                            aria-selected="true">{{$label}}</option>
                                                @endforeach
                                            </select>
                                            @error('element')
                                            <span class="text-danger">{{ $message }}</span>
                                            @enderror
                                        </div>
                                    </div>
                                </div>
                                <div class="form-group field-text">
                                    <div class="row">
                                        <div class="col-md-2 text-right">
                                            <label for="format" class="text-title-field control-label required">
                                                {{trans('layout.format')}}
                                            </label>
                                        </div>
                                        <div class="col-md-10">
                                            <select class="form-control select-search-full field-format" name="format">
                                                @foreach ($predefined_formats as $format => $label)
                                                    <option
                                                        {{old('format',$field->format)===$label ||$regex==$label ?'selected':'' }}
                                                        value="{{$label}}">{{$format}}</option>
                                                @endforeach
                                            </select>
                                            @error('format')
                                            <span class="text-danger">{{ $message }}</span>
                                            @enderror
                                        </div>
                                    </div>
                                </div>
                                <div class="form-group field-value hidden ">
                                    <div class="row">
                                        <div class="col-md-2 text-right">
                                            <label for="help_text" class="text-title-field control-label ">
                                                {{trans('layout.custom_field.field_value')}}</label>
                                        </div>
                                        <div class="col-md-10">
                                            <textarea class="form-control @error('field_values') is-invalid @enderror"
                                                      name="field_values" type="text" id="help_text"
                                                      rows="3">{!! old('field_values',$field->field_values) !!}</textarea>
                                            @error('field_values')
                                            <span class="text-danger">{{ $message }}</span>
                                            @enderror
                                        </div>
                                    </div>
                                </div>
                                <div class="form-group field-regex hidden">
                                    <div class="row">
                                        <div class="col-md-2 text-right">
                                            <label for="regex_format" class="text-title-field control-label ">
                                                {{trans('layout.custom_field.regex')}}</label>
                                        </div>
                                        <div class="col-md-10">
                                            <input class="form-control @error('regex_format') is-invalid @enderror"
                                                   placeholder="regex:/^[0-9]{15}$/"
                                                   name="regex_format" type="text" id="regex_format"
                                                   value="{{old('regex_format',$field->format)}}">
                                            @error('regex_format')
                                            <span class="text-danger">{{ $message }}</span>
                                            @enderror
                                        </div>
                                    </div>
                                </div>
                                <div class="form-group ">
                                    <div class="row">
                                        <div class="col-md-2 text-right">
                                            <label for="help_text" class="text-title-field control-label ">
                                                {{trans('layout.custom_field.help')}}</label>
                                        </div>
                                        <div class="col-md-10">
                                            <input class="form-control @error('help_text') is-invalid @enderror"
                                                   name="help_text" type="text" id="help_text"
                                                   value="{{old('help_text',$field->help_text)}}">
                                            @error('')
                                            <span class="text-danger">{{ $message }}</span>
                                            @enderror
                                        </div>
                                    </div>
                                </div>
                                <div class="clearfix"></div>
                            </div>
                            <div class="widget meta-boxes d-flex justify-content-center">
                                <div class="widget-body">
                                    <div class="btn-set">
                                        <a href="{{route('fields.index')}}" class="float-left btn btn-warning mr-5">
                                            {{trans('layout.cancel')}}</a>
                                        <button type="submit" name="submit" value="save" class="btn btn-info">
                                            <i class="fa fa-save"></i> {{trans('layout.save')}}
                                        </button>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </form>
        </div>
    </div>
@endsection
