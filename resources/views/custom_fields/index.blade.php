@extends('layouts.app')
@section('content')
    {{ Breadcrumbs::render('fields.index') }}
    <div class="table-wrapper">
        <div class="table-datatable ">
            <div class="portlet light bordered portlet-no-padding p-2">
                <div class="rp-card bg-white border position-relative">
                    <div class="rp-card__header">
                        <h5 class="p-2 text-center text-uppercase">{{trans('layout.custom_field.m_fieldset')}}</h5>

                    </div>
                    <div class="rp-card-content equal-height">
                        <button class="btn btn-secondary create-fieldset" data-bs-toggle="modal"
                                data-bs-target="#model-add-fieldset"><i class="fa fa-plus"></i><span> {{trans('table.create')}}</span></button>
                        {!! $fieldSets->renderTable() !!}
                    </div>
                </div>
            </div>
        </div>
        <div class="table-datatable ">
            <div class="portlet light bordered portlet-no-padding p-2">
                <div class="rp-card bg-white  mt-5 border">
                    <div class="rp-card-header">
                        <h5 class="p-2 text-center text-uppercase">{{trans('layout.custom_field.m_field')}}</h5>
                    </div>
                    <div class="rp-card-content equal-height">
                        {!! $fields->renderTable() !!}
                    </div>
                </div>
            </div>
        </div>

    </div>

    <!-- Modal -->
    <div class="modal fade" id="model-add-fieldset" tabindex="-1" aria-labelledby="model-add-fieldsetLabel"
         aria-hidden="true">
        <div class="modal-dialog">
            <div class="modal-content">
                <div class="modal-header bg-info">
                    <h4 class="modal-title" id="model-add-fieldsetLabel"><i class="til_img"></i><strong>{{trans('table.create')}}
                            {{trans('layout.custom_field.fieldset')}} </strong></h4>
                    <button type="button" data-bs-dismiss="modal" aria-hidden="true" class="close"><span
                            aria-hidden="true">×</span></button>
                </div>
                <div class="modal-body form-save-fieldset">
                    <div class="form-group row ">
                        <label for="name" class="col-md-3 control-label required">{{trans('layout.name')}}</label>
                        <div class="col-md-9"><input id="name" name="name" class=" form-control">
                        </div>
                    </div>
                </div>
                <div class="modal-footer">
                    <button data-bs-dismiss="modal" class="float-left btn btn-warning">{{trans('layout.cancel')}}</button>
                    <button class="float-right btn btn-info save-fieldset-button"
                            data-load-url="{{route('fields.fieldset.store')}}" data-parent-table="#table-fieldsets">{{trans('layout.save')}}
                    </button>
                </div>
            </div>
        </div>
    </div>
@endsection
@yield('javascript1')
