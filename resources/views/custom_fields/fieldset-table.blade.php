<div class="table-wrapper ">
    <div class="portlet light bordered portlet-no-padding">
        <div class="portlet-title">
        </div>
        <div class="portlet-body">
            <div class="table-responsive dataTable @if ($actions) table-has-actions @endif">
                {!! $dataTable->table(compact('id', 'class'), false) !!}
            </div>
        </div>
    </div>
</div>
@push('js')
    {!! $dataTable->scripts() !!}
@endpush
