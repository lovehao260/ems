<div class="portlet-title">
    <div class="caption">
        <div class="wrapper-action">
            @if ($actions)
                <div class="btn-group">
                    <a class="btn btn-secondary dropdown-toggle" href="#" data-toggle="dropdown">
                        {{trans('layout.bulk_action')}} </a>
                    <ul class="dropdown-menu">
                        @foreach ($actions as $action)
                            <li>
                                {!! $action !!}
                            </li>
                        @endforeach
                    </ul>
                </div>
            @endif
        </div>
    </div>
</div>
<div class="portlet-body">
    <div class="table-responsive dataTable @if ($actions) table-has-actions @endif">
        @section('main-table')
            {!! $dataTable->table(compact('id', 'class'), false) !!}
        @show
    </div>
</div>

@include('partials.table.modal')



@section('javascript')
    {!! $dataTable->scripts() !!}
@stop
