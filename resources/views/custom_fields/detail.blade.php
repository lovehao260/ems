@extends('layouts.app')
@section('content')
    <div class=" rv-media-integrate-wrapper">
        {{ Breadcrumbs::render('fields.fieldset.show',$fieldSet->id) }}
        <div id="main" class="">
            <div class="table-wrapper">
                <div class="table-datatable row">
                    <div class="d-flex justify-content-end">
                        <div class="mr-auto pt-2"><h5>{{$fieldSet->name}} Fieldset</h5></div>
                        <div class="p-2">
                            <button class="btn btn-secondary buttons-create " data-bs-toggle="modal"
                                    data-bs-target="#model-add-field"><i class="fa fa-plus"></i><span>{{trans('layout.custom_field.add')}}</span>
                            </button>
                        </div>
                    </div>
                    <table id="sortable" class="table table-striped table-hover dataTable no-footer" data-order-url="{{route('fields.fieldset.customOrder',$fieldSet->id)}}">
                        <thead>
                        <tr>
                            <th></th>
                            <th title="Order" class="text-left" tabindex="0">{{trans('layout.order')}}</th>
                            <th title="Field Name" class="text-left" tabindex="0">{{trans('layout.custom_field.name')}}</th>
                            <th title="Format" class="text-left" tabindex="0"> {{trans('layout.format')}}</th>
                            <th title="Form Element" class="text-left" tabindex="0">{{trans('layout.custom_field.f_element')}}</th>
                            <th title="Required"  tabindex="0"> {{trans('layout.required')}}</th>
                            <th title="Action" class="text-center" tabindex="0"> {{trans('layout.action')}}</th>
                        </tr>
                        </thead>
                        <tbody>
                        </tbody>
                    </table>
                </div>
            </div>
        </div>
    </div>
    <!-- Modal -->
    <div class="modal fade" id="model-add-field" tabindex="-1" aria-labelledby="model-add-fieldLabel"
         aria-hidden="true">
        <div class="modal-dialog">
            <div class="modal-content">
                <div class="modal-header bg-info">
                    <h4 class="modal-title" id="model-add-fieldLabel"><i class="til_img"></i><strong>{{trans('layout.custom_field.connection')}} </strong></h4>
                    <button type="button" data-bs-dismiss="modal" aria-hidden="true" class="close"><span
                            aria-hidden="true">×</span></button>
                </div>
                <div class="modal-body form-save-fieldset">
                    <div class="form-group row ">
                        <label for="name" class="col-md-2 control-label required">{{trans('layout.field')}}</label>
                        <div class="col-md-10">

                            @include('partials.select.field', ['fieldset_id' => $fieldSet->id ])
                        </div>
                    </div>
                    <div class="form-group row ">
                        <label for="order" class="col-md-2 control-label ">{{trans('layout.order')}}</label>
                        <div class="col-7 row">
                            <div class="col-md-9"><input id="name" name="order" class=" form-control">
                            </div>

                        </div>
                        <div class="col-3 flex-column">
                            <input type="checkbox" name="required">
                            <label for="required">{{trans('layout.required')}}</label>
                        </div>
                    </div>
                </div>
                <div class="modal-footer">
                    <button data-bs-dismiss="modal" class="float-left btn btn-warning">{{trans('layout.cancel')}}</button>
                    <button class="float-right btn btn-info save-fieldset-button" data-parent-table="#sortable"
                            data-load-url="{{route('fields.fieldset.custom',$fieldSet->id)}}">{{trans('layout.save')}}
                    </button>
                </div>
            </div>
        </div>
    </div>
    @include('partials.table.modal-item', [
        'type' => 'danger',
        'name' => 'modal-confirm-delete',
        'title' => trans('table.confirm_delete'),
        'content' => trans('table.confirm_delete_msg'),
        'action_name' =>trans('table.delete'),
        'action_button_attributes' => [
            'class' => 'delete-crud-entry',
        ],
    ])

@endsection
@push('js')
    <script src="https://cdnjs.cloudflare.com/ajax/libs/jqueryui/1.12.1/jquery-ui.min.js"></script>
    <script>
        $(function () {
            $("#sortable tbody").sortable({
                cursor: "move",
                placeholder: "sortable-placeholder",
                helper: function (e, tr) {
                    let $originals = tr.children();
                    let $helper = tr.clone();
                    $helper.children().each(function (index) {
                        $(this).width($originals.eq(index).width());
                    });
                    return $helper;
                },
                update: function (event, ui) {

                    let obj = $('#sortable tbody tr').map(function (e) { /* example: .class */
                        return $(this).data('id'); /* example: id */
                    }).get();
                    $.ajax({
                        url: $('#sortable').data('order-url'),
                        type: 'post',
                        data: {
                            item: obj
                        },
                        success: data => {
                            if (data.error) {
                                Ems.showError(data.message);
                            } else {
                                Ems.showSuccess(data.message);
                                $('#sortable').DataTable().ajax.reload()
                            }
                        },
                        error: data => {
                            Ems.handleError(data);
                        }
                    });
                }
            }).disableSelection();
            $("#sortable").DataTable({
                processing: true, // show preloader when load data
                bInfo: false,
                responsive: true,
                bSort: false,
                bFilter: false,
                ajax: {
                    url: $('#sortable').data('url'),
                },
                language: {
                    "url":   window._locale
                },
                lengthChange: false,
                columns: [
                    {data: "handle", className: "text-left"},
                    {data: "order", className: "text-left"},
                    {data: "name", className: "text-left"},
                    {data: "format",className: "text-left"},
                    {data: "element",className: "text-left"},
                    {data: "required", className: "text-left"},
                    {data: "operations", className: "text-center"},

                ],
                createdRow: function(row, data, dataIndex) {
                    $(row).attr('data-id', data.id);
                }
            });
        });
    </script>
@endpush
