@extends('layouts.app')
@section('content')
    <div class=" rv-media-integrate-wrapper">
        <div id="main" class="p-2">
            <div class="row">
                <div class="col-md-9">
                    <form method="POST" action="{{route($type =='edit'? 'users.update':'users.store',$user->id)}}"
                          accept-charset="UTF-8">
                        @csrf
                        <div class="main-form content-page">
                            <figure class="text-center border-bottom">
                                <p>{{trans('layout.profile.info')}}</p>
                            </figure>
                            <div class="form-body">
                                <div class="row">
                                    <div class="form-group col-md-6">
                                        <label for="first_name" class="text-title-field control-label required">
                                            {{trans('layout.user.first_name')}}</label>
                                        <input class="form-control @error('first_name') is-invalid @enderror"
                                               name="first_name" type="text" id="first_name"
                                               value="{{old('first_name', $type =='edit'? $user->first_name:'')}}">
                                        @error('first_name')
                                        <span class="text-danger">{{ $message }}</span>
                                        @enderror
                                    </div>
                                    <div class="form-group col-md-6">
                                        <label for="name"
                                               class="text-title-field control-label ">{{trans('layout.user.last_name')}}</label>
                                        <input class="form-control" name="last_name" type="text" id="name"
                                               value="{{old('last_name',$type =='edit'? $user->last_name:'')}}">
                                    </div>
                                </div>
                                <div class="row">
                                    <div class="form-group col-md-6 ">
                                        <label for="username"
                                               class="text-title-field control-label required">{{trans('layout.user.username')}}</label>
                                        <input class="form-control @error('username') is-invalid @enderror"
                                               name="username" type="text" id="username"
                                               value="{{old('username',$type =='edit'? $user->username:'')}}">
                                        @error('username')
                                        <span class="text-danger">{{ $message }}</span>
                                        @enderror
                                    </div>
                                    <div class="form-group col-md-6">
                                        <label for="email"
                                               class="text-title-field control-label required">{{trans('layout.email')}}</label>
                                        <input class="form-control @error('email') is-invalid @enderror"
                                               placeholder="Ex: example@gmail.com"
                                               name="email" type="text" id="email"
                                               value="{{old('email',$type =='edit'? $user->email:'')}}">
                                        @error('email')
                                        <span class="text-danger">{{ $message }}</span>
                                        @enderror
                                    </div>

                                </div>

                                <div class="row">
                                    <div class="form-group col-md-6 ">
                                        <label for="location"
                                               class="text-title-field control-label ">{{trans('layout.location.location')}}</label>
                                        @include('partials.select.locations',['location_id'=>old('location_id',$user->location_id)])
                                    </div>
                                    <div class="form-group col-md-6">
                                        <label for="phone"
                                               class="text-title-field control-label ">{{trans('layout.user.phone')}}</label>
                                        <input class="form-control @error('phone') is-invalid @enderror"
                                               placeholder="Ex: 0366 403210"
                                               name="phone" type="text" id="phone"
                                               value="{{old('phone',$user->phone)}}">
                                        @error('phone')
                                        <span class="text-danger">{{ $message }}</span>
                                        @enderror
                                    </div>

                                </div>
                                <div class="row">
                                    <div class="form-group col-md-6">
                                        <label for="address"
                                               class="text-title-field control-label ">{{trans('layout.address')}}</label>
                                        <input class="form-control @error('address') is-invalid @enderror"
                                               name="address" type="text" id="address"
                                               value="{{old('address',$user->address)}}">
                                    </div>

                                    <div class="form-group col-md-6 ">
                                        <label for="title"
                                               class="text-title-field control-label ">{{trans('layout.user.title')}}</label>
                                        <input class="form-control" name="title"
                                               type="text" id="title" value="{{old('title',$user->title)}}">
                                    </div>
                                </div>
                                <div class="row">
                                    <div class="form-group col-md-8">
                                        <label for="notes"
                                               class="text-title-field control-label ">{{trans('layout.note')}}</label>
                                        <textarea class="form-control" rows="6"
                                                  name="notes" type="text"
                                                  id="notes">{{old('notes',$user->notes)}}</textarea>
                                    </div>

                                    <div class="form-group col-md-4 ">
                                        <div class="col-md-2 text-right">
                                            <label for="name"
                                                   class="text-title-field control-label ">{{trans('layout.image')}}</label>
                                        </div>
                                        <!-- Button trigger modal -->
                                        <div class="image-box image">
                                            <input type="hidden" name="profile" value="true">
                                            <input type="hidden" name="role_id" value="{{$user->roles[0]->id??''}}">
                                            <input type="hidden" name="image" value="{{old('image',$user->image)}}"
                                                   class="image-data">
                                            <div class="preview-image-wrapper " style="">
                                                <img
                                                    src="{{ $user->image? url_storage($user->image):asset('images/placeholder.png')}}"
                                                    alt="Preview image" class="preview_image" width="150">
                                                <a class="btn_remove_image" title="Remove image">
                                                    <i class="fa fa-times"></i>
                                                </a>
                                            </div>
                                            <div class="image-box-actions">
                                                <a href="#" class="btn_gallery" data-result="image"
                                                   data-bs-toggle="modal"
                                                   data-bs-target="#exampleModal">
                                                    {{trans('layout.choose_image')}}
                                                </a>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <div class="clearfix"></div>
                            </div>
                            <div class="widget meta-boxes d-flex justify-content-center">
                                <div class="widget-body">
                                    <div class="btn-set">
                                        <a href="{{route('roles.index')}}"
                                           class="float-left btn btn-warning mr-5">  {{trans('layout.cancel')}}</a>
                                        <button type="submit" name="submit" value="save" class="btn btn-info">
                                            <i class="fa fa-save"></i> {{trans('layout.update')}}
                                        </button>

                                    </div>
                                </div>
                            </div>
                        </div>
                    </form>
                </div>

                <div class="col-md-3 right-sidebar content-page">
                    <figure class="text-center border-bottom">
                        <p>{{trans('layout.profile.change_password')}}</p>
                    </figure>
                    <form method="POST" action="{{route('profile.password',$user->id)}}"
                          accept-charset="UTF-8">
                        @csrf
                        <div class="form-group ">
                            <label for="password"
                                   class="control-label required"> {{trans('layout.user.password')}}</label>
                            <input class="form-control  @error('password') is-invalid @enderror"
                                   name="password" type="password" id="password"
                                   value="">
                            @error('password')
                            <span class="text-danger">{{ $message }}</span>
                            @enderror
                        </div>
                        <div class="form-group">
                            <label for="password_confirmation" class="control-label required">
                                {{trans('layout.user.cf_password')}}</label>
                            <input class="form-control @error('password_confirmation') is-invalid @enderror"
                                   name="password_confirmation" type="password" id="password_confirmation"
                                   value="">
                            @error('password_confirmation')
                            <span class="text-danger">{{ $message }}</span>
                            @enderror
                        </div>
                        <div class="widget meta-boxes d-flex justify-content-center">
                            <div class="widget-body">
                                <div class="btn-set">
                                    <a href="{{route('roles.index')}}"
                                       class="float-left btn btn-warning mr-5">  {{trans('layout.cancel')}}</a>
                                    <button type="submit" name="submit" value="save" class="btn btn-info">
                                        <i class="fa fa-save"></i> {{trans('layout.update')}}
                                    </button>

                                </div>
                            </div>
                        </div>
                    </form>
                </div>
            </div>
        </div>
    </div>
    @include('partials.media')
@stop

