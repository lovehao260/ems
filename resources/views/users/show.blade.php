@extends('layouts.app')
@section('content')

    <div class=" rv-media-integrate-wrapper">
        {{ Breadcrumbs::render('users.show',$user->id) }}
        <div id="main" class="">
            <div class="table-wrapper">
                <div class="table-datatable row">
                    <div class="breadcrumb">
                        <h4>{{trans('layout.user.view')}}{{$user->first_name.' '.$user->last_name}} </h4></div>
                    <ul class="nav nav-tabs pb-3" id="user-tab" role="tablist">
                        <li class="nav-item" role="presentation">
                            <button class="nav-link active" id="detail-tab" data-bs-toggle="tab"
                                    data-bs-target="#detail" type="button"
                                    role="tab" aria-controls="detail" aria-selected="true">{{trans('layout.info')}}
                            </button>
                        </li>
                        <li class="nav-item" role="presentation">
                            <button class="nav-link" id="users-assets" data-bs-toggle="tab" data-bs-target="#assets"
                                    type="button"
                                    role="tab" aria-controls="assets" aria-selected="false"
                                    onclick="assetsDataTables()">{{trans('layout.assets')}}
                            </button>
                        </li>
                        <li class="nav-item" role="presentation">

                            <button class="nav-link" id="users-licenses" data-bs-toggle="tab" data-bs-target="#seats"
                                    type="button"
                                    role="tab" aria-controls="seats" aria-selected="false">{{trans('layout.licenses')}}
                            </button>
                        </li>
                        <li class="nav-item" role="presentation">

                            <button class="nav-link" id="users-accessories" data-bs-toggle="tab"
                                    data-bs-target="#user-accessories" type="button"
                                    role="tab" aria-controls="accessories" aria-selected="false"
                                    onclick="accessoriesDataTables()">{{trans('layout.accessories')}}
                            </button>
                        </li>
                        {{--            <li class="nav-item" role="presentation">--}}
                        {{--                <button class="nav-link" id="contact-tab" data-bs-toggle="tab" data-bs-target="#contact" type="button"--}}
                        {{--                        role="tab" aria-controls="contact" aria-selected="false" >Checkout history--}}
                        {{--                </button>--}}
                        {{--            </li>--}}
                    </ul>
                    <div class="tab-content" id="myTabContent">
                        <div class="tab-pane fade show active" id="detail" role="tabpanel" aria-labelledby="detail-tab">
                            <div class="row">

                                <div class="col-10">
                                    <table class="table table-striped  mt-2">
                                        <tbody>
                                        <tr>
                                            <td class="text-nowrap">{{trans('layout.name')}}</td>
                                            <td>{{$user->first_name.' '. $user->last_name}} </td>
                                        </tr>
                                        <tr>
                                            <td class="text-nowrap">{{trans('layout.user.username')}}</td>
                                            <td>{{$user->username}} </td>
                                        </tr>
                                        <tr>
                                            <td class="text-nowrap">{{trans('layout.email')}}</td>
                                            <td><a href="mailto:{{$user->email}}">{{$user->email}}</a></td>
                                        </tr>
                                        <tr>
                                            <td class="text-nowrap">{{trans('layout.user.last_login')}}</td>
                                            <td>Updating</td>
                                        </tr>
                                        <tr>
                                            <td class="text-nowrap">{{trans('table.created_at')}}</td>
                                            <td>{{date_from_database($user->created_at)}}</td>
                                        </tr>
                                        <tr>
                                            <td class="text-nowrap">{{trans('layout.user.enable_login')}}</td>
                                            <td>
                                                @if($user->activated)
                                                    <i class="fa fa-check text-success"
                                                       aria-hidden="true"></i> {{trans('layout.yes')}}
                                                @else
                                                    <i class="fa fa-times text-danger"
                                                       aria-hidden="true"></i> {{trans('layout.no')}}
                                                @endif
                                            </td>
                                        </tr>
                                        <tr>
                                            <td class="text-nowrap">{{trans('layout.note')}}</td>
                                            <td></td>
                                        </tr>

                                        <tr>
                                            <td class="text-nowrap">{{trans('layout.user.total_asset')}}</td>
                                            <td> {{count($user->assets)}}</td>
                                        </tr>
                                        <tr>
                                            <td class="text-nowrap">{{trans('layout.user.total_license')}}</td>
                                            <td> {{count($user->licenses)}}</td>
                                        </tr>
                                        <tr>
                                            <td class="text-nowrap">{{trans('layout.user.total_accessory')}}</td>
                                            <td> {{count($user->accessories)}}</td>
                                        </tr>
                                        </tbody>
                                    </table>
                                </div>
                                <div class="text-center col-2 pt-2 d-flex justify-content-center">
                                    <p class="avatar">{{first_character_name($user->first_name.''.$user->last_name)}}</p>
                                </div>
                            </div>
                        </div>
                        <div class="tab-pane fade" id="assets" role="tabpanel" aria-labelledby="seats-tab">
                            <table class="table table-striped table-assets" id="table-assets">
                                <thead>
                                <tr>
                                    <td>{{trans('layout.name')}}</td>
                                    <td>{{trans('layout.asset.tag')}}</td>
                                    <td>{{trans('layout.component.serial')}}</td>
                                    <td>{{trans('layout.purchase_cost')}}</td>
                                    <td>{{trans('layout.order_number')}}</td>
                                    <td>{{trans('layout.checkin')}} </td>
                                </tr>
                                </thead>
                                <tbody>
                                </tbody>
                            </table>
                        </div>
                        <div class="tab-pane fade" id="seats" role="tabpanel" aria-labelledby="seats-tab">
                            <table class="table table-striped table-licenses" id="table-seats">
                                <thead>
                                <tr>
                                    <td>{{trans('layout.name')}}</td>
                                    <td>{{trans('layout.component.serial')}}</td>
                                    <td>{{trans('layout.purchase_cost')}}</td>
                                    <td>{{trans('layout.user.purchase')}}</td>
                                    <td>{{trans('layout.order_number')}}</td>
                                    <td>{{trans('layout.checkin')}} </td>
                                </tr>
                                </thead>
                                <tbody>
                                </tbody>
                            </table>
                        </div>
                        <div class="tab-pane fade" id="user-accessories" role="tabpanel"
                             aria-labelledby="accessories-tab">
                            <table class="table table-striped table-hover vertical-middle dataTable no-footer table-accessories " id="table-accessories">
                                <thead>
                                <tr>
                                    <td>{{trans('layout.name')}}</td>
                                    <td>{{trans('layout.purchase_cost')}}</td>
                                    <td>{{trans('layout.checkin')}}</td>
                                </tr>
                                </thead>
                                <tbody>
                                </tbody>
                            </table>
                        </div>
                        <div class="tab-pane fade" id="contact" role="tabpanel" aria-labelledby="contact-tab">...</div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    @include('partials.table.modal-item', [
    'type' => 'info',
    'name' => 'modal-checkout-license',
    'title' => 'Checkout ',
     'with'=>'modal-lg',
    'content' => view('partials.checkout.license')->render(),
    'action_name' => 'Checkout',
    'action_button_attributes' => [
        'class' => 'confirm-checkout-button',
    ],
])
    <div class="modal fade modal-checkout-accessories " role="dialog">
        <div class="modal-dialog   ">
            <div class="modal-content">
                <div class="modal-header">
                    <h4 class="modal-title"><i class="til_img"></i><strong>Checkin</strong></h4>
                    <button type="button" class="close" data-bs-dismiss="modal" aria-hidden="true">
                        <span aria-hidden="true">×</span>
                    </button>
                </div>

                <div class="modal-body with-padding">
                    <div id="form-checkout">
                        <div class="row">
                            <div class="box box-default">

                                <div class="box-body">
                                    <!-- Asset name -->
                                    <div class="form-group row">
                                        <label class="col-sm-3 control-label"> Name</label>
                                        <div class="col-md-6">
                                            <p class="form-control-static licenses-name"></p>
                                        </div>
                                    </div>
                                    <div class="form-group ">
                                        <div class="row">
                                            <div class="col-md-3 ">
                                                <label for="checkin_at" class="text-title-field control-label">Checkin
                                                    Date
                                                </label>
                                            </div>
                                            <div class="col-7">
                                                <div class="input-group">
                                                    <input type="text" class="form-control datepicker"
                                                           data-date-format="yyyy-mm-dd"
                                                           name="checkin_at" value="{{old('checkin_at')}}"
                                                           autocomplete="off" id="checkin_at">
                                                    <div class="input-group-append">
                                                    <span class="input-group-text">
                                                        <i class="fa fa-calendar"></i>
                                                    </span>
                                                    </div>
                                                </div>

                                            </div>
                                        </div>
                                    </div>
                                    <div class="form-group row ">
                                        <label for="note" class="col-md-3 control-label">Notes</label>
                                        <div class="col-md-7">
                                            <textarea class=" form-control" id="note" name="note"></textarea>
                                        </div>
                                    </div>
                                </div>

                            </div> <!-- /.box-->
                        </div>
                    </div>
                </div>

                <div class="modal-footer">
                    <button class="float-left btn btn-warning" data-bs-dismiss="modal">cancel</button>
                    <button class="float-right btn btn-danger confirm-checkout-button">Checkin

                    </button>
                </div>
            </div>
        </div>
    </div>
@stop
@push('js')
    <script>
        $(document).ready(function () {
            let table = $('#table-seats').DataTable({
                searching: false,
                ajax: '{{route('users.show',$user->id)}}',
                lengthChange: false,
                language: {
                    "url":   window._locale
                },
                columns: [
                    {data: 'name', name: 'name', className: 'text-left column-key-name'},
                    {data: 'serial', name: 'serial', className: 'text-left column-key-serial'},
                    {data: 'purchase_cost', name: 'purchase_cost', className: 'text-left'},
                    {data: 'purchase_order', name: 'purchase_order', className: 'text-left'},
                    {data: 'order_number', name: 'order_number', className: 'text-left'},
                    {data: 'checkout', name: 'checkout'},
                ]
            });
        });

        function accessoriesDataTables() {
            if (!$.fn.dataTable.isDataTable('#table-accessories')) {
                $('#table-accessories').DataTable({
                    searching: false,
                    ajax: '{{route('users.accessories',$user->id)}}',
                    lengthChange: false,
                    language: {
                        "url":   window._locale
                    },
                    columns: [
                        {data: 'name', name: 'name', className: 'text-left column-key-name'},
                        {data: 'purchase_cost', name: 'purchase_cost', className: 'text-left'},
                        {data: 'checkout', name: 'checkout'},
                    ]
                });
            }
        }

        function assetsDataTables() {
            if (!$.fn.dataTable.isDataTable('#table-assets')) {
                $('#table-assets').DataTable({
                    searching: false,
                    ajax: '{{route('users.assets',$user->id)}}',
                    lengthChange: false,
                    language: {
                        "url":   window._locale
                    },
                    columns: [
                        {data: 'name', name: 'name', className: 'text-left column-key-name'},
                        {data: 'asset_tag', name: 'asset_tag', className: 'text-left'},
                        {data: 'serial', name: 'serial', className: 'text-left'},
                        {data: 'purchase_cost', name: 'purchase_cost', className: 'text-left'},
                        {data: 'order_number', name: 'order_number', className: 'text-left'},
                        {data: 'checkout', name: 'checkout'},
                    ]
                });
            }
        }
    </script>

@endpush
