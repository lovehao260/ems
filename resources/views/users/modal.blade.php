
@include('users.modal-item', [
    'type' => 'info',
    'name' => 'company-modal',
    'title' => trans('layout.company.company'),
    'content' => view('partials.new.company')->render(),
    'action_name' => trans('table.submit'),
    'action_button_attributes' => [
        'class' => 'save-company-button',
        'data-load-url' => route('companies.store'),
    ],
])

@include('users.modal-item', [
    'type' => 'info',
    'name' => 'manager-modal',
    'title' => trans('layout.manager'),
    'content' => view('partials.new.user')->render(),
    'action_name' => trans('table.submit'),
    'action_button_attributes' => [
        'class' => 'save-user-button',
        'data-load-url' => route('users.store'),
    ],
])
