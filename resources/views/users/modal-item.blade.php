
<!-- Modal -->
<div class="modal fade" id="{{ $name }}" tabindex="-1" aria-labelledby="exampleModalLabel" aria-hidden="true">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header bg-{{ $type }}">
                <h4 class="modal-title"><i class="til_img"></i><strong>{{ $title }}</strong></h4>
                <button type="button" class="close" data-bs-dismiss="modal" aria-hidden="true">
                    <span aria-hidden="true">×</span>
                </button>
            </div>
            <div class="modal-body">
                <div>{!! $content !!}</div>
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-secondary" data-bs-dismiss="modal">{{trans('table.close')}}</button>
                <button class="float-right btn btn-{{ $type }} {{ Arr::get($action_button_attributes, 'class') }}"
                    {!! Html::attributes(Arr::except($action_button_attributes, 'class')) !!}>{{ $action_name }}
                </button>
            </div>
        </div>
    </div>
</div>
<!-- end Modal -->
