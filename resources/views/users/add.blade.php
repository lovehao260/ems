@extends('layouts.app')
@section('content')
    <div class=" rv-media-integrate-wrapper">
        {{ Breadcrumbs::render('users.add') }}
        <div id="main" class=" p-3">
            <form method="POST" action="{{route('users.store')}}" accept-charset="UTF-8">
                @csrf
                <div class="row">
                    <div class="col-md-9 ">
                        <div class="main-form content-page">
                            <div class="form-body">
                                <div class="row">
                                    <div class="form-group col-md-6">
                                        <label for="first_name" class="text-title-field control-label required">
                                            {{trans('layout.user.first_name')}}</label>
                                        <input class="form-control @error('first_name') is-invalid @enderror"
                                               name="first_name" type="text" id="first_name"
                                               value="{{old('first_name',$user->first_name??"")}}">
                                        @error('first_name')
                                        <span class="text-danger">{{ $message }}</span>
                                        @enderror
                                    </div>
                                    <div class="form-group col-md-6">
                                        <label for="name"
                                               class="text-title-field control-label ">{{trans('layout.user.last_name')}}</label>
                                        <input class="form-control" name="last_name" type="text" id="name"
                                               value="{{old('last_name',$user->last_name??"")}}">
                                    </div>
                                </div>
                                <div class="row">
                                    <div class="form-group col-md-6 ">
                                        <label for="username"
                                               class="text-title-field control-label required">{{trans('layout.user.username')}}</label>
                                        <input class="form-control @error('username') is-invalid @enderror"
                                               name="username" type="text" id="username" value="{{old('username')}}">
                                        @error('username')
                                        <span class="text-danger">{{ $message }}</span>
                                        @enderror
                                    </div>
                                    <div class="form-group col-md-6">
                                        <label for="email"
                                               class="text-title-field control-label required">{{trans('layout.email')}}</label>
                                        <input class="form-control @error('email') is-invalid @enderror"
                                               placeholder="Ex: example@gmail.com"
                                               name="email" type="text" id="email" value="{{old('email')}}">
                                        @error('email')
                                        <span class="text-danger">{{ $message }}</span>
                                        @enderror
                                    </div>

                                </div>
                                <div class="row">
                                    <div class="form-group col-md-6">
                                        <label for="password"
                                               class="control-label required"> {{trans('layout.user.password')}}</label>
                                        <input class="form-control  @error('password') is-invalid @enderror"
                                               name="password" type="password" id="password"
                                               value="{{old('password')}}">
                                        @error('password')
                                        <span class="text-danger">{{ $message }}</span>
                                        @enderror
                                    </div>
                                    <div class="form-group col-md-6">
                                        <label for="password_confirmation" class="control-label required">
                                            {{trans('layout.user.cf_password')}}</label>
                                        <input class="form-control @error('password_confirmation') is-invalid @enderror"
                                               name="password_confirmation" type="password" id="password_confirmation"
                                               value="{{old('password_confirmation')}}">
                                        @error('password_confirmation')
                                        <span class="text-danger">{{ $message }}</span>
                                        @enderror
                                    </div>
                                </div>
                                <div class="row">
                                    <div class="form-group col-md-6">
                                        <label for="password_confirmation"
                                               class="control-label ">{{trans('layout.company.company')}}</label>
                                        @include('partials.select.company',['company_id'=>old('company_id',$user->company_id??"")])
                                    </div>

                                    <div class="form-group col-md-6">
                                        <label for="password_confirmation" class="control-label ">{{trans('layout.manager')}}</label>
                                        @include('partials.select.manager',['manager_id'=>old('manager_id',$user->manager_id??"")])
                                    </div>
                                </div>
                                <div class="row">
                                    <div class="form-group col-md-6 ">
                                        <label for="phone" class="text-title-field control-label ">{{trans('layout.location.location')}}</label>
                                        @include('partials.select.locations',['location_id'=>old('location_id',$user->location_id??"")])
                                    </div>
                                    <div class="form-group col-md-6">
                                        <label for="phone" class="text-title-field control-label ">{{trans('layout.user.phone')}}</label>
                                        <input class="form-control @error('phone') is-invalid @enderror"
                                               placeholder="Ex: 0366 403210"
                                               name="phone" type="text" id="phone"
                                               value="{{old('phone',$user->phone??"")}}">
                                        @error('phone')
                                        <span class="text-danger">{{ $message }}</span>
                                        @enderror
                                    </div>

                                </div>
                                <div class="row">
                                    <div class="form-group col-md-6">
                                        <label for="address" class="text-title-field control-label ">{{trans('layout.address')}}</label>
                                        <input class="form-control @error('address') is-invalid @enderror"
                                               name="address" type="text" id="address"
                                               value="{{old('address',$user->address??"")}}">
                                    </div>

                                    <div class="form-group col-md-6 ">
                                        <label for="title"
                                               class="text-title-field control-label ">{{trans('layout.user.title')}}</label>
                                        <input class="form-control" name="title"
                                               type="text" id="title" value="{{old('title',$user->title??"")}}">
                                    </div>
                                </div>
                                <div class="row">
                                    <div class="form-group col-md-8">
                                        <label for="notes" class="text-title-field control-label ">{{trans('layout.note')}}</label>
                                        <textarea class="form-control" rows="6"
                                                  name="notes" type="text"
                                                  id="notes">{{old('notes',$user->notes??"")}}</textarea>
                                    </div>

                                    <div class="form-group col-md-4 ">
                                        <div class="col-md-2 text-right">
                                            <label for="name" class="text-title-field control-label ">{{trans('layout.image')}}</label>
                                        </div>
                                        <!-- Button trigger modal -->
                                        <div class="image-box image">
                                            <input type="hidden" name="image" value="{{old('image',$user->image??"")}}"
                                                   class="image-data">
                                            <div class="preview-image-wrapper " style="">
                                                <img
                                                    src="{{ isset($user->image)? url_storage($user->image):asset('images/placeholder.png')}}"
                                                    alt="user image" class="preview_image" width="150">
                                                <a class="btn_remove_image" title="Remove image">
                                                    <i class="fa fa-times"></i>
                                                </a>
                                            </div>
                                            <div class="image-box-actions">
                                                <a href="#" class="btn_gallery" data-result="image"
                                                   data-bs-toggle="modal"
                                                   data-bs-target="#exampleModal">
                                                    {{trans('layout.choose_image')}}
                                                </a>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <div class="clearfix"></div>
                            </div>
                        </div>
                    </div>
                    <div class="col-md-3 right-sidebar content-page">
                        <div class=" meta-boxes form-actions form-actions-default action-horizontal">
                            <div class="widget-title">
                                <label>{{trans('layout.public')}}</label>
                            </div>
                            <div class="widget-body">
                                <div class="btn-set">
                                    <a href="{{route('users.index')}}"
                                       class="float-left btn btn-warning mr-5">{{trans('layout.cancel')}}</a>
                                    <button type="submit" name="submit" value="save" class="btn btn-info">
                                        <i class="fa fa-save"></i> {{trans('layout.save')}}
                                    </button>

                                </div>
                            </div>
                        </div>
                        <div id="waypoint"></div>


                        <div class="widget meta-boxes">
                            <div class="widget-title">
                                <label for="role_id" class="control-label">{{trans('layout.role.roles')}}</label>
                            </div>
                            <div class="widget-body">
                                <div class="ui-select-wrapper">
                                    <select class="form-control roles-list ui-select ui-select @error('role_id') is-invalid @enderror" id="role_id"
                                            name="role_id">
                                        <option value="">{{trans('layout.user.role')}}</option>
                                        @foreach(get_data_select('roles') as $key=> $item)
                                            <option
                                                value="{{$item->id}}" {{old('role_id')==$item->id?'selected':''}}>{{$item->name}}</option>
                                        @endforeach
                                    </select>
                                    @error('role_id')
                                    <span class="text-danger">{{ $message }}</span>
                                    @enderror
                                    <svg class="svg-next-icon svg-next-icon-size-16">
                                        <use xmlns:xlink="http://www.w3.org/1999/xlink"
                                             xlink:href="#select-chevron"></use>
                                    </svg>
                                </div>
                            </div>
                        </div>

                    </div>
                </div>

            </form>
        </div>
    </div>
    @include('users.modal')
    @include('partials.media')
@stop


