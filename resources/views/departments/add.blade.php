@extends('layouts.app')
@section('content')
    <div class=" rv-media-integrate-wrapper p-3 container">
        <div id="main" class="container">
            <form method="POST" action="{{route( 'departments.store')}}" accept-charset="UTF-8">
                @csrf
                <div class="row">
                    <div class="col-md-12 ">
                        <div class="main-form content-page">
                            <div class="form-body">
                                <div class="form-group row">
                                    <div class="col-3">
                                        <label for="name" class="text-title-field control-label required">
                                            Department  Name</label>
                                    </div>
                                    <div class="col-9">
                                        <input class="form-control @error('name') is-invalid @enderror"
                                               placeholder="X Name"
                                               data-counter="120" name="name"
                                               type="text" id="first_name" value="{{old('name')}}">
                                        @error('name')
                                        <span class="text-danger">{{ $message }}</span>
                                        @enderror
                                    </div>

                                </div>
                                <div class="form-group row">
                                    <div class="col-3">
                                        <label for="company_id" class="text-title-field control-label ">
                                            Company</label>
                                    </div>
                                    <div class="col-9">
                                        @include('partials.select.company')
                                    </div>

                                </div>
                                <div class="form-group row">
                                    <div class="col-3">
                                        <label for="manager_id" class="text-title-field control-label ">
                                            Manager</label>
                                    </div>
                                    <div class="col-9">
                                        @include('partials.select.manager')
                                    </div>
                                </div>

                                <div class="form-group row">
                                    <div class="col-3">
                                        <label for="name" class="text-title-field control-label ">Image</label>
                                    </div>
                                    <div class="col-9">

                                        <!-- Button trigger modal -->
                                        <div class="image-box image">
                                            <input type="hidden" name="image" value="{{old('image')}}" class="image-data">
                                            <div class="preview-image-wrapper " style="">
                                                <img src="{{asset('images/placeholder.png')}}" alt="Preview image"
                                                     class="preview_image" width="150">
                                                <a class="btn_remove_image" title="Remove image">
                                                    <i class="fa fa-times"></i>
                                                </a>
                                            </div>
                                            <div class="image-box-actions">
                                                <a href="#" class="btn_gallery" data-result="image" data-bs-toggle="modal"
                                                   data-bs-target="#exampleModal">
                                                    Choose image
                                                </a>
                                            </div>
                                        </div>
                                    </div>
                                </div>

                                <div class="clearfix"></div>
                            </div>
                            <div class="widget meta-boxes d-flex justify-content-center">
                                <div class="widget-body">
                                    <div class="btn-set">
                                        <button type="submit" name="submit" value="save" class="btn btn-info">
                                            <i class="fa fa-save"></i> Save
                                        </button>

                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>

                </div>

            </form>
        </div>
    </div>
    <!-- Modal -->
    <div class="modal fade" id="exampleModal" tabindex="-1" aria-labelledby="exampleModalLabel" aria-hidden="true">
        <div class="modal-dialog modal-xl">
            <div class="modal-content">
                <div class="modal-header modal-header-media">
                    <h4 class="modal-title"><i class="til_img"></i><strong>Media</strong></h4>
                    <button type="button" class="btn-close" data-bs-dismiss="modal" aria-label="Close"></button>
                </div>
                <div class="modal-body">
                    <media-index :insert="true"></media-index>
                </div>
            </div>
        </div>
    </div>

@stop

@push('js')
    <script>
        $(document).on('click', '.save-user-button', event => {
            event.preventDefault();
            let _self = $(event.currentTarget);
            let values = {}
            $(".input-value").each(function () {
                values[this.name] = this.value
            });
            _self.addClass('button-loading');

            $.ajax({
                url: _self.data('load-url'),
                type: 'POST',
                headers: {
                    'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                },
                data: {
                    first_name: values.first_name,
                    last_name: values.last_name,
                    username: values.username,
                    email: values.email,
                    password: values.password,
                    password_confirmation: values.password_confirmation
                },
                success: data => {
                    if (data.error) {
                        Ems.showError(data.message);
                    } else {
                        Ems.showSuccess(data.message);
                        _self.closest('.modal').modal('hide');
                        _self.removeClass('button-loading');
                    }

                },
                error: data => {
                    Ems.handleError(data);
                    _self.removeClass('button-loading');
                }
            });
        });
    </script>
@endpush
