@extends('layouts.app')
@section('content')
    <div class=" rv-media-integrate-wrapper">
        {{ Breadcrumbs::render('roles.add') }}
        <div id="main" class="container">
            <form method="POST" action="{{route('roles.store')}}" accept-charset="UTF-8">
                @csrf
                <div class="row">
                    <div class="col-md-12 ">
                        <div class="main-form content-page">
                            <div class="form-body">
                                <div class="form-group row">
                                    <div class="col-md-2 text-right">
                                        <label for="name" class="text-title-field control-label required">
                                            {{trans('layout.name')}}</label>
                                    </div>
                                    <div class="col-md-10">
                                        <input class="form-control @error('name') is-invalid @enderror"
                                               name="name" type="text" id="name"
                                               value="{{old('name')}}">
                                        @error('name')
                                        <span class="text-danger">{{ $message }}</span>
                                        @enderror
                                    </div>
                                </div>
                                <div class="form-group row">
                                    <div class="col-md-2 text-right">
                                        <label for="description" class="text-title-field control-label required">
                                            {{trans('layout.description')}}</label>
                                    </div>
                                    <div class="col-md-10">
                                        <textarea class="form-control @error('description') is-invalid @enderror"
                                                  name="description" type="text" id="description" rows="3">
                                            {{old('description')}}
                                        </textarea>
                                        @error('description')
                                        <span class="text-danger">{{ $message }}</span>
                                        @enderror
                                    </div>
                                </div>
                                <div class="form-group row">
                                    <div class="col-md-2 text-right">

                                    </div>
                                    <div class="col-md-10">
                                        <div class="custom-control custom-switch custom-switch-sm">
                                            <input type="checkbox" class="custom-control-input" id="customSwitch2"
                                                   value="1" {{old('is_default')?"checked":""}}  name="is_default">
                                            <label class="custom-control-label"
                                                   for="customSwitch2">{{trans('layout.is_default')}}?</label>
                                        </div>
                                    </div>
                                </div>
                                <div class="form-group row">
                                    <div class="col-md-2 text-right">
                                        <label for="bs_main"
                                               class="text-title-field control-label required"> {{trans('layout.role.flags')}}</label>
                                    </div>
                                    <div class="col-md-10">
                                        <div class="tree_main">
                                            <ul id="bs_main" class="main_ul">
                                                <li id="bs_1">
                                                    <span class="plus minus">&nbsp;</span>
                                                    <input type="checkbox" id="c_bs_1"/>
                                                    <span> {{trans('layout.role.all_permissions')}}</span>
                                                    <ul id="bs_l_1" class="sub_ul">
                                                        @foreach($permissions as $key=> $item)
                                                            <li class="bf_1s">
                                                                <span
                                                                    class=" {{count($item) > 1 ? 'plus':'permission'}}">&nbsp;</span>
                                                                <input type="checkbox" id="c_bf_1"/>
                                                                <span class="text-capitalize">{{$key}} </span>
                                                                @if(count($item) >1)
                                                                    <ul id="bf_l_1" style="display: none"
                                                                        class="inner_ul">
                                                                        @foreach($item as $i=>$value)
                                                                            <li class="io_1">
                                                                                <input type="checkbox"
                                                                                       id="io_{{$value->id}}"
                                                                                       name="permissions[]"
                                                                                       value="{{$value->id}}"/><span>{{$value->name}} </span>
                                                                            </li>
                                                                        @endforeach

                                                                    </ul>
                                                                @endif
                                                            </li>
                                                        @endforeach
                                                    </ul>
                                                </li>
                                            </ul>
                                        </div>
                                        <div class="clearfix"></div>
                                    </div>
                                    <div class="widget meta-boxes d-flex justify-content-center">
                                        <div class="widget-body">
                                            <div class="btn-set">
                                                <a href="{{route('roles.index')}}"
                                                   class="float-left btn btn-warning mr-5"> {{trans('layout.cancel')}}</a>
                                                <button type="submit" name="submit" value="save" class="btn btn-info">
                                                    <i class="fa fa-save"></i> {{trans('layout.save')}}
                                                </button>

                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </form>
        </div>
    </div>

@stop

@push('js')
    <script>
        $(document).ready(function () {
            $(".plus").click(function () {
                $(this).toggleClass("minus").siblings("ul").toggle();
            })

            $("input[type=checkbox]").click(function () {
                $(this).siblings("ul").find("input[type=checkbox]").prop('checked', $(this).prop('checked'));
                //}
            })

            $("input[type=checkbox]").change(function () {
                let sp = $(this).attr("id");
                if (sp.substring(0, 4) === "c_io") {
                    let ff = $(this).parents("ul[id^=bf_l]").attr("id");
                    if ($('#' + ff + ' > li input[type=checkbox]:checked').length == $('#' + ff + ' > li input[type=checkbox]').length) {
                        $('#' + ff).siblings("input[type=checkbox]").prop('checked', true);
                        check_fst_lvl(ff);
                    } else {
                        $('#' + ff).siblings("input[type=checkbox]").prop('checked', false);
                        check_fst_lvl(ff);
                    }
                }

                if (sp.substring(0, 4) === "c_bf") {
                    let ss = $(this).parents("ul[id^=bs_l]").attr("id");
                    if ($('#' + ss + ' > li input[type=checkbox]:checked').length == $('#' + ss + ' > li input[type=checkbox]').length) {
                        $('#' + ss).siblings("input[type=checkbox]").prop('checked', true);
                        check_fst_lvl(ss);
                    } else {
                        $('#' + ss).siblings("input[type=checkbox]").prop('checked', false);
                        check_fst_lvl(ss);
                    }
                }
            });

        })

        function check_fst_lvl(dd) {
            //var ss = $('#' + dd).parents("ul[id^=bs_l]").attr("id");
            let ss = $('#' + dd).parent().closest("ul").attr("id");
            if ($('#' + ss + ' > li input[type=checkbox]:checked').length == $('#' + ss + ' > li input[type=checkbox]').length) {
                //$('#' + ss).siblings("input[id^=c_bs]").prop('checked', true);
                $('#' + ss).siblings("input[type=checkbox]").prop('checked', true);
            } else {
                //$('#' + ss).siblings("input[id^=c_bs]").prop('checked', false);
                $('#' + ss).siblings("input[type=checkbox]").prop('checked', false);
            }

        }

        function pageLoad() {
            $(".plus").click(function () {
                $(this).toggleClass("minus").siblings("ul").toggle();
            })
        }
    </script>
@endpush
