@extends('layouts.app')
@section('content')
    <div class=" rv-media-integrate-wrapper">
        {{ Breadcrumbs::render('companies.add') }}
        <div id="main" class="container">
            <form method="POST" action="{{route('companies.update',$company->id)}}" accept-charset="UTF-8">
                @csrf
                <div class="row">
                    <div class="col-md-12 ">
                        <div class="main-form content-page">
                            <div class="form-body">
                                <div class="form-group row ">
                                    <div class="col-2">
                                        <label for="name" class="text-title-field control-label required">
                                            {{trans('layout.name')}}</label>
                                    </div>
                                    <div class="col-10">
                                        <input class="form-control @error('name') is-invalid @enderror"
                                         name="name" type="text" id="first_name" value=" {{old('name',$company->name)}}">
                                        @error('name')
                                        <span class="text-danger">{{ $message }}</span>
                                        @enderror
                                    </div>

                                </div>
                                <div class="form-group row">
                                    <div class="col-2">
                                        <label for="name" class="text-title-field control-label ">{{trans('layout.image')}}</label>
                                    </div>
                                    <!-- Button trigger modal -->
                                    <div class="col-10">
                                        <div class="image-box image">
                                            <input type="hidden" name="image" value="{{old('image',$company->image)}}"
                                                   class="image-data">
                                            <div class="preview-image-wrapper " style="">
                                                <img src="{{ $company->image? url_storage($company->image):asset('images/placeholder.png')}}"
                                                    class="preview_image" width="150">
                                                <a class="btn_remove_image" title="Remove image">
                                                    <i class="fa fa-times"></i>
                                                </a>
                                            </div>
                                            <div class="image-box-actions">
                                                <a href="#" class="btn_gallery" data-result="image"
                                                   data-bs-toggle="modal"
                                                   data-bs-target="#exampleModal">
                                                    {{trans('layout.choose_image')}}
                                                </a>
                                            </div>
                                        </div>
                                    </div>

                                </div>

                                <div class="clearfix"></div>
                            </div>
                            <div class="widget meta-boxes d-flex justify-content-center">
                                <div class="widget-body">
                                    <div class="btn-set">
                                        <a  href="{{route('companies.index')}}" class="float-left btn btn-warning mr-5"> {{trans('layout.cancel')}}</a>
                                        <button type="submit" name="submit" value="save" class="btn btn-info">
                                            <i class="fa fa-save"></i> {{trans('layout.update')}}
                                        </button>

                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>

                </div>

            </form>
        </div>
    </div>
    @include('partials.media')

@stop

