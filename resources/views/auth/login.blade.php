@extends('layouts.master')

@section('content')
    <p>{{trans('auth.sign_in_below')}}</p>
    {!! Form::open(['route' => 'login', 'class' => 'login-form ']) !!}
    <div class="form-group mb-3  @error('username') has-error @enderror" id="emailGroup">
        <label>{{ trans('auth.login.username') }}</label>
        {!! Form::text('username', old('username', app()->environment('local') ? 'calc' : null),
                ['class' => 'form-control ', 'placeholder' => trans('auth.login.username')]) !!}
        @error('email')
              <span class="text-danger">{{ $message }}</span>
        @enderror
    </div>

    <div class="form-group mb-3" id="passwordGroup">
        <label>{{ trans('auth.login.password') }}</label>
        {!! Form::input('password', 'password', (app()->environment('local') ? 'calc5344' : null), ['class' => 'form-control', 'placeholder' => trans('auth.login.password')]) !!}
    </div>

    <div>
        <label>
            {!! Form::checkbox('remember', '1', true, ['class' => 'hrv-checkbox']) !!} {{ trans('auth.login.remember') }}
        </label>
    </div>
    <br>

    <button type="submit" class="btn btn-block login-button">
        <span class="signin">{{ trans('auth.login.login') }}</span>
    </button>
    <div class="clearfix"></div>

    <br>
    <p><a class="lost-pass-link" href="{{ route('password.request') }}"
          title="{{ trans('auth.forgot_password.title') }}">{{ trans('auth.lost_your_password') }}</a></p>

    {!! Form::close() !!}
@endsection
