@extends('layouts.app')
@section('content')
    <div class=" rv-media-integrate-wrapper">
        {{ Breadcrumbs::render('models.add') }}
        <div id="main" class="container">
            <form method="POST" action="{{route('models.store')}}" accept-charset="UTF-8" id="model-form">
                @csrf
                <div class="row">
                    <div class="col-md-12 ">
                        <div class="main-form content-page">
                            <div class="form-body">
                                <div class="form-group row">
                                    <div class="col-md-2 text-right">
                                        <label for="name" class="text-title-field control-label required">
                                            {{trans('layout.name')}}</label>
                                    </div>
                                    <div class="col-md-10">
                                        <input class="form-control @error('name') is-invalid @enderror"
                                               name="name"
                                               type="text" id="name" value="{{old('name',$model->name ?? '')}}">
                                        @error('name')
                                        <span class="text-danger">{{ $message }}</span>
                                        @enderror
                                    </div>
                                </div>
                                <div class="form-group  ">
                                    <div class="row">
                                        <div class="col-md-2 text-right">
                                            <label for="select-company" class="control-label required">
                                                {{trans('layout.category.category')}}</label>
                                        </div>
                                        <div class="col-md-10 w-100">
                                            @include('partials.select.category', ['category_type' => 'component',
                                                        'category_id'=>old('category_id',$model->category_id ?? '') ])
                                        </div>

                                    </div>
                                </div>
                                <div class="form-group ">
                                    <div class="d-flex ">
                                        <div class="col-md-2 text-right">
                                            <label for="model_number" class="text-title-field control-label ">
                                                {{trans('layout.model.no')}}</label>
                                        </div>
                                        <input class="form-control @error('model_number') is-invalid @enderror"
                                               name="model_number" type="text" id="model_number"
                                               value="{{old('model_number',$model->model_number ?? '')}}">
                                        @error('model_number')
                                        <span class="text-danger">{{ $message }}</span>
                                        @enderror
                                    </div>
                                </div>
                                <div class="form-group ">
                                    <div class="row ">
                                        <div class="col-md-2 text-right">
                                            <label for="eol" class="text-title-field control-label">
                                                EOL</label>
                                        </div>
                                        <div class="col-3">
                                            <div class="input-group">
                                                <input type="text" class="form-control"
                                                       value=" {{old('eol',$model->eol ?? '')}}"
                                                       name="eol">
                                                <div class="input-group-append">
                                                    <span class="input-group-text">   {{trans('layout.months')}}</span>
                                                </div>
                                            </div>
                                        </div>
                                    </div>

                                </div>
                                <div class="form-group ">
                                    <div class="row ">
                                        <div class="col-md-2 text-right">
                                            <label for="fieldset_id" class="text-title-field control-label">   {{trans('layout.custom_field.fieldset')}}</label>
                                        </div>
                                        <div class="col-6">
                                            <div class="input-group">
                                                <select name="fieldset_id" class="select-search-full model-field-custom"
                                                        data-url-field="{{route('api.fields.getModelField')}}">
                                                    <option value="">  {{trans('layout.model.no_field')}}</option>
                                                    @foreach($custom_fieldset_list as $key=> $field)
                                                        <option value="{{$field->id}}" {{old('fieldset_id')==$field->id?'selected':''}}>{{$field->name}}</option>
                                                    @endforeach
                                                </select>
                                            </div>
                                        </div>
                                        <div class="col-4">
                                            <label class="m-l-xs">
                                                <input name="add_default_values" type="checkbox" value="1"
                                                       class="js-default-values-toggler" {{old('add_default_values')=='1'?'checked':''}}>
                                                {{trans('layout.model.add_default')}}
                                            </label>
                                        </div>
                                    </div>
                                </div>
                                <div class="form-group default-field-model hidden">
                                    <div class="row ">
                                        <div class="col-md-2 text-right">
                                            <label for="fieldset_id" class="text-title-field control-label">
                                                {{trans('layout.model.default')}}
                                            </label>
                                        </div>
                                        <div class="col-10">
                                            <div class="card">
                                                <div class="card-body">
                                                    <div class="asset-field">

                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>

                                <div class="form-group ">
                                    <div class="row">
                                        <div class="col-md-2 text-right">
                                            <label for="note" class="text-title-field control-label ">{{trans('layout.note')}}</label>
                                        </div>
                                        <div class="col-md-10">
                                            <textarea name="note" type="text" id="note"
                                                      class="form-control">{{old('note',$model->note ?? '')}}</textarea>
                                        </div>
                                    </div>
                                </div>
                                <div class="form-group ">
                                    <div class="d-flex">
                                        <div class="col-md-2 text-right">
                                            <label for="name" class="text-title-field control-label ">{{trans('layout.image')}}</label>
                                        </div>
                                        <!-- Button trigger modal -->
                                        <div class="image-box image">
                                            <input type="hidden" name="image" value="{{old('image')}}"
                                                   class="image-data">
                                            <div class="preview-image-wrapper " style="">
                                                <img src="{{asset('images/placeholder.png')}}" alt="Preview image"
                                                     class="preview_image" width="150">
                                                <a class="btn_remove_image" title="Remove image">
                                                    <i class="fa fa-times"></i>
                                                </a>
                                            </div>
                                            <div class="image-box-actions">
                                                <a href="#" class="btn_gallery" data-result="image"
                                                   data-bs-toggle="modal"
                                                   data-bs-target="#exampleModal">
                                                    {{trans('layout.choose_image')}}
                                                </a>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <div class="clearfix"></div>
                            </div>
                            <div class="widget meta-boxes d-flex justify-content-center">
                                <div class="widget-body">
                                    <div class="btn-set">
                                        <a href="{{route('models.index')}}" class="float-left btn btn-warning mr-5">{{trans('layout.cancel')}}</a>
                                        <button type="submit" name="submit" value="save" class="btn btn-info">
                                            <i class="fa fa-save"></i> {{trans('layout.save')}}
                                        </button>

                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>

                </div>

            </form>
        </div>
    </div>
    <!-- Modal -->
    @include('partials.media')

@stop



