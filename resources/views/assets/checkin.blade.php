@extends('layouts.app')
@section('content')
    <div class=" rv-media-integrate-wrapper ">
        {{ Breadcrumbs::render('assets.checkin',$asset->id) }}
        <div id="main">
            <form method="POST" action="{{route( 'assets.checkin.post',$asset->id)}}" accept-charset="UTF-8">
                @csrf
                <div id="form-checkout-license">
                    <div class="box-body">
                        <div class="col-md-6 col-12 ">
                            <div class="main-form content-page">
                                <div class="box-header with-border border-bottom mb-3">
                                    <h4 class="box-title licenses-name"> {{$asset->name}}</h4>
                                </div>
                                <div id="form-checkout-license">
                                    <div class="row">
                                        <div class="box box-default">
                                            <div class="box-body">
                                                <!-- Asset name -->
                                                <div class="form-group row">
                                                    <label class="col-sm-3 control-label text-right">
                                                        {{trans('layout.model.model')}}
                                                    </label>
                                                    <div class="col-md-6">
                                                        <p class="form-control-static licenses-name">{{$asset->model->name}}</p>
                                                    </div>
                                                </div>
                                                <!-- Serial -->
                                                <div class="form-group ">
                                                    <div class="row">
                                                        <div class="col-md-3 text-right">
                                                            <label for="asset_name"
                                                                   class="text-title-field control-label ">
                                                                {{trans('layout.asset.asset')}} {{trans('layout.name')}}</label>
                                                        </div>
                                                        <div class="col-md-9">
                                                            <input
                                                                class="form-control @error('name') is-invalid @enderror"
                                                                name="name"
                                                                type="text" id="asset_name"
                                                                value="{{old('name',$asset->name)}}">
                                                            @error('name')
                                                            <span class="text-danger">{{ $message }}</span>
                                                            @enderror
                                                        </div>
                                                    </div>
                                                </div>
                                                <div id="status_user" class="form-group row">
                                                    <label for="assigned_to"
                                                           class="col-md-3 control-label text-right">  {{trans('layout.status')}}</label>
                                                    <div class="col-md-6 ">
                                                        @include('partials.select.status', ['status_id' => old('status_id',$asset->status_id) ])
                                                    </div>
                                                </div>
                                                <div id="assigned_location" class="form-group row ">
                                                    <label for="assigned_location"
                                                           class="col-md-3 control-label text-right">
                                                        {{trans('layout.location.location')}}
                                                    </label>
                                                    <div class="col-md-6 ">
                                                        @include('partials.select.locations')
                                                    </div>
                                                </div>
                                                <!-- Note -->
                                                <div class="form-group ">
                                                    <div class="row ">
                                                        <div class="col-md-3 text-right">
                                                            <label for="checkin_at"
                                                                   class="text-title-field control-label required">
                                                                {{trans('layout.checkin_date')}}
                                                            </label>
                                                        </div>
                                                        <div class="col-6">
                                                            <div class="input-group">
                                                                <input type="text" class="form-control datepicker"
                                                                       data-date-format="yyyy-mm-dd" name="checkin_at"
                                                                       value="{{old('checkin_at',\Illuminate\Support\Carbon::today()->toDateString())}}"
                                                                       autocomplete="off" id="checkin_at">
                                                                <div class="input-group-append">
                                                                     <span class="input-group-text">
                                                                          <i class="fa fa-calendar"></i>
                                                                     </span>
                                                                </div>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>

                                                <div class="form-group row ">
                                                    <label for="note" class="col-md-3 control-label text-right">
                                                        {{trans('layout.note')}}
                                                    </label>
                                                    <div class="col-md-7">
                                                        <textarea class=" form-control" id="note"
                                                                  name="note"></textarea>
                                                    </div>
                                                </div>
                                            </div>
                                            <div
                                                class="box-header with-border border-top mt-3 d-flex justify-content-center">
                                                <div class="pt-3">
                                                    <a href="{{route('assets.index')}}"
                                                       class="float-left btn btn-warning mr-5">
                                                        {{trans('layout.cancel')}}
                                                    </a>
                                                    <button class="float-right btn btn-info ml-5" type="submit">
                                                        {{trans('layout.checkin')}}
                                                    </button>
                                                </div>
                                            </div>
                                        </div> <!-- /.box-->
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </form>
        </div>
    </div>
    <!-- Modal -->
@stop
