@extends('layouts.app')
@section('content')
    <div class=" rv-media-integrate-wrapper">
        {{ Breadcrumbs::render('assets.checkout',$asset->id) }}
        <div id="main" class="container">
            <form method="POST" action="{{route( 'assets.checkout.post',$asset->id)}}" accept-charset="UTF-8">
                @csrf
                <div id="form-checkout-license">
                    <div class="row">
                        <div class="box box-default">

                            <div class="box-body">

                                <div class=" col-12 ">

                                    <div class="main-form content-page">
                                        <div class="box-header with-border border-bottom mb-3">
                                            <h4 class="box-title licenses-name"> {{$asset->name}}</h4>
                                        </div>
                                        <div id="form-checkout-license">

                                            <div class="row">
                                                <div class="box box-default">
                                                    <div class="box-body">
                                                        <!-- Asset name -->
                                                        <div class="form-group row">
                                                            <label class="col-sm-3 control-label text-right">
                                                                {{trans('layout.model.model')}}
                                                            </label>
                                                            <div class="col-md-6">
                                                                <p class="form-control-static licenses-name">{{$asset->model->name}}</p>
                                                            </div>
                                                        </div>
                                                        <!-- Serial -->
                                                        <div class="form-group ">
                                                            <div class="row">
                                                                <div class="col-md-3 text-right">
                                                                    <label for="asset_name"
                                                                           class="text-title-field control-label ">
                                                                        {{trans('layout.asset.asset')}}{{trans('layout.name')}}</label>
                                                                </div>
                                                                <div class="col-md-9">
                                                                    <input
                                                                        class="form-control @error('name') is-invalid @enderror"
                                                                        name="name"
                                                                        type="text" id="asset_name"
                                                                        value="{{old('name',$asset->name)}}">
                                                                    @error('name')
                                                                    <span class="text-danger">{{ $message }}</span>
                                                                    @enderror
                                                                </div>
                                                            </div>
                                                        </div>
                                                        <div id="status_user" class="form-group row">
                                                            <label for="assigned_to"
                                                                   class="col-md-3 control-label text-right required">
                                                                {{trans('layout.status')}}</label>
                                                            <div class="col-md-6 ">
                                                                @include('partials.select.status', ['status_id' => old('status_id',$asset->status_id),'checkout'=>'true' ])
                                                            </div>
                                                        </div>
                                                        <div class="form-checkout-to">
                                                            <div class="form-group row" id="assignto_selector">
                                                                <label for="checkout_to_type"
                                                                       class="col-md-3 control-label text-right">
                                                                    {{trans('layout.asset.checkout_to')}}</label>
                                                                <div class="col-md-8">
                                                                    <div class="btn-group" data-toggle="buttons">
                                                                        <label class="btn btn-default {{old('checkout_to_type')=='user'?'active':""}}">
                                                                            <input name="checkout_to_type" value="user"
                                                                                   aria-label="checkout_to_type" hidden
                                                                                   type="radio" checked="checked"><i
                                                                                class="fa fa-user"
                                                                                aria-hidden="true"></i>{{trans('layout.user.user')}}
                                                                        </label>
                                                                        <label class="btn btn-default {{old('checkout_to_type')=='asset'?'active':""}}">
                                                                            <input name="checkout_to_type" value="asset"
                                                                                   aria-label="checkout_to_type" hidden
                                                                                   type="radio">
                                                                            <i class="fa fa-barcode" aria-hidden="true"></i>
                                                                            {{trans('layout.asset.asset')}}
                                                                        </label>
                                                                        <label class="btn btn-default {{old('checkout_to_type')=='location'?'active':""}}">
                                                                            <input name="checkout_to_type" value="location"
                                                                                   aria-label="checkout_to_type" hidden
                                                                                   type="radio">
                                                                            <i class="fa fa-barcode" aria-hidden="true"></i>
                                                                            {{trans('layout.location.location')}}
                                                                        </label>
                                                                    </div>
                                                                </div>
                                                            </div>

                                                            <div id="assigned_user" class="form-group row">
                                                                <label for="assigned_user "
                                                                       class="col-md-3 control-label text-right required">
                                                                    {{trans('layout.user.user')}}
                                                                </label>
                                                                <div class="col-md-6 ">
                                                                    @include('partials.select.manager')
                                                                </div>
                                                            </div>

                                                            <div id="assigned_asset" class="form-group row " style="display: none">
                                                                <label for="assigned_asset "
                                                                       class="col-md-3 control-label text-right required">
                                                                    {{trans('layout.asset.asset')}}
                                                                </label>
                                                                <div class="col-md-6 ">
                                                                    @include('partials.select.asset')
                                                                </div>
                                                            </div>
                                                            <div id="assigned_location" class="form-group row " style="display: none">
                                                                <label for="assigned_location "
                                                                       class="col-md-3 control-label text-right required">
                                                                    {{trans('layout.location.location')}}
                                                                </label>
                                                                <div class="col-md-6 ">
                                                                    @include('partials.select.locations')
                                                                </div>
                                                            </div>
                                                        </div>
                                                    <!-- Note -->
                                                        <div class="form-group ">
                                                            <div class="row ">
                                                                <div class="col-md-3 text-right">
                                                                    <label  class="text-title-field control-label" for="checkout_at">
                                                                    {{trans('layout.checkout_date')}}
                                                                    </label>
                                                                </div>
                                                                <div class="col-6">
                                                                    <div class="input-group">
                                                                        <input type="text"
                                                                               class="form-control datepicker"
                                                                               data-date-format="yyyy-mm-dd"
                                                                               name="checkout_at"
                                                                               value="{{old('checkout_at',\Illuminate\Support\Carbon::today()->toDateString())}}"
                                                                               autocomplete="off" id="checkout_at">
                                                                        <div class="input-group-append">
                                                                            <span class="input-group-text">
                                                                                <i class="fa fa-calendar"></i>
                                                                            </span>
                                                                        </div>
                                                                    </div>
                                                                </div>
                                                            </div>
                                                        </div>
                                                        <div class="form-group ">
                                                            <div class="row ">
                                                                <div class="col-md-3 text-right">
                                                                    <label for="expected_checkin"
                                                                           class="text-title-field control-label">
                                                                        {{trans('layout.asset.expected')}}
                                                                    </label>
                                                                </div>
                                                                <div class="col-6">
                                                                    <div class="input-group">
                                                                        <input type="text" class="form-control datepicker"
                                                                               data-date-format="yyyy-mm-dd"
                                                                               name="expected_checkin"
                                                                               placeholder="{{trans('layout.select_date')}}"
                                                                               value="{{old('expected_checkin')}}"
                                                                               autocomplete="off" id="expected_checkin">
                                                                        <div class="input-group-append">
                                                                            <span class="input-group-text">
                                                                                <i class="fa fa-calendar"></i>
                                                                            </span>
                                                                        </div>
                                                                    </div>
                                                                </div>
                                                            </div>
                                                        </div>
                                                        <div class="form-group row ">
                                                            <label for="note" class="col-md-3 control-label text-right">
                                                                {{trans('layout.note')}}</label>
                                                            <div class="col-md-7">
                                                                <textarea class=" form-control" id="note" name="note"></textarea>
                                                            </div>
                                                        </div>
                                                    </div>
                                                    <div class="box-header with-border border-top mt-3 d-flex justify-content-center">
                                                        <div class="pt-3">
                                                            <a  href="{{route('assets.index')}}" class="float-left btn btn-warning mr-5">
                                                                {{trans('layout.cancel')}}
                                                            </a>
                                                            <button class="float-right btn btn-info ml-5" type="submit">
                                                                {{trans('layout.checkout')}}
                                                            </button>
                                                        </div>
                                                    </div>
                                                </div> <!-- /.box-->
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div> <!-- /.box-->
                    </div>
                </div>
            </form>
        </div>
    </div>


@stop
