<!doctype html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport"
          content="width=device-width, user-scalable=no, initial-scale=1.0, maximum-scale=1.0, minimum-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <title>Document</title>
</head>
<body>
<div class="card-body-qr w-25 ">
    <div class="pt-3 d-flex">
        <a href="{{route('assets.qr.detail',encrypt($asset->id))}}" class="qr-image">
            {!! \QrCode::size(250)->generate(route('assets.show',encrypt($asset->id))); !!}
        </a>
        <a class="btn btn-info qr-download" href="{{route('assets.qr.download',$asset->id)}}">
            <i class="fa fa-download"></i>
        </a>
        <div class="">{{$asset->name}} - {{$asset->serial}}</br> {!! $asset-> assetCheckoutTo()??'' !!} QR CODE</div>
    </div>
</div>
</body>
</html>
<style>
    .card-body-qr{
        display: flex;
        text-align: center;
        width: 25%;
    }
</style>

