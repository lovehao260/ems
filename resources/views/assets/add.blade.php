@extends('layouts.app')
@section('content')
    <div class=" rv-media-integrate-wrapper ">
        {{ Breadcrumbs::render('assets.add') }}
        <div id="main" class="container">
            <form method="POST" action="{{route( 'assets.store')}}" accept-charset="UTF-8">
                @csrf
                <div class="row">
                    <div class="col-md-12 ">
                        <div class="main-form content-page">
                            <div class="form-body">

                                <div class="form-group  ">
                                    <div class="row">
                                        <div class="col-md-2 text-right">
                                            <label for="select-company" class="control-label ">
                                                {{trans('layout.company.company')}}</label>
                                        </div>
                                        <div class="col-md-10 w-100">
                                            @include('partials.select.company', ['company_id' => old('company_id') ])
                                        </div>

                                    </div>
                                </div>
                                <div class="form-group ">
                                    <div class="row">
                                        <div class="col-md-2 text-right">
                                            <label for="asset_tag" class="text-title-field control-label required">
                                                {{trans('layout.asset.tag')}}</label>
                                        </div>
                                        <div class="col-md-10">
                                            <input class="form-control @error('asset_tag') is-invalid @enderror"
                                                  name="asset_tag"
                                                   type="text" id="asset_tag" value="{{old('asset_tag')}}">
                                            @error('asset_tag')
                                                <span class="text-danger">{{ $message }}</span>
                                            @enderror
                                        </div>
                                    </div>
                                </div>
                                <div class="form-group ">
                                    <div class="row">
                                        <div class="col-md-2 text-right">
                                            <label for="serial" class="text-title-field control-label ">   {{trans('layout.component.serial')}} </label>
                                        </div>
                                        <div class="col-md-10">
                                            <input class="form-control @error('serial') is-invalid @enderror" name="serial"
                                                      type="text" id="serial" value="{{old('serial')}}">
                                            @error('serial')
                                            <span class="text-danger">{{ $message }}</span>
                                            @enderror
                                        </div>
                                    </div>
                                </div>

                                <div class="form-group  ">
                                    <div class="row">
                                        <div class="col-md-2 text-right">
                                            <label for="select-company" class="control-label required">
                                                {{trans('layout.model.model')}} </label>
                                        </div>
                                        <div class="col-md-10 w-100">
                                            @include('partials.select.model', ['model_id' => old('model_id') ])
                                        </div>

                                    </div>
                                </div>
                                <div class="asset-field">

                                </div>
                                <div class="form-group  ">
                                    <div class="row">
                                        <div class="col-md-2 text-right">
                                            <label for="select-company" class="control-label required">{{trans('layout.status')}}</label>
                                        </div>
                                        <div class="col-md-10 w-100">
                                            @include('partials.select.status', ['status_id' => old('status_id') ])
                                        </div>

                                    </div>
                                </div>
                                <div class="form-group ">
                                    <div class="row">
                                        <div class="col-md-2 text-right">
                                            <label for="asset_name" class="text-title-field control-label ">
                                                {{trans('layout.name')}}</label>
                                        </div>
                                        <div class="col-md-10">
                                            <input class="form-control @error('name') is-invalid @enderror" name="name"
                                                   type="text" id="asset_name" value="{{old('name')}}">
                                            @error('name')
                                            <span class="text-danger">{{ $message }}</span>
                                            @enderror
                                        </div>
                                    </div>
                                </div>
                                <div class="form-group ">
                                    <div class="row ">
                                        <div class="col-md-2 text-right">
                                            <label for="termination_date" class="text-title-field control-label">
                                                {{trans('layout.purchase_date')}}
                                            </label>
                                        </div>
                                        <div class="col-3">
                                            <div class="input-group">
                                                <input type="text" class="form-control datepicker"  data-date-format="yyyy-mm-dd"
                                                       name="purchase_date" value="{{old('purchase_date')}}"
                                                       autocomplete="off" id="purchase_date">
                                                <div class="input-group-append">
                                                    <span class="input-group-text">
                                                        <i class="fa fa-calendar"></i>
                                                    </span>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <div class="form-group ">
                                    <div class="row">
                                        <div class="col-md-2 text-right">
                                            <label for="order_number" class="text-title-field control-label ">
                                                {{trans('layout.order_number')}}</label>
                                        </div>
                                        <div class="col-md-10">
                                            <input class="form-control" name="order_number"
                                                   type="text" id="order_number" value="{{old('order_number')}}">
                                        </div>
                                    </div>
                                </div>
                                <div class="form-group  ">
                                    <div class="row">
                                        <div class="col-md-2 text-right">
                                            <label for="select-category" class="control-label ">
                                                {{trans('layout.location.location')}}</label>
                                        </div>
                                        <div class="col-md-10 w-100">
                                            @include('partials.select.locations', ['location_id' => old('location_id') ])
                                        </div>

                                    </div>
                                </div>
                                <div class="form-group ">
                                    <div class="row ">
                                        <div class="col-md-2 text-right">
                                            <label for="order_number" class="text-title-field control-label">
                                                {{trans('layout.purchase_cost')}}</label>
                                        </div>
                                        <div class="col-3">
                                            <div class="input-group">
                                                <input type="text" class="form-control @error('purchase_cost') is-invalid @enderror"
                                                       value="{{old('purchase_cost')}}" name="purchase_cost"
                                                       aria-label="Amount (to the nearest dollar)">
                                                <div class="input-group-append">
                                                    <span class="input-group-text">$</span>
                                                </div>
                                            </div>
                                            @error('purchase_cost')
                                            <span class="text-danger">{{ $message }}</span>
                                            @enderror
                                        </div>
                                    </div>

                                </div>

                                <div class="form-group ">
                                    <div class="row ">
                                        <div class="col-md-2 text-right">
                                            <label for="warranty_months" class="text-title-field control-label">
                                                {{trans('layout.asset.warranty')}}</label>
                                        </div>
                                        <div class="col-3">
                                            <div class="input-group">
                                                <input type="text" class="form-control @error('warranty_months') is-invalid @enderror"
                                                       value="{{old('warranty_months')}}" name="warranty_months"
                                                       aria-label="Amount (to the nearest dollar)">
                                                <div class="input-group-append">
                                                    <span class="input-group-text">{{trans('layout.months')}}</span>
                                                </div>
                                                @error('warranty_months')
                                                <span class="text-danger">{{ $message }}</span>
                                                @enderror
                                            </div>
                                        </div>
                                    </div>

                                </div>



                                <div class="form-group ">
                                    <div class="row ">
                                        <div class="col-md-2 text-right">
                                            <label for="requestable" class="text-title-field control-label">
                                                {{trans('layout.asset.requestable')}}</label>
                                        </div>
                                        <div class="col-3">
                                            <input class="table-check-all" name="requestable" type="checkbox"
                                                   value="1">
                                            <label class="form-check-label" for="exampleCheck1">{{trans('layout.yes')}}</label>
                                            @error('requestable')
                                            <span class="text-danger">{{ $message }}</span>
                                            @enderror
                                        </div>
                                    </div>

                                </div>
                                <div class="form-group ">
                                    <div class="row">
                                        <div class="col-md-2 text-right">
                                            <label for="notes" class="text-title-field control-label">
                                                {{trans('layout.note')}}</label>
                                        </div>
                                        <div class="col-md-10">
                                            <textarea name="notes" type="text" id="notes"
                                                      class="form-control">{{old('notes')}}</textarea></div>
                                    </div>
                                </div>
                                <div class="form-group ">
                                    <div class="row">
                                        <div class="col-md-2 text-right">
                                            <label for="image" class="text-title-field control-label ">
                                                {{trans('layout.image')}}</label>
                                        </div>
                                        <div class="col-md-10">
                                            <div class="image-box image">
                                                <input type="hidden" name="image" value="{{old('image')}}" class="image-data">
                                                <div class="preview-image-wrapper " style="">
                                                    <img src="{{asset('images/placeholder.png')}}" alt="Preview image"
                                                         class="preview_image" width="150">
                                                    <a class="btn_remove_image" title="Remove image">
                                                        <i class="fa fa-times"></i>
                                                    </a>
                                                </div>
                                                <div class="image-box-actions">
                                                    <a href="#" class="btn_gallery" data-result="image" data-bs-toggle="modal"
                                                       data-bs-target="#exampleModal">
                                                        {{trans('layout.choose_image')}}
                                                    </a>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>

                                <div class="clearfix"></div>
                            </div>
                            <div class="widget meta-boxes d-flex justify-content-center">
                                <div class="widget-body">
                                    <div class="btn-set">
                                        <a  href="{{route('assets.index')}}" class="float-left btn btn-warning mr-5">
                                            {{trans('layout.cancel')}}</a>
                                        <button type="submit" name="submit" value="save" class="btn btn-info">
                                            <i class="fa fa-save"></i> {{trans('layout.save')}}
                                        </button>

                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>

                </div>

            </form>
        </div>
    </div>
    @include('partials.media')
@stop

