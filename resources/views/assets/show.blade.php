@extends('layouts.app')
@section('content')

    <div class=" rv-media-integrate-wrapper">
        {{ Breadcrumbs::render('assets.show',$asset->id) }}
        <div class=" rv-media-integrate-wrapper p-3">
            <div class="tab-pane fade show active" id="detail" role="tabpanel" aria-labelledby="detail-tab">
                <div class="row  ">
                    <div class="col-md-9 col-12">
                        <table class="table table-striped " id="sortableDetail"
                               data-url="{{route('show.table.detail')}}" data-setting-key="asset">
                            <tbody>
                            @foreach($table_detail as $item)
                                <tr data-detail="{{$item->value}}" data-label="{{$item->label}}">
                                    <td class=" text-left"><span
                                            class="handle">
                                            <i class="fa fa-ellipsis-v"></i>
                                            <i class="fa fa-ellipsis-v"></i>
                                            </span>
                                    </td>

                                    <td class="w-25">{{trans($item->label)}}</td>
                                    @switch($item->value)
                                        @case('company')
                                        @case('model')
                                        <td>{{$asset[$item->value]['name']}}</td>
                                        @break
                                        @case('status')
                                        <td>{{$asset->assetstatus['name']}}</td>
                                        @break
                                        @case('category')
                                        <td>{{$asset->model['category']['name']??''}}</td>
                                        @break
                                        @case('custom_fields')
                                        @break
                                        @case('checkout_to')
                                        <td>{!! $asset->assetCheckoutTo() !!}</td>
                                        @break
                                        @default
                                        <td>{{$asset[$item->value]}}</td>
                                        @break
                                    @endswitch
                                    <td class="w-5">
                                        <div class="table-actions">
                                            <a href="#" class="btn btn-icon btn-sm  deleteDetailAsset ">
                                                <i class="fa fa-trash"></i>
                                            </a>
                                        </div>
                                    </td>
                                </tr>
                            @endforeach
                            @if(!setting('asset'))
                                @foreach($custom_fields_list->fields as $field)
                                    <tr data-detail="{{$field->db_column}}" data-label="{{$field->name}}">
                                        <td class=" text-left"><span
                                                class="handle">
                                            <i class="fa fa-ellipsis-v"></i>
                                            <i class="fa fa-ellipsis-v"></i>
                                            </span>
                                        </td>
                                        <td class="w-25">{{$field->name}}</td>
                                        <td>{{$asset[$field->db_column]}}</td>
                                        <td class="w-5">
                                            <div class="table-actions">
                                                <a href="#" class="btn btn-icon btn-sm  deleteDetailAsset ">
                                                    <i class="fa fa-trash"></i>
                                                </a>
                                            </div>
                                        </td>
                                    </tr>
                                @endforeach
                            @endif
                            </tbody>
                        </table>
                        <div class="form-group row pt-2">
                            <label for="select-field-detail"
                                   class="col-md-2 col-form-label ">{{trans('layout.asset.custom_detail')}}</label>
                            <div class="col-md-4">
                                <select id="select-field-detail" class="form-control w-100 js-data-ajax"
                                        data-url="{{route('api.asset.detail','asset')}}" name="field-detail" data-detail-id="{{$asset->id}}">

                                </select>
                            </div>
                        </div>
                    </div>
                    <div class="col-md-3 col-12">
                        <div class="image-box image">
                            <input type="hidden" name="image" value="{{old('image')}}" class="image-data">
                            <div class="preview-image-wrapper " style="max-width: unset;width: 100%">
                                <img
                                    src="{{ $asset->image? url_storage($asset->image):asset('images/placeholder.png')}}"
                                    class="preview_image" width="150">
                            </div>
                            <div class="card-body text-center">
                                <div class="pt-3 ">
                                    <a href="{{route('assets.qr.detail',encrypt($asset->id))}}" class="qr-image"
                                       target=blank>
                                        {!! \QrCode::size(250)->generate(route('assets.show',encrypt($asset->id))); !!}
                                    </a>
                                    <a class="btn btn-info qr-download"
                                       href="{{route('assets.qr.download',$asset->id)}}">
                                        <i class="fa fa-download"></i>
                                    </a>
                                </div>

                                <span>{{$asset->name}} - {{$asset->serial}}</br> {!! $asset-> assetCheckoutTo()??'' !!} QR CODE</span>
                            </div>

                        </div>
                    </div>
                </div>
            </div>
            <img src="" width="">
        </div>
    </div>

@stop

