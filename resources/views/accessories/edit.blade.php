@extends('layouts.app')
@section('content')
    <div class=" rv-media-integrate-wrapper">
        {{ Breadcrumbs::render('accessories.edit',$accessory->id) }}
        <div id="main" class="container">
            <form method="POST" action="{{route( 'accessories.update',$accessory->id)}}" accept-charset="UTF-8">
                @csrf
                <div class="row">
                    <div class="col-md-12 ">
                        <div class="main-form content-page">
                            <div class="form-body">
                                <div class="form-group  ">
                                    <div class="row">
                                        <div class="col-md-2 text-right">
                                            <label for="select-company"
                                                   class="control-label ">{{trans('layout.company.company')}}
                                            </label>
                                        </div>
                                        <div class="col-md-10 w-100">
                                            @include('partials.select.company', ['company_id' => old('company_id',$accessory->company_id) ])
                                        </div>

                                    </div>
                                </div>
                                <div class="form-group ">
                                    <div class="row">
                                        <div class="col-md-2 text-right">
                                            <label for="name" class="text-title-field control-label required">
                                                {{trans('layout.name')}}</label>
                                        </div>
                                        <div class="col-md-10">
                                            <input class="form-control @error('name') is-invalid @enderror"
                                                   name="name" type="text" id="first_name"
                                                   value="{{old('name',$accessory->name)}}">
                                            @error('name')
                                            <span class="text-danger">{{ $message }}</span>
                                            @enderror
                                        </div>
                                    </div>
                                </div>
                                <div class="form-group  ">
                                    <div class="row">
                                        <div class="col-md-2 text-right">
                                            <label for="select-category" class="control-label required">
                                                {{trans('layout.category.category')}}</label>
                                        </div>
                                        <div class="col-md-10 w-100">
                                            @include('partials.select.category', ['category_type' => 'accessory','category_id'=>$accessory->category_id ])
                                        </div>

                                    </div>
                                </div>
                                <div class="form-group  ">
                                    <div class="row">
                                        <div class="col-md-2 text-right">
                                            <label for="select-category" class="control-label ">
                                                {{trans('layout.location.location')}}</label>
                                        </div>
                                        <div class="col-md-10 w-100">
                                            @include('partials.select.locations', ['location_id' => old('location_id',$accessory->location_id) ])
                                        </div>

                                    </div>
                                </div>
                                <div class="form-group ">
                                    <div class="row">
                                        <div class="col-md-2 text-right">
                                            <label for="model_number" class="text-title-field control-label ">
                                                {{trans('layout.model.no')}}</label>
                                        </div>
                                        <div class="col-md-10">
                                            <input class="form-control @error('model_number') is-invalid @enderror"
                                                   name="model_number" type="text" id="first_name"
                                                   value="{{old('model_number',$accessory->model_number)}}">
                                            @error('model_number')
                                            <span class="text-danger">{{ $message }}</span>
                                            @enderror
                                        </div>
                                    </div>
                                </div>
                                <div class="form-group ">
                                    <div class="row ">
                                        <div class="col-md-2 text-right">
                                            <label for="serial" class="text-title-field control-label">
                                                {{trans('layout.order_number')}}</label>
                                        </div>
                                        <div class="col-10">
                                            <input class="form-control"
                                                   data-counter="120" name="order_number"
                                                   type="text" id="order_number"
                                                   value="{{old('order_number',$accessory->order_number)}}">
                                        </div>
                                    </div>
                                </div>
                                <div class="form-group ">
                                    <div class="row">
                                        <div class="col-md-2 text-right">
                                            <label for="purchase_date" class="text-title-field control-label">
                                                {{trans('layout.purchase_date')}}</label>
                                        </div>
                                        <div class="col-3">
                                            <div class="input-group">
                                                <input type="text" class="form-control datepicker"
                                                       data-date-format="yyyy-mm-dd"
                                                       name="purchase_date"
                                                       value="{{old('purchase_date',$accessory->purchase_date)}}"
                                                       autocomplete="off" id="purchase_date">
                                                <div class="input-group-append">
                                                    <span class="input-group-text">
                                                        <i class="fa fa-calendar"></i>
                                                    </span>
                                                </div>
                                            </div>

                                        </div>
                                    </div>
                                </div>
                                <div class="form-group ">
                                    <div class="row ">
                                        <div class="col-md-2 text-right">
                                            <label for="purchase_cost" class="text-title-field control-label">
                                                {{trans('layout.purchase_cost')}}</label>
                                        </div>
                                        <div class="col-3">
                                            <div class="input-group">
                                                <input type="text" class="form-control @error('purchase_cost') is-invalid @enderror"
                                                       value="{{old('purchase_cost',$accessory->purchase_cost)}}"
                                                       name="purchase_cost"
                                                       aria-label="Amount (to the nearest dollar)">
                                                <div class="input-group-append">
                                                    <span class="input-group-text">$</span>
                                                </div>
                                                @error('purchase_cost')
                                                <span class="text-danger">{{ $message }}</span>
                                                @enderror
                                            </div>
                                        </div>
                                    </div>

                                </div>
                                <div class="form-group ">
                                    <div class="row">
                                        <div class="col-md-2 text-right">
                                            <label for="qty" class="text-title-field control-label required">
                                                {{trans('layout.qty')}}</label>
                                        </div>
                                        <div class="col-3">
                                            <input class="form-control @error('qty') is-invalid @enderror"
                                                   name="qty" type="text" id="first_name"
                                                   value="{{old('qty',$accessory->qty)}}">
                                            @error('qty')
                                            <span class="text-danger">{{ $message }}</span>
                                            @enderror
                                        </div>
                                    </div>
                                </div>
                                <div class="form-group ">
                                    <div class="row ">
                                        <div class="col-md-2 text-right">
                                            <label for="min_amt" class="text-title-field control-label">
                                                {{trans('layout.min_qty')}}</label>
                                        </div>
                                        <div class="col-3">
                                            <input class="form-control @error('min_amt') is-invalid @enderror"
                                                 name="min_amt"type="text" id="min_amt" value="{{old('min_amt',$accessory->min_amt)}}">
                                            @error('min_amt')
                                            <span class="text-danger">{{ $message }}</span>
                                            @enderror
                                        </div>
                                    </div>
                                </div>
                                <div class="form-group row">
                                    <div class="col-md-2 text-right">
                                        <label for="image" class="text-title-field control-label "> {{trans('layout.image')}}</label>
                                    </div>
                                    <!-- Button trigger modal -->
                                    <div class="col-10">
                                        <div class="image-box image">
                                            <input type="hidden" name="image" value="{{old('image',$accessory->image)}}"
                                                   class="image-data">
                                            <div class="preview-image-wrapper " style="">
                                                <img
                                                    src="{{ $accessory->image? url_storage($accessory->image):asset('images/placeholder.png')}}"
                                                    class="preview_image" width="150">
                                                <a class="btn_remove_image" title="Remove image">
                                                    <i class="fa fa-times"></i>
                                                </a>
                                            </div>
                                            <div class="image-box-actions">
                                                <a href="#" class="btn_gallery" data-result="image"
                                                   data-bs-toggle="modal"
                                                   data-bs-target="#exampleModal">
                                                    {{trans('layout.choose_image')}}
                                                </a>
                                            </div>
                                        </div>
                                    </div>

                                </div>
                                <div class="clearfix"></div>
                            </div>
                            <div class="widget meta-boxes d-flex justify-content-center">
                                <div class="widget-body">
                                    <div class="btn-set">
                                        <a href="{{route('components.index')}}" class="float-left btn btn-warning mr-5">{{trans('layout.cancel')}}</a>
                                        <button type="submit" name="submit" value="save" class="btn btn-info">
                                            <i class="fa fa-save"></i> {{trans('layout.save')}}
                                        </button>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </form>
        </div>
    </div>
    @include('partials.media')
@stop
