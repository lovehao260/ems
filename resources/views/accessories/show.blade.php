@extends('layouts.app')
@section('content')

    <div class=" rv-media-integrate-wrapper">
        {{ Breadcrumbs::render('accessories.show',$accessory->id) }}
        <div id="main" class="">
            <div class="table-wrapper">
                <div class="table-datatable row">
                    <div class="breadcrumb"><h4> {{$accessory->name}} </h4></div>
                    <ul class="nav nav-tabs" id="myTab" role="tablist">
                        <li class="nav-item" role="presentation">
                            <button class="nav-link active" id="detail-tab" data-bs-toggle="tab"
                                    data-bs-target="#detail" type="button"
                                    role="tab" aria-controls="detail" aria-selected="true">{{trans('layout.detail')}}
                            </button>
                        </li>
                        <li class="nav-item" role="presentation">
                            <button class="nav-link" id="seats-tab" data-bs-toggle="tab" data-bs-target="#seats"
                                    type="button"
                                    role="tab" aria-controls="seats"
                                    aria-selected="false">{{trans('layout.users')}}
                            </button>
                        </li>
                        {{--            <li class="nav-item" role="presentation">--}}
                        {{--                <button class="nav-link" id="contact-tab" data-bs-toggle="tab" data-bs-target="#contact" type="button"--}}
                        {{--                        role="tab" aria-controls="contact" aria-selected="false">Checkout history--}}
                        {{--                </button>--}}
                        {{--            </li>--}}
                    </ul>
                    <div class="tab-content" id="myTabContent">
                        <div class="tab-pane fade show active" id="detail" role="tabpanel" aria-labelledby="detail-tab">
                            <div class="row">
                                <div class="col-md-9 col-12">
                                    <table class="table table-striped " id="sortableDetail"
                                           data-url="{{route('show.table.detail')}}" data-setting-key="accessory">
                                        <tbody>
                                        @foreach($table_detail as $item)
                                            <tr data-detail="{{$item->value}}" data-label="{{$item->label}}">
                                                <td class=" text-left"><span class="handle">
                                            <i class="fa fa-ellipsis-v"></i>
                                            <i class="fa fa-ellipsis-v"></i>
                                            </span>
                                                </td>
                                                <td class="w-25">{{trans($item->label)}}</td>
                                                @switch($item->value)
                                                    @case('company')
                                                    <td>{{$accessory[$item->value]['name']??''}}</td>
                                                    @break
                                                    @case('category')
                                                    <td>{{$accessory->category->name}}</td>
                                                    @break
                                                    @default
                                                    <td>{{$accessory[$item->value]}}</td>
                                                    @break
                                                @endswitch
                                                <td class="w-5">
                                                    <div class="table-actions">
                                                        <a href="#" class="btn btn-icon btn-sm  deleteDetailAsset ">
                                                            <i class="fa fa-trash"></i>
                                                        </a>
                                                    </div>
                                                </td>
                                            </tr>
                                        @endforeach
                                        </tbody>
                                    </table>
                                    <div class="form-group row pt-2">
                                        <label for="select-field-detail"
                                               class="col-md-2 col-form-label ">{{trans('layout.asset.custom_detail')}}</label>
                                        <div class="col-md-4">
                                            <select id="select-field-detail" class="form-control w-100 js-data-ajax"
                                                    data-url="{{route('api.accessories.detail','accessory')}}"
                                                    name="field-detail">
                                            </select>
                                        </div>
                                    </div>
                                </div>
                                <div class="col-md-3 col-12">

                                    <div class="card-body text-center">
                                        <div class="image-box ml-5">
                                            <input type="hidden" name="image" value="{{old('image')}}"
                                                   class="image-data">
                                            <div class="preview-image-wrapper " style="max-width: unset;width: 100%">
                                                <img src="{{ $accessory->image? url_storage($accessory->image):asset('images/placeholder.png')}}"
                                                    class="preview_image" width="250">
                                            </div>
                                        </div>
                                        <div class="pt-3 text-center">
                                            <div class="qr-image" target=blank>
                                                {!! \QrCode::size(250)->generate(route('accessories.show',$accessory->id)); !!}
                                            </div>
                                            <a class="btn btn-info qr-download"
                                               href="{{route('accessories.qr.download',$accessory->id)}}">
                                                <i class="fa fa-download"></i>
                                            </a>
                                        </div>

                                        <span>{{$accessory->name}} - {{$accessory->serial}}</br>  QR CODE</span>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="tab-pane fade" id="seats" role="tabpanel" aria-labelledby="seats-tab">
                            <div class="tab-content" id="myTabContent">
                                <table class="table table-striped table-seats" id="table-accessories">
                                    <thead>
                                    <tr>
                                        <td>{{trans('layout.user.user')}}</td>
                                        <td>{{trans('layout.note')}}</td>
                                        <td>{{trans('layout.checkout_date')}}</td>
                                        <td>{{trans('layout.checkout_check')}}</td>
                                    </tr>
                                    </thead>
                                    <tbody>
                                    </tbody>
                                </table>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <div class="modal fade modal-checkout-accessories " role="dialog">
        <div class="modal-dialog   ">
            <div class="modal-content">
                <div class="modal-header">
                    <h4 class="modal-title"><i class="til_img"></i><strong>{{trans('layout.checkin')}}</strong></h4>
                    <button type="button" class="close" data-bs-dismiss="modal" aria-hidden="true">
                        <span aria-hidden="true">×</span>
                    </button>
                </div>

                <div class="modal-body with-padding">
                    <div id="form-checkout">
                        <div class="row">
                            <div class="box box-default">

                                <div class="box-body">
                                    <!-- Asset name -->
                                    <div class="form-group row">
                                        <label class="col-sm-3 control-label"> {{trans('layout.name')}}</label>
                                        <div class="col-md-6">
                                            <p class="form-control-static ">{{$accessory->name}}</p>
                                        </div>
                                    </div>
                                    <div class="form-group ">
                                        <div class="row">
                                            <div class="col-md-3 ">
                                                <label for="checkin_at"
                                                       class="text-title-field control-label">{{trans('layout.checkin_date')}}
                                                </label>
                                            </div>
                                            <div class="col-7">
                                                <div class="input-group">
                                                    <input type="text" class="form-control datepicker"
                                                           data-date-format="yyyy-mm-dd"
                                                           name="checkin_at" value="{{old('checkin_at')}}"
                                                           autocomplete="off" id="checkin_at">
                                                    <div class="input-group-append">
                                                    <span class="input-group-text">
                                                        <i class="fa fa-calendar"></i>
                                                    </span>
                                                    </div>
                                                </div>

                                            </div>
                                        </div>
                                    </div>
                                    <div class="form-group row ">
                                        <label for="note"
                                               class="col-md-3 control-label">{{trans('layout.note')}}</label>
                                        <div class="col-md-7">
                                            <textarea class=" form-control" id="note" name="note"></textarea>
                                        </div>
                                    </div>
                                </div>
                            </div> <!-- /.box-->
                        </div>
                    </div>
                </div>

                <div class="modal-footer">
                    <button class="float-left btn btn-warning"
                            data-bs-dismiss="modal">{{trans('layout.cancel')}}</button>
                    <button
                        class="float-right btn btn-danger confirm-checkout-button">{{trans('layout.checkin')}}</button>
                </div>
            </div>
        </div>
    </div>
@stop
@push('js')
    <script>
        $(document).ready(function () {
            let table = $('#table-accessories').DataTable({
                searching: false,
                ajax: '{{route('accessories.show',$accessory->id)}}',
                language: {
                    "url": window._locale
                },
                lengthChange: false,
                columns: [
                    {data: 'user', name: 'user', className: 'text-left'},
                    {data: 'note', name: 'note'},
                    {data: 'created_at', name: 'created_at'},
                    {data: 'checkout', name: 'checkout'},
                ]
            });
        });
    </script>
@endpush
