<nav class="main-header navbar navbar-expand navbar-white navbar-light">
    <!-- Left navbar links -->
    <ul class="navbar-nav">
        <li class="nav-item">
            <a class="nav-link" data-widget="pushmenu" href="#" role="button"><i class="fas fa-bars"></i></a>
        </li>
    </ul>

    <!-- Right navbar links -->
    <ul class="navbar-nav ml-auto">
        <!-- Navbar Search -->
        <li class="nav-item">
            <a class="nav-link" data-widget="navbar-search" href="#" role="button">
                <i class="fas fa-search"></i>
            </a>
            <div class="navbar-search-block">
                <form class="form-inline">
                    <div class="input-group input-group-sm">
                        <input class="form-control form-control-navbar" type="search" placeholder="Search"
                               aria-label="Search">
                        <div class="input-group-append">
                            <button class="btn btn-navbar" type="submit">
                                <i class="fas fa-search"></i>
                            </button>
                            <button class="btn btn-navbar" type="button" data-widget="navbar-search">
                                <i class="fas fa-times"></i>
                            </button>
                        </div>
                    </div>
                </form>
            </div>
        </li>
        <li>
            <div class="sl-wrapper">
                <div class="sl-nav">
                    <ul>
                        <li>
                            @if(   app()->getLocale() =='ja')
                                <b>JA</b>
                            @else
                                <b>EN</b>
                            @endif
                            <i class="fa fa-angle-down" aria-hidden="true"></i>
                            <div class="triangle"></div>
                            <ul>
                                <li>
                                    <i class="sl-flag flag-de">
                                        <div id="germany"></div>
                                    </i>
                                    <span class="active"><a
                                            href="{{ route('settings.locale',['locale' => 'ja']) }}">{{__('layout.ja')}}</a></span>
                                </li>
                                <li><i class="sl-flag flag-usa">
                                        <div id="germany"></div>
                                    </i>
                                    <span><a href="{{ route('settings.locale',['locale' => 'en']) }}">{{__('layout.en')}}</span>
                                </li>
                            </ul>

                        </li>
                    </ul>
                </div>
            </div>
        </li>


        <!-- Notifications Dropdown Menu -->
        <li class="nav-item dropdown">
            <a class="nav-link" data-toggle="dropdown" href="#">
                <i class="far fa-bell"></i>
                <span class="badge badge-warning navbar-badge">{{auth()->user()->notifications()->count() ?? 0}}</span>
            </a>
            <div class="dropdown-menu dropdown-menu-lg dropdown-menu-right">

                <div class="dropdown-divider"></div>
                <a href="#" class="dropdown-item">
                    <i class="fas fa-save mr-2"></i> {{count(total_checkout()->assets)}}{{trans('layout.assigned_asset')}}
                </a>
                <div class="dropdown-divider"></div>
                <a href="#" class="dropdown-item">
                    <i class="fas fa-barcode mr-2"></i>{{count(total_checkout()->licenses)}} {{trans('layout.assigned_license')}}
                </a>
                <div class="dropdown-divider"></div>
                <a href="#" class="dropdown-item">
                    <i class="fa fa-keyboard mr-2"></i> {{count(total_checkout()->accessories)}} {{trans('layout.assigned_accessory')}}
                    accessories
                </a>
                <div class="dropdown-divider"></div>

            </div>
        </li>
        <li class="nav-item">
            <a class="nav-link" data-widget="fullscreen" href="#" role="button">
                <i class="fas fa-expand-arrows-alt"></i>
            </a>
        </li>
        <li class="nav-item">
            <a class="nav-link" data-widget="control-sidebar" data-slide="true" href="#" role="button">
                <i class="fas fa-th-large"></i>
            </a>
        </li>
        <li class="nav-item dropdown ">
            <a id="dropdownSubMenu1" href="#" data-toggle="dropdown" aria-haspopup="true" aria-expanded="true"
               class="nav-link dropdown-toggle">{{auth()->user()->name}}</a>
            <ul aria-labelledby="dropdownSubMenu1" class="dropdown-menu border-0 shadow"
                style="left: 0px; right: inherit;">
                <li><a href="{{route('profile.index')}}" class="dropdown-item">{{trans('layout.profile.profile')}}</a></li>
                <li><a class="dropdown-item" href="{{ route('logout') }}"
                       onclick="event.preventDefault();
                                                     document.getElementById('logout-form').submit();">
                        {{ __('layout.logout') }}
                    </a>

                    <form id="logout-form" action="{{ route('logout') }}" method="POST" class="d-none">
                        @csrf
                    </form>
                </li>
            </ul>
        </li>
    </ul>
</nav>

