<!-- Main Sidebar Container -->
<aside class="main-sidebar sidebar-dark-primary elevation-4 ">
    <!-- Brand Logo -->
    <a href="{{route('dashboard.index')}}" class="brand-link">
        <img src="{{ setting('admin_logo')? url_storage(setting('admin_logo')):asset('images/placeholder.png')}}"
             alt="AdminLTE Logo" class=""
             style="opacity: .8; max-height: 180px">

    </a>

    <!-- Sidebar -->
    <div class="sidebar">


        <!-- Sidebar Menu -->
        <nav class="mt-2">
            <ul class="nav nav-pills nav-sidebar flex-column" data-widget="treeview" role="menu" data-accordion="false">
                <!-- Add icons to the links using the .nav-icon class
                     with font-awesome or any other icon font library -->

                <li class="nav-item">
                    <a href="{{route('dashboard.index')}}"
                       class="nav-link {{ request()->is('dashboard*') ? 'active' : ''}}">
                        <i class="nav-icon fas fa-tachometer-alt"></i>
                        <p class="text">{{trans('layout.dashboard.dashboard')}}</p>
                    </a>
                </li>
                <li class="nav-item">
                    <a href="{{route('assets.index')}}" class="nav-link {{ request()->is('assets*') ? 'active' : ''}}">
                        <i class="nav-icon fa fa-save"></i>
                        <p class="text">{{trans('layout.assets')}}</p>
                    </a>
                </li>

                <li class="nav-item">
                    <a href="{{route('licenses.index')}}"
                       class="nav-link {{ request()->is('licenses*') ? 'active' : ''}}">
                        <i class="nav-icon fa fa-barcode"></i>
                        <p class="text">{{trans('layout.licenses')}}</p>
                    </a>
                </li>
                <li class="nav-item">
                    <a href="{{route('accessories.index')}}"
                       class="nav-link {{ request()->is('accessories*') ? 'active' : ''}}">
                        <i class="nav-icon fa fa-keyboard"></i>
                        <p class="text">{{trans('layout.accessories')}}</p>
                    </a>
                </li>
                <li class="nav-item">
                    <a href="{{route('components.index')}}"
                       class="nav-link {{ request()->is('components*') ? 'active' : ''}}">
                        <i class="nav-icon far fa-hdd"></i>
                        <p class="text">{{trans('layout.components')}}</p>
                    </a>
                </li>
                <li class="nav-item">
                    <a href="{{route('users.index')}}" class="nav-link  {{ request()->is('users*') ? 'active' : ''}}">
                        <i class="nav-icon far fa-user"></i>
                        <p class="text">{{trans('layout.users')}}</p>
                    </a>
                </li>
                <li class="nav-item">
                    <a href="{{route('medias.index')}}" class="nav-link {{ request()->is('medias*') ? 'active' : ''}}">
                        <i class="nav-icon fa fa-photo-video"></i>
                        <p class="text">{{trans('layout.medias')}}</p>
                    </a>
                </li>
                <li class="nav-item {{ request()->is('models*') ||request()->is('models*')||request()->is('companies*')||
                            request()->is('departments*') ||    request()->is('categories*')||    request()->is('locations*') || request()->is('custom-field*') ? 'menu-open ' : ''}}">
                    <a href="#" class="nav-link {{ request()->is('models*') ||request()->is('models*')||request()->is('companies*')||
                            request()->is('departments*')||    request()->is('categories*')|| request()->is('locations*') ||   request()->is('custom-field*') ? 'active' : ''}}">
                        <i class="nav-icon fas fa-cogs"></i>
                        <p>
                            {{trans('layout.settings')}}
                            <i class="right fas fa-angle-left"></i>
                        </p>
                    </a>

                    <ul class="nav nav-treeview ">
                        <li class="nav-item">
                            <a href="{{route('fields.index')}}"
                               class="nav-link {{ request()->is('custom-field*') ? 'active' : ''}}">
                                <i class="far fa-circle nav-icon"></i>
                                <p class="text">{{trans('layout.custom_fields')}}</p>
                            </a>
                        </li>
                        <li class="nav-item">
                            <a href="{{route('models.index')}}"
                               class="nav-link {{ request()->is('models*') ? 'active' : ''}}">
                                <i class="far fa-circle nav-icon"></i>
                                <p class="text">{{trans('layout.asset_models')}}</p>
                            </a>
                        </li>
                        <li class="nav-item">
                            <a href="{{route('companies.index')}}"
                               class="nav-link {{ request()->is('companies*') ? 'active' : ''}}">
                                <i class="far fa-circle nav-icon"></i>
                                <p class="text">{{trans('layout.companies')}}</p>
                            </a>
                        </li>
{{--                        <li class="nav-item">--}}
{{--                            <a href="{{route('departments.index')}}"--}}
{{--                               class="nav-link {{ request()->is('departments*') ? 'active' : ''}}">--}}
{{--                                <i class="far fa-circle nav-icon"></i>--}}
{{--                                <p>Departments</p>--}}
{{--                            </a>--}}
{{--                        </li>--}}
                        <li class="nav-item">
                            <a href="{{route('categories.index')}}"
                               class="nav-link {{ request()->is('categories*') ? 'active' : ''}}">
                                <i class="far fa-circle nav-icon"></i>
                                <p class="text">{{trans('layout.categories')}}</p>
                            </a>
                        </li>
                        <li class="nav-item">
                            <a href="{{route('locations.index')}}"
                               class="nav-link {{ request()->is('locations*') ? 'active' : ''}}">
                                <i class="far fa-circle nav-icon"></i>
                                <p class="text">{{trans('layout.locations')}}</p>
                            </a>
                        </li>
                    </ul>
                </li>

                <li class="nav-item">
                    <a href="{{route('imports.index')}}"
                       class="nav-link  {{ request()->is('imports*') ? 'active' : ''}}">
                        <i class="nav-icon fa fa-file-import"></i>
                        <p class="text">{{trans('layout.imports')}}</p>
                    </a>
                </li>
                <li class="nav-item {{ request()->is('activities*')| request()->is('roles*')|request()->is('settings*') ? 'menu-open' : ''}}">
                    <a href="#" class="nav-link {{ request()->is('activities*') ? 'active' : ''}}">
                        <i class="nav-icon fas fa-user-shield"></i>
                        <p>{{trans('layout.platform')}} <i class="right fas fa-angle-left"></i></p>
                    </a>
                    <ul class="nav nav-treeview active">
                        <li class="nav-item">
                            <a href="{{route('settings.index')}}"
                               class="nav-link {{ request()->is('settings*') ? 'active' : ''}}">
                                <i class="far fa-circle nav-icon"></i>
                                <p>{{trans('layout.general')}}</p>
                            </a>
                        </li>

                        <li class="nav-item">
                            <a href="{{route('roles.index')}}"
                               class="nav-link {{ request()->is('roles*') ? 'active' : ''}}">
                                <i class="far fa-circle nav-icon"></i>
                                <p>{{trans('layout.roles_permissions')}}
                                </p>
                            </a>
                        </li>
                        <li class="nav-item">
                            <a href="{{route('activities.index')}}"
                               class="nav-link {{ request()->is('activities*') ? 'active' : ''}}">
                                <i class="far fa-circle nav-icon"></i>
                                <p>{{trans('layout.activities')}}
                                </p>
                            </a>
                        </li>
                    </ul>
                </li>
            </ul>
        </nav>
        <!-- /.sidebar-menu-->
    </div>
    <!-- /.sidebar -->
</aside>
