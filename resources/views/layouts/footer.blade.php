<footer class="main-footer">
    <strong>{{trans('layout.copyright')}} © {{date("Y")}} <a href="{{url('/')}}">{{setting('admin_title','CALC')}}</a>.</strong>
    {{trans('layout.all_rights')}}
    <div class="float-right d-none d-sm-inline-block">
        <b>{{trans('layout.version')}}</b> 1.0
    </div>
</footer>
