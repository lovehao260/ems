@extends('layouts.app')
@section('content')
    <div class=" rv-media-integrate-wrapper ">
        {{ Breadcrumbs::render('components.add') }}
        <div id="main" class=" container">
            <form method="POST" action="{{route( 'components.store')}}" accept-charset="UTF-8">
                @csrf
                <div class="row">
                    <div class="col-md-12 ">
                        <div class="main-form content-page">
                            <div class="form-body">
                                <div class="form-group ">
                                    <div class="row">
                                        <div class="col-md-2 text-right">
                                            <label for="name" class="text-title-field control-label required">
                                               {{trans('layout.name')}}</label>
                                        </div>
                                        <div class="col-md-10">
                                            <input class="form-control @error('name') is-invalid @enderror"
                                               name="name" type="text" id="first_name" value="{{old('name')}}">
                                            @error('name')
                                            <span class="text-danger">{{ $message }}</span>
                                            @enderror
                                        </div>
                                    </div>
                                </div>
                                <div class="form-group  ">
                                    <div class="row">
                                        <div class="col-md-2 text-right">
                                            <label for="select-company" class="control-label required">
                                                {{trans('layout.category.category')}}</label>
                                        </div>
                                        <div class="col-md-10 w-100">
                                            @include('partials.select.category', ['category_type' => 'component','category_id'=>old('category_id') ])
                                        </div>

                                    </div>
                                </div>


                                <div class="form-group ">
                                    <div class="row">
                                        <div class="col-md-2 text-right">
                                            <label for="qty" class="text-title-field control-label required">
                                                {{trans('layout.qty')}}</label>
                                        </div>
                                        <div class="col-3">
                                            <input class="form-control @error('qty') is-invalid @enderror" name="qty"
                                                   type="text" id="seats" value="{{old('qty')}}">
                                            @error('qty')
                                            <span class="text-danger">{{ $message }}</span>
                                            @enderror
                                        </div>
                                    </div>
                                </div>
                                <div class="form-group ">
                                    <div class="row">
                                        <div class="col-md-2 text-right">
                                            <label for="min_amt" class="text-title-field control-label">
                                                {{trans('layout.min_qty')}}</label>
                                        </div>
                                        <div class="col-3">
                                            <input class="form-control @error('min_amt') is-invalid @enderror" name="min_amt"
                                                   type="text" id="seats" value="{{old('min_amt')}}">
                                            @error('min_amt')
                                            <span class="text-danger">{{ $message }}</span>
                                            @enderror
                                        </div>
                                    </div>
                                </div>
                                <div class="form-group ">
                                    <div class="row">
                                        <div class="col-md-2 text-right">
                                            <label for="serial" class="text-title-field control-label">
                                                {{trans('layout.component.serial')}}</label>
                                        </div>
                                        <div class="col-md-10">
                                            <input class="form-control" name="serial"
                                                   type="text" id="serial" value="{{old('serial')}}">

                                        </div>
                                    </div>
                                </div>
                                <div class="form-group  ">
                                    <div class="row">
                                        <div class="col-md-2 text-right">
                                            <label for="select-company" class="control-label "> {{trans('layout.company.company')}}</label>
                                        </div>
                                        <div class="col-10">
                                            @include('partials.select.company')
                                        </div>

                                    </div>
                                </div>
                                <div class="form-group  ">
                                    <div class="row">
                                        <div class="col-md-2 text-right">
                                            <label for="select-company" class="control-label ">{{trans('layout.location.location')}}</label>
                                        </div>
                                        <div class="col-10">
                                            @include('partials.select.locations')
                                        </div>

                                    </div>
                                </div>
                                <div class="form-group ">
                                    <div class="row ">
                                        <div class="col-md-2 text-right">
                                            <label for="serial" class="text-title-field control-label">
                                                {{trans('layout.order_number')}}</label>
                                        </div>
                                        <div class="col-10">
                                            <input class="form-control"
                                                   data-counter="120" name="order_number"
                                                   type="text" id="order_number" value="{{old('order_number')}}">
                                        </div>
                                    </div>
                                </div>
                                <div class="form-group ">
                                    <div class="row ">
                                        <div class="col-md-2 text-right">
                                            <label for="order_number" class="text-title-field control-label">
                                                {{trans('layout.purchase_cost')}}</label>
                                        </div>
                                        <div class="col-3">
                                            <div class="input-group">
                                                <input type="text" class="form-control
                                                        @error('purchase_cost') is-invalid @enderror"
                                                       value=" {{old('purchase_cost')}} " name="purchase_cost"
                                                       aria-label="Amount (to the nearest dollar)">
                                                <div class="input-group-append">
                                                    <span class="input-group-text">$</span>
                                                </div>
                                                @error('purchase_cost')
                                                <span class="text-danger">{{ $message }}</span>
                                                @enderror
                                            </div>
                                        </div>
                                    </div>

                                </div>

                                <div class="form-group ">
                                    <div class="row">
                                        <div class="col-md-2 text-right">
                                            <label for="purchase_date" class="text-title-field control-label">
                                                {{trans('layout.purchase_date')}}</label>
                                        </div>
                                        <div class="col-3">
                                            <div class="input-group">
                                                <input type="text" class="form-control datepicker
                                                        @error('purchase_date') is-invalid @enderror" name="purchase_date"
                                                       data-date-format="yyyy-mm-dd" autocomplete="off" id="purchase_date">
                                                <div class="input-group-append">
                                                    <span class="input-group-text">
                                                        <i class="fa fa-calendar"></i>
                                                    </span>
                                                </div>
                                                @error('purchase_date')
                                                <span class="text-danger">{{ $message }}</span>
                                                @enderror
                                            </div>
                                        </div>
                                    </div>
                                </div>

                                <div class="form-group row">
                                    <div class="col-2 text-right">
                                        <label for="name" class="text-title-field control-label ">{{trans('layout.image')}}</label>
                                    </div>
                                    <div class="col-9">

                                        <!-- Button trigger modal -->
                                        <div class="image-box image">
                                            <input type="hidden" name="image" value="{{old('image')}}" class="image-data">
                                            <div class="preview-image-wrapper " style="">
                                                <img src="{{asset('images/placeholder.png')}}" alt="Preview image"
                                                     class="preview_image" width="150">
                                                <a class="btn_remove_image" title="Remove image">
                                                    <i class="fa fa-times"></i>
                                                </a>
                                            </div>
                                            <div class="image-box-actions">
                                                <a href="#" class="btn_gallery" data-result="image" data-bs-toggle="modal"
                                                   data-bs-target="#exampleModal">
                                                    {{trans('layout.choose_image')}}
                                                </a>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <div class="clearfix"></div>
                            </div>
                            <div class="widget meta-boxes d-flex justify-content-center">
                                <div class="widget-body">
                                    <div class="btn-set">
                                        <a  href="{{route('components.index')}}" class="float-left btn btn-warning mr-5">{{trans('layout.cancel')}}</a>
                                        <button type="submit" name="submit" value="save" class="btn btn-info">
                                            <i class="fa fa-save"></i> {{trans('layout.save')}}
                                        </button>

                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>

            </form>
        </div>
    </div>
    @include('partials.media')

@stop

