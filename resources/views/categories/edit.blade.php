@extends('layouts.app')
@section('content')
    <div class=" rv-media-integrate-wrapper ">
        {{ Breadcrumbs::render('categories.edit',$category->id) }}
        <div id="main" class="container">
            <form method="POST" action="{{route( 'categories.update',$category->id)}}" accept-charset="UTF-8">
                @csrf
                <div class="row">
                    <div class="col-md-12 ">
                        <div class="main-form content-page">
                            <div class="form-body">
                                <div class="form-group ">
                                    <div class="row ">
                                        <div class="col-md-2 text-right">
                                            <label for="name" class="text-title-field control-label required">
                                              {{trans('layout.name')}}</label>
                                        </div>
                                        <div class="col-10">
                                            <input class="form-control @error('name') is-invalid @enderror"
                                               name="name" type="text" id="name" value="{{old('name',$category->name)}}">
                                            @error('name')
                                            <span class="text-danger">{{ $message }}</span>
                                            @enderror
                                        </div>
                                    </div>

                                </div>
                                <div class="form-group ">
                                    <div class="d-flex ">
                                        <div class="col-md-2 text-right">
                                            <label for="category_type" class="text-title-field control-label ">
                                                {{trans('layout.type')}}
                                            </label>
                                        </div>
                                        <select class="form-control" name="category_type" disabled>
                                            <option value="asset" {{$category->category_type=='asset'?'selected':''}}>Asset</option>
                                            <option value="license" {{$category->category_type=='license'?'selected':''}}>License</option>
                                            <option value="component" {{$category->category_type=='component'?'selected':''}}>Component</option>
                                            <option value="accessory"{{$category->category_type=='accessory' ?'selected':''}}>Accessories</option>
                                        </select>

                                    </div>

                                </div>
                                <div class="form-group ">
                                    <div class="d-flex ">
                                        <div class="col-md-2 text-right">
                                            <label for="eula_text" class="text-title-field control-label ">
                                                {{trans('layout.category.eula')}}</label>
                                        </div>

                                        <div class="col-10">
                                            <textarea class="form-control" name="eula_text" type="text" id="eula_text">
                                            </textarea>
                                        </div>
                                    </div>
                                </div>
                                <div class="form-group ">
                                    <div class="d-flex">
                                        <div class="col-md-2 text-right">
                                            <label for="name" class="text-title-field control-label "> {{trans('layout.image')}}</label>
                                        </div>
                                        <!-- Button trigger modal -->
                                        <div class="image-box image">
                                            <input type="hidden" name="image" value="{{old('image',$category->image)}}"
                                                   class="image-data">
                                            <div class="preview-image-wrapper " style="">
                                                <img
                                                    src="{{ $category->image? url_storage($category->image):asset('images/placeholder.png')}}"
                                                    alt="Preview image" class="preview_image" width="150">
                                                <a class="btn_remove_image" title="Remove image">
                                                    <i class="fa fa-times"></i>
                                                </a>
                                            </div>
                                            <div class="image-box-actions">
                                                <a href="#" class="btn_gallery" data-result="image"
                                                   data-bs-toggle="modal"
                                                   data-bs-target="#exampleModal">
                                                    {{trans('layout.choose_image')}}
                                                </a>
                                            </div>
                                        </div>

                                    </div>


                                </div>

                                <div class="clearfix"></div>
                            </div>
                            <div class="widget meta-boxes d-flex justify-content-center">
                                <div class="widget-body">
                                    <div class="btn-set">
                                        <a  href="{{route('categories.index')}}" class="float-left btn btn-warning mr-5"> {{trans('layout.cancel')}}</a>
                                        <button type="submit" name="submit" value="save" class="btn btn-info">
                                            <i class="fa fa-save"></i>  {{trans('layout.update')}}
                                        </button>

                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>

                </div>
            </form>
        </div>
    </div>
    @include('partials.media')
@stop

