@extends('layouts.app')
@section('content')
    <div class=" rv-media-integrate-wrapper">
        {{ Breadcrumbs::render('licenses.show',$license->id) }}
        <div id="main" class="">
            <div class="table-wrapper">
                <div class="table-datatable row">
                    <div class="breadcrumb"><h4> {{$license->name}} </h4></div>
                    <ul class="nav nav-tabs" id="myTab" role="tablist">
                        <li class="nav-item" role="presentation">
                            <button class="nav-link active" id="detail-tab" data-bs-toggle="tab"
                                    data-bs-target="#detail" type="button"
                                    role="tab" aria-controls="detail" aria-selected="true">{{trans('layout.detail')}}
                            </button>
                        </li>
                        <li class="nav-item" role="presentation">
                            <button class="nav-link" id="seats-tab" data-bs-toggle="tab" data-bs-target="#seats"
                                    type="button"
                                    role="tab" aria-controls="seats"
                                    aria-selected="false">{{trans('layout.license.seats')}}
                            </button>
                        </li>
                        {{--            <li class="nav-item" role="presentation">--}}
                        {{--                <button class="nav-link" id="contact-tab" data-bs-toggle="tab" data-bs-target="#contact" type="button"--}}
                        {{--                        role="tab" aria-controls="contact" aria-selected="false">Checkout history--}}
                        {{--                </button>--}}
                        {{--            </li>--}}
                    </ul>
                    <div class="tab-content" id="myTabContent">
                        <div class="tab-pane fade show active" id="detail" role="tabpanel" aria-labelledby="detail-tab">
                            <div class="row">
                                <div class="col-md-9 col-12">
                                    <table class="table table-striped " id="sortableDetail"
                                           data-url="{{route('show.table.detail')}}" data-setting-key="license">
                                        <tbody>
                                        @foreach($table_detail as $item)
                                            <tr data-detail="{{$item->value}}" data-label="{{$item->label}}">
                                                <td class=" text-left"><span class="handle">
                                            <i class="fa fa-ellipsis-v"></i>
                                            <i class="fa fa-ellipsis-v"></i>
                                            </span>
                                                </td>
                                                <td class="w-25">{{trans($item->label)}}</td>
                                                @switch($item->value)
                                                    @case('company')
                                                    <td>{{$license[$item->value]['name']}}</td>
                                                    @break
                                                    @case('category')
                                                    <td>{{$license->category->name}}</td>
                                                    @break
                                                    @default
                                                    <td>{{$license[$item->value]}}</td>
                                                    @break
                                                @endswitch
                                                <td class="w-5">
                                                    <div class="table-actions">
                                                        <a href="#" class="btn btn-icon btn-sm  deleteDetailAsset ">
                                                            <i class="fa fa-trash"></i>
                                                        </a>
                                                    </div>
                                                </td>
                                            </tr>
                                        @endforeach
                                        </tbody>
                                    </table>
                                    <div class="form-group row pt-2">
                                        <label for="select-field-detail"
                                               class="col-md-2 col-form-label ">{{trans('layout.asset.custom_detail')}}</label>
                                        <div class="col-md-4">
                                            <select id="select-field-detail" class="form-control w-100 js-data-ajax"
                                                    data-url="{{route('api.license.detail','license')}}" name="field-detail">
                                            </select>
                                        </div>
                                    </div>
                                </div>
                                <div class="col-md-3 col-12">
                                    <div class="card-body text-center">
                                        <div class="pt-3 ">
                                            <div class="qr-image" target=blank>
                                                {!! \QrCode::size(250)->generate(route('licenses.show',$license->id)); !!}
                                            </div>
                                            <a class="btn btn-info qr-download"
                                               href="{{route('licenses.qr.download',$license->id)}}">
                                                <i class="fa fa-download"></i>
                                            </a>
                                        </div>

                                        <span>{{$license->name}} - {{$license->serial}}</br>  QR CODE</span>
                                    </div>
                                </div>
                            </div>

                        </div>
                        <div class="tab-pane fade" id="seats" role="tabpanel" aria-labelledby="seats-tab">
                            <table class="table table-striped table-seats" id="table-seats">
                                <thead>
                                <tr>
                                    <td>{{trans('layout.seat')}}</td>
                                    <td>{{trans('layout.user.user')}}</td>
                                    <td>{{trans('layout.asset.asset')}}</td>
                                    <td>{{trans('layout.checkout_check')}}</td>
                                    <td class="hidden">Location</td>
                                    <td class="hidden">Location</td>
                                </tr>
                                </thead>
                                <tbody>
                                </tbody>
                            </table>
                        </div>
                        <div class="tab-pane fade" id="contact" role="tabpanel" aria-labelledby="contact-tab">...</div>
                    </div>
                </div>
            </div>
        </div>
    </div>

    @include('partials.table.modal-item', [
    'type' => 'info',
    'name' => 'modal-checkout-license',
    'title' => trans('layout.checkout'),
    'with'=>'modal-lg',
    'content' => view('partials.checkout.license')->render(),
    'action_name' => 'Checkout',
    'action_button_attributes' => [
        'class' => 'confirm-checkout-button',
    ],
])

@stop
@push('js')
    <script>
        $(document).ready(function () {
            let table = $('#table-seats').DataTable({
                searching: false,
                language: {
                    "url": window._locale
                },
                lengthChange: false,
                ajax: '{{route('licenses.show',$license->id)}}',
                columns: [
                    {data: 'seat', name: 'seat', className: 'text-left'},
                    {data: 'user', name: 'user', className: 'text-left'},
                    {data: 'asset', name: 'asset'},
                    {data: 'checkout', name: 'checkout'},
                    {data: 'license', name: 'license', className: 'column-key-name hidden'},
                    {data: 'serial', name: 'serial', className: 'column-key-serial hidden'},
                ]
            });
            table.on('order.dt search.dt', function () {
                table.column(0, {search: 'applied', order: 'applied'}).nodes().each(function (cell, i) {
                    cell.innerHTML = `Seat ${i + 1}`;
                });
            }).draw();
        });
    </script>
@endpush

