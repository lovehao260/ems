@extends('layouts.app')
@section('content')
    <div class=" rv-media-integrate-wrapper ">
        {{ Breadcrumbs::render('licenses.add') }}
        <div id="main" class=" container">
            <form method="POST" action="{{route( 'licenses.store')}}" accept-charset="UTF-8">
                @csrf
                <div class="row">
                    <div class="col-md-12 ">
                        <div class="main-form content-page">
                            <div class="form-body">
                                <div class="form-group ">
                                    <div class="row">
                                        <div class="col-md-2 text-right">
                                            <label for="name" class="text-title-field control-label required">
                                                {{trans('layout.name')}}</label>
                                        </div>
                                        <div class="col-md-10">
                                            <input class="form-control @error('name') is-invalid @enderror"
                                                   name="name" type="text" id="name" value="{{old('name')}}">
                                            @error('name')
                                            <span class="text-danger">{{ $message }}</span>
                                            @enderror
                                        </div>
                                    </div>
                                </div>
                                <div class="form-group  ">
                                    <div class="row">
                                        <div class="col-md-2 text-right">
                                            <label for="select-company" class="control-label required">
                                                {{trans('layout.category.category')}}</label>
                                        </div>
                                        <div class="col-md-10 w-100">
                                            @include('partials.select.category', ['category_type' => 'license' ])
                                        </div>

                                    </div>
                                </div>
                                <div class="form-group ">
                                    <div class="row">
                                        <div class="col-md-2 text-right">
                                            <label for="serial" class="text-title-field control-label">
                                                {{trans('layout.license.key')}}</label>
                                        </div>
                                        <div class="col-md-10">
                                            <textarea class="form-control" name="serial"
                                                      type="text" id="serial">{{old('serial')}}</textarea>
                                        </div>
                                    </div>
                                </div>
                                <div class="form-group ">
                                    <div class="row">
                                        <div class="col-md-2 text-right">
                                            <label for="qty" class="text-title-field control-label required">
                                                {{trans('layout.license.seats')}}</label>
                                        </div>
                                        <div class="col-3">
                                            <input class="form-control @error('seats') is-invalid @enderror"
                                                   name="seats"
                                                   type="text" id="seats" value="{{old('seats')}}">
                                            @error('seats')
                                            <span class="text-danger">{{ $message }}</span>
                                            @enderror
                                        </div>
                                    </div>
                                </div>
                                <div class="form-group  ">
                                    <div class="row">
                                        <div class="col-md-2 text-right">
                                            <label for="select-company"
                                                   class="control-label ">{{trans('layout.company.company')}}</label>
                                        </div>
                                        <div class="col-10">
                                            @include('partials.select.company')
                                        </div>

                                    </div>
                                </div>
                                <div class="form-group ">
                                    <div class="row">
                                        <div class="col-md-2 text-right">
                                            <label for="license_name" class="text-title-field control-label">
                                                {{trans('layout.license.to_name')}}</label>
                                        </div>
                                        <div class="col-md-10">
                                            <input class="form-control" name="license_name"
                                                   type="text" id="license_name" value="{{old('license_name')}}">

                                        </div>
                                    </div>
                                </div>
                                <div class="form-group ">
                                    <div class="row">
                                        <div class="col-md-2 text-right">
                                            <label for="name" class="text-title-field control-label ">
                                                {{trans('layout.license.to_mail')}}</label>
                                        </div>
                                        <div class="col-md-10">
                                            <input class="form-control @error('license_email') is-invalid @enderror"
                                                   name="license_email"
                                                   type="text" id="license_email" value="{{old('license_email')}}">
                                            @error('license_email')
                                            <span class="text-danger">{{ $message }}</span>
                                            @enderror
                                        </div>
                                    </div>
                                </div>
                                <div class="form-group ">
                                    <div class="row ">
                                        <div class="col-md-2 text-right">
                                            <label for="min_amt" class="text-title-field control-label">
                                                {{trans('layout.reassignable')}}</label>
                                        </div>
                                        <div class="col-3">
                                            <input class="table-check-all" name="reassignable" type="checkbox"
                                                   value="1">
                                            <label class="form-check-label"
                                                   for="exampleCheck1">{{trans('layout.yes')}}</label>
                                            @error('min_amt')
                                            <span class="text-danger">{{ $message }}</span>
                                            @enderror
                                        </div>
                                    </div>

                                </div>

                                <div class="form-group ">
                                    <div class="row ">
                                        <div class="col-md-2 text-right">
                                            <label for="serial" class="text-title-field control-label">
                                                {{trans('layout.order_number')}}</label>
                                        </div>
                                        <div class="col-10">
                                            <input class="form-control"
                                                   data-counter="120" name="order_number"
                                                   type="text" id="order_number" value="{{old('order_number')}}">
                                        </div>
                                    </div>
                                </div>
                                <div class="form-group ">
                                    <div class="row ">
                                        <div class="col-md-2 text-right">
                                            <label for="order_number " class="text-title-field control-label">
                                                {{trans('layout.purchase_cost')}}</label>
                                        </div>
                                        <div class="col-3">
                                            <div class="input-group">
                                                <input type="text" class="form-control @error('purchase_cost') is-invalid @enderror"
                                                       {{old('purchase_cost')}} name="purchase_cost"
                                                       aria-label="Amount (to the nearest dollar)">
                                                <div class="input-group-append">
                                                    <span class="input-group-text">$</span>
                                                </div>
                                                @error('purchase_cost')
                                                <span class="text-danger">{{ $message }}</span>
                                                @enderror
                                            </div>
                                        </div>
                                    </div>

                                </div>

                                <div class="form-group ">
                                    <div class="row">
                                        <div class="col-md-2 text-right">
                                            <label for="purchase_date" class="text-title-field control-label">
                                                {{trans('layout.purchase_date')}}</label>
                                        </div>
                                        <div class="col-3">
                                            <div class="input-group">
                                                <input type="text" class="form-control datepicker
                                                        @error('expiration_date') is-invalid @enderror" name="purchase_date"
                                                       data-date-format="yyyy-mm-dd" autocomplete="off"
                                                       id="purchase_date">
                                                <div class="input-group-append">
                                                    <span class="input-group-text">
                                                        <i class="fa fa-calendar"></i>
                                                    </span>
                                                </div>
                                                @error('expiration_date')
                                                <span class="text-danger">{{ $message }}</span>
                                                @enderror
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <div class="form-group ">
                                    <div class="row">
                                        <div class="col-md-2 text-right">
                                            <label for="expiration_date" class="text-title-field control-label">
                                                {{trans('layout.license.expiration_date')}}</label>
                                        </div>
                                        <div class="col-3">
                                            <div class="input-group">
                                                <input type="text" class="form-control datepicker
                                                      @error('expiration_date') is-invalid @enderror"
                                                       data-date-format="yyyy-mm-dd" name="expiration_date"
                                                       value=" {{old('expiration_date')}}"
                                                       autocomplete="off" id="expiration_date">
                                                <div class="input-group-append">
                                                    <span class="input-group-text">
                                                        <i class="fa fa-calendar"></i>
                                                    </span>
                                                </div>
                                                @error('expiration_date')
                                                <span class="text-danger">{{ $message }}</span>
                                                @enderror
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <div class="form-group ">
                                    <div class="row ">
                                        <div class="col-md-2 text-right">
                                            <label for="termination_date" class="text-title-field control-label">
                                                {{trans('layout.license.termination_date')}}
                                            </label>
                                        </div>
                                        <div class="col-3">
                                            <div class="input-group">
                                                <input type="text" class="form-control datepicker
                                                        @error('termination_date') is-invalid @enderror"
                                                       name="termination_date" value="{{old('termination_date')}}"
                                                       data-date-format="yyyy-mm-dd"
                                                       autocomplete="off" id="termination_date">
                                                <div class="input-group-append">
                                                    <span class="input-group-text">
                                                        <i class="fa fa-calendar"></i>
                                                    </span>
                                                </div>
                                                @error('termination_date')
                                                <span class="text-danger">{{ $message }}</span>
                                                @enderror
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <div class="form-group ">
                                    <div class="row ">
                                        <div class="col-md-2 text-right">
                                            <label for="maintained" class="text-title-field control-label">
                                                {{trans('layout.license.maintained')}}</label>
                                        </div>
                                        <div class="col-3">
                                            <input name="reassignable" id="maintained" type="checkbox" value="1"
                                                   class="table-check-all">
                                            <label for="maintained"
                                                   class="form-check-label">{{trans('layout.yes')}}</label>
                                        </div>
                                    </div>
                                </div>
                                <div class="form-group ">
                                    <div class="row">
                                        <div class="col-md-2 text-right">
                                            <label for="note" class="text-title-field control-label ">
                                                {{trans('layout.note')}}</label>
                                        </div>
                                        <div class="col-md-10">
                                            <textarea name="note" type="text" id="serial"
                                                      class="form-control">{{old('note')}}</textarea></div>
                                    </div>
                                </div>

                                <div class="clearfix"></div>
                            </div>
                            <div class="widget meta-boxes d-flex justify-content-center">
                                <div class="widget-body">
                                    <div class="btn-set">
                                        <a href="{{route('licenses.index')}}"
                                           class="float-left btn btn-warning mr-5">   {{trans('layout.cancel')}}</a>
                                        <button type="submit" name="submit" value="save" class="btn btn-info">
                                            <i class="fa fa-save"></i> {{trans('layout.save')}}
                                        </button>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </form>
        </div>
    </div>

@stop

