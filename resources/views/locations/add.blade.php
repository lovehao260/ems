@extends('layouts.app')
@section('content')
    <div class=" rv-media-integrate-wrapper">
        {{ Breadcrumbs::render('locations.add') }}
        <div id="main" class="container">
            <form method="POST" action="{{route( 'locations.store')}}" accept-charset="UTF-8">
                @csrf
                <div class="row">
                    <div class="col-md-12 ">
                        <div class="main-form content-page">
                            <div class="form-body">
                                <div class="form-group ">
                                    <div class="row">
                                        <div class="col-md-3 text-right">
                                            <label for="name" class="text-title-field control-label required">
                                                {{trans('layout.name')}}</label>
                                        </div>
                                        <div class="col-md-9">
                                            <input   name="name" type="text" id="name" value="{{old('name')}}"
                                                   class="form-control  @error('name') is-invalid @enderror ">
                                            @error('name')
                                            <span class="text-danger">{{ $message }}</span>
                                            @enderror
                                        </div>
                                    </div>
                                </div>
                                <div class="form-group ">
                                    <div class="row">
                                        <div class="col-md-3 text-right">
                                            <label for="location_id" class="text-title-field control-label ">
                                                {{trans('layout.location.parent')}}</label>
                                        </div>
                                        <div class="col-md-9">
                                            @include('partials.select.locations',['location_id'=>old('location_id')])
                                        </div>
                                    </div>
                                </div>
                                <div class="form-group ">
                                    <div class="row">
                                        <div class="col-md-3 text-right">
                                            <label for="manager_id" class="text-title-field control-label ">
                                                {{trans('layout.manager')}}</label>
                                        </div>
                                        <div class="col-md-9">
                                            @include('partials.select.manager',['manager_id'=>old('manager_id')])
                                        </div>
                                    </div>
                                </div>
                                <div class="form-group ">
                                    <div class="row">
                                        <div class="col-md-3 text-right">
                                            <label for="currency" class="text-title-field control-label ">
                                                {{trans('layout.currency')}}</label>
                                        </div>
                                        <div class="col-md-2">
                                            <input placeholder="USD"  name="currency" type="text"
                                                   id="currency" value="{{old('currency')}}" class="form-control w-50">
                                        </div>
                                    </div>
                                </div>
                                <div class="form-group ">
                                    <div class="row">
                                        <div class="col-md-3 text-right">
                                            <label for="address" class="text-title-field control-label ">
                                                {{trans('layout.address')}}</label>
                                        </div>
                                        <div class="col-md-9">
                                            <input name="address" type="text" id="address" value="{{old('address')}}" class="form-control ">
                                            <input name="address2" type="text" id="address2" value="{{old('address2')}}" class="form-control mt-2">
                                        </div>
                                    </div>
                                </div>
                                <div class="form-group ">
                                    <div class="row">
                                        <div class="col-md-3 text-right">
                                            <label for="city" class="text-title-field control-label">
                                                {{trans('layout.location.city')}}</label>
                                        </div>
                                        <div class="col-md-9">
                                            <input name="city" type="text" id="city" value="{{old('city')}}"
                                                   class="form-control @error('city') is-invalid @enderror">
                                            @error('city')
                                            <span class="text-danger">{{ $message }}</span>
                                            @enderror
                                        </div>
                                    </div>
                                </div>
                                <div class="form-group ">
                                    <div class="row">
                                        <div class="col-md-3 text-right">
                                            <label for="state" class="text-title-field control-label">
                                                {{trans('layout.location.state')}}</label>
                                        </div>
                                        <div class="col-md-9">
                                            <input name="state" type="text" id="state" value="{{old('state')}}"
                                                   class="form-control  @error('state') is-invalid @enderror">
                                            @error('state')
                                            <span class="text-danger">{{ $message }}</span>
                                            @enderror
                                        </div>
                                    </div>
                                </div>
                                <div class="form-group ">
                                    <div class="row">
                                        <div class="col-md-3 text-right">
                                            <label for="country" class="text-title-field control-label"> {{trans('layout.country')}}</label>
                                        </div>
                                        <div class="col-md-6">
                                            <select id="select-country" class="form-control w-100 " name="country">
                                                @foreach($countries as $key=> $item)
                                                    <option value="{{$key}}" {{$key==old('category_id')? "selected":""}}>
                                                        {{$item}}
                                                    </option>
                                                @endforeach
                                            </select>
                                        </div>
                                    </div>
                                </div>
                                <div class="form-group ">
                                    <div class="row">
                                        <div class="col-md-3 text-right">
                                            <label for="zip" class="text-title-field control-label">{{trans('layout.location.zip')}}</label>
                                        </div>
                                        <div class="col-md-9">
                                            <input name="zip" type="text" id="zip" value="{{old('zip')}}" class="form-control ">
                                        </div>
                                    </div>
                                </div>
                                <div class="form-group ">
                                    <div class="row">
                                        <div class="col-md-3 text-right">
                                            <label for="image" class="text-title-field control-label ">{{trans('layout.image')}}</label>
                                        </div>
                                        <div class="col-md-9">
                                            <!-- Button trigger modal -->
                                            <div class="image-box image">
                                                <input type="hidden" name="image" value="{{old('image')}}" class="image-data">
                                                <div class="preview-image-wrapper " style="">
                                                    <img src="{{asset('images/placeholder.png')}}" alt="Preview image"
                                                         class="preview_image" width="150">
                                                    <a class="btn_remove_image" title="Remove image">
                                                        <i class="fa fa-times"></i>
                                                    </a>
                                                </div>
                                                <div class="image-box-actions">
                                                    <a href="#" class="btn_gallery" data-result="image" data-bs-toggle="modal"
                                                       data-bs-target="#exampleModal">
                                                        {{trans('layout.choose_image')}}
                                                    </a>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>

                                <div class="clearfix"></div>
                            </div>
                            <div class="widget meta-boxes d-flex justify-content-center">
                                <div class="widget-body">
                                    <div class="btn-set">
                                        <a  href="{{route('locations.index')}}" class="float-left btn btn-warning mr-5">{{trans('layout.cancel')}}</a>
                                        <button type="submit" name="submit" value="save" class="btn btn-info">
                                            <i class="fa fa-save"></i> {{trans('layout.save')}}
                                        </button>

                                    </div>
                                </div>
                            </div>
                        </div>

                    </div>

                </div>

            </form>
        </div>
    </div>
    @include('partials.media')
@stop

