@extends('layouts.app')
@section('content')
    @if ($breadcrumb)
        {{ Breadcrumbs::render($breadcrumb) }}
    @endif
    <div class="table-wrapper ">
        <div class="table-datatable">
            <div class="portlet light bordered portlet-no-padding">
                <div class="portlet-title">
                    <div class="caption">
                        <div class="wrapper-action">
                            @if ($actions)
                                <div class="btn-group">
                                    <a class="btn btn-secondary dropdown-toggle" href="#" data-toggle="dropdown">
                                        {{trans('table.bulk_actions')}}</a>
                                    <ul class="dropdown-menu">
                                        @foreach ($actions as $action)
                                            <li>
                                                {!! $action !!}
                                            </li>
                                        @endforeach
                                    </ul>
                                </div>
                            @endif
                        </div>
                    </div>
                </div>
                <div class="portlet-body">
                    <div class="table-responsive dataTable @if ($actions) table-has-actions @endif">
                        <div class="position-relative">
                            @section('main-table')
                                {!! $dataTable->table(compact('id', 'class'), false) !!}
                            @show
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>

    @include('partials.table.modal')
    @switch($breadcrumb)
        @case('components')
        @include('partials.table.modal-item', [
            'type' => 'info',
            'name' => 'modal-checkout-accessories',
            'title' => trans('layout.checkout_check'),
            'with'=>'modal-lg',
            'content' => view('partials.checkout.components')->render(),
            'action_name' => trans('layout.checkout'),
            'action_button_attributes' => [
                'class' => 'confirm-checkout-button',
            ],
        ])
        @break
        @case('licenses')
            @include('partials.table.modal-item', [
              'type' => 'info',
              'name' => 'modal-checkout-license',
              'title' =>  trans('layout.checkout_check'),
              'with'=>'modal-lg',
              'content' => view('partials.checkout.license')->render(),
              'action_name' => trans('layout.checkout'),
              'action_button_attributes' => [
                  'class' => 'confirm-checkout-button',
              ],
          ])
        @break
        @case('accessories')
            @include('partials.table.modal-item', [
                'type' => 'info',
                'name' => 'modal-checkout-accessories',
                'title' =>  trans('layout.checkout_check'),
                'with'=>'modal-lg',
                'content' => view('partials.checkout.accessories')->render(),
                'action_name' => trans('layout.checkout'),
                'action_button_attributes' => [
                    'class' => 'confirm-checkout-button',
                ],
            ])
        @break

    @endswitch

@stop
@section('javascript')
    {!! $dataTable->scripts() !!}
@stop
