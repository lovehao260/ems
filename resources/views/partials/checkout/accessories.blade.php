<div id="form-checkout">
    <div class="row">
        <div class="box box-default">
            <div class="box-header with-border">
                <h2 class="box-title licenses-name"> Lê Minh Hảo</h2>
            </div>
            <div class="box-body">
                <!-- Asset name -->
                <div class="form-group row">
                    <label class="col-sm-3 control-label">{{trans('layout.accessory.accessory')}}{{trans('layout.name')}}</label>
                    <div class="col-md-6">
                        <p class="form-control-static licenses-name">Lê Minh Hảo</p>
                    </div>
                </div>
                <!-- Serial -->
                <div class="form-group row">
                    <label class="col-sm-3 control-label">{{trans('layout.accessory.accessory')}}{{trans('layout.category.category')}}</label>
                    <div class="col-md-9">
                        <p class="form-control-static licenses-serial" style="word-wrap: break-word;">
                            ssssss
                        </p>
                    </div>
                </div>
                <div class="form-checkout-to">
                    <input name="checkout_to_type" value="user" aria-label="checkout_to_type" hidden
                           type="radio" checked="checked">
                    <div id="assigned_user" class="form-group row">
                        <label for="assigned_to" class="col-md-3 control-label required">{{trans('layout.select_user')}}
                        </label>
                        <div class="col-md-6 ">
                            @include('partials.select.manager')
                        </div>
                    </div>


                </div>

                <div class="form-group row ">
                    <label for="note" class="col-md-3 control-label">{{trans('layout.note')}}</label>
                    <div class="col-md-7">
                        <textarea class=" form-control" id="note" name="note"></textarea>

                    </div>
                </div>
            </div>

        </div> <!-- /.box-->
    </div>
</div>
