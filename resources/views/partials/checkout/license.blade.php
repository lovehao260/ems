<div id="form-checkout">
    <div class="row">
        <div class="box box-default">
            <div class="box-header with-border">
                <h2 class="box-title licenses-name"> Lê Minh Hảo</h2>
            </div>
            <div class="box-body">
                <!-- Asset name -->
                <div class="form-group row">
                    <label class="col-sm-3 control-label">{{trans('layout.license.license')}} {{trans('layout.name')}} </label>
                    <div class="col-md-6">
                        <p class="form-control-static licenses-name">Lê Minh Hảo</p>
                    </div>
                </div>
                <!-- Serial -->
                <div class="form-group row">
                    <label class="col-sm-3 control-label">{{trans('layout.component.serial')}} </label>
                    <div class="col-md-9">
                        <p class="form-control-static licenses-serial" style="word-wrap: break-word;">
                            ssssss
                        </p>
                    </div>
                </div>
                <div class="form-checkout-to">
                    <div class="form-group row" id="assignto_selector">
                        <label for="checkout_to_type" class="col-md-3 control-label">{{trans('layout.checkout')}} {{trans('table.to')}}</label>
                        <div class="col-md-8">
                            <div class="btn-group" data-toggle="buttons">
                                <label class="btn btn-default active">
                                    <input name="checkout_to_type" value="user" aria-label="checkout_to_type" hidden
                                           type="radio" checked="checked"><i class="fa fa-user" aria-hidden="true"></i> {{trans('layout.user.user')}}
                                </label>
                                <label class="btn btn-default ">
                                    <input name="checkout_to_type" value="asset" aria-label="checkout_to_type" hidden
                                           type="radio"><i class="fa fa-barcode" aria-hidden="true"></i> {{trans('layout.asset.asset')}}
                                </label>

                            </div>
                        </div>
                    </div>

                    <div id="assigned_user" class="form-group row">
                        <label for="assigned_to" class="col-md-3 control-label required">{{trans('layout.user.user')}}</label>
                        <div class="col-md-6 ">
                            @include('partials.select.manager')
                        </div>
                    </div>
                </div>
                <div id="assigned_asset" class="form-group row " style="display: none">
                    <label for="assigned_asset"
                           class="col-md-3 control-label required">{{trans('layout.asset.asset')}}</label>
                    <div class="col-md-6 ">
                        @include('partials.select.asset')
                    </div>
                </div>


            <!-- Note -->
                <div class="form-group row ">
                    <label for="note" class="col-md-3 control-label">{{trans('layout.note')}}</label>
                    <div class="col-md-7">
                        <textarea class=" form-control" id="note" name="note"></textarea>

                    </div>
                </div>
            </div>

        </div> <!-- /.box-->
    </div>
</div>
