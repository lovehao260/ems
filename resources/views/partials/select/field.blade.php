<div class="d-flex @error('field_id') is-invalid @enderror">
    <div class="form-selects w-100">
        <select id="select-model" class="form-control w-100 js-data-ajax"
                data-url="{{route('api.fields.index',$fieldset_id)}}"  name="field_id">
            <option value="">Select model</option>
            @foreach(get_data_select('custom_fields') as $key=> $item)
                <option
                    value="{{$item->id}}"{{$item->id==old('field_id')? "selected":""}}>
                    {{$item->name}}
                </option>
            @endforeach
        </select>
        @error('model_id')
        <span class="text-danger">{{ $message }}</span>
        @enderror
    </div>
    {{--    <button class="btn btn-success ml-2" data-bs-toggle="modal"--}}
    {{--            data-bs-target="#manager-modal" type="button">New--}}
    {{--    </button>--}}
</div>
