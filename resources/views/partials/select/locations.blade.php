<div class="d-flex  @error('location_id') is-invalid @enderror">
    <div class="form-selects w-100">
        <select id="select-location" class="form-control w-100 js-data-ajax "
                data-url="{{route('api.locations.index')}}" name="location_id">
            <option value=""></option>
            @foreach(get_data_select('locations') as $key=> $item)

                @if(isset($location_id))
                    <option
                        value="{{$item->id}}" {{$item->id==old('location_id', $location_id)? "selected":""}}>
                        {{$item->name}}
                    </option>
                @else
                    <option
                        value="{{$item->id}}" {{$item->id==old('location_id')? "selected":""}}>
                        {{$item->name}}
                    </option>
                @endif
            @endforeach
        </select>
        @error('location_id')
        <span class="text-danger">{{ $message }}</span>
        @enderror
    </div>
{{--    <button class="btn btn-success ml-2" data-bs-toggle="modal"--}}
{{--            data-bs-target="#company-modal" type="button">New--}}
{{--    </button>--}}
</div>
<div class="clearfix"></div>
