<div class="d-flex  ">
    <div class="form-selects w-100">
        <select id="select-company" class="form-control w-100 js-data-ajax @error('company_id') is-invalid @enderror"
                data-url="{{route('api.companies.index')}}" name="company_id">
            <option value=""></option>
            @foreach(get_data_select('companies') as $key=> $item)

                @if(isset($company_id))
                    <option
                        value="{{$item->id}}" {{$item->id==old('company_id', $company_id)? "selected":""}}>
                        {{$item->name}}
                    </option>
                @else
                    <option
                        value="{{$item->id}}" {{$item->id==old('company_id')? "selected":""}}>
                        {{$item->name}}
                    </option>
                @endif
            @endforeach
        </select>
        @error('company_id')
        <span class="text-danger">{{ $message }}</span>
        @enderror
    </div>
{{--    <button class="btn btn-success ml-2" data-bs-toggle="modal"--}}
{{--            data-bs-target="#company-modal" type="button">New--}}
{{--    </button>--}}
</div>
<div class="clearfix"></div>
