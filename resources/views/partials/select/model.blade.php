<div class="d-flex @error('model_id') is-invalid @enderror">
    <div class="form-selects w-100">
        <select id="select-model" class="form-control w-100 js-data-ajax"
                data-url="{{route('api.models.index')}}" name="model_id"   data-url-field="{{route('api.assets.field',$asset_id??'')}}"  >
            <option value="">Select model</option>
            @foreach(get_data_select('models') as $key=> $item)
                @if(isset($model_id))
                    <option
                        value="{{$item->id}}"{{$item->id==old('model_id' ,$model_id)? "selected":""}}>
                        {{$item->name}}
                    </option>
                @else
                    <option
                        value="{{$item->id}}"{{$item->id==old('model_id')? "selected":""}}>
                        {{$item->name}}
                    </option>
                @endif
            @endforeach
        </select>
        @error('model_id')
        <span class="text-danger">{{ $message }}</span>
        @enderror
    </div>
    {{--    <button class="btn btn-success ml-2" data-bs-toggle="modal"--}}
    {{--            data-bs-target="#manager-modal" type="button">New--}}
    {{--    </button>--}}
</div>
