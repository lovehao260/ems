
<div class="d-flex">
    <select id="select-user" class="form-control w-100 select-user js-data-ajax "
            data-url="{{route('api.users.index')}}" name="manager_id">
        <option value="">Manager</option>
        @foreach(get_data_select('users') as $key=> $item)
            @if(isset($manager_id))
                <option
                    value="{{$item->id}}"{{$item->id==old('manager_id' ,$manager_id)? "selected":""}}>
                    {{$item->first_name}} {{$item->last_name}}
                </option>
            @else
                <option
                    value="{{$item->id}}"{{$item->id==old('manager_id')? "selected":""}}>
                    {{$item->first_name}} {{$item->last_name}}
                </option>
            @endif
        @endforeach
    </select>
{{--    <button class="btn btn-success ml-2" data-bs-toggle="modal"--}}
{{--            data-bs-target="#manager-modal" type="button">New--}}
{{--    </button>--}}
</div>
