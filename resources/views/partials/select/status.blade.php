<div class="d-flex @error('status_id') is-invalid @enderror">
    <div class="form-selects w-100">
        <select id="select-user" class="form-control w-100"
                name="status_id">
            {{--            <option value="">{{trans('layout.select_item')}}</option>--}}
            @foreach(get_data_select('status_labels') as $key=> $item)
                @if(isset($status_id))
                    @if(isset($checkout))
                        <option value="{{$item->id}}" {{$item->id==old('status_id' ,$status_id)? "selected":""}}
                        class="{{$item->deployable?'':'hidden'}}">
                            {{$item->name}}
                        </option>
                    @else
                        <option value="{{$item->id}}"{{$item->id==old('status_id' ,$status_id)? "selected":""}}>
                            {{$item->name}}
                        </option>
                    @endif
                @else
                    <option
                        value="{{$item->id}}"{{$item->id==old('status_id')? "selected":""}}>
                        {{$item->name}}
                    </option>
                @endif
            @endforeach
        </select>
        @error('status_id')
        <span class="text-danger">{{ $message }}</span>
        @enderror
    </div>
</div>
