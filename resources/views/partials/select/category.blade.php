<div class="d-flex @error('category_id') is-invalid @enderror">
    <div class="form-selects w-100">
        <select id="select-category" class="form-control w-100 js-data-ajax  "
                data-url="{{route('api.categories.index')}}" data-category="{{$category_type}}" name="category_id">
            <option value=""></option>
            @foreach(get_data_select('categories') as $key=> $item)

                @if(isset($category_id))
                    <option
                        value="{{$item->id}}" {{$item->id==old('category_id', $category_id)? "selected":""}}>
                        {{$item->name}}
                    </option>
                @else
                    <option
                        value="{{$item->id}}" {{$item->id==old('category_id')? "selected":""}}>
                        {{$item->name}}
                    </option>
                @endif
            @endforeach
        </select>
        @error('category_id')
        <span class="text-danger">{{ $message }}</span>
        @enderror
    </div>
{{--    <button class="btn btn-success ml-2" data-bs-toggle="modal"--}}
{{--            data-bs-target="#company-modal" type="button">New--}}
{{--    </button>--}}
</div>
<div class="clearfix"></div>
