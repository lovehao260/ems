<label for="select-department" class="control-label ">Select Company</label>
<div class="d-flex">
    <select id="select-department" class="form-control w-100 js-data-ajax"
            data-url="{{route('api.department.index')}}" name="company_id">
        <option value=""></option>
        @foreach(get_data_select('department') as $key=> $item)

            @if(isset($department_id))
                <option
                    value="{{$item->id}}" {{$item->id==old('department_id', $department_id)? "selected":""}}>
                    {{$item->name}}
                </option>
            @else
                <option
                    value="{{$item->id}}" {{$item->id==old('department_id')? "selected":""}}>
                    {{$item->name}}
                </option>
            @endif
        @endforeach
    </select>
{{--    <button class="btn btn-success ml-2" data-bs-toggle="modal"--}}
{{--            data-bs-target="#company-modal" type="button">New--}}
{{--    </button>--}}
</div>
