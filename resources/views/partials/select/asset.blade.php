
<div class="d-flex">
    <select id="select-asset" class="form-control w-100 js-data-ajax"
            data-url="{{route('api.assets.index')}}" name="asset_id">
        <option value="">Select Asset</option>
        @foreach(get_data_select('assets') as $key=> $item)
            @if(isset($asset_id))
                <option
                    value="{{$item->id}}"{{$item->id==old('manager_id' ,$asset_id)? "selected":""}}>
                    {{$item->name}}
                </option>
            @else
                <option
                    value="{{$item->id}}"{{$item->id==old('manager_id')? "selected":""}}>
                    {{$item->name}}
                </option>
            @endif
        @endforeach
    </select>
{{--    <button class="btn btn-success ml-2" data-bs-toggle="modal"--}}
{{--            data-bs-target="#manager-modal" type="button">New--}}
{{--    </button>--}}
</div>
