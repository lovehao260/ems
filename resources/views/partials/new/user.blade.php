<div class="form-group ">
    <label for="first_name_modal" class="text-title-field control-label required">
        First Name</label>
    <input class="form-control input-value"
           placeholder="First Name"
           name="first_name"
           type="text" id="first_name_modal"
           value="">

</div>
<div class="form-group ">
    <label for="last_name_modal" class="text-title-field control-label ">
        Last name</label>
    <input class="form-control input-value"
           placeholder="Last Name"
           data-counter="120" name="last_name"
           type="text" id="last_name_modal"
           value="">

</div>
<div class="form-group ">
    <label for="username_modal" class="text-title-field control-label required">Username</label>
    <input class="form-control input-value"
           placeholder="UserName"
           data-counter="120" name="username"
           type="text" id="username_modal"
           value="">

</div>
<div class="form-group ">
    <label for="email_modal" class="text-title-field control-label required">Email</label>
    <input class="form-control input-value"
           placeholder="Email"
           name="email"
           type="email" id="email_modal"
           value="">
</div>
<div class="form-group ">
    <label for="password_modal" class="text-title-field control-label required">Password</label>
    <input class="form-control input-value"
           placeholder="Password"
           name="password"
           type="password" id="password_modal"
           value="">
</div>
<div class="form-group ">
    <label for="password_confirmation_modal" class="text-title-field control-label required">Confirm Password</label>
    <input class="form-control input-value"
           placeholder="Confirm Password"
           data-counter="120" name="password_confirmation"
           type="password" id="password_confirmation_modal"
           value="">
</div>
