@if($custom_fields_list)
    @foreach($custom_fields_list->fields as $field)
        <div class="form-group ">
            <div class="row">
                <div class="col-md-2 text-right">
                    <label for="{{$field->db_column}}"
                           class="text-title-field control-label text-capitalize">{{$field->name}}</label>
                </div>
                <div class="col-md-10">
                    @switch($field->element)
                        @case('text')
                        <input name="default_values[{{$field->id}}]" type="text" id="{{$field->db_column}}"
                               value="{{default_value_field($field->id, $mode_id??'')}}"
                               placeholder="{{$field->format}}"
                               class="form-control">
                        @break
                        @case('textarea')
                        <textarea id="{{$field->db_column}}" name="default_values[{{$field->id}}]" rows="3"
                                  class="form-control">{{default_value_field($field->id, $mode_id??'')??""}}</textarea>
                        @break
                        @case('listbox')
                        <select name="default_values[{{$field->id}}]" class="form-control">
                            <option value="">Select Item</option>
                            @foreach(explode("\n", $field->field_values) as $value)
                                <option value="{{strtolower($value)}}"
                                    {{ default_value_field($field->id, $mode_id??'') ? (stristr($value,default_value_field($field->id, $mode_id??'')) ?'selected':""):" "}} >
                                    {{$value}}
                                </option>
                            @endforeach
                        </select>
                        @break
                        @case('checkbox'||'radio')
                        @foreach(explode("\n", $field->field_values) as $value)
                            <div class="col-12 flex-column ">
                                <input type="checkbox" name="default_values[{{$field->id}}]" value="{{$value}}"
                                    {{ default_value_field($field->id, $mode_id??'') ? (stristr($value,default_value_field($field->id, $mode_id??'')) ?'checked':""):" "}} >
                                <label class="text-capitalize"> {{$value}} </label>
                            </div>
                        @endforeach
                        @break
                    @endswitch
                    @if($field->help_text)
                        <p class="help-block">{{$field->help_text}}</p>
                    @endif
                </div>
            </div>
        </div>
    @endforeach
@endif

