@if($custom_fields_list)
    @foreach($custom_fields_list->fields as $field)
        <div class="form-group ">
            <div class="row">
                <div class="col-md-2 text-right">
                    <label for="{{$field->db_column}}"
                           class="text-title-field control-label text-capitalize">{{$field->name}}</label>
                </div>
                <div class="col-md-10">
                    @switch($field->element)
                        @case('text')
                        <input name="{{$field->db_column}}" type="text" id="{{$field->db_column}}"
                               value="@if(isset($asset) && $asset->{$field->db_column}){{$asset->{$field->db_column}??""}}
                               @else{{default_value_field($field->id, $model_id??'')??""}}@endif"
                               placeholder="{{$field->format}}" class="form-control">
                        @break
                        @case('textarea')
                        <textarea id="{{$field->db_column}}" name="{{$field->db_column}}"  class="form-control"
                                  rows="3">@if(isset($asset) && $asset->{$field->db_column}){{$asset->{$field->db_column}??""}}@else{{default_value_field($field->id, $model_id??'')??""}}@endif</textarea>
                        @break
                        @case('listbox')
                        <select name="{{$field->db_column}}" class="form-control">
                            <option value="">Select Item</option>
                            @foreach(explode("\n", $field->field_values) as $value)
                                <option value="{{strtolower($value)}}"
                                @if(isset($asset) && $asset->{$field->db_column})
                                    {{stristr($value,$asset->{$field->db_column} ) ?'selected':""}}
                                    @else
                                    {{ default_value_field($field->id, $model_id??'') ? (stristr($value,default_value_field($field->id, $model_id??'')) ?'selected':""):" "}}
                                    @endif>
                                    {{$value}}
                                </option>
                            @endforeach
                        </select>
                        @break
                        @case('checkbox'||'radio')
                        @foreach(explode("\n", $field->field_values) as $value)
                            <div class="col-12 flex-column ">
                                <input type="checkbox" name="{{$field->db_column}}" value="{{$value}}"
                                @if(isset($asset) && $asset->{$field->db_column})
                                    {{stristr($value,$asset->{$field->db_column} ) ?'checked':""}}
                                    @else
                                    {{ default_value_field($field->id, $model_id??'') ? (stristr($value,default_value_field($field->id, $model_id??'')) ?'selected':""):" "}}
                                    @endif>
                                <label class="text-capitalize"> {{$value}} </label>
                            </div>
                        @endforeach
                        @break
                    @endswitch
                        <p class="help-block">The speed of the processor on this device.</p>
                </div>
            </div>
        </div>
    @endforeach
@endif

