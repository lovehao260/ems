<div class="table-actions">
    @if (!empty($clone))
        <a href="{{ route($clone, $item->id) }}" class="btn btn-icon btn-sm btn-primary" data-toggle="tooltip"
           title="{{trans('table.clone')}}"><i class="fa fa-clone"></i></a>
    @endif
    @if (!empty($edit))
        <a href="{{ route($edit, $item->id) }}" class="btn btn-icon btn-sm btn-primary" data-toggle="tooltip"
          title="{{trans('table.edit')}}"><i class="fa fa-edit"></i></a>
    @endif

    @if (!empty($delete))
        <a href="#" class="btn btn-icon btn-sm btn-danger deleteDialog {{$disable==1 ? 'disabled':''}}" data-toggle="tooltip"
           data-section="{{ route($delete, $item->id) }}" role="button"
          title="{{trans('table.delete')}}">
            <i class="fa fa-trash"></i>
        </a>
    @endif
{{--        Show actions in trash list--}}
    @if (!empty($deleted))
        <a href="#" class="btn btn-icon btn-sm btn-danger deleteDialog" data-toggle="tooltip"
           data-section="{{ route($deleted, $item->id) }}" role="button"
          title="{{trans('table.delete')}}">
            <i class="fa fa-trash"></i>
        </a>
    @endif
    @if (!empty($restore))
        <a href="{{ route($restore, $item->id) }}" class="btn btn-icon btn-sm btn-primary" data-toggle="tooltip"
            title="{{trans('table.restore')}}">
            <i class="fa fa-retweet"></i>
        </a>
    @endif
</div>
