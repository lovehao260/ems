@if($status)
    <button  type="button" class="btn bg-purple btn-checkout " {{$avail==0? 'disabled':''}}
    data-parent-table="{{$table}}"
    data-checkout-url="{{  route('licenses.checkin', ['id'=>$id, 'seat_id' =>$seat_id ]) }}" data-checkin="true"> {{trans('layout.checkin')}} </button>
@else
    <button  type="button" class="btn btn-info btn-checkout " {{$avail==0? 'disabled':''}}
    data-parent-table="{{$table}}"
    data-checkout-url="{{ $seat_id ? route('licenses.checkout', ['id'=>$id, 'seat_id' =>$seat_id ]): route('licenses.checkout', ['id'=>$id ]) }}">{{trans('layout.checkout')}}  </button>
@endif
