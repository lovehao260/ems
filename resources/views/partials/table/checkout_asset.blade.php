@if($checkout)
    <a href="{{route('assets.checkout',$id)}}" disabled class="btn btn-info {{$status === 0 ? ' btn-disabled':''}}">{{trans('layout.checkout')}}  </a>
@else
    <a href="{{route('assets.checkin',$id)}}" class="btn bg-purple">{{trans('layout.checkin')}} </a>
@endif
