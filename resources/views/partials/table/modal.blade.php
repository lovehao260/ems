@include('partials.table.modal-item', [
    'type' => 'danger',
    'name' => 'modal-confirm-delete',
    'title' => trans('table.confirm_delete'),
    'content' => trans('table.confirm_delete_msg'),
    'action_name' =>trans('table.delete'),
    'action_button_attributes' => [
        'class' => 'delete-crud-entry',
    ],
])

@include('partials.table.modal-item', [
    'type' => 'danger',
    'name' => 'delete-many-modal',
    'title' => trans('table.confirm_delete'),
    'content' => trans('table.confirm_delete_many_msg'),
    'action_name' => trans('table.delete'),
    'action_button_attributes' => [
        'class' => 'delete-many-entry-button',
    ],
])

@include('partials.table.modal-item', [
    'type' => 'danger',
    'name' => 'delete-trash-modal',
    'title' => trans('table.empty_trash'),
    'content' => trans('table.confirm_empty_trash_msg'),
    'action_name' => trans('table.delete'),
    'action_button_attributes' => [
        'class' => 'delete-all-trash-button'
    ],
])

@include('partials.table.modal-item', [
    'type' => 'info',
    'name' => 'modal-bulk-change-items',
    'title' => trans('table.bulk_changes'),
    'content' => '<div class="modal-bulk-change-content"></div>',
    'action_name' => trans('layout.submit'),
    'action_button_attributes' => [
        'class' => 'confirm-bulk-change-button',
        'data-load-url' => route('tables.bulk-change.data'),
    ],
])

