@if($status)
    <button  type="button" class="btn bg-purple btn-checkout-button"
    data-parent-table="{{$table}}"
             data-checkout-url="{{  route($route,$id ) }}" data-checkin="true"> {{trans('layout.checkin')}}  </button>
@else
    <button  type="button" class="btn btn-info btn-checkout-button " {{$avail==0? 'disabled':''}}
    data-parent-table="{{$table}}"
             data-checkout-url="{{  route($route,$id) }}">{{trans('layout.checkout')}}  </button>
@endif
