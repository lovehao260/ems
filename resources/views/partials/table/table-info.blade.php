<span class="dt-length-records">
    <i class="fa fa-globe"></i> <span class="d-none d-sm-inline">{{ trans('tables.show_from') }}</span> _START_ {{ trans('tables.to') }} _END_ {{ trans('tables.in') }} <span class="badge bg-secondary bold badge-dt">_TOTAL_</span> <span class="hidden-xs">{{ trans('tables.records') }}</span>
</span>
