<!-- Modal -->
<div class="modal fade" id="exampleModal" tabindex="-1" aria-labelledby="exampleModalLabel" aria-hidden="true">
    <div class="modal-dialog modal-xl">
        <div class="modal-content">
            <div class="modal-header modal-header-media">
                <h4 class="modal-title"><i class="til_img"></i><strong> {{trans('layout.media')}}</strong></h4>
                <button type="button" class="btn-close" data-bs-dismiss="modal" aria-label="Close"></button>
            </div>
            <div class="modal-body">
                <media-index :insert="true"></media-index>
            </div>
        </div>
    </div>
</div>
