@extends('layouts.app')

@section('content')
<div class="container-pages">
    <div class="rows justify-content-center">
        <div class="col-2s slider-left">

        </div>
        <div class="col-8s slider-center">
            <div class="card">
                <div class="card-header">{{ __('Dashboard') }}</div>

                <div class="card-body">
                    @if (session('status'))
                        <div class="alert alert-success" role="alert">
                            {{ session('status') }}
                        </div>
                    @endif

                    {{ __('You are logged in!') }}
                </div>
            </div>
        </div>
        <div class="col-2s slider-right">

        </div>
    </div>
</div>

@endsection
<style>
    .rows{
        display: -webkit-flex;
        display: -ms-flexbox;
        display: flex;
        -webkit-flex-wrap: nowrap;
        -ms-flex-wrap: nowrap;
        flex-wrap: nowrap;
        margin-right: -7.5px;
        margin-left: -7.5px;
    }
    .justify-content-center {
        justify-content: center!important;
    }
    .slider-center{
        width: 900px;
        margin: 0 2%;
    }
    .slider-left, .slider-right{
        width: 10%;
        background: red;
        height: 100px;

    }
    @media (max-width: 1000px) {
        .slider-left, .slider-right{
            display: none;
        }
    }
</style>
