@extends('layouts.app')
@section('content')
    <div class=" rv-media-integrate-wrapper">
        {{ Breadcrumbs::render('settings.index') }}
        <div id="main" class="container">
            <div class="main-form ">
                <div class="row form-body">
                    <div class="col-md-4 col-12">
                        <div class="annotated-section-title pd-all-20">
                            <h4>{{trans('layout.setting.general_info')}}</h4>
                        </div>
                        <div class="annotated-section-description pd-all-20 p-none-t">
                            <p class="color-note">{{trans('layout.setting.site_info')}}</p>
                        </div>
                    </div>
                    <div class="col-md-8 col-12 content-page">
                        <form method="POST" action="{{route('settings.store')}}" accept-charset="UTF-8">
                            @csrf
                            <div class="wrapper-content pd-all-20">
                                <div class="form-group mb-3">
                                    <label class="text-title-field" for="admin-logo">{{trans('layout.setting.admin_logo')}}
                                    </label>
                                    <div class="admin-logo-image-setting">
                                        <div class="image-box admin_logo">
                                            <input type="hidden" name="admin_logo"
                                                   value="{{ setting('admin_logo')}}" class="image-data">
                                            <div class="preview-image-wrapper  preview-image-wrapper-not-allow-thumb ">
                                                <img src="{{ setting('admin_logo')? url_storage(setting('admin_logo')):asset('images/placeholder.png')}}"
                                                     alt="Preview image" class="preview_image ">
                                                <a class="btn_remove_image" title="Xoá ảnh">
                                                    <i class="fa fa-times"></i>
                                                </a>
                                            </div>
                                            <div class="image-box-actions">
                                                <a href="#" class="btn_gallery" data-bs-toggle="modal"
                                                   data-bs-target="#exampleModal" data-result="admin_logo">
                                                    {{trans('layout.choose_image')}}
                                                </a>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <div class="form-group mb-3">
                                    <label class="text-title-field" for="admin-favicon">{{trans('layout.setting.admin_favicon')}}
                                    </label>
                                    <div class="admin-favicon-image-setting">
                                        <div class="image-box admin_favicon">
                                            <input type="hidden" name="admin_favicon"
                                                   value="{{ setting('admin_favicon')}}" class="image-data">
                                            <div class="preview-image-wrapper  preview-image-wrapper-not-allow-thumb ">
                                                <img src="{{ setting('admin_favicon')? url_storage(setting('admin_favicon')):asset('images/placeholder.png')}}"
                                                     alt="Preview image" class="preview_image ">
                                                <a class="btn_remove_image" title="Xoá ảnh">
                                                    <i class="fa fa-times"></i>
                                                </a>
                                            </div>
                                            <div class="image-box-actions">
                                                <a href="#" class="btn_gallery" data-bs-toggle="modal"
                                                   data-bs-target="#exampleModal" data-result="admin_favicon">
                                                    {{trans('layout.choose_image')}}
                                                </a>
                                            </div>
                                        </div>
                                    </div>
                                </div>

                                <div class="form-group mb-3">
                                    <label class="text-title-field" for="admin-login-screen-backgrounds">
                                        {{trans('layout.setting.login')}}
                                    </label>
                                    <div class="admin-login-screen-backgrounds-setting">
                                        <div class="gallery-images-wrapper list-images">
                                            <div class="image-box login_backgrounds">
                                                <input type="hidden" name="login_backgrounds"
                                                       value="{{ setting('login_backgrounds')}}" class="image-data">
                                                <div class=" text-center cursor-pointer js-btn-trigger-add-image  ">
                                                    <img src="{{ setting('login_backgrounds')? url_storage(setting('login_backgrounds')):asset('images/placeholder.png')}}"
                                                         alt="Preview image" class="preview_image " width="120">
                                                    <a class="btn_remove_image" title="Xoá ảnh">
                                                        <i class="fa fa-times"></i>
                                                    </a>
                                                </div>
                                                <div class="image-box-actions">
                                                    <a href="#" class="btn_gallery" data-bs-toggle="modal"
                                                       data-bs-target="#exampleModal" data-result="login_backgrounds">
                                                        {{trans('layout.choose_image')}}
                                                    </a>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <div class="form-group mb-3">
                                    <label for="admin_title" class="form-label">{{trans('layout.setting.admin_title')}}</label>
                                    <input type="text" class="form-control" id="admin_title"
                                           value="{{setting('admin_title') }}" name="admin_title">
                                </div>
                                <div class="ui-select-wrapper">
                                    <label for="admin_title" class="form-label">{{trans('layout.setting.timezone')}}</label>
                                    <select name="time_zone" class=" form-control select-search-full" id="time_zone">
                                        @foreach(DateTimeZone::listIdentifiers() as $timezone)
                                            <option value="{{ $timezone }}"
                                                    @if (setting('time_zone', 'UTC') === $timezone) selected @endif>{{ $timezone }}</option>
                                        @endforeach
                                    </select>
                                </div>
                                <div class="ui-select-wrapper">
                                    <label for="locale" class="form-label">{{trans('layout.setting.site_language')}}</label>
                                    <select name="locale" class=" form-control select-search-full" id="locale">
                                        <option value="en" @if (setting('locale') === 'en') selected @endif>{{trans('layout.en')}} -
                                            EN
                                        </option>
                                        <option value="ja" @if (setting('locale') === 'ja') selected @endif>{{trans('layout.ja')}} -
                                            JA
                                        </option>
                                    </select>
                                </div>
                            </div>
                            <div class="page-footer pt-3">
                                <button class="btn btn-info" type="submit">{{trans('layout.setting.save')}}</button>
                            </div>
                        </form>
                    </div>
                </div>
            </div>
        </div>
    </div>
    @include('partials.media')
@stop

@push('js')

@endpush
