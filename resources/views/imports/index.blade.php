@extends('layouts.app')
@section('content')
    {{ Breadcrumbs::render('import') }}
    <div class="table-wrapper pl-3 pr-3">

        <form action="{{ route('imports.post') }}" method="POST" enctype="multipart/form-data" id="import_file">
            @csrf
            <div class="form-group mb-4 hidden">
                <div class="custom-file text-left">
                    <input type="file" name="file" class="custom-file-input" id="customFile">
                    <label class="custom-file-label" for="customFile">Choose file</label>
                </div>
            </div>

        </form>
        <div class="row">
            <div class="col-9">
                <div class="d-flex justify-content-between pb-3">
                    <div class="w-75 progress-import hidden">
                        <div class="w3-light-grey w3-tiny">
                            <div class="w3-container progress-bar" style="width:0%" id="progressBar"><span
                                    class="status">0%</span></div>
                        </div>
                    </div>
                    <div class="d-flex justify-content-end pb-3">
                        <button class="btn btn-primary import-data">{{trans('layout.import.import_data')}}</button>
                    </div>
                </div>
                <table id="table-imports" class="table table-striped table-hover  ">
                    <thead>
                    <tr>
                        <th title="name" class="sorting">{{trans('layout.name')}}</th>
                        <th title="created at" class=" sorting">{{trans('table.created_at')}}
                        </th>
                        <th title="size" class=" sorting">{{trans('layout.import.size')}}
                        </th>
                        <th title="type" class=" sorting_disabled">{{trans('layout.import.type')}}</th>
                        <th title="Operations" class=" sorting_disabled">{{trans('table.operations')}}</th>

                    </tr>
                    </thead>
                    <tbody>

                    </tbody>
                </table>
            </div>
            <div class="col-3">
                <div class="importing">
                    <h2>{{trans('layout.import.importing')}}</h2>
                    <p> {{trans('layout.import.info')}}</p>
                </div>
            </div>
        </div>
    </div>
    @include('partials.table.modal-item', [
        'type' => 'danger',
        'name' => 'modal-confirm-delete',
        'title' => trans('table.confirm_delete'),
        'content' => trans('table.confirm_delete_msg'),
        'action_name' =>trans('table.delete'),
        'action_button_attributes' => [
            'class' => 'delete-crud-entry',
        ],
    ])
    @include('partials.table.modal-item', [
        'type' => 'info',
        'with' => 'modal-lg',
        'name' => 'import-type-modal',
        'title' => trans('layout.import.import_type'),
        'content' => view('imports.type-import',compact('types_import'))->render(),
        'action_name' =>trans('layout.submit'),
        'action_button_attributes' => [
            'class' => 'confirm-import-button',
        ],
    ])


@stop
<style>
    .w3-light-grey {
        color: #000 !important;
        background-color: #f1f1f1;
        border-radius: 10px;
    }

    #progressBar {
        text-align: center;
        color: #fff !important;
        background-color: #4CAF50;
        position: relative;
        margin-top: 0.5rem;
        border-radius: 10px;
    }

    .import-type-modal .input[type=checkbox] {
        top: 2px;
    }

    .importing {
        margin-top: 65px;
    }

    .importing h2,
    .import-errors h3 {
        font-size: 22px;
    }
</style>
@push('js')
    <script>
        $(document).ready(function () {
            let table = $('#table-imports').DataTable({
                searching: false,
                order:[],
                language: {
                    "url":   window._locale
                },
                lengthChange: false,
                ajax: '{{route('imports.index')}}',
                columns: [
                    {data: 'name', name: 'name', className: 'text-left'},
                    {data: 'created_at', name: 'created_at'},
                    {data: 'filesize', name: 'filesize'},
                    {data: 'import_type', name: 'import_type'},
                    {data: 'operations', name: 'operations'},
                ]
            });
            let fileInput = document.querySelector(".custom-file-input");
            $('.import-data').click(function (e) {
                fileInput.click();
                $('.progress-import').addClass('hidden')
            })
            fileInput.onchange = ({target}) => {
                let formData = new FormData($("#import_file")[0]);
                $.ajax({
                    url: '{{route('imports.post')}}',
                    type: 'POST',
                    data: formData,
                    cache: false,
                    contentType: false,
                    processData: false,
                    enctype: 'multipart/form-data',
                    xhr: function () {
                        //upload Progress
                        $('.progress-import').removeClass('hidden');
                        let xhr = $.ajaxSettings.xhr();
                        if (xhr.upload) {
                            xhr.upload.addEventListener('progress', function (event) {
                                let percent = 0;
                                let position = event.loaded || event.position;
                                let total = event.total;
                                if (event.lengthComputable) {
                                    percent = Math.ceil(position / total * 100);
                                }
                                //update progressbar
                                $(".progress-bar").css("width", +percent + "%");
                                $(".status").text(percent + "%");
                            }, true);
                        }
                        return xhr;
                    },
                    success: function (data) {
                        table.ajax.reload();
                        Ems.showSuccess(data.message);
                    },
                    error: data => {
                        $('.progress-bar').css({'background-color': '#dd4b39'});
                        $(".status").text("Errors");
                        Ems.handleError(data);
                    }
                });
            }
        });
        $(".type-import").select2({
            placeholder: 'Select a type…',
            width: '100%',
        });

        $(document).on('click', '.importDialog', event => {
            event.preventDefault();
            let _self = $(event.currentTarget);

            $('.confirm-import-button').data('import', _self.data('import-file'));
            $('#import-id').val(_self.data('import-id'))
            $('.import-type-modal').modal('show');
        });
        $('#type-import').on('change', function (e) {
            let import_id=    $('#import-id').val();
            let type_import = $('#type-import').val();
            $.ajax({
                url: '{{route('imports.custom')}}',
                type: 'get',
                data: {
                    type_import: type_import,
                    import_id: import_id,
                },
                success: data => {
                    $('.import-field').html(data);
                },
                error: data => {
                    Ems.handleError(data);

                }

            });

        });
        $('.confirm-import-button').on('click', event => {
            event.preventDefault();
            let _self = $(event.currentTarget);
            let fileName = _self.data('import');
            let type_import = $('#type-import').val();
            let update_import = '';
            if ($(' input[name=import-update]:checked').val() === 'edit') {
                update_import = 'edit';
            }
            let import_values={};
            $(".select-fields").each(function () {
                if (this.value){
                    import_values[this.name] = this.value
                }
            });

            if (type_import) {
                _self.addClass('button-loading');
                $.ajax({
                    url: $('#type-import ').select2().find(":selected").data('type-import'),
                    type: 'get',
                    data: {
                        type_import: type_import,
                        import_update: update_import,
                        file_name: fileName,
                        import_values:import_values
                    },
                    success: data => {
                        if (data.error) {
                            Ems.showError(data.message);
                            $('#table-imports-errors tbody').html('');
                            $.each(data.message, function (key, value) {
                                let html = `<tr class="odd">
                                                <td class="w-10">${value.row}</td>
                                                <td class="w-10">${value.attribute}</td>
                                                <td>${value.errors}</td>
                                            </tr> `
                                $('#table-imports-errors tbody').append(html);
                            });
                        } else {
                            Ems.showSuccess(data.message);
                        }
                        _self.closest('.modal').modal('hide');
                        _self.removeClass('button-loading');
                    },
                    error: data => {
                        Ems.handleError(data);
                        _self.removeClass('button-loading');
                    }

                });
                _self.removeClass('button-loading');
            } else {
                alert('Please choose an import type')
            }
        })
        $('.import-type-modal').on('hidden.bs.modal', function (e) {
            $('.type-import').val(null);
            $('.import-field').html('');
        })

    </script>
@endpush
