<div class="control-group">
    <label for="type-import" class="control-label">{{trans('layout.import.import_type')}}:</label>
    <select id="type-import" class="form-control type-import w-100" >
        <option value="" ></option>
        @foreach($types_import as $key=> $item)
        <option value="{{$key}}" data-type-import="{{route($item['route'])}}">{{$item['name']}}</option>
        @endforeach
    </select>
    <input type="hidden" id="import-id">
</div>

<div class="control-group pt-3">
    <div class="form-check">
        <input type="checkbox"  id="import-update" name="import-update" value="edit">
        <label class="form-check-label" for="import-update">{{trans('layout.import.update')}}</label>
    </div>
</div>
<div class="control-group pt-3 import-field">

</div>
