
<div class="row">
    <div class="col-md-3 text-right">
        <h4>Header Field</h4>
    </div>
    <div class="col-md-5 text-center">
        <h4>Import Field</h4>
    </div>
    <div class="col-md-4 text-left ">
        <h4 class="pl-2"> Sample Value</h4>
    </div>
</div>
@foreach( json_decode($header_field->header_row) as $key=> $row)
<div class="row">
    <div class="col-md-3 text-capitalize text-right">
        <label for="Email" class="control-label">{{$key}}</label>
    </div>
    <div class="col-md-5  form-group">
        <select  class="form-control w-100 select-fields select2-hidden-accessible" name="{{$key}}">
            <option value="">Do Not Import</option>
            @foreach($import_field as $key_field=> $field)
                <option value="{{$key_field}}"{{$field==$key ? 'selected':''}}>{{$field}}</option>
            @endforeach
        </select>

    </div>
    <div class="col-md-4 ">
        <label for="Email" class="control-label pl-2">{{$row}}</label>
    </div>
</div>
@endforeach
<script>
    $('.select2-hidden-accessible').select2();
    // $(".select2-hidden-accessible").on('select2:selecting', function (e) {
    //
    //     $('.select2-hidden-accessible option[value="'+e.params.args.data.id+'"]').attr("disabled", true);
    //     if (e.params.data){
    //
    //     }
    // });
    // $(".select2-hidden-accessible").on('select2:open ', function (e) {
    //     console.log(e)
    //
    // });
</script>
