<div class="table-actions">
    <a href="#" class="btn btn-icon btn-sm btn-info importDialog" data-toggle="tooltip"
       data-import-file="{{ $item->name }}" data-import-id="{{ $item->id }}"
         role="button" title="{{trans('layout.import.process')}}">
       {{trans('layout.import.process')}}
    </a>

    <a href="#" class="btn btn-icon btn-sm btn-danger deleteDialog" data-toggle="tooltip"
       data-section="{{ route($delete, $item->id) }}" role="button"
       title="{{trans('table.delete')}}">
        <i class="fa fa-trash"></i>
    </a>
</div>
