<?php

return [

    /*
    |--------------------------------------------------------------------------
    | Authentication Language Lines
    |--------------------------------------------------------------------------
    |
    | The following language lines are used during authentication for various
    | messages that we need to display to the user. You are free to modify
    | these language lines according to your application's requirements.
    |
    */

    'failed' => 'These credentials do not match our records.',
    'password' => 'The provided password is incorrect.',
    'throttle' => 'Too many login attempts. Please try again in :seconds seconds.',
    'sign_in_below'=> 'Sign In Below:',
    'login'=>[
        'username'=>'Email/Username',
        'password'=>'Password',
        'remember'=>'Remember me?',
        'login'=>'Sign in'
    ],
    'forgot_password'       => [
        'title'   => 'Forgot Password',
        'message' => '<p>Have you forgotten your password?</p><p>Please enter your email account. System will send a email with active link to reset your password.</p>',
        'submit'  => 'Submit',
    ],
    'reset'                 => [
        'new_password'          => 'New password',
        'password_confirmation' => 'Confirm new password',
        'email'                 => 'Email',
        'title'                 => 'Reset your password',
        'update'                => 'Update',
        'wrong_token'           => 'This link is invalid or expired. Please try using reset form again.',
        'user_not_found'        => 'This username is not exist.',
        'success'               => 'Reset password successfully!',
        'fail'                  => 'Token is invalid, the reset password link has been expired!',
        'reset'                 => [
            'title' => 'Email reset password',
        ],
        'send'                  => [
            'success' => 'A email was sent to your email account. Please check and complete this action.',
            'fail'    => 'Can not send email in this time. Please try again later.',
        ],
        'new-password'          => 'New password',
    ],
    'email'                 => [
        'reminder' => [
            'title' => 'Email reset password',
        ],
    ],
    'password_confirmation' => 'Password confirm',

    'not_member'            => 'Not a member yet?',
    'register_now'          => 'Register now',
    'lost_your_password'    => 'Lost your password?',
    'login_title'           => 'Admin',
    'login_via_social'      => 'Login with social networks',
    'back_to_login'         => 'Back to login page',
    'languages'             => 'Languages',
    'reset_password'        => 'Reset Password',


];
