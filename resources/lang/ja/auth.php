<?php

return [

    /*
    |--------------------------------------------------------------------------
    | Authentication Language Lines
    |--------------------------------------------------------------------------
    |
    | The following language lines are used during authentication for various
    | messages that we need to display to the user. You are free to modify
    | these language lines according to your application's requirements.
    |
    */

    'failed' => 'これらのクレデンシャルは私たちの記録と一致しません。',
    'password' => '提供されたパスワードが正しくありません。',
    'throttle' => 'ログイン試行回数が多すぎます。 ：seconds秒後に再試行してください。',
    'sign_in_below'=> '以下のサインイン：',
    'login'=>[
        'username'=>'メール/ユーザー名',
        'password'=>'パスワード',
        'remember'=>'私を覚えてますか？',
        'login'=>'サインイン'
    ],
    'forgot_password'       => [
        'title'   => 'パスワードをお忘れですか',
        'message' => '<p>パスワードを忘れましたか?</p><p>メールアカウントを入力してください。システムは、パスワードをリセットするためのアクティブなリンクを記載した電子メールを送信します</p>',
        'submit'  => 'Submit',
    ],
    'reset'                 => [
        'new_password'          => 'New password',
        'password_confirmation' => 'Confirm new password',
        'email'                 => 'Eメール',
        'title'                 => 'Reset your password',
        'update'                => 'Update',
        'wrong_token'           => 'This link is invalid or expired. Please try using reset form again.',
        'user_not_found'        => 'This username is not exist.',
        'success'               => 'Reset password successfully!',
        'fail'                  => 'Token is invalid, the reset password link has been expired!',
        'reset'                 => [
            'title' => 'Email reset password',
        ],
        'send'                  => [
            'success' => 'A email was sent to your email account. Please check and complete this action.',
            'fail'    => 'Can not send email in this time. Please try again later.',
        ],
        'new-password'          => 'New password',
    ],
    'email'                 => [
        'reminder' => [
            'title' => 'Email reset password',
        ],
    ],
    'password_confirmation' => 'Password confirm',

    'not_member'            => 'Not a member yet?',
    'register_now'          => 'Register now',
    'lost_your_password'    => 'パスワードを忘れましたか?',
    'login_title'           => 'Admin',
    'login_via_social'      => 'Login with social networks',
    'back_to_login'         => 'Back to login page',
    'languages'             => 'Languages',
    'reset_password'        => 'Reset Password',


];
