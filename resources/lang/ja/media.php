<?php

return [
    'filter'                     => 'フィルター',
    'everything'                 => 'すべての',
    'image'                      => '画像',
    'video'                      => 'ビデオ',
    'document'                   => '書類',
    'view_in'                    => 'で見る',
    'all_media'                  => 'すべてのメディア',
    'trash'                      => 'ごみ',
    'recent'                     => '最近',
    'favorites'                  => 'お気に入り',
    'upload'                     => 'アップロード',
    'create_folder'              => 'フォルダーを作る',
    'refresh'                    => '更新',
    'empty_trash'                => '空のごみ箱',
    'search_file_and_folder'     => 'ファイルとフォルダを検索',
    'sort'                       => '選別',
    'file_name_asc'              => 'ファイル名-ASC',
    'file_name_desc'             => 'ファイル名-DESC',
    'uploaded_date_asc'          => 'アップロード日-ASC',
    'uploaded_date_desc'         => 'アップロード日-DESC',
    'size_asc'                   => 'サイズ-ASC',
    'size_desc'                  => 'サイズ-DESC',
    'actions'                    => '行動',
    'nothing_is_selected'        => '何も選択されていません',
    'insert'                     => '入れる',
    'folder_name'                => 'フォルダ名',
    'create'                     => '作成',
    'rename'                     => '名称を変更',
    'close'                      => '選ぶ',
    'save_changes'               => '変更内容を保存',
    'move_to_trash'              => 'アイテムをゴミ箱に移動する',
    'confirm_trash'              => 'これらのアイテムをゴミ箱に移動してもよろしいですか？',
    'confirm'                    => '確認',
    'confirm_delete'             => 'アイテムを削除します',
    'confirm_delete_description' => 'リクエストをロールバックすることはできません。これらのアイテムを削除してもよろしいですか？',
    'empty_trash_title'          => '空のごみ箱',
    'empty_trash_description'    => 'リクエストをロールバックすることはできません。ゴミ箱にあるすべてのアイテムを削除してもよろしいですか？',
    'up_level'                   => '1つ上のレベル',
    'upload_progress'            => 'アップロードの進行状況',

    'folder_created' => 'フォルダが正常に作成されました！',
    'gallery'        => 'メディアギャラリー',

    'trash_error'                   => '選択したアイテムを削除するとエラーが発生します',
    'trash_success'                 => '選択したアイテムを正常にゴミ箱に移動しました!',
    'restore_error'                 => '選択したアイテムを復元するときにエラーが発生する',
    'restore_success'               => '選択したアイテムを正常に復元します。',
    'copy_success'                  => '選択したアイテムを正常にコピーしました。',
    'delete_success'                => '選択したアイテムを正常に削除しました！',
    'favorite_success'              => 'お気に入りの選択アイテムが正常に！',
    'remove_favorite_success'       => '選択したアイテムをお気に入りから正常に削除します。',
    'rename_error'                  => 'アイテムの名称を変更するときにエラーが発生しました',
    'rename_success'                => '選択したアイテムの名称を正常に変更します!',
    'empty_trash_success'           => 'ごみ箱を正常に空にする!',
    'invalid_action'                => '無効なアクション!',
    'file_not_exists'               => 'ファイルが存在しません!',
    'download_file_error'           => 'ファイルのダウンロード時にエラーが発生しました!',
    'missing_zip_archive_extension' => 'ファイルをダウンロードするには、ZipArchive拡張機能を有効にしてください!',
    'can_not_download_file'         => 'このファイルをダウンロードできません!',
    'invalid_request'               => '無効なリクエスト!',
    'add_success'                   => 'アイテムを正常に追加!',
    'file_too_big'                  => 'ファイルが大きすぎます。最大ファイルアップロードは：sizeバイトです',
    'can_not_detect_file_type'      => 'ファイルタイプが許可されていないか、ファイルタイプを検出できません!',
    'upload_failed'                 => 'ファイルが完全にアップロードされていません。サーバーでは、アップロードファイルの最大サイズは：sizeです。ファイルサイズを確認するか、ネットワークエラーが発生した場合はアップロードを再試行してください',

    'menu_name' => 'メディア',
    'add'       => 'メディアを追加する',
    'store' => 'ストアフォルダの成功 !!',
    'move' => 'メディアファイルの移動の成功 !!',
    'exists'=>'メディアディレクトリファイルが存在しません!!'
];
