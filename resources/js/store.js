import Vue from "vue";
import Vuex from "vuex";
Vue.use(Vuex);
export default new Vuex.Store(
    {
        state: {
            mediaLoad:"all",
            mediaFolders: [],
            mediaFiles: [],
            mediaTitles:null,
            folderId:0,
            folderParentId:0,
            files:[],
            isLoading:false,

        },
        mutations: {
            setMediaFolders(state, payload) {
                state.mediaFolders = payload;
            },
            setMediaFiles(state, payload) {
                state.mediaFiles = payload;
            },
            isLoading(state, payload) {
                state.isLoading = payload;
            },
            mediaTitles(state, payload) {
                state.mediaTitles = payload;
            },
            mediaFolderId(state, payload){
                state.folderId = payload.id;
                state.folderParentId = payload.parent_id;
            },
            uploadFile(state, payload){
              state.files.push(payload)
            },
            mediaLoad(state, payload){
                state.mediaLoad = payload;
            }
        },
        actions: {
            increment (context) {
                setTimeout(function () {
                    let index = context.state.files.findIndex(x => x.status !== 2);
                    context.state.files.splice(index,1)
                }, 3000)
            }
        }
    }
);
