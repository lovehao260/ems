const axios = require("axios");
const CONFIG = {
    API_URL_ROOT: 'http://ems.test/',
}

export const requests = {
    methods: {
        getMediaContent(id = 0, search = null) {
            this.$store.commit("isLoading", true);
            axios.get(
                CONFIG.API_URL_ROOT+`get-data/medias/${id}`,
                {
                    crossDomain: true,
                    params: {
                        search: search,
                        loadType: this.$store.state.mediaLoad
                    }
                }
            ).then(
                response => {
                    this.$store.commit("setMediaFolders", response.data.folders);
                    this.$store.commit("setMediaFiles", response.data.files);

                    setTimeout(() => this.$store.commit("isLoading", false), 300);
                }
            );
        },
        mediaUploadFile(parent_id, data_medias) {

            let data = {
                name: data_medias.name,
                size: parseInt(data_medias.size / 1024),
                status: 0,
                message: ""
            }
            this.$store.commit("uploadFile", data);
            let form = this.setFormData(data_medias)
            axios.post(CONFIG.API_URL_ROOT+`post-data/medias/${parent_id}`, form, {
                headers: {
                    'Content-Type': 'multipart/form-data'
                },
            }).then(
                response => {
                    let data = this.$store.state.files;
                    this.$store.dispatch('increment')
                    const index = this.findLastIndex(data, 'name', data_medias.name);
                    data[index].status = 1;
                    this.getMediaContent(parent_id);
                },
                error => {
                    let data = this.$store.state.files;
                    const index = this.findLastIndex(data, 'name', data_medias.name);
                    data[index].status = 2
                    data[index].message = error.response.data.errors.file[0];
                }
            );

        },
        createMediaFolder(name) {
            let id = this.$store.state.folderId;
            axios.post(CONFIG.API_URL_ROOT+'medias-folder/store', {
                name: name,
                folder_id: id
            }).then(
                response => {
                    this.getMediaContent(id)
                },
                error => {
                    alert(error.response.data.errors.name)
                }
            );

        },
        renameItemsSubmit(item_data) {
            axios.post(CONFIG.API_URL_ROOT+'medias-rename/update', {
                item_data: item_data,
            }).then(
                response => {
                    this.getMediaContent(this.$store.state.folderId);
                },
                error => {
                    alert(error.response.data.errors.name);
                }
            );
            return true
        },
        trashItemsSubmit(item_data) {
            axios.post(CONFIG.API_URL_ROOT+'medias-move/trash', {
                item_data: item_data
            }).then(
                response => {
                    this.getMediaContent(this.$store.state.folderId);
                },
                error => {
                    alert(error.response.data.errors.name);
                }
            );
        },
        deleteItemsSubmit( item_data) {
            axios.post(CONFIG.API_URL_ROOT+'medias/delete', {
                item_data: item_data
            }, {}).then(
                response => {

                    this.getMediaContent(this.$store.state.folderId);
                },
                error => {
                    alert(error.response.data.errors.name);
                }
            );
        },
        restoreItemsSubmit(item_data ){
            axios.post(CONFIG.API_URL_ROOT+'medias/restore', {
                item_data: item_data
            }, {}).then(
                response => {

                    this.getMediaContent(this.$store.state.folderId);
                },
                error => {
                    alert(error.response.data.errors.name);
                }
            );
        },
        setFormData(file) {
            let formData = new FormData();
            formData.append('file', file);
            return formData
        },
        findLastIndex(array, searchKey, searchValue) {
            let index = array.slice().reverse().findIndex(x => x[searchKey] === searchValue);
            let count = array.length - 1
            let finalIndex = index >= 0 ? count - index : index;
            return finalIndex;
        }

    }
}





