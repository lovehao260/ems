class EmsCore {
    constructor() {
        this.init();
    }

    init() {

        $(document).on('click', '.save-user-button', event => {
            let _self = $(event.currentTarget);
            _self.addClass('button-loading');
            let obj = {};
            $.each($('#manager-modal input'), function (key, value) {
                obj[value.name] = $(this).val();
            });
            $.ajax({
                url: _self.data('load-url'),
                type: 'Post',
                data: obj,
                success: data => {
                    if (data.error) {
                        Ems.showError(data.message);
                    } else {
                        Ems.showSuccess(data.message);
                    }
                    _self.closest('.modal').modal('hide');
                    _self.removeClass('button-loading');
                },
                error: data => {
                    Ems.handleError(data);
                    _self.removeClass('button-loading');
                }
            });
        });
        $(document).on('click', '.save-fieldset-button', event => {
            let _self = $(event.currentTarget);
            _self.addClass('button-loading');
            let obj = {};
            $.each($('.form-save-fieldset input:text,.form-save-fieldset select,input:checked'), function (key, value) {
                obj[value.name] = $(this).val();
            });
            $.ajax({
                url: _self.data('load-url'),
                type: 'post',
                data: obj,
                success: data => {
                    if (data.error) {
                        Ems.showError(data.message);
                    } else {
                        $(_self.data('parent-table')).DataTable().ajax.reload();
                        Ems.showSuccess(data.message);
                    }
                    _self.closest('.modal').modal('hide');
                    _self.removeClass('button-loading');
                },
                error: data => {
                    Ems.handleError(data);
                    _self.removeClass('button-loading');
                }
            });

        });
        $(document).on('click', '.btn_remove_image', event => {
            event.preventDefault();
            $(event.currentTarget).closest('.image-box').find('.preview-image-wrapper img').hide();
            $(event.currentTarget).closest('.image-box').find('.image-data').val('');
        });
        $('.image-box-actions .btn_gallery').on('click',function (event){
            let _self = $(event.currentTarget);
            $('.js-insert-to-editor').data('type',_self.data('result'))
        })
        $('.js-insert-to-editor').off('click').on('click', function (event) {
            event.preventDefault();
            let _self = $(event.currentTarget);
            let selectedFiles = getSelectedFiles();
            let image=_self.data('type')
            $('.'+image+ ' .preview_image ').attr("src", APP_URL + '/storage/' + selectedFiles[0]).show()
            $('.'+image+ ' .image-data ').val(selectedFiles[0])
            $('#exampleModal').modal('hide')
        });
        checkField($('#element-field').val());
        $('#element-field').on('change', function (event) {
            let _self = $(event.currentTarget);
            checkField(_self.val());
        });


        $('.field-format').on('change', function (event) {

            let _self = $(event.currentTarget);

            if (_self.val() === 'regex') {
                $('.field-regex').removeClass('hidden')
            } else {
                $('.field-regex').addClass('hidden')
            }
        });
        $('#select-model').on('change', function (event) {
            let _self = $(event.currentTarget);
            if (_self.val()) {
                loadFiledAsset(_self.data('url-field'), _self.val())
            }
        });
        if ($('#select-model').val()) {
            loadFiledAsset($('#select-model').data('url-field'), $('#select-model').val());
        }

        $('.model-field-custom').on('change', function (event) {
            let _self = $(event.currentTarget);
            if ($('input[name=add_default_values]:checked').length > 0) {
                loadFiled(_self.data('url-field'), _self.val())
            }
        });
        $('.js-default-values-toggler').on('change', function (event) {
            if (event.currentTarget.checked) {
                $('.default-field-model').removeClass('hidden');
                loadFiled($('.model-field-custom').data('url-field'), $('.model-field-custom').val())
            } else {
                $('.default-field-model').addClass('hidden')
            }
        });

        if ($('input[name=add_default_values]:checked').length > 0) {
            loadFiled($('.model-field-custom').data('url-field'), $('.model-field-custom').val())
        }

    }
}

function getSelectedFiles() {
    let selected = [];
    $('.js-media-list-title[data-context=file] input[type=checkbox]:checked').each((index, el) => {
        let $box = $(el).closest('input');
        let data = $box.val() || {};
        data.index_key = $box.index();
        selected.push(data);
    });
    return selected;
}

function goToPage(url) {
    location.href = url
}
function checkField(value) {
    if (value === 'checkbox' || value === 'radio' || value === 'listbox') {
        $('.field-value').removeClass('hidden')
        $('.field-text, .field-regex ').addClass('hidden')
    } else {
        $('.field-value').addClass('hidden')
        $('.field-text').removeClass('hidden');
        if ($('.field-format').val() === 'regex') {
            $('.field-regex').removeClass('hidden');
        }
    }
}

function loadFiled(url, id) {
    if (id) {
        $('.default-field-model').removeClass('hidden')
        $.ajax({
            url: url,
            type: 'get',
            data: {
                id: id,
            },
            success: data => {
                if (data.error) {
                    Ems.showError(data.message);
                } else {
                    if (data) {
                        $('.asset-field').html(data)
                    } else {
                        $('.default-field-model, .asset-field').addClass('hidden')
                    }
                }
            },
            error: data => {
                Ems.handleError(data);

            }
        });
    } else {
        $('.default-field-model').addClass('hidden')
    }
}

function loadFiledAsset(url, id) {
    console.log(url, id)
    $.ajax({
        url: url,
        type: 'get',
        data: {
            id: id,
        },
        success: data => {
            if (data.error) {
                Ems.showError(data.message);
            } else {
                $('.asset-field').html(data)
            }
        },
        error: data => {
            Ems.handleError(data);

        }
    });
}

function trans(key, replace = {}) {
    let translation = key.split('.').reduce((t, i) => t[i] || null, window.translations);
    return translation??replace;
}
$(document).ready(() => {
    new EmsCore();

    window.Ems = EmsCore;
});
