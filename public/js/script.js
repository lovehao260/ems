class Ems {
    constructor() {
        this.manageSidebar();
        Ems.initResources();
    }

    static showNotice(messageType, message) {

        toastr.clear();
        toastr.options = {
            closeButton: true,
            positionClass: 'toast-bottom-right',
            onclick: null,
            showDuration: 1000,
            hideDuration: 1000,
            timeOut: 5000,
            extendedTimeOut: 1000,
            showEasing: 'swing',
            hideEasing: 'linear',
            showMethod: 'fadeIn',
            hideMethod: 'fadeOut'
        };

        let messageHeader = '';

        switch (messageType) {
            case 'error':
                messageHeader = trans('table.error','Error');
                break;
            case 'success':
                messageHeader = trans('table.success','Success');
                break;
        }
        toastr[messageType](message, messageHeader);
    }

    static showError(message) {
        this.showNotice('error', message);
    }

    static showSuccess(message) {
        this.showNotice('success', message);
    }

    static handleError(data) {
        if (typeof (data.errors) !== 'undefined' && !_.isArray(data.errors)) {
            Ems.handleValidationError(data.errors);
        } else {
            if (typeof (data.responseJSON) !== 'undefined') {
                if (typeof (data.responseJSON.errors) !== 'undefined') {
                    if (data.status === 422) {
                        Ems.handleValidationError(data.responseJSON.errors);
                    }
                } else if (typeof (data.responseJSON.message) !== 'undefined') {
                    Ems.showError(data.responseJSON.message);
                } else {
                    $.each(data.responseJSON, (index, el) => {
                        $.each(el, (key, item) => {
                            Ems.showError(item);
                        });
                    });
                }
            } else {
                Ems.showError(data.statusText);
            }
        }
    }

    static handleValidationError(errors) {
        let message = '';
        $.each(errors, (index, item) => {
            message += item + '<br />';

            let $input = $('*[name="' + index + '"]');
            if ($input.closest('.next-input--stylized').length) {
                $input.closest('.next-input--stylized').addClass('field-has-error');
            } else {
                $input.addClass('field-has-error');
            }

            let $input_array = $('*[name$="[' + index + ']"]');

            if ($input_array.closest('.next-input--stylized').length) {
                $input_array.closest('.next-input--stylized').addClass('field-has-error');
            } else {
                $input_array.addClass('field-has-error');
            }
        });
        Ems.showError(message);
    }



    manageSidebar() {
        let body = $('body');
        let navigation = $('.navigation');
        let sidebar_content = $('.sidebar-content');

        navigation.find('li.active').parents('li').addClass('active');
        navigation.find('li').has('ul').children('a').parent('li').addClass('has-ul');


        $(document).on('click', '.sidebar-toggle.d-none', event =>  {
            event.preventDefault();

            body.toggleClass('sidebar-narrow');
            body.toggleClass('page-sidebar-closed');

            if (body.hasClass('sidebar-narrow')) {
                navigation.children('li').children('ul').css('display', '');

                sidebar_content.delay().queue(() => {
                    $(event.currentTarget).show().addClass('animated fadeIn').clearQueue();
                });
            } else {
                navigation.children('li').children('ul').css('display', 'none');
                navigation.children('li.active').children('ul').css('display', 'block');

                sidebar_content.delay().queue(() => {
                    $(event.currentTarget).show().addClass('animated fadeIn').clearQueue();
                });
            }
        });
    }

    static initDatePicker(element) {
        if (jQuery().bootstrapDP) {
            let format = $(document).find(element).data('date-format');
            if (!format) {
                format = 'yyyy-mm-dd';
            }
            $(document).find(element).bootstrapDP({
                maxDate: 0,
                changeMonth: true,
                changeYear: true,
                autoclose: true,
                dateFormat: format,
            });
        }
    }
    static initResources() {
        if (jQuery().select2) {
            $(document).find('.select-multiple').select2({
                width: '100%',
                allowClear: true,
            });
            $(document).find('.select-search-full').select2({
                width: '100%'
            });
            $(document).find('.select-full').select2({
                width: '100%',
                minimumResultsForSearch: -1
            });
            $(document).find("select-field").select2({
                placeholder: 'Select a type…',
                width: '100%',
            });
            $(document).find("#select-country").select2({
                placeholder: 'Select a type…',
                width: '100%',
            });
            $("#select-company").select2({
                placeholder: '',

                width: '100%',
                height: '10px',

            });
            $("#select-user").select2({
                placeholder: '',
                width: '100%',
                height: '10px',

            });
        }
        $('.deleteDetailAsset').click(function () {
            $(this).closest("tr").remove();

            updateTableDetailAsset();
        })
        $("#sortableDetail tbody").sortable({
            cursor: "move",
            placeholder: "sortableDetail-placeholder",
            helper: function (e, tr) {
                let $originals = tr.children();
                let $helper = tr.clone();
                $helper.children().each(function (index) {
                    $(this).width($originals.eq(index).width());
                });
                return $helper;
            },
            update: function (event, ui) {
                updateTableDetailAsset();
            }
        }).disableSelection();
        $('#select-field-detail').on('select2:selecting', function(e) {
            let html =`<tr data-detail="${e.params.args.data.id}" data-label="${e.params.args.data.label}" class="ui-sortable-handle hidden">
                        <td class=" text-left">
                            <span class="handle">
                            <i class="fa fa-ellipsis-v"></i> <i class="fa fa-ellipsis-v"></i>
                             </span>
                        </td>
                    </tr>`
            $('#sortableDetail').append(html);
            updateTableDetailAsset('reload');
        });

        function updateTableDetailAsset($reload =null) {

            let obj = [];
            $('#sortableDetail tbody tr').map(function (e) {
                obj.push({
                    value: $(this).data('detail'),
                    label: $(this).data('label')
                });
            }).get();

            let setting_key=$('#sortableDetail').data('setting-key');

            $.ajax({
                url: $('#sortableDetail').data('url'),
                type: 'post',
                data: {
                    obj: obj,
                    setting_key:setting_key,
                },
                success: data => {
                    if (data.error) {
                        Ems.showError(data.message);
                    } else {
                        Ems.showSuccess(data.message);
                    }
                    if ($reload){
                        location.reload()
                    }
                },
                error: data => {
                    Ems.handleError(data);
                }
            });
        }
        // This handles the radio button selectors for the checkout-to-foo options
        // on asset checkout and also on asset edit
        $('input[name=checkout_to_type]').on("change", function () {
            let assignto_type = $('input[name=checkout_to_type]:checked').val();
            let userid = $('#assigned_user option:selected').val();

            if (assignto_type == 'asset') {
                $('#current_assets_box').fadeOut();
                $('#assigned_asset').show();
                $('#assigned_user').hide();
                $('#assigned_location').hide();
                $('.notification-callout').fadeOut();
            } else if (assignto_type == 'location') {
                $('#current_assets_box').fadeOut();
                $('#assigned_asset').hide();
                $('#assigned_user').hide();
                $('#assigned_location').show();
                $('.notification-callout').fadeOut();
            } else {

                $('#assigned_asset').hide();
                $('#assigned_user').show();
                $('#assigned_location').hide();
                if (userid) {
                    $('#current_assets_box').fadeIn();
                }
                $('.notification-callout').fadeIn();
            }
        });
        // Crazy select2 rich dropdowns with images!
        $('.js-data-ajax').each(function (i, item) {
            let data_url = $(item).data("url");
            let category_type = $(item).data("category");
            let detail_id = $(item).data("detail-id");
            $(item).select2({
                placeholder: '',
                allowClear: true,
                width: '100%',
                height: '10px',
                language:{
                    "noResults": function(){
                        return trans('layout.no_results');
                    }
                },
                ajax: {
                    // the baseUrl includes a trailing slash
                    url: data_url,
                    dataType: 'json',
                    delay: 20,
                    headers: {
                        "X-Requested-With": 'XMLHttpRequest',
                        "X-CSRF-TOKEN": $('meta[name="csrf-token"]').attr('content')
                    },
                    data: function (params) {
                        return {
                            term: params.term || '',
                            category_type: category_type || '',
                            detail_id: detail_id || '',
                            page: params.page || 1
                        }
                    },
                    cache: true
                },
                escapeMarkup: function escapeMarkup(markup) {
                    return markup;
                },
            });
        });

        //Language
        jQuery(function ($) {
            $.datepicker.regional['ja'] = {
                closeText: '閉じる',
                prevText: '&#x3C;前',
                nextText: '次&#x3E;',
                currentText: '今日',
                monthNames: ['1月', '2月', '3月', '4月', '5月', '6月',
                    '7月', '8月', '9月', '10月', '11月', '12月'],
                monthNamesShort: ['1月', '2月', '3月', '4月', '5月', '6月',
                    '7月', '8月', '9月', '10月', '11月', '12月'],
                dayNames: ['日曜日', '月曜日', '火曜日', '水曜日', '木曜日', '金曜日', '土曜日'],
                dayNamesShort: ['日', '月', '火', '水', '木', '金', '土'],
                dayNamesMin: ['日', '月', '火', '水', '木', '金', '土'],
                weekHeader: '週',
                firstDay: 0,
                isRTL: false,
                showMonthAfterYear: true,
                yearSuffix: '年'
            };
            $.datepicker.setDefaults($.datepicker.regional[document.documentElement.lang]);
            $('.datepicker').datepicker({
                dateFormat: 'yy-mm-dd',
                beforeShow: function() {
                    setTimeout(function(){
                        $('.ui-datepicker').css('z-index', 10000);
                    }, 0);
                }
            });
        });

        if (jQuery().timepicker) {
            if (jQuery().timepicker) {

                $('.timepicker-default').timepicker({
                    autoclose: true,
                    showSeconds: true,
                    minuteStep: 1,
                    defaultTime: false
                });

                $('.timepicker-no-seconds').timepicker({
                    autoclose: true,
                    minuteStep: 5,
                    defaultTime: false,
                });

                $('.timepicker-24').timepicker({
                    autoclose: true,
                    minuteStep: 5,
                    showSeconds: false,
                    showMeridian: false,
                    defaultTime: false
                });
            }
        }

        if (jQuery().inputmask) {
            $(document).find('.input-mask-number').inputmask({
                alias: 'numeric',
                rightAlign: false,
                digits: 2,
                groupSeparator: ',',
                placeholder: '0',
                autoGroup: true,
                autoUnmask: true,
                removeMaskOnSubmit: true,
            });
        }

        if (jQuery().colorpicker) {
            $('.color-picker').colorpicker({
                inline: false,
                container: true,
                format: 'hex',
                extensions: [
                    {
                        name: 'swatches',
                        options: {
                            colors: {
                                'tetrad1': '#000000',
                                'tetrad2': '#000000',
                                'tetrad3': '#000000',
                                'tetrad4': '#000000'
                            },
                            namesAsValues: false
                        }
                    }
                ]
            })
                .on('colorpickerChange colorpickerCreate', function (e) {
                    var colors = e.color.generate('tetrad');

                    colors.forEach(function (color, i) {
                        var colorStr = color.string(),
                            swatch = e.colorpicker.picker
                                .find('.colorpicker-swatch[data-name="tetrad' + (i + 1) + '"]');

                        swatch
                            .attr('data-value', colorStr)
                            .attr('title', colorStr)
                            .find('> i')
                            .css('background-color', colorStr);
                    });
                });
        }

        if (jQuery().fancybox) {
            $('.iframe-btn').fancybox({
                width: '900px',
                height: '700px',
                type: 'iframe',
                autoScale: false,
                openEffect: 'none',
                closeEffect: 'none',
                overlayShow: true,
                overlayOpacity: 0.7
            });
            $('.fancybox').fancybox({
                openEffect: 'none',
                closeEffect: 'none',
                overlayShow: true,
                overlayOpacity: 0.7,
                helpers: {
                    media: {}
                }
            });
        }
        $('[data-toggle="tooltip"]').tooltip({placement: 'top'});

        if (jQuery().areYouSure) {
            $('form').areYouSure();
        }

        Ems.initDatePicker('.datepicker');
        if (jQuery().mCustomScrollbar) {
            Ems.callScroll($('.list-item-checkbox'));
        }

        if (jQuery().textareaAutoSize) {
            $('textarea.textarea-auto-height').textareaAutoSize();
        }
    }
}

if (jQuery().datepicker && jQuery().datepicker.noConflict) {
    $.fn.bootstrapDP = $.fn.datepicker.noConflict();
}

$(document).ready(() => {
    new Ems();
    window.Ems = Ems;
});
