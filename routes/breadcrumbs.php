<?php

// Home
use DaveJamesMiller\Breadcrumbs\Facades\Breadcrumbs;

Breadcrumbs::for('home', function ($trail) {
    $trail->push(trans('layout.home'), route('dashboard.index'));
});

// Home > Imports
Breadcrumbs::for('import', function ($trail) {
    $trail->parent('home');
    $trail->push(trans('layout.imports'), route('imports.index'));
});
// Home > Imports
Breadcrumbs::for('activities', function ($trail) {
    $trail->parent('home');
    $trail->push(trans('layout.log.activities'), route('activities.index'));
});

// Home > Assets
Breadcrumbs::for('assets', function ($trail) {
    $trail->parent('home');
    $trail->push(trans('layout.assets'), route('assets.index'));
});

// Home > Assets > [Create]
Breadcrumbs::for('assets.add', function ($trail) {
    $trail->parent('assets');
    $trail->push(trans('layout.asset.create'), route('assets.create'));
});
// Home > Assets > [update]
Breadcrumbs::for('assets.show', function ($trail,$id) {
    $trail->parent('assets');
    $trail->push(trans('layout.asset.show'), route('assets.edit',$id));
});
// Home > Assets > [update]
Breadcrumbs::for('assets.edit', function ($trail,$id) {
    $trail->parent('assets');
    $trail->push(trans('layout.asset.edit'), route('assets.edit',$id));
});
// Home > Assets > [Checkin]
Breadcrumbs::for('assets.checkin', function ($trail,$id) {
    $trail->parent('assets');
    $trail->push(trans('layout.asset.checkin'), route('assets.checkin',$id));
});
// Home > Assets > [Checkout]
Breadcrumbs::for('assets.checkout', function ($trail,$id) {
    $trail->parent('assets');
    $trail->push(trans('layout.asset.checkout'), route('assets.checkout',$id));
});

// Home > Assets Model
Breadcrumbs::for('models', function ($trail) {
    $trail->parent('home');
    $trail->push(trans('layout.asset_models'), route('models.index'));
});

// Home > Assets Model> [Create]
Breadcrumbs::for('models.add', function ($trail) {
    $trail->parent('models');
    $trail->push(trans('layout.model.create'), route('models.create'));
});
// Home > Assets Model> [update]
Breadcrumbs::for('models.edit', function ($trail,$id) {
    $trail->parent('models');
    $trail->push(trans('layout.model.edit'), route('models.edit',$id));
});



// Home > License
Breadcrumbs::for('licenses', function ($trail) {
    $trail->parent('home');
    $trail->push(trans('layout.licenses'), route('licenses.index'));
});
// Home > License > [Create]
Breadcrumbs::for('licenses.add', function ($trail) {
    $trail->parent('licenses');
    $trail->push(trans('layout.license.create'), route('licenses.create'));
});
// Home > License > [update]
Breadcrumbs::for('licenses.edit', function ($trail,$id) {
    $trail->parent('licenses');
    $trail->push(trans('layout.license.edit'), route('licenses.edit',$id));
});
// Home > License > [detail]
Breadcrumbs::for('licenses.show', function ($trail,$id) {
    $trail->parent('licenses');
    $trail->push(trans('layout.license.show'), route('licenses.show',$id));
});



// Home > Components
Breadcrumbs::for('components', function ($trail) {
    $trail->parent('home');
    $trail->push(trans('layout.components'), route('components.index'));
});

// Home > Components > [Create]
Breadcrumbs::for('components.add', function ($trail) {
    $trail->parent('components');
    $trail->push(trans('layout.component.create'), route('components.create'));
});
// Home > Components > [update]
Breadcrumbs::for('components.edit', function ($trail,$id) {
    $trail->parent('components');
    $trail->push(trans('layout.component.edit'), route('components.edit',$id));
});
// Home > Components > [show]
Breadcrumbs::for('components.show', function ($trail,$id) {
    $trail->parent('components');
    $trail->push(trans('layout.component.detail'), route('components.show',$id));
});

// Home > Accessories
Breadcrumbs::for('accessories', function ($trail) {
    $trail->parent('home');
    $trail->push(trans('layout.accessories'), route('accessories.index'));
});
// Home > Accessories > [Create]
Breadcrumbs::for('accessories.add', function ($trail) {
    $trail->parent('accessories');
    $trail->push(trans('layout.accessory.create'), route('accessories.create'));
});
// Home > Accessories > [update]
Breadcrumbs::for('accessories.edit', function ($trail,$id) {
    $trail->parent('accessories');
    $trail->push(trans('layout.accessory.edit'), route('accessories.edit',$id));
});
// Home > Accessories > [detail]
Breadcrumbs::for('accessories.show', function ($trail,$id) {
    $trail->parent('accessories');
    $trail->push(trans('layout.accessory.show'), route('accessories.show',$id));
});


// Home > Categories
Breadcrumbs::for('categories', function ($trail) {
    $trail->parent('home');
    $trail->push(trans('layout.categories'), route('categories.index'));
});

// Home > Categories > [Create]
Breadcrumbs::for('categories.add', function ($trail) {
    $trail->parent('categories');
    $trail->push(trans('layout.category.create'), route('categories.create'));
});
// Home > Categories > [update]
Breadcrumbs::for('categories.edit', function ($trail,$id) {
    $trail->parent('categories');
    $trail->push(trans('layout.category.edit'), route('categories.edit',$id));
});

// Home > Companies
Breadcrumbs::for('companies', function ($trail) {
    $trail->parent('home');
    $trail->push(trans('layout.companies'), route('companies.index'));
});
// Home > Companies > [Create]
Breadcrumbs::for('companies.add', function ($trail) {
    $trail->parent('companies');
    $trail->push(trans('layout.company.create'), route('companies.create'));
});
// Home > Companies > [update]
Breadcrumbs::for('companies.edit', function ($trail,$id) {
    $trail->parent('companies');
    $trail->push(trans('layout.company.edit'), route('companies.edit',$id));
});

// Home > Departments
Breadcrumbs::for('departments', function ($trail) {
    $trail->parent('home');
    $trail->push('Departments', route('departments.index'));
});
// Home > Departments > [Create]
Breadcrumbs::for('departments.add', function ($trail) {
    $trail->parent('departments');
    $trail->push('Departments Create', route('departments.create'));
});
// Home > Departments > [update]
Breadcrumbs::for('departments.edit', function ($trail,$id) {
    $trail->parent('departments');
    $trail->push('Departments Edit', route('departments.edit',$id));
});

// Home > Locations
Breadcrumbs::for('locations', function ($trail) {
    $trail->parent('home');
    $trail->push(trans('layout.locations'), route('locations.index'));
});
// Home > Locations > [Create]
Breadcrumbs::for('locations.add', function ($trail) {
    $trail->parent('locations');
    $trail->push(trans('layout.location.create'), route('locations.create'));
});
// Home > Locations > [update]
Breadcrumbs::for('locations.edit', function ($trail,$id) {
    $trail->parent('locations');
    $trail->push(trans('layout.location.edit'), route('locations.edit',$id));
});

// Home > Users
Breadcrumbs::for('users', function ($trail) {
    $trail->parent('home');
    $trail->push(trans('layout.users'), route('users.index'));
});
// Home > Users > [Create]
Breadcrumbs::for('users.add', function ($trail) {
    $trail->parent('users');
    $trail->push(trans('layout.user.create'), route('users.create'));
});
// Home > Users > [update]
Breadcrumbs::for('users.edit', function ($trail,$id) {
    $trail->parent('users');
    $trail->push(trans('layout.user.edit'), route('users.edit',$id));
});
// Home > Users > [detail]
Breadcrumbs::for('users.show', function ($trail,$id) {
    $trail->parent('users');
    $trail->push(trans('layout.user.detail'), route('users.show',$id));
});
// Home > Roles
Breadcrumbs::for('roles', function ($trail) {
    $trail->parent('home');
    $trail->push(trans('layout.role.roles'), route('roles.index'));
});
// Home > Roles > [Create]
Breadcrumbs::for('roles.add', function ($trail) {
    $trail->parent('roles');
    $trail->push( trans('layout.role.create'), route('roles.create'));
});
// Home > Roles > [update]
Breadcrumbs::for('roles.edit', function ($trail,$id) {
    $trail->parent('roles');
    $trail->push(trans('layout.role.edit'), route('roles.edit',$id));
});

// Home > Custom Fields
Breadcrumbs::for('fields.index', function ($trail) {
    $trail->parent('home');
    $trail->push(trans('layout.custom_field.manage'), route('fields.index'));
});
// Home > Fields > [Create]
Breadcrumbs::for('fields.field.create', function ($trail) {
    $trail->parent('fields.index');
    $trail->push(trans('layout.custom_field.create'), route('fields.field.create'));
});
Breadcrumbs::for('fields.field.update', function ($trail,$id) {
    $trail->parent('fields.index');
    $trail->push(trans('layout.custom_field.edit'), route('fields.field.update',$id));
});
// Home > Custom Fields > [detail]
Breadcrumbs::for('fields.fieldset.show', function ($trail,$id) {
    $trail->parent('fields.index');
    $trail->push(trans('layout.custom_fields'), route('fields.fieldset.show',$id));
});
// Home > Settings
Breadcrumbs::for('settings.index', function ($trail) {
    $trail->parent('home');
    $trail->push(trans('layout.setting.setting'), route('settings.index'));
});
