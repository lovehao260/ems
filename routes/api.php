<?php

use App\Http\Controllers\Api\AssetController;
use App\Http\Controllers\Api\CategoryController;
use App\Http\Controllers\Api\CustomFieldController;
use App\Http\Controllers\Api\CommonController;
use App\Http\Controllers\Api\LocationController;
use App\Http\Controllers\Api\ModelController;
use App\Http\Controllers\Api\UserController;
use Illuminate\Support\Facades\Route;
use App\Http\Controllers\Api\CompanyController;
/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/
/*--- Asset API ---*/

Route::group(['prefix' => 'assets'], function () {
    Route::get('/index',[AssetController::class,'index'])->name('api.assets.index');
    Route::get('/table-field/{table}',[CommonController::class,'fieldTableDetail'])->name('api.asset.detail');
    Route::get('/get-filed/{asset_id?}',[AssetController::class,'getFiled'])->name('api.assets.field');
});
/*--- License API ---*/

Route::group(['prefix' => 'licenses'], function () {
    Route::get('/table-field/{table}',[CommonController::class,'fieldTableDetail'])->name('api.license.detail');
});

Route::group(['prefix' => 'accessories'], function () {
    Route::get('/table-field/{table}',[CommonController::class,'fieldTableDetail'])->name('api.accessories.detail');
});

Route::group(['prefix' => 'components'], function () {
    Route::get('/table-field/{table}',[CommonController::class,'fieldTableDetail'])->name('api.components.detail');
});

/*--- Location API ---*/

Route::group(['prefix' => 'locations'], function () {
    Route::get('/index',[LocationController::class,'index'])->name('api.locations.index');
});
/*--- Companies API ---*/

Route::group(['prefix' => 'companies'], function () {
    Route::get('/index',[CompanyController::class,'index'])->name('api.companies.index');
});
/*--- Categories API ---*/

Route::group(['prefix' => 'categories'], function () {
    Route::get('/index',[CategoryController::class,'index'])->name('api.categories.index');
});

/*--- User API ---*/
Route::group(['prefix' => 'users'], function () {
    Route::get('/index',[UserController::class,'index'])->name('api.users.index');
});

/*--- Companies API ---*/

Route::group(['prefix' => 'models'], function () {
    Route::get('/index',[ModelController::class,'index'])->name('api.models.index');
});
Route::group(['prefix' => 'categories'], function () {

    Route::get('{item_type}/selectlist',
        [
            'as' => 'api.categories.selectlist',
            'uses' => 'CategoriesController@selectlist'
        ]
    );

});
/*--- User API ---*/
Route::group(['prefix' => 'fields'], function () {
    Route::get('/fieldset/{id}',[CustomFieldController::class,'index'])->name('api.fields.index');
    Route::get('/custom/model/{mode_id?}',[CustomFieldController::class,'getModelField'])->name('api.fields.getModelField');
});
