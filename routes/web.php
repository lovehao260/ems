<?php

use App\Http\Controllers\AccessoryController;
use App\Http\Controllers\ActiveLogController;
use App\Http\Controllers\AssetController;
use App\Http\Controllers\AssetModelController;
use App\Http\Controllers\CategoryController;
use App\Http\Controllers\CompanyController;
use App\Http\Controllers\ComponentController;
use App\Http\Controllers\CustomFieldController;
use App\Http\Controllers\DashboardController;
use App\Http\Controllers\DepartmentController;
use App\Http\Controllers\ImportController;
use App\Http\Controllers\LicenseController;
use App\Http\Controllers\LocationController;
use App\Http\Controllers\ProfileController;
use App\Http\Controllers\RoleController;
use App\Http\Controllers\SettingController;
use App\Http\Controllers\TableController;
use App\Http\Controllers\UserController;
use Illuminate\Support\Facades\Cookie;
use Illuminate\Support\Facades\Route;
use App\Http\Controllers\MediaController;
/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/
Auth::routes();
Route::get('dashboard',[DashboardController::class,'index'])->name('dashboard.index');
Route::get('/',[DashboardController::class,'index']);

Route::get('/medias',[MediaController::class,'index'])->name('medias.index');
Route::get('get-data/medias/{id}',[MediaController::class,'getData'])->name('medias.data');
Route::post('post-data/medias/{id}',[MediaController::class,'uploadFile'])->name('medias.upload');
Route::post('medias-folder/store',[MediaController::class,'storeFolder'])->name('medias.folder.store');
Route::post('/medias-rename/update',[MediaController::class,'renameMedia'])->name('medias.rename');
Route::post('/medias-move/trash',[MediaController::class,'moveTrashMedia'])->name('medias.move.trash');
Route::get('/medias/download/{id}',[MediaController::class,'downloadMedia'])->name('medias.download');
Route::post('/medias/delete',[MediaController::class,'deleteMedia'])->name('medias.delete');
Route::post('/medias/restore',[MediaController::class,'restoreMedia'])->name('medias.restore');


Route::get('/home', [App\Http\Controllers\HomeController::class, 'index'])->name('home');

Route::group( ['prefix'=>'users','as'=>'users.'],function () {
    Route::get('/',[UserController::class,'index'])->name('index');
    Route::get('/list/{trash}',[UserController::class,'index'])->name('trash');
    Route::get('/create',[UserController::class,'create'])->name('create');
    Route::post('/store',[UserController::class,'store'])->name('store');
    Route::get('/edit/{id}',[UserController::class,'edit'])->name('edit');
    Route::post('/update/{id}',[UserController::class,'update'])->name('update');
    Route::get('/clone/{id}',[UserController::class,'clone'])->name('clone');

    Route::delete('/deletes',[UserController::class,'deletes'])->name('deletes');
    Route::delete('/deleted/{id}',[UserController::class,'deleted'])->name('deleted');
    Route::delete('/empty-trash',[UserController::class,'emptyAllTrash'])->name('empty_trash');
    Route::get('/restore/{id}',[UserController::class,'restore'])->name('restore');
    Route::delete('/destroy/{id}',[UserController::class,'destroy'])->name('destroy');
    Route::get('{id}',[UserController::class,'show'])->name('show');
    Route::get('accessories/{id}',[UserController::class,'accessory'])->name('accessories');
    Route::get('assets/{id}',[UserController::class,'asset'])->name('assets');
});

Route::group( ['prefix'=>'companies','as'=>'companies.'],function () {
    Route::prefix('companies')->group(function () {
        Route::get('/',[CompanyController::class,'index'])->name('index');
        Route::get('/create',[CompanyController::class,'create'])->name('create');
        Route::post('/store',[CompanyController::class,'store'])->name('store');
        Route::get('/edit/{id}',[CompanyController::class,'edit'])->name('edit');
        Route::post('/update/{id}',[CompanyController::class,'update'])->name('update');
        Route::delete('/deletes/{id}',[CompanyController::class,'destroy'])->name('destroy');
    });
});
Route::prefix('departments')->group(function () {
    Route::get('/',[DepartmentController::class,'index'])->name('departments.index');
    Route::get('/create',[DepartmentController::class,'create'])->name('departments.create');
    Route::post('/store',[DepartmentController::class,'store'])->name('departments.store');
    Route::get('/edit/{id}',[DepartmentController::class,'edit'])->name('departments.edit');
    Route::post('/update/{id}',[DepartmentController::class,'update'])->name('departments.update');
    Route::delete('/deletes/{id}',[DepartmentController::class,'destroy'])->name('departments.destroy');

});

Route::prefix('categories')->group(function () {
    Route::get('/',[CategoryController::class,'index'])->name('categories.index');
    Route::get('/create',[CategoryController::class,'create'])->name('categories.create');
    Route::post('/store',[CategoryController::class,'store'])->name('categories.store');
    Route::get('/edit/{id}',[CategoryController::class,'edit'])->name('categories.edit');
    Route::post('/update/{id}',[CategoryController::class,'update'])->name('categories.update');
    Route::delete('/deletes/{id}',[CategoryController::class,'destroy'])->name('categories.destroy');
    Route::get('{id}',[CategoryController::class,'show'])->name('categories.show');
});
Route::prefix('locations')->group(function () {
    Route::get('/',[LocationController::class,'index'])->name('locations.index');
    Route::get('/create',[LocationController::class,'create'])->name('locations.create');
    Route::post('/store',[LocationController::class,'store'])->name('locations.store');
    Route::get('/edit/{id}',[LocationController::class,'edit'])->name('locations.edit');
    Route::post('/update/{id}',[LocationController::class,'update'])->name('locations.update');
    Route::delete('/deletes/{id}',[LocationController::class,'destroy'])->name('locations.destroy');
    Route::get('{id}',[LocationController::class,'show'])->name('locations.show');
});

Route::prefix('components')->group(function () {
    Route::get('/',[ComponentController::class,'index'])->name('components.index');
    Route::get('/create',[ComponentController::class,'create'])->name('components.create');
    Route::post('/store',[ComponentController::class,'store'])->name('components.store');
    Route::get('/edit/{id}',[ComponentController::class,'edit'])->name('components.edit');
    Route::post('/update/{id}',[ComponentController::class,'update'])->name('components.update');
    Route::delete('/deletes/{id}',[ComponentController::class,'destroy'])->name('components.destroy');
    Route::get('{id}',[ComponentController::class,'show'])->name('components.show');
    Route::post('{id}/checkout',[ComponentController::class,'checkout'])->name('components.checkout');
    Route::post('{id}/checkin',[ComponentController::class,'checkin'])->name('components.checkin');
    Route::get('/{id}/qr-code-download',[ComponentController::class,'qrCodeDownload'])->name('components.qr.download');
});

Route::prefix('licenses')->group(function () {
    Route::get('/',[LicenseController::class,'index'])->name('licenses.index');
    Route::get('/list/{trash}',[LicenseController::class,'index'])->name('licenses.trash');
    Route::get('/create',[LicenseController::class,'create'])->name('licenses.create');
    Route::post('/store',[LicenseController::class,'store'])->name('licenses.store');
    Route::get('/edit/{id}',[LicenseController::class,'edit'])->name('licenses.edit');
    Route::post('/update/{id}',[LicenseController::class,'update'])->name('licenses.update');
    Route::get('/clone/{id}',[LicenseController::class,'clone'])->name('licenses.clone');
    Route::delete('/deletes/{id}',[LicenseController::class,'destroy'])->name('licenses.destroy');

    Route::post('{id}/checkout/{seat_id?}',[LicenseController::class,'checkout'])->name('licenses.checkout');
    Route::post('{id}/checkin/{seat_id?}',[LicenseController::class,'checkin'])->name('licenses.checkin');
    Route::get('/{id}',[LicenseController::class,'show'])->name('licenses.show');
    Route::get('/{id}/qr-code-download',[LicenseController::class,'qrCodeDownload'])->name('licenses.qr.download');
});

Route::prefix('accessories')->group(function () {
    Route::get('/',[AccessoryController::class,'index'])->name('accessories.index');
    Route::get('/list/{trash}',[AccessoryController::class,'index'])->name('accessories.trash');
    Route::get('/create',[AccessoryController::class,'create'])->name('accessories.create');
    Route::post('/store',[AccessoryController::class,'store'])->name('accessories.store');
    Route::get('/edit/{id}',[AccessoryController::class,'edit'])->name('accessories.edit');
    Route::post('/update/{id}',[AccessoryController::class,'update'])->name('accessories.update');
    Route::get('/clone/{id}',[AccessoryController::class,'clone'])->name('accessories.clone');

    Route::delete('/deletes',[AccessoryController::class,'deletes'])->name('accessories.deletes');
    Route::delete('/deleted/{id}',[AccessoryController::class,'deleted'])->name('accessories.deleted');
    Route::delete('/empty-trash',[AccessoryController::class,'emptyAllTrash'])->name('accessories.empty_trash');
    Route::get('/restore/{id}',[AccessoryController::class,'restore'])->name('accessories.restore');
    Route::delete('/destroy/{id}',[AccessoryController::class,'destroy'])->name('accessories.destroy');
    Route::post('{id}/checkout/{seat_id?}',[AccessoryController::class,'checkout'])->name('accessories.checkout');
    Route::post('{id}/checkin/{seat_id?}',[AccessoryController::class,'checkin'])->name('accessories.checkin');
    Route::get('/{id}',[AccessoryController::class,'show'])->name('accessories.show');
    Route::get('/{id}/qr-code-download',[AccessoryController::class,'qrCodeDownload'])->name('accessories.qr.download');
});


Route::prefix('assets')->group(function () {
    Route::get('/',[AssetController::class,'index'])->name('assets.index');
    Route::get('/list/{trash}',[AssetController::class,'index'])->name('assets.trash');
    Route::get('/create',[AssetController::class,'create'])->name('assets.create');
    Route::post('/store',[AssetController::class,'store'])->name('assets.store');
    Route::get('/edit/{id}',[AssetController::class,'edit'])->name('assets.edit');
    Route::post('/update/{id}',[AssetController::class,'update'])->name('assets.update');
    Route::get('/clone/{id}',[AssetController::class,'clone'])->name('assets.clone');

    Route::delete('/deletes',[AssetController::class,'deletes'])->name('assets.deletes');
    Route::delete('/deleted/{id}',[AssetController::class,'deleted'])->name('assets.deleted');
    Route::delete('/empty-trash',[AssetController::class,'emptyAllTrash'])->name('assets.empty_trash');
    Route::get('/restore/{id}',[AssetController::class,'restore'])->name('assets.restore');
    Route::delete('/destroy/{id}',[AssetController::class,'destroy'])->name('assets.destroy');
    Route::get('{id}/checkout',[AssetController::class,'checkout'])->name('assets.checkout');
    Route::post('{id}/checkout',[AssetController::class,'checkoutPost'])->name('assets.checkout.post');

    Route::get('{id}/checkin',[AssetController::class,'checkin'])->name('assets.checkin');
    Route::post('{id}/checkin',[AssetController::class,'checkinPost'])->name('assets.checkin.post');
    Route::get('/{id}',[AssetController::class,'show'])->name('assets.show');

    Route::get('/{id}/qr-code',[AssetController::class,'qrCode'])->name('assets.qr.detail');
    Route::get('/{id}/qr-code-download',[AssetController::class,'qrCodeDownload'])->name('assets.qr.download');
});
Route::group( ['prefix'=>'custom-field','as'=>'fields.'],function () {
    Route::get('/',[CustomFieldController::class,'index'])->name('index');
    Route::get('field',[CustomFieldController::class,'getFields'])->name('field');
    Route::get('field/create',[CustomFieldController::class,'createField'])->name('field.create');
    Route::post('field/store',[CustomFieldController::class,'storeField'])->name('field.store');
    Route::get('field/edit/{id}',[CustomFieldController::class,'editField'])->name('field.edit');
    Route::post('field/update/{id}',[CustomFieldController::class,'updateField'])->name('field.update');
    Route::delete('field/destroy/{id}',[CustomFieldController::class,'destroyField'])->name('field.destroy');

    Route::get('fieldset',[CustomFieldController::class,'getFieldSets'])->name('fieldset');
    Route::post('fieldset/store',[CustomFieldController::class,'storeFieldSet'])->name('fieldset.store');
    Route::get('fieldset/edit/{id}',[CustomFieldController::class,'editFieldSet'])->name('fieldset.edit');
    Route::post('fieldset/update/{id}',[CustomFieldController::class,'updateFieldSet'])->name('fieldset.update');
    Route::delete('fieldset/destroy/{id}',[CustomFieldController::class,'destroyFieldSet'])->name('fieldset.destroy');
    Route::get('fieldset/{id}',[CustomFieldController::class,'showFieldSet'])->name('fieldset.show');
    Route::post('fieldset/custom/{id}',[CustomFieldController::class,'storeCustomFieldSet'])->name('fieldset.custom');
    Route::post('fieldset/custom-order/{id}',[CustomFieldController::class,'postReorder'])->name('fieldset.customOrder');

    Route::delete('fieldset/{id}/disassociate/{field_id}',[CustomFieldController::class,'disassociate'])->name('fieldset.disassociate');
});

Route::prefix('models')->group(function () {
    Route::get('/',[AssetModelController::class,'index'])->name('models.index');
    Route::get('/list/{trash}',[AssetModelController::class,'index'])->name('models.trash');
    Route::get('/create',[AssetModelController::class,'create'])->name('models.create');
    Route::post('/store',[AssetModelController::class,'store'])->name('models.store');
    Route::get('/edit/{id}',[AssetModelController::class,'edit'])->name('models.edit');
    Route::post('/update/{id}',[AssetModelController::class,'update'])->name('models.update');
    Route::get('/clone/{id}',[AssetModelController::class,'clone'])->name('models.clone');

    Route::delete('/deletes',[AssetModelController::class,'deletes'])->name('models.deletes');
    Route::delete('/deleted/{id}',[AssetModelController::class,'deleted'])->name('models.deleted');
    Route::delete('/empty-trash',[AssetModelController::class,'emptyAllTrash'])->name('models.empty_trash');
    Route::get('/restore/{id}',[AssetModelController::class,'restore'])->name('models.restore');
    Route::delete('/destroy/{id}',[AssetModelController::class,'destroy'])->name('models.destroy');

    Route::get('/{id}',[AssetModelController::class,'show'])->name('models.show');
});
//Route::resource('companies', CompanyController::class);

Route::get('bulk-change/data', [TableController::class, 'getDataForBulkChanges'])->name('tables.bulk-change.data');
Route::post('bulk-change/save', [TableController::class, 'postSaveBulkChange'])->name('tables.bulk-change.save');
Route::get('get-filter-input', [TableController::class, 'getFilterInput'])->name('tables.get-filter-input');
Route::post('/show/table',[TableController::class,'showTableDetail'])->name('show.table.detail');
Route::group( ['prefix'=>'imports','as'=>'imports.'],function () {
    Route::get('/', [ImportController::class, 'index'])->name('index');
    Route::get('custom-import', [ImportController::class, 'custom'])->name('custom');
    Route::post('file-import', [ImportController::class, 'fileImport'])->name('post');
    Route::delete('import/delete/{id}', [ImportController::class, 'destroy'])->name('destroy');
    Route::get('user-import', [ImportController::class, 'progressImport'])->name('progress');
});

Route::group( ['prefix'=>'activities','as'=>'activities.'],function () {
    Route::get('/', [ActiveLogController::class, 'index'])->name('index');
    Route::delete('/destroy/{id}', [ActiveLogController::class, 'destroy'])->name('destroy');
    Route::delete('/deletes', [ActiveLogController::class, 'deletes'])->name('deletes');
    Route::delete('/clearLogs', [ActiveLogController::class, 'clearLogs'])->name('clear');
});

Route::group( ['prefix'=>'roles','as'=>'roles.'],function () {
    Route::get('/', [RoleController::class, 'index'])->name('index');
    Route::delete('/destroy/{id}', [RoleController::class, 'destroy'])->name('destroy');
    Route::delete('/deletes', [RoleController::class, 'deletes'])->name('deletes');

    Route::get('/create',[RoleController::class,'create'])->name('create');
    Route::post('/store',[RoleController::class,'store'])->name('store');
    Route::get('/edit/{id}',[RoleController::class,'edit'])->name('edit');
    Route::post('/update/{id}',[RoleController::class,'update'])->name('update');

});
Route::group( ['prefix'=>'settings','as'=>'settings.'],function () {
    Route::get('/general', [SettingController::class, 'index'])->name('index');
    Route::post('/save', [SettingController::class, 'save'])->name('store');
    Route::get('/language/{locale}', [SettingController::class, 'locale'])->name('locale');
});

Route::group( ['prefix'=>'profile','as'=>'profile.'],function () {
    Route::get('/', [ProfileController::class, 'index'])->name('index');
    Route::post('/', [ProfileController::class, 'password'])->name('password');
});
