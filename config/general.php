<?php

return [
    /*
    |--------------------------------------------------------------------------
    | Types import
    |--------------------------------------------------------------------------
    |
    */

    'types_import' => [
        'users' => [
            'name'=>'Users',
            'route'=>'imports.progress'
        ],
        'assets' => [
            'name'=>'Assets',
            'route'=>'imports.progress'
        ],
        'accessories' => [
            'name'=>'Accessories',
            'route'=>'imports.progress'
        ],
        'components' => [
            'name'=>'Components',
            'route'=>'imports.progress'
        ],
        'licenses' => [
            'name'=>'Licenses',
            'route'=>'imports.progress'
        ],
    ],
    'types_table'=>[
        'edit'=>'edit',
        'clone'=>'clone'
    ],
    'asset_detail'=>[
        'name'=>[
            'label'=>'layout.name',
            'value'=>'name'
        ],
        'status'=>[
            'label'=>'layout.status',
            'value'=>'status'
        ],
        'serial'=>[
            'label'=>'layout.component.serial',
            'value'=>'serial'
        ],
        'category'=>[
            'label'=>'layout.category.category',
            'value'=>'category'
        ],
        'model'=>[
            'label'=>'layout.model.model',
            'value'=>'model'
        ],
        'asset_tag'=>[
            'label'=>'layout.asset.tag',
            'value'=>'asset_tag'
        ],
        'purchase_cost'=>[
            'label'=>'layout.purchase_cost',
            'value'=>'purchase_cost'
        ],
        'purchase_date'=>[
            'label'=>'layout.purchase_date',
            'value'=>'purchase_date'
        ],
        'order_number'=>[
            'label'=>'layout.order_number',
            'value'=>'order_number'
        ],
        'company'=>[
            'label'=>'layout.company.company',
            'value'=>'company'
        ],
        'checkout_to'=>[
            'label'=>'layout.asset.checkout_to',
            'value'=>'checkout_to'
        ],

        'created_at'=>[
            'label'=>'table.created_at',
            'value'=>'created_at'
        ],
        'note'=>[
            'label'=>'layout.note',
            'value'=>'note'
        ],
    ],
    'license_detail'=>[
        'name'=>[
            'label'=>'layout.name',
            'value'=>'name'
        ],
        'serial'=>[
            'label'=>'layout.license.key',
            'value'=>'serial'
        ],
        'category'=>[
            'label'=>'layout.category.category',
            'value'=>'category'
        ],
        'license_email'=>[
            'label'=>'layout.license.to_mail',
            'value'=>'license_email'
        ],
        'purchase_cost'=>[
            'label'=>'layout.purchase_cost',
            'value'=>'purchase_cost'
        ],
        'purchase_date'=>[
            'label'=>'layout.purchase_date',
            'value'=>'purchase_date'
        ],
        'purchase_order'=>[
            'label'=>'layout.purchase_order',
            'value'=>'purchase_date'
        ],
        'order_number'=>[
            'label'=>'layout.order_number',
            'value'=>'order_number'
        ],
        'company'=>[
            'label'=>'layout.company.company',
            'value'=>'company'
        ],
        'created_at'=>[
            'label'=>'table.created_at',
            'value'=>'created_at'
        ],
        'note'=>[
            'label'=>'layout.note',
            'value'=>'note'
        ],
    ],
    'accessory_detail'=>[
        'name'=>[
            'label'=>'layout.name',
            'value'=>'name'
        ],

        'category'=>[
            'label'=>'layout.category.category',
            'value'=>'category'
        ],
        'purchase_cost'=>[
            'label'=>'layout.purchase_cost',
            'value'=>'purchase_cost'
        ],
        'purchase_date'=>[
            'label'=>'layout.purchase_date',
            'value'=>'purchase_date'
        ],
        'order_number'=>[
            'label'=>'layout.order_number',
            'value'=>'order_number'
        ],
        'company'=>[
            'label'=>'layout.company.company',
            'value'=>'company'
        ],
        'created_at'=>[
            'label'=>'table.created_at',
            'value'=>'created_at'
        ],
    ],
    'component_detail'=>[
        'name'=>[
            'label'=>'layout.name',
            'value'=>'name'
        ],
        'serial'=>[
            'label'=>'layout.component.serial',
            'value'=>'serial'
        ],
        'category'=>[
            'label'=>'layout.category.category',
            'value'=>'category'
        ],

        'purchase_cost'=>[
            'label'=>'layout.purchase_cost',
            'value'=>'purchase_cost'
        ],
        'purchase_date'=>[
            'label'=>'layout.purchase_date',
            'value'=>'purchase_date'
        ],
        'order_number'=>[
            'label'=>'layout.order_number',
            'value'=>'order_number'
        ],
        'company'=>[
            'label'=>'layout.company.company',
            'value'=>'company'
        ],
        'created_at'=>[
            'label'=>'table.created_at',
            'value'=>'created_at'
        ],

    ],
    'formats'=>[
        'text' => 'Text Box',
        'listbox' => 'List Box',
        'textarea' => 'Textarea (multi-line) ',
        'checkbox' => 'Checkbox',
        'radio' => 'Radio Buttons',
    ],
    'predefined_formats' => [
        'ANY'           => '',
        'CUSTOM REGEX'  => 'regex',
        'ALPHA'         => 'alpha',
        'NUMERIC'       => 'numeric',
        'EMAIL'         => 'email',
        'DATE'          => 'date',
        'URL'           => 'url',
        'IP'            => 'ip',
        'MAC'           => 'regex:/^[a-fA-F0-9]{2}:[a-fA-F0-9]{2}:[a-fA-F0-9]{2}:[a-fA-F0-9]{2}:[a-fA-F0-9]{2}:[a-fA-F0-9]{2}$/',
        'BOOLEAN'       => 'boolean',
    ],
    'log'=>[
        'create'=>'create',
        'update'=>'update',
        'destroy'=>'destroy',
        'deleted'=>'deleted',
        'restore'=>'restore',
        'checkin'=>'checkin',
        'checkout'=>'checkout',
        'remove'=>'remove',
        'login'=>'Login',
        'logout'=>'Logout',

    ],

    'column_option'=>[
        'users' => [
            'first_name'=>'First Name',
            'last_name'=>'Last Name',
            'company'=>'Company',
            'manager'=>'Manager',
            'email'=>'Email',
            'username'=>'Username',
            'phone'=>'Phone',
            'address'=>'Address',
            'location'=>'Location',
            'created_at'=>'Created at'
        ],
        'licenses' => [
            'name'=>'Name',
            'company'=>'Company',
            'serial'=>'Product Key',
            'expiration_date'=>'Expiration Date',
            'license_email'=>'Licensed To Email',
            'license_name'=>'Licensed To Name',
            'category'=>'Category',
            'seats'=>'Total',
            'purchase_cost'=>'Purchase Cost',
            'purchase_order'=>'Purchase Order',
            'purchase_date'=>'Purchase Date',
            'order_number'=>'Order Number',
            'notes'=>'Notes',
            'termination_date'=>'Termination Date',
            'reassignable'=>'Reassignable',

        ],
        'accessories' => [
            'name'=>'Name',
            'company'=>'Company',
            'category'=>'Category',
            'location'=>'Location',
            'model_number'=>'Model.No',
            'order_number'=>'Order Number',
            'purchase_cost'=>'Purchase Cost',
            'purchase_date'=>'Purchase Date',
            'qty'=>'Total',
            'min_amt'=>'Min. QTY',
            'notes'=>'Notes'
        ],
        'components' => [
            'name'=>'Name',
            'category'=>'Category',
            'serial'=>'Serial',
            'qty'=>'Total',
            'min_amt'=>'Min. QTY',
            'location'=>'Location',
            'company'=>'Company',
            'order_number'=>'Order Number',
            'purchase_cost'=>'Purchase Cost',
            'purchase_date'=>'Purchase Date',
        ],
        'assets' => [
            'name'=>'Asset name',
            'asset_tag'=>'Asset Tag',
            'category'=>'Category',
            'serial'=>'Serial',
            'model'=>'Model',
            'status'=>'Status',
            'purchase_date'=>'Purchase Date',
            'order_number'=>'Order number',
            'purchase_cost'=>'Purchase Cost',
            'warranty_months'=>'Warranty',
            'location'=>'Location',
            'company'=>'Company',
            'notes'=>'Notes'
        ],

    ],

    'country'=>[
        "AF"=> "Afghanistan",
    "AL"=> "Albania",
    "DZ"=> "Algeria",
    "AS"=> "American Samoa",
    "AD"=> "Andorra",
    "AO"=> "Angola",
    "AI"=> "Anguilla",
    "AQ"=> "Antarctica",
    "AG"=> "Antigua and Barbuda",
    "AR"=> "Argentina",
    "AM"=> "Armenia",
    "AW"=> "Aruba",
    "AU"=> "Australia",
    "AT"=> "Austria",
    "AZ"=> "Azerbaijan",
    "BS"=> "Bahamas (the)",
    "BH"=> "Bahrain",
    "BD"=> "Bangladesh",
    "BB"=> "Barbados",
    "BY"=> "Belarus",
    "BE"=> "Belgium",
    "BZ"=> "Belize",
    "BJ"=> "Benin",
    "BM"=> "Bermuda",
    "BT"=> "Bhutan",
    "BO"=> "Bolivia (Plurinational State of)",
    "BQ"=> "Bonaire, Sint Eustatius and Saba",
    "BA"=> "Bosnia and Herzegovina",
    "BW"=> "Botswana",
    "BV"=> "Bouvet Island",
    "BR"=> "Brazil",
    "IO"=> "British Indian Ocean Territory (the)",
    "BN"=> "Brunei Darussalam",
    "BG"=> "Bulgaria",
    "BF"=> "Burkina Faso",
    "BI"=> "Burundi",
    "CV"=> "Cabo Verde",
    "KH"=> "Cambodia",
    "CM"=> "Cameroon",
    "CA"=> "Canada",
    "KY"=> "Cayman Islands (the)",
    "CF"=> "Central African Republic (the)",
    "TD"=> "Chad",
    "CL"=> "Chile",
    "CN"=> "China",
    "CX"=> "Christmas Island",
    "CC"=> "Cocos (Keeling) Islands (the)",
    "CO"=> "Colombia",
    "KM"=> "Comoros (the)",
    "CD"=> "Congo (the Democratic Republic of the)",
    "CG"=> "Congo (the)",
    "CK"=> "Cook Islands (the)",
    "CR"=> "Costa Rica",
    "HR"=> "Croatia",
    "CU"=> "Cuba",
    "CW"=> "Curaçao",
    "CY"=> "Cyprus",
    "CZ"=> "Czechia",
    "CI"=> "Côte d'Ivoire",
    "DK"=> "Denmark",
    "DJ"=> "Djibouti",
    "DM"=> "Dominica",
    "DO"=> "Dominican Republic (the)",
    "EC"=> "Ecuador",
    "EG"=> "Egypt",
    "SV"=> "El Salvador",
    "GQ"=> "Equatorial Guinea",
    "ER"=> "Eritrea",
    "EE"=> "Estonia",
    "SZ"=> "Eswatini",
    "ET"=> "Ethiopia",
    "FK"=> "Falkland Islands (the) [Malvinas]",
    "FO"=> "Faroe Islands (the)",
    "FJ"=> "Fiji",
    "FI"=> "Finland",
    "FR"=> "France",
    "GF"=> "French Guiana",
    "PF"=> "French Polynesia",
    "TF"=> "French Southern Territories (the)",
    "GA"=> "Gabon",
    "GM"=> "Gambia (the)",
    "GE"=> "Georgia",
    "DE"=> "Germany",
    "GH"=> "Ghana",
    "GI"=> "Gibraltar",
    "GR"=> "Greece",
    "GL"=> "Greenland",
    "GD"=> "Grenada",
    "GP"=> "Guadeloupe",
    "GU"=> "Guam",
    "GT"=> "Guatemala",
    "GG"=> "Guernsey",
    "GN"=> "Guinea",
    "GW"=> "Guinea-Bissau",
    "GY"=> "Guyana",
    "HT"=> "Haiti",
    "HM"=> "Heard Island and McDonald Islands",
    "VA"=> "Holy See (the)",
    "HN"=> "Honduras",
    "HK"=> "Hong Kong",
    "HU"=> "Hungary",
    "IS"=> "Iceland",
    "IN"=> "India",
    "ID"=> "Indonesia",
    "IR"=> "Iran (Islamic Republic of)",
    "IQ"=> "Iraq",
    "IE"=> "Ireland",
    "IM"=> "Isle of Man",
    "IL"=> "Israel",
    "IT"=> "Italy",
    "JM"=> "Jamaica",
    "JP"=> "Japan",
    "JE"=> "Jersey",
    "JO"=> "Jordan",
    "KZ"=> "Kazakhstan",
    "KE"=> "Kenya",
    "KI"=> "Kiribati",
    "KP"=> "Korea (the Democratic People's Republic of)",
    "KR"=> "Korea (the Republic of)",
    "KW"=> "Kuwait",
    "KG"=> "Kyrgyzstan",
    "LA"=> "Lao People's Democratic Republic (the)",
    "LV"=> "Latvia",
    "LB"=> "Lebanon",
    "LS"=> "Lesotho",
    "LR"=> "Liberia",
    "LY"=> "Libya",
    "LI"=> "Liechtenstein",
    "LT"=> "Lithuania",
    "LU"=> "Luxembourg",
    "MO"=> "Macao",
    "MG"=> "Madagascar",
    "MW"=> "Malawi",
    "MY"=> "Malaysia",
    "MV"=> "Maldives",
    "ML"=> "Mali",
    "MT"=> "Malta",
    "MH"=> "Marshall Islands (the)",
    "MQ"=> "Martinique",
    "MR"=> "Mauritania",
    "MU"=> "Mauritius",
    "YT"=> "Mayotte",
    "MX"=> "Mexico",
    "FM"=> "Micronesia (Federated States of)",
    "MD"=> "Moldova (the Republic of)",
    "MC"=> "Monaco",
    "MN"=> "Mongolia",
    "ME"=> "Montenegro",
    "MS"=> "Montserrat",
    "MA"=> "Morocco",
    "MZ"=> "Mozambique",
    "MM"=> "Myanmar",
    "NA"=> "Namibia",
    "NR"=> "Nauru",
    "NP"=> "Nepal",
    "NL"=> "Netherlands (the)",
    "NC"=> "New Caledonia",
    "NZ"=> "New Zealand",
    "NI"=> "Nicaragua",
    "NE"=> "Niger (the)",
    "NG"=> "Nigeria",
    "NU"=> "Niue",
    "NF"=> "Norfolk Island",
    "MP"=> "Northern Mariana Islands (the)",
    "NO"=> "Norway",
    "OM"=> "Oman",
    "PK"=> "Pakistan",
    "PW"=> "Palau",
    "PS"=> "Palestine, State of",
    "PA"=> "Panama",
    "PG"=> "Papua New Guinea",
    "PY"=> "Paraguay",
    "PE"=> "Peru",
    "PH"=> "Philippines (the)",
    "PN"=> "Pitcairn",
    "PL"=> "Poland",
    "PT"=> "Portugal",
    "PR"=> "Puerto Rico",
    "QA"=> "Qatar",
    "MK"=> "Republic of North Macedonia",
    "RO"=> "Romania",
    "RU"=> "Russian Federation (the)",
    "RW"=> "Rwanda",
    "RE"=> "Réunion",
    "BL"=> "Saint Barthélemy",
    "SH"=> "Saint Helena, Ascension and Tristan da Cunha",
    "KN"=> "Saint Kitts and Nevis",
    "LC"=> "Saint Lucia",
    "MF"=> "Saint Martin (French part)",
    "PM"=> "Saint Pierre and Miquelon",
    "VC"=> "Saint Vincent and the Grenadines",
    "WS"=> "Samoa",
    "SM"=> "San Marino",
    "ST"=> "Sao Tome and Principe",
    "SA"=> "Saudi Arabia",
    "SN"=> "Senegal",
    "RS"=> "Serbia",
    "SC"=> "Seychelles",
    "SL"=> "Sierra Leone",
    "SG"=> "Singapore",
    "SX"=> "Sint Maarten (Dutch part)",
    "SK"=> "Slovakia",
    "SI"=> "Slovenia",
    "SB"=> "Solomon Islands",
    "SO"=> "Somalia",
    "ZA"=> "South Africa",
    "GS"=> "South Georgia and the South Sandwich Islands",
    "SS"=> "South Sudan",
    "ES"=> "Spain",
    "LK"=> "Sri Lanka",
    "SD"=> "Sudan (the)",
    "SR"=> "Suriname",
    "SJ"=> "Svalbard and Jan Mayen",
    "SE"=> "Sweden",
    "CH"=> "Switzerland",
    "SY"=> "Syrian Arab Republic",
    "TW"=> "Taiwan",
    "TJ"=> "Tajikistan",
    "TZ"=> "Tanzania, United Republic of",
    "TH"=> "Thailand",
    "TL"=> "Timor-Leste",
    "TG"=> "Togo",
    "TK"=> "Tokelau",
    "TO"=> "Tonga",
    "TT"=> "Trinidad and Tobago",
    "TN"=> "Tunisia",
    "TR"=> "Turkey",
    "TM"=> "Turkmenistan",
    "TC"=> "Turks and Caicos Islands (the)",
    "TV"=> "Tuvalu",
    "UG"=> "Uganda",
    "UA"=> "Ukraine",
    "AE"=> "United Arab Emirates (the)",
    "GB"=> "United Kingdom of Great Britain and Northern Ireland (the)",
    "UM"=> "United States Minor Outlying Islands (the)",
    "US"=> "United States of America (the)",
    "UY"=> "Uruguay",
    "UZ"=> "Uzbekistan",
    "VU"=> "Vanuatu",
    "VE"=> "Venezuela (Bolivarian Republic of)",
    "VN"=> "Viet Nam",
    "VG"=> "Virgin Islands (British)",
    "VI"=> "Virgin Islands (U.S.)",
    "WF"=> "Wallis and Futuna",
    "EH"=> "Western Sahara",
    "YE"=> "Yemen",
    "ZM"=> "Zambia",
    "ZW"=> "Zimbabwe",
    "AX"=> "Åland Islands",
    ],

];
