<?php

namespace Database\Seeders;

use App\Models\Permission;
use App\Models\Role;
use App\Models\User;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;

class PermissionSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $data_per = [
            [
                'View User',
                'view-user',
                'user',
            ],
            [
                'Create User',
                'create-user',
                'user',
            ],
            [
                'Edit User',
                'edit-user',
                'user',
            ],
            [
                'Destroy User',
                'delete-user',
                'user',
            ],
            [
                'View Role',
                'view-role',
                'role',
            ],
            [
                'Create Role',
                'create-role',
                'role',
            ],
            [
                'Edit Role',
                'edit-role',
                'role',
            ],
            [
                'Destroy Role',
                'delete-role',
                'role',
            ],
            [
                'View Asset',
                'view-asset',
                'asset',
            ],
            [
                'Create Asset',
                'create-asset',
                'asset',
            ],
            [
                'Edit Asset',
                'edit-asset',
                'asset',
            ],
            [
                'Destroy Asset',
                'delete-asset',
                'asset',
            ],
            [
                'View License',
                'view-license',
                'license',
            ],
            [
                'Create License',
                'create-license',
                'license',
            ],
            [
                'Edit License',
                'edit-license',
                'license',
            ],
            [
                'Destroy License',
                'delete-license',
                'license',
            ],
            [
                'View Accessory',
                'view-accessory',
                'accessory',
            ],
            [
                'Create Accessory',
                'create-accessory',
                'accessory',
            ],
            [
                'Edit Accessory',
                'edit-accessory',
                'accessory',
            ],
            [
                'Destroy Accessory',
                'delete-accessory',
                'accessory',
            ],
            [
                'View Component',
                'view-component',
                'component',
            ],
            [
                'Create Component',
                'create-component',
                'component',
            ],
            [
                'Edit Component',
                'edit-component',
                'component',
            ],
            [
                'Destroy Component',
                'delete-component',
                'component',
            ],
            [
                'View Custom Field',
                'view-field',
                'field',
            ],
            [
                'Create Custom Field',
                'create-field',
                'field',
            ],
            [
                'Edit Custom Field',
                'edit-field',
                'field',
            ],
            [
                'Destroy Custom Field',
                'delete-field',
                'field',
            ],
            [
                'View Asset Model',
                'view-model',
                'model',
            ],
            [
                'Create Asset Model',
                'create-model',
                'model',
            ],
            [
                'Edit Asset Model',
                'edit-model',
                'model',
            ],
            [
                'Destroy Asset model',
                'delete-model',
                'model',
            ],
            [
                'View Company',
                'view-company',
                'company',
            ],
            [
                'Create Company',
                'create-company',
                'company',
            ],
            [
                'Edit Company',
                'edit-company',
                'company',
            ],
            [
                'Destroy Company',
                'delete-company',
                'company',
            ],
            [
                'View Category',
                'view-category',
                'category',
            ],
            [
                'Create Category',
                'create-category',
                'category',
            ],
            [
                'Edit Category',
                'edit-category',
                'category',
            ],
            [
                'Destroy Category',
                'delete-category',
                'category',
            ],
            [
                'View Location',
                'view-location',
                'location',
            ],
            [
                'Create Location',
                'create-location',
                'location',
            ],
            [
                'Edit Location',
                'edit-location',
                'location',
            ],
            [
                'Destroy Location',
                'delete-location',
                'location',
            ],
            [
                'View Log',
                'view-log',
                'log',
            ],
            [
                'Destroy Log',
                'delete-log',
                'log',
            ],

            [
                'View Media',
                'view-media',
                'media',
            ],
            [
                'Create Media',
                'create-media',
                'media',
            ],
            [
                'Update Media',
                'update-media',
                'media',
            ],
            [
                'Destroy Media',
                'delete-media',
                'media',
            ],
        ];
        $role = new Role();
        $role->name='Admin';
        $role->slug='admin';
        $role->save();
        foreach ($data_per as $item) {
            $permission = new Permission();
            $permission->name = $item[0];
            $permission->slug = $item[1];
            $permission->group = $item[2];
            $permission->save();
            $role->permissions()->attach($permission['id']);
        }
        $user = new User();
        $user->first_name = 'Calc Admin';
        $user->email = 'calc@gmail.com';
        $user->username = 'calc';
        $user->password = bcrypt('calc5344');
        $user->created_at =date('Y-m-d');
        $user->save();
        $user->roles()->attach($role['id'] ?? $user->is_default());
    }
}
