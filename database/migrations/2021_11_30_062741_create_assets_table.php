<?php


use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateAssetsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('assets', function (Blueprint $table) {
            $table->id();
            $table->string('name')->nullable();
            $table->string('asset_tag')->nullable();
            $table->integer('model_id')->nullable();
            $table->string('serial')->nullable();
            $table->date('purchase_date')->nullable();
            $table->decimal('purchase_cost', 8, 2)->nullable();
            $table->string('order_number')->nullable();
            $table->integer('assigned_to')->nullable();
            $table->text('notes')->nullable();
            $table->integer('user_id')->nullable();

            $table->boolean('physical')->default(1);
            $table->integer('status_id')->nullable();
            $table->tinyInteger('archived')->default(0);
            $table->integer('warranty_months')->nullable();
            $table->tinyInteger('requestable')->default(0);
            $table->integer('rtd_location_id')->nullable();
            $table->string('accepted')->nullable();
            $table->dateTime('last_checkout')->nullable();
            $table->date('expected_checkin')->nullable();
            $table->integer('company_id')->nullable();
            $table->string('assigned_type')->nullable();
            $table->dateTime('last_audit_date')->nullable();
            $table->date('next_audit_date')->nullable();
            $table->integer('location_id')->nullable();
            $table->integer('checkin_counter')->default(0);
            $table->integer('checkout_counter')->default(0);
            $table->integer('requests_counter')->default(0);
             $table->text('_snipeit_imei_1')->nullable();
            $table->text('_snipeit_phone_number_2')->nullable();
            $table->text('_snipeit_ram_3')->nullable();
            $table->text('_snipeit_cpu_4')->nullable();
            $table->text('_snipeit_mac_address_5')->nullable();
            $table->timestamps();
            $table->softDeletes();


        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('assets');
    }
}
