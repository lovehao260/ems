<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateModelsCustomFieldsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('models_custom_fields', function (Blueprint $table) {
            $table->id();
            $table->integer('asset_model_id')->unsigned()->references('id')->on('models')->onDelete('cascade')->index();
            $table->integer('custom_field_id')->unsigned()->references('id')->on('custom_fields')->onDelete('cascade')->index();
            $table->text('default_value');
            $table->nullableTimestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('models_custom_fields');
    }
}
