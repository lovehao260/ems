<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateLocationsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('locations', function (Blueprint $table) {
            $table->increments('id');
            $table->string('name')->nullable();
            $table->string('city')->nullable();
            $table->string('state',2)->nullable();
            $table->string('country',2)->nullable();
            $table->integer('user_id')->nullable();
            $table->integer('manager_id')->nullable();
            $table->integer('parent_id')->default(0);
            $table->string('address',191)->nullable();
            $table->string('address2',191)->nullable();
            $table->string('zip',10)->nullable();
            $table->string('currency',10)->nullable();
            $table->string('ldap_ou',191)->nullable();
            $table->string('image',191)->nullable();
            $table->timestamps();

            $table->engine = 'InnoDB';
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('locations');
    }
}
