<?php
use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;


class CreateModelsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('models', function ($table) {
            $table->id();
            $table->string('name');
            $table->string('model_number')->nullable();
            $table->integer('category_id')->nullable();
            $table->integer('user_id')->nullable();
            $table->integer('fieldset_id')->nullable();
            $table->integer('eol')->nullable();
            $table->string('image')->nullable();
            $table->text('note')->nullable();
            $table->tinyInteger('deprecated_mac_address')->default(0);
            $table->tinyInteger('requestable')->default(0);
            $table->timestamps();
            $table->softDeletes();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('models');
    }

}
