<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateCustomFieldsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('custom_fieldsets', function (Blueprint $table) {
            $table->id();
            $table->string('name');
            $table->bigInteger('user_id');
            $table->timestamps();
        });

        Schema::create('custom_fields', function (Blueprint $table) {
            $table->id();
            $table->string('name');
            $table->string('format')->nullable();
            $table->string('element');
            $table->text('field_values')->nullable();
            $table->string('db_column')->nullable();
            $table->text('help_text')->nullable();
            $table->integer('user_id');
            $table->timestamps();
        });

        Schema::create('custom_field_custom_fieldset', function (Blueprint $table) {
            $table->id();
            $table->integer('custom_field_id')->unsigned()->references('id')->on('custom_fields')->index();
            $table->integer('custom_fieldset_id')->unsigned()->references('id')->on('custom_fieldsets')->index();
            $table->integer('order');
            $table->tinyInteger('required')->default(0);
            $table->nullableTimestamps();
        });

    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('custom_fieldsets');
        Schema::dropIfExists('custom_fields');
        Schema::dropIfExists('custom_field_custom_fieldset');

    }
}
