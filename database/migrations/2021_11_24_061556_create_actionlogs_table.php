<?php

use App\Models\Actionlog;
use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateActionLogsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('action_logs', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('user_id')->nullable();
            $table->string('action_type');
            $table->integer('target_id')->nullable();  // Was checkedout_to
            $table->string('target_type')->nullable(); // For polymorphic thingies
            $table->integer('location_id')->nullable();
            $table->text('note')->nullable();
            $table->text('filename')->nullable();
            $table->string('item_type');
            $table->integer('item_id');  // Replaces asset_id, accessory_id, etc.
            $table->integer('thread_id') ->nullable()
                ->default(null);
            $table->date('expected_checkin')->nullable()->default(null);
            $table->date('action_date')->nullable()->default(null);
            $table->integer('accepted_id')->nullable();
            $table->integer('company_id')->nullable()->default(null);
            $table->string('accept_signature',100)->nullable();
            $table->timestamps();
            $table->softDeletes();



            $table->index('thread_id');
            $table->index(['target_id', 'target_type']);
            $table->index('created_at');
            $table->index(['item_type', 'item_id', 'action_type']);
            $table->index(['target_type', 'target_id', 'action_type']);
        });

        $logs = Actionlog::with('item')->get();
        foreach ($logs as $log) {
            if($log->item) {
                $log->company_id = $log->item->company_id;
                $log->save();
            } else {
                var_dump($log);
            }
        }
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('action_logs');
        Schema::table('action_logs', function (Blueprint $table) {
            $table->dropIndex(['target_id', 'target_type']);
            $table->dropIndex(['created_at']);
            $table->dropIndex(['item_type', 'item_id', 'action_type']);
            $table->dropIndex(['target_type', 'target_id', 'action_type']);
        });
    }
}
