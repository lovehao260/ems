<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateAccessoriesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('accessories', function (Blueprint $table) {
            $table->id();
            $table->string('name')->nullable();
            $table->integer('category_id')->nullable();
            $table->integer('user_id')->nullable();
            $table->integer('location_id')->nullable();
            $table->integer('company_id')->nullable();
            $table->integer('qty')->default(0);
            $table->date('purchase_date')->nullable();
            $table->decimal('purchase_cost')->nullable();
            $table->string('order_number')->nullable();
            $table->string('model_number')->nullable();
            $table->string('image')->nullable();
            $table->boolean('requestable')->default(0);
            $table->timestamps();
            $table->softDeletes();

        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('accessories');
    }
}
