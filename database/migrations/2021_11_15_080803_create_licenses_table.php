<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateLicensesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('licenses', function (Blueprint $table) {
            $table->id();
            $table->string('name',191);
            $table->string('license_name',120)->nullable();
            $table->string('license_email',191)->nullable();
            $table->string('serial',191)->nullable();
            $table->date('purchase_date')->nullable();
            $table->decimal('purchase_cost')->nullable();
            $table->string('purchase_order',191)->nullable();
            $table->string('order_number',191)->nullable();
            $table->integer('seats')->default(1);
            $table->integer('user_id')->nullable();
            $table->integer('company_id')->nullable();
            $table->integer('category_id')->nullable();
            $table->date('termination_date')->nullable();
            $table->tinyInteger('depreciate')->default(1);
            $table->tinyInteger('maintained')->nullable();
            $table->tinyInteger('reassignable')->default(1);
            $table->date('expiration_date')->nullable();
            $table->text('notes')->nullable();
            $table->softDeletes();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('licenses');
    }
}
