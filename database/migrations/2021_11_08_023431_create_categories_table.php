<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateCategoriesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('categories', function (Blueprint $table) {
            $table->id();
            $table->string('name',191);
            $table->integer('user_id');
            $table->longText('eula_text')->nullable();
            $table->tinyInteger('use_default_eula')->default(0);
            $table->tinyInteger('require_acceptance')->default(0);
            $table->tinyInteger('checkin_email')->default(0);
            $table->char('category_type')->default('asset');
            $table->string('image',191)->nullable();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('categories');
    }
}
